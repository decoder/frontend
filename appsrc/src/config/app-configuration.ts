export abstract class AppConfiguration {
    serverURL: string;
    asfmServerURL: string;
    jupyterURL: string;
    toolsServerURL: string;
}
