import { Injectable } from '@angular/core';
import { AppConfiguration } from './app-configuration';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JsonAppConfigService extends AppConfiguration {
  constructor(private http: HttpClient) {
    super();
  }

  load(): Promise<void> {
    return this.http.get<AppConfiguration>('app.config.json')
      .toPromise()
      .then((data: AppConfiguration) => {
        this.serverURL = data.serverURL;
        this.asfmServerURL = data.asfmServerURL;
        this.jupyterURL = data.jupyterURL;
        this.toolsServerURL = data.toolsServerURL;
      })
      .catch(() => {
        console.error('Could not load configuration');
      });
  }
}
