import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './core/containers/app-component/app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AuthGuard } from './core/services/guards/auth.guard';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { LoginComponent } from './core/containers/login/login.component';
import { LoginFormComponent } from './core/components/login-form/login-form.component';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';
import { ProfileSelectorComponent } from './core/components/profile-selector/profile-selector.component';
import { JwtHttpInterceptor } from './interceptors/jwt-http-interceptor.interceptor';
import { JsonAppConfigService } from 'src/config/json-app-config.service';
import { AppConfiguration } from 'src/config/app-configuration';

import 'prismjs';
import 'prismjs/components/prism-typescript.min.js';

export const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: 'administrator' },
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  {
    path: 'login',
    component: LoginComponent,
    // Subrouting of login (the path must be /login/pagePath)
    children: [
      // If you enter into login page without more path, charge login form component
      { path: '', pathMatch: 'full', component: LoginFormComponent },
      {
        path: 'profile-selector',
        component: ProfileSelectorComponent,
      },
    ],
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m: any) => m.HomeModule),
    canActivate: [AuthGuard],
  },
];

export function initializerFn(jsonAppConfigService: JsonAppConfigService): any {
  return () => {
    return jsonAppConfigService.load();
  };
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    MonacoEditorModule.forRoot(), // use forRoot() in main app module only.
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: AppConfiguration,
      deps: [HttpClient],
      useExisting: JsonAppConfigService
    },
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [JsonAppConfigService],
      useFactory: initializerFn
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtHttpInterceptor, multi: true },
  ],
})
export class AppModule {}
