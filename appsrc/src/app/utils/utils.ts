import { PKMArtifact } from '../core/models/artifact';

/**
 * Returns the file extension given a file
 */
export function getFileExtension(file: string): string {
  if (!file) {
    return 'json';
  }
  const fileChunks: string[] = file.split('.');
  const extension: string = '';

  if (fileChunks.length >= 2) {
    return fileChunks.pop();
  }

  return extension;
}

/**
 * Returns the index at which the format string of an artifact is located
 */
export function getFormatFileIndex(artifactName: string): number {
  let index: number;
  const maxIndex: number = artifactName.length;

  for (let i: number = 0; i < maxIndex; i++) {
    if (!artifactName.split('.')[i] ) {
      index = i - 1;
      return index;
    }
  }
}

/**
 *
 *  Returns the corresponding artifacts that can be used by each tool
 */
export function getArtifactsIdByTool(artifacts: PKMArtifact[], tool: string): string[] {
  const formats: string[] = [];
  const types: string[] = [];
  let artifactNameList: string[] = [];

  switch (tool) {
    case 'Javaparser':
    case 'OpenJML': {
      formats.push('java');
      types.push('Code');
      break;
    }
    case 'FramaCLang': {
      formats.push('cpp');
      formats.push('cc');
      formats.push('cp');
      formats.push('cxx');
      formats.push('c++');
      types.push('Code');
      break;
    }
    case 'FramaC': {
      formats.push('c');
      types.push('Code');
      break;
    }
    case 'SemanticParser':
    case 'VariableMisuse': {
      formats.push('c');
      formats.push('cpp');
      formats.push('java');
      types.push('Code');
      break;
    }
    case 'CodeSumarization': {
      formats.push('c');
      formats.push('cpp');
      formats.push('java');
      formats.push('h');
      formats.push('hpp');
      types.push('Code');
      break;
    }
    case 'codeToASFM': {
      formats.push('hpp');
      formats.push('cpp');
      types.push('Code');
      break;
    }
    case 'ClassModelTransformer': {
      formats.push('uml');
      types.push('Diagram');
      break;
    }
    case 'docToASFM': {
      formats.push('docx');
      types.push('Document');
      break;
    }
    case 'Excavator': {
      types.push('Binary Executable');
      break;
    }
    case 'ASFMToDoc': {
      formats.push('docx');
      types.push('ASFM Docs');
      break;
    }
    default: {
      break;
    }
  }

  artifactNameList = artifacts
  .filter((artifact: PKMArtifact) => {
    if (artifact.name !== undefined) {
      if (artifact.type === 'ASFM Docs' && types.includes(artifact.type)) {
        return true;
      }
      if (formats.includes(artifact.name.split('.').pop()) && types.includes(artifact.type)) {
        return true;
      } else if (artifact.type === 'Binary Executable' && types.includes('Binary Executable')) {
        return true;
      } else { return false; }
    } else if (artifact.sourceFile !== undefined ) {
      if (formats.includes(artifact.sourceFile.split('.').pop()) && types.includes(artifact.type)) {
        return true;
      } else { return false; }
    }
  } )
  .map((element: PKMArtifact) => {
    if (element.name !== undefined) {
       return element.name;
    } else if (element.sourceFile !== undefined) {
      return element.sourceFile;
    }
  });

  return artifactNameList;

}

/**
 * Converts a long string of bytes into a readable format e.g KB, MB, GB, TB, YB
 */
export function readableBytes(bytes: number): string {
  const i: number =
    bytes === 0 ? 0 : Math.floor(Math.log(bytes) / Math.log(1024));
  const sizes: string[] = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  return `${Number((bytes / Math.pow(1024, i)).toFixed(2))}${sizes[i]}`;
}

/**
 * Returns relativePath of the artifact given true on the second argument,
 * will return filename of the artifact given false as second argument
 */
export function artifactNameRetriever(artifactName: string, relativePath: boolean): string {
  if (relativePath) {
    return artifactName;
  } else {
    return artifactName.replace(/^.*[\\\/]/, '');
  }
}

/**
 * Returns relativePath name of the artifact encoded in order to been able to
 * make a post request with the correct format
 */
export function urlEncodeArtifactName(artifactName: string): string {
  return encodeURIComponent(artifactName);
}

export function urlDecodeArtifactName(artifactName: string): string {
  return decodeURIComponent(artifactName);
}

/**
 * Returns relativePath name of the artifact, everything before the last slash
 */
export function obtainPathFromArtifact(artifactName: string): any {
  const regex: any = /^(.*[\\\/])/;
  return artifactName.match(regex) || '';
}

interface Pos {
  pos_path?: string;
  pos_lnum: number;
  pos_cnum: number;
}

interface Loc {
  pos_start: Pos;
  pos_end: Pos;
}

/**
 * Returns a piece of text given a location and a content
 */
export function extractCommentsAnnotationsFromArtifact(
  content: string,
  loc: Loc,
  type: string
): object[] {
  const lines: string[] = content.split('\n');
  const result: object[] = [];
  let commentAnnotationStart: number = loc.pos_start.pos_lnum;
  let commentAnnotationEnd: number = loc.pos_end.pos_lnum;

  // Controls the display of the numbers of lines of code before based on the number of line ( from 0 to 3)
  switch (loc.pos_start.pos_lnum) {
    case 1:
      break;
    case 2:
      commentAnnotationStart -= 1;
      break;
    case 3:
      commentAnnotationStart -= 2;
      break;
    default:
      commentAnnotationStart -= 3;
      break;
  }

  // Controls the display of the numbers of lines of code after based on the number of line ( from 0 to 3)
  switch (loc.pos_end.pos_lnum) {
    case lines.length:
      break;
    case lines.length - 1:
      commentAnnotationEnd += 1;
      break;
    case lines.length - 2:
      commentAnnotationEnd += 2;
      break;
    default:
      commentAnnotationEnd += 3;
      break;
  }

  let poslnum: number = commentAnnotationStart;
  let pos_cnum: number = loc.pos_start.pos_cnum;

  while (poslnum <= commentAnnotationEnd) {
    const individualSplittedLine: string = lines[poslnum - 1];
    let splitCommentAnotation: object = {};
    let index: string = '';
    let newStart: string = '';
    let newComments: string = '';
    let newEnd: string = '';

    // Controls to put the line number at the beggining of the string (add 0 before if lower than 10)
    if (poslnum < 10) {
      index = '0' + poslnum + ':';
    } else {
      index = poslnum + ':';
    }

    if (poslnum < loc.pos_start.pos_lnum) {
      // Controls lines before the comments (they should be normal, no highlighted)
      newStart = index + individualSplittedLine;
      newComments = '';
      newEnd = '';
    } else {
      if (
        poslnum === loc.pos_start.pos_lnum &&
        loc.pos_start.pos_lnum === loc.pos_end.pos_lnum
      ) {
        // Controls the comments only one line long
        newStart = '';
        newComments =
          index +
          individualSplittedLine.slice(0, loc.pos_start.pos_cnum) +
          individualSplittedLine.slice(
            loc.pos_end.pos_cnum,
            individualSplittedLine.length
          );
        newEnd = '';
      } else if (
        poslnum >= loc.pos_start.pos_lnum &&
        poslnum <= loc.pos_end.pos_lnum
      ) {
        newStart = '';
        newComments = index + individualSplittedLine;
        newEnd = '';
      } else if (poslnum > loc.pos_end.pos_lnum) {
        newStart = '';
        newComments = '';
        newEnd = index + individualSplittedLine;
      } else if (
        poslnum === loc.pos_start.pos_lnum &&
        poslnum < loc.pos_end.pos_lnum
      ) {
        newStart =
          index + individualSplittedLine.slice(0, loc.pos_start.pos_cnum);
        newComments = individualSplittedLine.slice(
          loc.pos_start.pos_cnum,
          individualSplittedLine.length
        );
        newEnd = '';
      } else if (
        poslnum === loc.pos_end.pos_lnum &&
        poslnum > loc.pos_start.pos_lnum
      ) {
        newStart = '';
        newComments =
          index + individualSplittedLine.slice(0, loc.pos_end.pos_cnum);
        newEnd = individualSplittedLine.slice(
          loc.pos_end.pos_cnum,
          individualSplittedLine.length
        );
      }
    }

    if (type === 'Comment') {
      splitCommentAnotation = {
        starts: newStart,
        midles: newComments,
        ends: newEnd,
      };
    } else if (type === 'Annotation') {
      splitCommentAnotation = {
        start : newStart,
        annotations: newComments,
        end: newEnd
      };
    }

    result.push(splitCommentAnotation);
    ++poslnum;
    pos_cnum = 1;
  }

  return result;
}

/**
 * Returns generated random string in a secure way
 */
export function generateRandomString(): string {
  const crypto: Crypto = window.crypto;
  const array: Uint32Array = new Uint32Array(1);
  return crypto.getRandomValues(array).toString();
}
