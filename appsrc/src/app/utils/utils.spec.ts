import { PKMArtifact } from '../core/models/artifact';
import * as utils from './utils';

describe('utils', () => {

    const artifactName: string = 'java/mtsj/core/src/main/java/com/UsermanagementImpl.java';
    const artifacts: string[] = ['java/com/UsermanagementImpl.java', 'ej.of.file/Example.cpp'];
    const invalidArtifacts: PKMArtifact[] = [
        {
            id: 'img/User/img.jpg',
            content: '',
            encoding: 'utf-8',
            format: '',
            git_commit_id: '',
            mime_type: '',
            git_repository_url: '',
            name: 'img/User/img.jpg',
            rel_path: 'img/User/img.jpg',
            sourceFile: 'img/User/img.jpg',
            type: 'Code',
        },
        {
            id: 'vd/User/video.mpg',
            content: '',
            encoding: 'utf-8',
            format: '',
            git_commit_id: '',
            mime_type: '',
            git_repository_url: '',
            name: 'vd/User/video.mpg',
            rel_path: 'vd/User/video.mpg',
            sourceFile: 'vd/User/video.mpg',
            type: 'Code',
        },
        {
            id: 'ad/User/audio.mp3',
            content: '',
            encoding: 'utf-8',
            format: '',
            git_commit_id: '',
            mime_type: '',
            git_repository_url: '',
            name: 'ad/User/audio.mp3',
            rel_path: 'ad/User/audio.mp3',
            sourceFile: 'ad/User/audio.mp3',
            type: 'Code',
        }];

    it('should execute correctly getFileExtension and retrieve file extension', () => {
        const file: string = 'ExampleJavaMain.java';
        expect(utils.getFileExtension(file)).toEqual('java');
    });

    it('should execute correctly getFileExtension and retrieve file extension', () => {
        const file: string = 'Example.CMain.c';
        expect(utils.getFileExtension(file)).toEqual('c');
    });

    it('should convert a long string of bytes into a readable format', () => {
        const ramdomLongStringOfBytes: number = 459524573587398579358;
        expect(utils.readableBytes(ramdomLongStringOfBytes)).toEqual('398.57EB');
    });

    it('should execute asrtifactNameRetriever and obtain name with relativePath', () => {
        expect(utils.artifactNameRetriever(artifactName, true)).toEqual(artifactName);
    });

    it('should execute asrtifactNameRetriever and obtain name without relativePath', () => {
        expect(utils.artifactNameRetriever(artifactName, false)).toEqual('UsermanagementImpl.java');
    });

    it('should return encoded artifact name', () => {
        const encodedArtifactName: string = utils.urlEncodeArtifactName(artifactName);
        expect(utils.urlEncodeArtifactName(artifactName)).toEqual(encodedArtifactName);
    });

    it('should return decoded artifact name', () => {
        const encodedArtifactName: string = utils.urlEncodeArtifactName(artifactName);
        expect(utils.urlDecodeArtifactName(encodedArtifactName)).toEqual(artifactName);
    });

    it('should return path from an artifact name', () => {
        expect(utils.obtainPathFromArtifact(artifactName)[0]).toEqual('java/mtsj/core/src/main/java/com/');
    });

    it('should return extension file index correctly', () => {
        const index: number = utils.getFormatFileIndex(artifacts[1]);
        expect(artifacts[1].split('.')[index]).toBe('cpp');
    });

    it('should return empty content when artifacts are invalid', () => {
        const emptyArray: string[] = [];
        expect(utils.getArtifactsIdByTool(invalidArtifacts, 'Javaparser')).toEqual(emptyArray);

    });
});
