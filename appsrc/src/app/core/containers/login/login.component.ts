import { Component } from '@angular/core';
import { LoginService } from '../../../core/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  pass: string;

  constructor(
    private readonly loginService: LoginService) {
      this.loginService.partiallyLogged = false;
      this.loginService.logged = false;
    }

}
