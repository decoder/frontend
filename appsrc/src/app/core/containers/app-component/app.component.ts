import { Component } from '@angular/core';
import { LoginService } from '../../../core/services/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private readonly login: LoginService) { }

  isLogged(): boolean {
    return this.login.logged;
  }
}
