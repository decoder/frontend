import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CoreModule } from '../../core.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginService } from '../../services/login/login.service';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let mockLoginService: any;
  beforeEach(async(() => {
    mockLoginService = { logged: false };
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, CoreModule, HttpClientTestingModule],
      providers: [
        { provide: LoginService, useValue: mockLoginService }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    const app: any = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should return false if the user is not logged', () => {
    expect(fixture.componentInstance.isLogged()).toEqual(mockLoginService.logged);
  });
});
