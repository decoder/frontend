import { asyncScheduler, of, scheduled } from 'rxjs';
import { LoginService } from '../../../core/services/login/login.service';

export function getMockLoginService(
  validUser: string,
  validPassword: string
): Partial<LoginService> {
  return {
    login: jasmine
      .createSpy('login')
      .and.callFake((username: string, password: string) => {
        const isUserValid: boolean = username === validUser;
        const isPasswordValid: boolean = password === validPassword;
        const areCredentialsValid: boolean = isUserValid && isPasswordValid;

        return scheduled(of(areCredentialsValid), asyncScheduler);
      }),
    logout: jasmine.createSpy('logout'),
    goToHome: jasmine.createSpy('goToHome'),
  };
}
