import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { RolesRetrieverService } from '../../services/roles-retriever/roles-retriever.service';
import { Role } from 'src/app/core/models/role';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-role-selector',
  templateUrl: './role-selector.component.html',
  styleUrls: ['./role-selector.component.scss'],
})
export class RoleSelectorComponent implements OnInit {

  roles$: Observable<Role[]>;
  @Output() roleSelect: EventEmitter<Role>;

  constructor(
    private rolesService: RolesRetrieverService
  ) {
      this.roleSelect = new EventEmitter<Role>();
      this.roles$ = of([]);
    }

  ngOnInit(): void {
    this.setRoles();
  }

  onRoleSelect(role: Role): void {
    this.roleSelect.emit(role);
  }

  private setRoles(): void {
    this.roles$ = this.rolesService.getRoles(localStorage.getItem('currentUser'));
  }

}
