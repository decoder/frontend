import {
  async,
  ComponentFixture,
  TestBed,
  flush,
  fakeAsync,
} from '@angular/core/testing';

import { RoleSelectorComponent } from './role-selector.component';
import { RolesRetrieverService } from '../../services/roles-retriever/roles-retriever.service';
import { of } from 'rxjs/internal/observable/of';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from 'src/app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Role } from '../../models/role';
import { connectionProfilesMock } from 'src/app/mocks/core/fixtures/role.mock';
import { AppConfiguration } from 'src/config/app-configuration';

describe('RoleSelectorComponent', () => {
  let component: RoleSelectorComponent;
  let fixture: ComponentFixture<RoleSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoleSelectorComponent],
      providers: [RolesRetrieverService, AppConfiguration],
      imports: [NoopAnimationsModule, HttpClientTestingModule, MaterialModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the selected role [onRoleSelected]', fakeAsync(() => {
    let selected: boolean = false;
    component.roleSelect.subscribe(() => (selected = true));
    component.onRoleSelect({db: 'test', role: 'Developer'});
    flush();
    expect(selected).toBeTruthy();
  }));

  it('should get connection profiles', fakeAsync(() => {
    const retriever: RolesRetrieverService = TestBed.inject(
      RolesRetrieverService
    );
    const retrievedRoles: Role[] = connectionProfilesMock;
    spyOn(retriever, 'getRoles').and.returnValue(
      of(retrievedRoles)
    );

    component['setRoles']();
    fixture.detectChanges();

    component.roles$.subscribe((roles: Role[]) => {
      expect(roles).toEqual(retrievedRoles);
    });
    flush();
  }));
});
