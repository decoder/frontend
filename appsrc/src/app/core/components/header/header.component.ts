import { Component } from '@angular/core';
import { LoginService } from '../../../core/services/login/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public loginService: LoginService;
  userProfile: string;
  userName: string = '';

  constructor(loginService: LoginService) {
    this.loginService = loginService;
    this.userProfile = localStorage.getItem('currentRole');
    this.userName = localStorage.getItem('currentUser') || '';
  }

  logout(): void {
    this.loginService.logout();
  }
}
