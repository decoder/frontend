import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Role } from '../../models/role';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-profile-selector',
  templateUrl: './profile-selector.component.html',
  styleUrls: ['./profile-selector.component.scss']
})
export class ProfileSelectorComponent implements OnInit {

  profileWasModified: boolean;
  roleIsUndefined: boolean;
  role: Role;
  @Output() roleSelected: EventEmitter<Role>;

  constructor(
    private loginService: LoginService,
  ) {}

  ngOnInit(): void {
    this.roleIsUndefined = true;
    this.profileWasModified = false;
  }

  onRoleSelect(role: Role): void {
    this.profileWasModified = true;
    this.role = role;
    if (role === undefined) {
      this.roleIsUndefined = true;
    } else {
      this.roleIsUndefined = false;
    }
  }

  roleIsNotValid(): boolean {
    if (this.profileWasModified && this.roleIsUndefined) {
      return true;
    } else {
      return false;
    }
  }

  onConnect(): void {
    this.loginService.goToHome(this.role);
  }
}
