import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileSelectorComponent } from './profile-selector.component';
import { CoreModule } from '../../core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginService } from '../../services/login/login.service';
import { getMockLoginService } from '../../testing/services/login.mock';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Role } from '../../models/role';
import { By } from '@angular/platform-browser';
import { AppConfiguration } from 'src/config/app-configuration';

const DEFAULT_USER: string = 'user';
const DEFAULT_PASSWORD: string = 'user';

describe('ProfileSelectorComponent', () => {
  let component: ProfileSelectorComponent;
  let fixture: ComponentFixture<ProfileSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileSelectorComponent],
      imports: [
        CoreModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: LoginService,
          useValue: getMockLoginService(DEFAULT_USER, DEFAULT_PASSWORD),
        },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with undefined role and not modified status', () => {
    component.ngOnInit();
    expect(component.roleIsUndefined).toBe(true);
    expect(component.profileWasModified).toBe(false);
  });

  it('should set a role', () => {
    const testUserRole: Role = { db: 'test', role: 'Developer'};
    component.onRoleSelect(testUserRole);
    expect(component.role.role).toEqual('Developer');
  });

  it('should display "error-content" if role is not selected', () => {
    component.profileWasModified = true;
    component.roleIsUndefined = true;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.error-content'))).toBeTruthy();
  });

  it('should not display button if role is not selected', () => {
    component.roleIsUndefined = true;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.button'))).toBeFalsy();
  });

  it('should [goToHome] [onConnect] when button is clicked', fakeAsync(() => {
    const onClickMock: jasmine.Spy<() => void> = spyOn(component, 'onConnect');
    fixture.debugElement
      .query(By.css('.connect-button'))
      .triggerEventHandler('click', null);
    expect(onClickMock).toHaveBeenCalled();
  }));
});
