import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { PKMLogArtifact } from '../../models/artifact';
import { Invocation, InvocationArtifact, InvocationArtifactResult } from '../../models/invocation';
import { ProcessEngineService } from '../../services/process-engine/process-engine.service';

const EMPTY_INVOCATIONS: InvocationArtifact = {
    dbName: 'mythaistar',
    sourceFileName: 'java/mtsj'
  };

const EMPTY_INVOCATIONS_RESULT: InvocationArtifactResult[] = [
  {
    path: 'path/exmpl',
    type: 'Code',
  },
];

interface IResultItem {
  path: string;
  type: string;
}

interface InvocationInfo {
  data: Invocation;
  projectName: string;
}

@Component({
  selector: 'app-invocation-info',
  templateUrl: './invocation-info.component.html',
  styleUrls: ['./invocation-info.component.scss'],
})
export class InvocationInfoComponent implements OnInit {
  status: string;
  displayedColumnsArtifacts: string[] = ['Collection', 'FileName'];
  displayedColumnsResults: string[] = ['Type', 'Path'];
  dataSourceArtifacts: MatTableDataSource<InvocationArtifact>;
  dataSourceResults: MatTableDataSource<InvocationArtifactResult>;
  loading: boolean;
  invocationKeys: string[];
  invocationValues: any[] = [];
  jsonContent: any;

  constructor(
    public dialogRef: MatDialogRef<InvocationInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public invocationInfo: InvocationInfo,
    private artifactRetriever: ArtifactsRetrieverService,
    private peService: ProcessEngineService,
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.dataSourceArtifacts = new MatTableDataSource<InvocationArtifact>([
      EMPTY_INVOCATIONS,
    ]);
    this.dataSourceResults = new MatTableDataSource<InvocationArtifactResult>(
      EMPTY_INVOCATIONS_RESULT
    );
    this.invocationKeys = Object.keys(this.invocationInfo.data.invocationConfiguration);
    this.invocationKeys = this.hideCollectionValueFromList(this.invocationKeys);

    this.invocationKeys.forEach((value: string) => {
      this.invocationValues.push(this.invocationInfo.data.invocationConfiguration[value]);
    });

    if (this.invocationInfo.data) {
      this.dataSourceArtifacts = new MatTableDataSource<InvocationArtifact>([
        this.invocationInfo.data.invocationConfiguration,
      ]);

      this.dataSourceResults = new MatTableDataSource<InvocationArtifactResult>(
        this.invocationInfo.data.invocationResults.filter((item: InvocationArtifactResult) => item.type === 'log')
      );

      this.obtainLogIdFromResults();
    }
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  close(): void {
    this.dialogRef.close();
  }

  calculateStyle(): any {
    switch (this.invocationInfo.data.invocationStatus) {
      case 'COMPLETED':
        return { 'tag-status completed': true };

      case 'RUNNING':
        return { 'tag-status running': true };

      case 'PENDING':
        return { 'tag-status pending': true };
      default: {
        return { 'tag-status pending': true };
      }
    }
  }

  obtainLogIdFromResults(): void {
    const logResultFullPath: string = this.invocationInfo.data.invocationResults
      .filter((element: IResultItem) => element.type === 'log')
      .map((item: IResultItem) => item.path)[0];

    if (logResultFullPath) {
      this.artifactRetriever
        .getArtfifactFromAProjectByID(logResultFullPath)
        .subscribe((result: PKMLogArtifact | boolean) => {
          if (typeof result === 'boolean') { return; }
          this.jsonContent = result;
        });
    }
  }

  refreshInvocationState(): void {
    this.loading = true;
    this.peService
    .getInvocation(
      this.invocationInfo.projectName,
      this.invocationInfo.data.invocationID
    ).subscribe((invocation: Invocation) => {
      this.loading = false;
      this.invocationInfo.data = invocation;
    });
  }

  hideCollectionValueFromList(keys: string[]): string[] {
    let result: string[] = [];
    const hideResult: string[] = [
      'dbName',
      'project_id',
      'collectionId',
      'file',
      'source_file_paths',
      'sourceFileName',
      'path',
      'artefactName',
      'PKMdatabase',
      'DataStoreDirectory',
    ];

    result = keys.filter((res: string) => {
      if (res !== hideResult[0]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[1]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[2]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[3]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[4]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[5]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[6]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[7]) {
        return true;
      } else {
        return false;
      }
    });
    result = result.filter((res: string) => {
      if (res !== hideResult[8]) {
        return true;
      } else {
        return false;
      }
    });

    return result;
  }

  isFramaCObject(val: any): boolean {
    if (val === null) {
      return false;
    }
    return typeof val === 'object';
  }

  decodeValue(value: string = ''): string {
    return decodeURIComponent(value);
  }
}
