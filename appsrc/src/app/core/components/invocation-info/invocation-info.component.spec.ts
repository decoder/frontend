import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { invocationMock } from 'src/app/mocks/developer/invocation.mock';
import { TimestampPipe } from 'src/app/shared/pipes/timestamp/timestamp.pipe';
import { AppConfiguration } from 'src/config/app-configuration';
import { Invocation } from '../../models/invocation';
import { ProcessEngineService } from '../../services/process-engine/process-engine.service';

import { InvocationInfoComponent } from './invocation-info.component';

class PeServiceStub {
  getInvocation(
    dbName: string = '',
    invocationId: string = ''
  ): Observable<Invocation> {
    return of({
      invocationID: 'testId',
      tool: 'javaparser',
      invocationStatus: 'COMPLETED',
      message: '',
      timestampStart: '0220119_172404',
      timestampCompleted: '0220119_172404',
      timestampRequest: '0220119_172404',
      invocationConfiguration: {
        dbName: '',
        file: '',
        generate: '',
      }
    });
  }
}

describe('InvocationInfoComponent', () => {
  let component: InvocationInfoComponent;
  let fixture: ComponentFixture<InvocationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvocationInfoComponent, TimestampPipe],
      imports: [MatDialogModule, HttpClientTestingModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        {
          provide: ProcessEngineService,
          useClass: PeServiceStub
        },
        AppConfiguration,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvocationInfoComponent);
    component = fixture.componentInstance;
    component.invocationInfo.data = invocationMock[0];
    component.invocationInfo.projectName = 'testProject';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog on click close button', () => {
    component.loading = false;
    fixture.detectChanges();
    spyOn(component, 'close').and.callFake(() => { return; });
    const buttonElement: DebugElement = fixture.debugElement.query(By.css('.close-button'));
    buttonElement.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.close).toHaveBeenCalled();
  });

  it('should calculate styles based on invocationStatus set to COMPLETED', () => {
    component.invocationInfo.data.invocationStatus = 'COMPLETED';
    expect(component.calculateStyle()).toEqual({ 'tag-status completed': true });
  });

  it('should calculate styles based on invocationStatus set to RUNNING', () => {
    component.invocationInfo.data.invocationStatus = 'RUNNING';
    expect(component.calculateStyle()).toEqual({ 'tag-status running': true });
  });

  it('should calculate styles based on invocationStatus set to PENDING', () => {
    component.invocationInfo.data.invocationStatus = 'PENDING';
    expect(component.calculateStyle()).toEqual({ 'tag-status pending': true });
  });

  it('should calculate styles as default value', () => {
    component.invocationInfo.data.invocationStatus = '';
    expect(component.calculateStyle()).toEqual({ 'tag-status pending': true });
  });

  it('should refresh state of invocation details', () => {
    component.refreshInvocationState();
    fixture.detectChanges();
    expect(component.loading).toEqual(false);
    expect(component.invocationInfo.data).toEqual({
      invocationID: 'testId',
      tool: 'javaparser',
      invocationStatus: 'COMPLETED',
      message: '',
      timestampStart: '0220119_172404',
      timestampCompleted: '0220119_172404',
      timestampRequest: '0220119_172404',
      invocationConfiguration: {
        dbName: '',
        file: '',
        generate: '',
      }
    });
  });

  it('should decodeValue before rendering', () => {
    const encodePath: string = '%5B0%5D.annotations%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%603280%60%5D.annotations%5B%5D';
    const decodePath: string = '[0].annotations[?loc.pos_start.pos_cnum == `3280`].annotations[]';
    expect(component.decodeValue(encodePath)).toEqual(decodePath);
  });

  it('should check isis not a framaC object', () => {
    expect(component.isFramaCObject(null)).toEqual(false);
    expect(component.isFramaCObject({})).toEqual(true);
  });
});
