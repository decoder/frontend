import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
  tick,
} from '@angular/core/testing';

import { LoginFormComponent } from './login-form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { getMockLoginService } from '../../testing/services/login.mock';
import { LoginService } from '../../../core/services/login/login.service';
import { ReactiveFormsModule } from '@angular/forms';

const DEFAULT_USER: string = 'user';
const DEFAULT_PASSWORD: string = 'user';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: LoginService,
          useValue: getMockLoginService(DEFAULT_USER, DEFAULT_PASSWORD),
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true when the username is checked after setting it', () => {
    component.ngOnInit();
    fixture.detectChanges();
    component.loginForm.controls.username.setValue(DEFAULT_USER);
    fixture.detectChanges();

    expect(component.checkUsername()).toBe(true);
  });

  it('should be true when the password is checked after setting it', () => {
    component.ngOnInit();
    fixture.detectChanges();
    component.loginForm.controls.password.setValue(DEFAULT_PASSWORD);
    fixture.detectChanges();

    expect(component.checkPassword()).toBe(true);
  });

  it('should not be modified fields at start', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.checkPristine()).toBe(false);
  });

  it('should be invalid at start', () => {
    expect(component.checkInvalidation()).toBe(true);
  });

  it('should login with user and password correctly', fakeAsync(
    inject([LoginService], (loginService: LoginService) => {
      component.ngOnInit();
      fixture.detectChanges();
      component.loginForm.controls.username.setValue(DEFAULT_USER);
      component.loginForm.controls.password.setValue(DEFAULT_PASSWORD);
      fixture.detectChanges();
      component.onLogin();
      tick();

      expect(loginService.login).toHaveBeenCalledWith(
        DEFAULT_USER,
        DEFAULT_PASSWORD
      );
    })
  ));
});
