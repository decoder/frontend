import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from '@angular/forms';
import { User } from '../../models/user';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  user: User;
  loginError: boolean = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly loginService: LoginService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  checkUsername(): boolean {
    return this.checkControl(this.loginForm.controls.username);
  }

  checkPassword(): boolean {
    return this.checkControl(this.loginForm.controls.password);
  }

  checkControl(control: AbstractControl): boolean {
    return !this.checkPristine() || control.valid;
  }

  checkPristine(): boolean {
    return !(
      this.loginForm.controls.username.pristine &&
      this.loginForm.controls.password.pristine
    );
  }

  checkInvalidation(): boolean {
    return this.loginForm.invalid;
  }

  subscribeUser(): void {
    alert(this.user.firstName);
  }

  checkLoginButtonState(): boolean {
    return this.loginForm.invalid && this.checkPristine();
  }

  onLogin(): void {
    this.submitted = true;
    // If the login is successful goes to home page, else, display an informational message
    const username: string = this.loginForm.get('username').value;
    const password: string = this.loginForm.get('password').value;
    this.loginService
      .login(username, password)
      .subscribe(
        (success: boolean) => {
        if (success) {
          // KIRBY - Put a dialog that gives welcome login
        } else {
          this.loginError = true;
          this.submitted = false;
          // KIRBY - Put a dialog that explain why cant login
        }
      });
  }
}
