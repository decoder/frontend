import { Component, OnInit, HostBinding } from '@angular/core';
import { interfacePlugins } from 'src/app/plugins/plugins.config';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  plugins: any;
  paths: string[];
  loadPluginsEvent: Subject<void> = new Subject<void>();
  @HostBinding('class.isVisible') visible: boolean;

  constructor(private router: Router) {
    this.plugins = interfacePlugins;
    this.paths = this.pluginPaths();
    this.visible = this.shouldHaveSidebar(this.router.url);
  }

  ngOnInit(): void {
    this.setVisibleForPlugins();
  }

  private setVisibleForPlugins(): void {
    this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        map((navStart: NavigationEnd) => navStart.urlAfterRedirects)
      )
      .subscribe((currentPath: string) => {
        this.visible = this.shouldHaveSidebar(currentPath);
        this.initSidebarOnDashboard(currentPath);
      });
  }

  private initSidebarOnDashboard(currentPath: string): void {
    if (currentPath.startsWith('/home/dashboard/')) {
      this.loadPluginsEvent.next();
    }
  }

  private pluginPaths(): string[] {
    return this.plugins.map((plugin: { path: string }) => `${plugin.path}`);
  }

  private shouldHaveSidebar(currentPath: string): boolean {
    return this.paths.some((path: string) => {
      const withoutParams: string = path.replace(/\/?:[a-zA-Z0-9\-]*/g, '');
      const isEndPath: boolean = currentPath.endsWith(`/${withoutParams}`);
      const isSubpath: boolean = currentPath.includes(`/${withoutParams}/`);
      const isHelpPath: boolean = currentPath.includes('/help/');
      return isEndPath || isSubpath || isHelpPath;
    });
  }
}
