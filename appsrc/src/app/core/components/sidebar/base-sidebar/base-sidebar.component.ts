import { Component, OnInit, Input } from '@angular/core';
import {
  Router,
  ActivationEnd,
  convertToParamMap,
  ParamMap
} from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

class ParamsCache {
  private cache: {};

  constructor() {
    this.cache = {};
  }

  set(key: string, value: string): void {
    this.cache[key] = value;
  }

  get(key: string): string {
    return this.cache[key];
  }
}

@Component({
  selector: 'app-base-sidebar',
  templateUrl: './base-sidebar.component.html',
  styleUrls: ['./base-sidebar.component.scss']
})
export class BaseSidebarComponent implements OnInit {
  private paramsCache: ParamsCache;
  @Input() event: Observable<void>;
  @Input() plugins: { path: string };
  selectedPlugin: string = `projects/:projectId/:versionId`;

  constructor(private router: Router) {
    this.paramsCache = new ParamsCache();
  }

  ngOnInit(): void {
    this.cacheRouterUrlParams();
  }

  onSelection(selectedPath: string): void {
    this.selectedPlugin = selectedPath;
  }

  isSelected(selectedPath: string): boolean {
    this.event.subscribe();
    return this.selectedPlugin === selectedPath;
  }

  go(path: string): void {
    this.router.navigate([`/home/${this.resolveParams(path)}`]);
  }

  baseDefaultActive(): void {
    this.paramsCache = new ParamsCache();
    this.selectedPlugin = `projects/:projectId/:versionId`;
  }

  private cacheRouterUrlParams(): void {
    this.router.events
      .pipe(
        filter((e: any) => e instanceof ActivationEnd),
        filter((e: ActivationEnd) => Object.keys(e.snapshot.params).length > 0),
        map((e: ActivationEnd) => convertToParamMap(e.snapshot.params)),
        )
        .subscribe((params: ParamMap) => {
          this.cacheParams(params);
      });
  }

  private cacheParams(params: ParamMap): void {
    for (const key of params.keys) {
      this.paramsCache.set(key, params.get(key));
    }
  }

  private resolveParams(path: string): string {
    return path.replace(/:([a-zA-Z0-9\-]*)/g, (_: string, match1: string) =>
      this.paramsCache.get(match1)
    );
  }
}
