import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseSidebarComponent } from './base-sidebar.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('BaseSidebarComponent', () => {
  let component: BaseSidebarComponent;
  let fixture: ComponentFixture<BaseSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseSidebarComponent ],
      imports: [RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save the selected item', () => {
    const selectedItem: string = '1';
    component.onSelection(selectedItem);
    expect(component.selectedPlugin).toEqual(selectedItem);
  });

  it('should return true if the item is selected, false otherwise', () => {
    const selectedItem: string = '1';
    const nonSelectedItem: string = '2';
    component.selectedPlugin = selectedItem;
    expect(component.selectedPlugin).toEqual(selectedItem);
    expect(component.selectedPlugin).not.toEqual(nonSelectedItem);
  });
});
