import { async, ComponentFixture, TestBed, fakeAsync, flush, tick } from '@angular/core/testing';

import { SidebarComponent } from './sidebar.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  const eventsSub: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarComponent],
      imports: [RouterTestingModule],
      providers: [
        {
          provide: Router,
          useValue: {
            url: 'home',
            events: eventsSub
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not be visible on home', fakeAsync(() => {
    const homeNav: NavigationEnd = new NavigationEnd(1, 'home', 'home');
    eventsSub.next(homeNav);
    fixture.detectChanges();
    tick();
    expect(component.visible).toBeFalsy();
  }));

  it('should be visible on home/projects', fakeAsync(() => {
    const projectsNav: NavigationEnd = new NavigationEnd(1, 'home', 'home/projects');
    eventsSub.next(projectsNav);
    fixture.detectChanges();
    flush();
    expect(component.visible).toBeTruthy();
  }));
});
