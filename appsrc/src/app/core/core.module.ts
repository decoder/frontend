import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthGuard } from './services/guards/auth.guard';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './containers/app-component/app.component';
import { BaseSidebarComponent } from './components/sidebar/base-sidebar/base-sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './containers/login/login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MaterialModule } from '../material.module';
import { ProfileSelectorComponent } from './components/profile-selector/profile-selector.component';
import { RoleSelectorComponent } from './components/role-selector/role-selector.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SharedModule } from '../shared/shared.module';
import { InvocationInfoComponent } from './components/invocation-info/invocation-info.component';
import { RenderersModule } from '../plugins/renderers/renderers.module';

@NgModule({
  declarations: [
    AppComponent,
    BaseSidebarComponent,
    HeaderComponent,
    LoginComponent,
    LoginFormComponent,
    ProfileSelectorComponent,
    RoleSelectorComponent,
    SidebarComponent,
    InvocationInfoComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule,
    SharedModule,
    RenderersModule
  ],
  exports: [BaseSidebarComponent, SidebarComponent],
  providers: [AuthGuard],
})
export class CoreModule {}
