import { TestBed } from '@angular/core/testing';

import { SnackbarService } from './snackbar.service';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../../shared/shared.module';
import { SnackbarComponent } from 'src/app/shared/components/snackbar/snackbar.component';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('SnackbarService', () => {
  let service: SnackbarService;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, BrowserAnimationsModule, SharedModule],
      providers: [{ provide: MatSnackBar, useClass: MatSnackBarStub }],
    });

    service = TestBed.inject(SnackbarService);
    snackBar = TestBed.inject(MatSnackBar);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create error SnackBar', () => {
    spyOn(snackBar, 'openFromComponent');
    service.openSnackBar('Error', 'error');

    expect(snackBar.openFromComponent).toHaveBeenCalledWith(SnackbarComponent, {
      data: { message: 'Error', snackClass: 'error' },
      panelClass: ['error', 'multi-line-snackbar'],
      verticalPosition: 'top',
      duration: 5000
    });
  });

  it('should create success SnackBar', () => {
    service.openSnackBar('Success', 'check_circle');
    expect(service).toBeTruthy();
  });

  it('should open a [success] snackbar', () => {
    const message: string = 'success messsage';
    spyOn(service, 'openSnackBar');
    service.success(message);
    expect(service.openSnackBar).toHaveBeenCalledWith(message, 'check_circle');
  });

  it('should open an [error] snackbar', () => {
    const message: string = 'error message';
    spyOn(service, 'openSnackBar');
    service.error(message);
    expect(service.openSnackBar).toHaveBeenCalledWith(message, 'error');
  });
});
