import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  SnackbarComponent,
  messageType,
} from '../../../shared/components/snackbar/snackbar.component';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor(public snackBar: MatSnackBar) {}

  openSnackBar(message: string, snackClass: messageType): void {
    this.snackBar.openFromComponent(SnackbarComponent, {
      data: { message, snackClass },
      panelClass: [snackClass, 'multi-line-snackbar'],
      verticalPosition: 'top',
      duration: 5000
    });
  }

  success(message: string): void {
    this.openSnackBar(message, 'check_circle');
  }

  error(message: string): void {
    this.openSnackBar(message, 'error');
  }
}
