import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Role } from 'src/app/core/models/role';
import { User } from '../../models/user';
import { AppConfiguration } from 'src/config/app-configuration';

@Injectable({
  providedIn: 'root'
})
export class RolesRetrieverService {

  projectBaseURL: string = `${this.appConfig.serverURL}/user`;
  emptyProjects: Role[] = [{
    db: '',
    role: 'Developer',
  }, {
    db: '',
    role: 'Maintainer',
  }, {
    db: '',
    role: 'Reviewer',
  }];

  constructor(private http: HttpClient, private appConfig: AppConfiguration) {}

  getRoles(currentUser: string): Observable<Role[]> {
  return this.getRolesFromUrl(`${this.projectBaseURL}/${currentUser}`).pipe(
    map((response: Role[]) => {
      response.sort((a: Role, b: Role) => {
        return a.role < b.role ? -1 : 1;
      });
      if (response.length === 0) {
        return this.emptyProjects;
      } else {
        return response;
      }
    }));
  }

  getRolesFromUrl(url: string): Observable<Role[]> {
    // This code has been modified in order to NOT retrieve 'Owner' role
    // since is not a role used in the FE, please take it in account for new features in the future
    return this.http.get(url).pipe(map((response: User ) => {
      if (response.roles === undefined)  { return []; }
      return response.roles.filter((v: Role, i: any, a: Role[]) => {
        return a.findIndex( (t: Role) => (t.role === v.role && t.role !== 'Owner' && t.role !== 'root')) === i;
      });
    }));
  }

}
