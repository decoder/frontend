import { TestBed } from '@angular/core/testing';
import { RolesRetrieverService } from './roles-retriever.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { Role } from '../../models/role';
import { rolesMock, rolesMockSorted } from 'src/app/mocks/core/fixtures/role.mock';
import { of } from 'rxjs';
import { AppConfiguration } from 'src/config/app-configuration';

describe('RolesRetrieverService', () => {
  let service: RolesRetrieverService;
  let mockRoles: Role[];

  const testUser: string = 'test-user';
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule], providers: [RolesRetrieverService, AppConfiguration]});
    service = TestBed.inject(RolesRetrieverService);
    httpMock = TestBed.inject(HttpTestingController);
    mockRoles = rolesMock;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort Roles alphabetically', () => {
    spyOn(service, 'getRolesFromUrl').and.returnValue(of(mockRoles));
    service
      .getRoles(testUser)
      .subscribe((roles: Role[]) => {
        const trimmedRoles: string[] = roles.map((r: Role) => r.role.trim());
        expect(trimmedRoles).toEqual(rolesMockSorted.map((r: Role) => r.role)); });
  });

  it('should retrieve an empty list of roles', () => {
    const emptyRoles: Role[] = [];
    spyOn(service, 'getRolesFromUrl').and.returnValue(of(emptyRoles));
    service.getRoles(testUser)
      .subscribe((roles: Role[]) => {
        expect(roles).toEqual(service.emptyProjects);
      });
  });

  it('should retrieve roles from url', () => {
    const emptyRoles: Role[] = [];
    const url: string = 'test-url';
    service.getRolesFromUrl(url)
      .subscribe((roles: Role[]) => {
        expect(roles).toEqual(emptyRoles);
      });
    const req: TestRequest = httpMock.expectOne(url);
    expect(req.request.method).toBe('GET');
    req.flush(emptyRoles);
    httpMock.verify();
  });
});
