import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AppConfiguration } from 'src/config/app-configuration';
import { Invocation } from '../../models/invocation';
import { SnackbarService } from '../snackbar/snackbar.service';

export interface ToolInvocationBody {
  tool: string;
  user: string;
  invocationConfiguration: {
    dbName?: string;
    sourceFileName?: string | string[]; // Desi: source? because jmlGen hasn't sourceFileName
    collectionId?: string;
    file?: string;
    source_language?: string;
    target_language?: string;
    project_id?: string;
  };
}

export interface ToolInvocationResponse {
  invocationID: string;
  success: boolean;
}

export interface StepUpdatedInformation {
  taskID: string;
  completed: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class ProcessEngineService {
  invocationsPath: string = 'decoder/pe/invocations';
  invocationsProcess: string = 'decoder/pe/process';
  baseURL: string = this.appConfig.toolsServerURL;

  constructor(
    public snackBarService: SnackbarService,
    private http: HttpClient,
    private appConfig: AppConfiguration
  ) { }

  getInvocations(dbName: string): Observable<Invocation[]> {
    return this.http.get<Invocation[]>(`${this.baseURL}/${this.invocationsPath}/${dbName}?skip=0&limit=25`)
      .pipe(
        catchError((err: any) => {
          this.snackBarService.openSnackBar(err.error, 'error');
          return of([]);
        })
      );
  }

  getInvocation(dbName: string, invocationID: string): Observable<Invocation> {
    return this.http.get<Invocation>(`${this.baseURL}/${this.invocationsPath}/${dbName}/${invocationID}`)
      .pipe(
        catchError((err: any) => {
          this.snackBarService.openSnackBar(err.error, 'error');
          return of({});
        })
      );
  }

  processEngineToolInvocations(dbName: string, configuration: ToolInvocationBody): Observable<ToolInvocationResponse> {
    return this.http.post<ToolInvocationResponse>(`${this.baseURL}/${this.invocationsPath}/${dbName}`, configuration)
      .pipe(
        tap((result: ToolInvocationResponse) => { result.success = true; }),
        catchError((err: any) => {
          this.snackBarService.openSnackBar(err.error, 'error');
          return of({ invocationID: 'Something went wrong while calling tools', success: false});
        })
      );
  }

  updateStepInformation(dbName: string, configuration: StepUpdatedInformation): Observable<any> {
    return this.http.post(`${this.baseURL}/${this.invocationsProcess}/${dbName}/status`, configuration)
      .pipe(
        catchError((err: any) => {
          this.snackBarService.openSnackBar('Something went wrong while updating the steps', 'error');
          return of({ errorMessage: 'Something went wrong while updating the steps'});
        })
      );
  }

  deleteInvocation(dbName: string, id: string): Observable<any> {
    return this.http.delete(`${this.baseURL}/${this.invocationsPath}/${dbName}/${id}`)
      .pipe(
        catchError((err: any) => {
          return of({ errorMessage: 'Something went wrong while deleting the results', failed: true});
        })
      );
  }
}
