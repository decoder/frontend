import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfiguration } from 'src/config/app-configuration';
import { Invocation } from '../../models/invocation';
import { ProcessEngineService } from './process-engine.service';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('ProcessEngineService', () => {
  let service: ProcessEngineService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProcessEngineService, { provide: MatSnackBar, useClass: MatSnackBarStub }, AppConfiguration],
    });
    service = TestBed.inject(ProcessEngineService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call get with the right URL', () => {
    service.getInvocations('johnny').subscribe((invocations: Invocation[]) => {
      expect(invocations[0].invocationID).toEqual('johnny');
    });

    const req: TestRequest  = httpTestingController.expectOne(`${service.baseURL}/${service.invocationsPath}/johnny?skip=0&limit=25`);

    req.flush([{invocationID: 'johnny'}]);
    httpTestingController.verify();
  });
});
