import { TestBed, inject } from '@angular/core/testing';
import { AuthGuard } from './auth.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginService } from '../login/login.service';
import { LoginComponent } from '../../containers/login/login.component';
import { Router } from '@angular/router';

class MockLoginService {
  logged: boolean;
}

const routes: {}[] = [{ path: 'login', component: LoginComponent }];

describe('AuthGuard', () => {
  beforeEach(() => {
    const mockRouter: {} = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      providers: [
        AuthGuard,
        { provide: LoginService, useClass: MockLoginService },
        { provide: Router, useValue: mockRouter }
      ],
      declarations: [LoginComponent]
    });
  });

  it('should create AuthGuard', inject([AuthGuard], (authGuard: AuthGuard) => {
    expect(authGuard).toBeTruthy();
  }));

  it('should not navigate to "/login" if logged (MockLoginService)', inject(
    [Router, LoginService],
    async (router: Router, loginService: LoginService) => {
      loginService.logged = true;
      const guard: AuthGuard = new AuthGuard(router, loginService);
      const activate: Promise<boolean> = Promise.resolve(guard.canActivate());

      activate.then(() => {
        expect(router.navigate).not.toHaveBeenCalledWith(['/login']);
      });
    }
  ));

  it('should navigate to "/login" if not logged (MockLoginService)', inject(
    [Router, LoginService],
    async (router: Router, loginService: LoginService) => {
      loginService.logged = false;
      const guard: AuthGuard = new AuthGuard(router, loginService);
      const activate: Promise<boolean> = Promise.resolve(guard.canActivate());

      activate.then(() => {
        expect(router.navigate).toHaveBeenCalledWith(['/login']);
      });
    }
  ));
});
