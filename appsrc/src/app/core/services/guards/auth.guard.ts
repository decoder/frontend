import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly login: LoginService
  ) {}

  canActivate(): boolean | Promise<boolean> {
    const logged: boolean = this.login.logged;

    if (!logged) {
      return this.router.navigate(['/login']);
    }

    return logged;
  }
}
