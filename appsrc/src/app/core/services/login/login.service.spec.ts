import { TestBed, inject, fakeAsync, flush } from '@angular/core/testing';
import { LoginService } from './login.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest
} from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { userMock } from '../../../mocks/core/fixtures/user.mock';
import { SnackbarService } from '../snackbar/snackbar.service';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppConfiguration } from 'src/config/app-configuration';
import { generateRandomString } from 'src/app/utils/utils';

const mockRouter: {} = {
  navigate: jasmine.createSpy('navigate')
};

describe('LoginService mock', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        SharedModule
      ],
      providers: [
        { provide: Router, useValue: mockRouter },
        SnackbarService,
        AppConfiguration
      ],
    });
  });

  it('should be created', () => {
    const service: LoginService = TestBed.inject(LoginService);
    expect(service).toBeTruthy();
  });

  // SnackBar Service
  it('SnackbarService should be created', () => {
    const snackbarService: SnackbarService = TestBed.inject(SnackbarService);
    expect(snackbarService).toBeTruthy();
  });

  // logout
  it('should reset password, set logged state to false and navigate to login', inject(
    [Router, LoginService],
    (router: Router, service: LoginService) => {
      service.logout();
      expect(service.email).toBe('');
      expect(service.logged).toBe(false);
      expect(router.navigate).toHaveBeenCalledWith(['/login']);
    }
  ));

  // resetForgottenPassword
  it('should reset email', inject([LoginService], (service: LoginService) => {
    service.resetForgottenPassword();
    expect(service.email).toBe('');
  }));

  // addEmail(email: string): boolean
  it('should add a new non existing email', (done: () => void) => {
    inject([LoginService], (service: LoginService) => {
      service.addEmail('mail@non.exists').subscribe((doesItExist: boolean) => {
        expect(doesItExist).toBe(false);
        done();
      });
    })();
  });

  it('should not add an existing email', (done: () => void) => {
    inject([LoginService], (service: LoginService) => {
      service
        .addEmail('mail@already.exists')
        .subscribe((doesItExist: boolean) => {
          expect(doesItExist).toBe(true);
          done();
        });
    })();
  });

  // changePassword(newPassword: string, newPasswordAgain: string)
  it('should change the password successfully', (done: () => void) => {
    inject([LoginService], (service: LoginService) => {
      service.changePassword('newPassword').subscribe((success: boolean) => {
        expect(success).toBe(true);
        done();
      });
    })();
  });

  // sendUserPassLink
  it('should send user pass link', inject(
    [LoginService],
    (service: LoginService) => {
      expect(service.sendUserPassLink()).toBe(true);
    }
  ));
});

describe('Login with user and password', () => {
  interface AuthData {
    username: string;
    password: string;
    firstLog: boolean;
    valid: boolean;
  }

  let service: LoginService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        SharedModule
      ],
      providers: [
        { provide: Router, useValue: mockRouter },
        AppConfiguration
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(LoginService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should login succesfully with (user, password)', fakeAsync(() => {
    const authData: AuthData = {
      username: 'user',
      password: generateRandomString(),
      firstLog: false,
      valid: true
    };
    loginTest(authData);
  }));

  it('should not login with (user, badpass)', fakeAsync(() => {
    const authData: AuthData = {
      username: 'user',
      password: generateRandomString(),
      firstLog: false,
      valid: false
    };
    loginTest(authData);
  }));

  it('should not login first time with (user, password)', fakeAsync(() => {
    const authData: AuthData = {
      username: 'user',
      password: generateRandomString(),
      firstLog: true,
      valid: false
    };
    loginTest(authData);
  }));

  function loginTest(authData: AuthData): void {
    service.firstLog = authData.firstLog;
    isLogginDataValid(authData);
    flush();
    setupHttp(authData);
    httpMock.verify();
  }

  function isLogginDataValid(authData: AuthData): void {
    service
      .login(authData.username, authData.password)
      .subscribe((valid: boolean) => {
        expect(valid).toBe(authData.valid);
      });
  }

  function setupHttp(authData: AuthData): void {
    const loginUrl: string = service.loginUrl();
    const req: TestRequest = httpMock.expectOne(loginUrl);
    expect(req.request.method).toBe('POST');

    if (authData.valid) {
      req.flush(userMock);
    } else {
      req.flush('User not found', { status: 404, statusText: 'User not found' });
    }
  }
});
