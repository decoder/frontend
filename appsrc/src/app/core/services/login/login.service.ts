import { Injectable } from '@angular/core';
import { SnackbarService } from '../snackbar/snackbar.service';
import { Router } from '@angular/router';
import { of, Observable, Subscriber } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AppConfiguration } from 'src/config/app-configuration';

interface StorageItems {
  currentUser: string;
  currentUserId: string;
  currentRole: string;
  toolsExplanationViewed: string;
}

export const STORAGE_ITEMS: StorageItems = {
  currentUser: 'currentUser',
  currentUserId: 'currentUserId',
  currentRole: 'currentRole',
  toolsExplanationViewed: 'toolsExplanationViewed',
};

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  partiallyLogged: boolean = false;
  logged: boolean = false;
  userId: string = '';
  email: string = '';
  firstLog: boolean = false;
  logiBaseURL: string = 'user/login';

  constructor(
    private router: Router,
    public snackBarService: SnackbarService,
    private httpClient: HttpClient,
    private appConfig: AppConfiguration
  ) {}

  login(username: string, password: string): Observable<boolean> {
    return this.isLoginDataValid(username, password).pipe(
      map((isSuccess: boolean) => {
        if (isSuccess) {
          // KIRBY - Must exchange true for the function of the API that returns if the login is the first login or not.
          return this.firstLog;
        } else {
          throw new Error('User or password wrong');
        }
      }),
      map((isFirstLog: boolean) => {
        this.partiallyLogged = true;
        if (isFirstLog) {
          this.router.navigate(['/home/profile/change-user-password']);
        } else {
          localStorage.setItem(STORAGE_ITEMS.currentUser, username);
          localStorage.setItem(STORAGE_ITEMS.currentUserId, this.userId);

          this.router.navigate(['/login/profile-selector']);
        }

        return !isFirstLog;
      }),
      catchError((err: any) => {
        if (err.message === 'User or password wrong') {
          this.snackBarService.openSnackBar('User or password wrong', 'error');
        } else {
          this.snackBarService.openSnackBar(
            'Could not connect to the server, please try again later.',
            'error'
          );
        }
        return of(false);
      })
    );
  }

  goToHome(selectedRole: any): void {
    try {
      localStorage.setItem(STORAGE_ITEMS.currentRole, selectedRole);
      this.logged = true;
      this.router.navigate(['/home']);
    } catch (error) {
      this.snackBarService.openSnackBar(
        'Could not connect to the server, please try again later.',
        'error'
      );
    }
  }

  isLoginDataValid(username: string, password: string): Observable<boolean> {
    const objUser: any = {
      user_name: username,
      user_password: password,
    };

    const loginUrl: any = this.loginUrl();
    return new Observable(
      (subscriber: Subscriber<boolean>) => {
        this.httpClient.post<string>(loginUrl, objUser).subscribe(
          (jwtdata: any) => {
            localStorage.setItem('access_token', jwtdata.key);
            subscriber.next(true);
          },
          (e: any) => {
            console.log(e);
            subscriber.error(false);
          }
        );
      }
    );
  }

  loginUrl(): string {
    const server: string = this.appConfig.serverURL;
    const login: string = this.logiBaseURL;
    return `${server}/${login}`;
  }

  logout(): void {
    this.httpClient.post(this.appConfig.serverURL + '/user/logout', {}).subscribe(
      () => { localStorage.clear(); }
    );
    this.resetForgottenPassword();
    this.partiallyLogged = false;
    this.logged = false;
    this.router.navigate(['/login']);
  }

  resetForgottenPassword(): void {
    this.email = '';
  }

  addEmail(email: string): Observable<boolean> {
    // KIRBY - Must exchange true for the function of the API that returns if the email exists.
    const doesEmailExist: Observable<boolean> = this.doesMailExist(email);

    return doesEmailExist.pipe(
      map((emailExists: boolean) => {
        if (emailExists) {
          this.email = email;
        } else {
          this.snackBarService.openSnackBar(
            'The email does not exist.',
            'error'
          );
        }

        return emailExists;
      })
    );
  }

  doesMailExist(email: string): Observable<boolean> {
    return of(email === 'mail@already.exists');
  }

  changePassword(newPassword: string): Observable<boolean> {
    // KIRBY - Use the API to check the username(email) and password. If is all ok change the password.
    // checkFromApi(email, newPassword);
    const isAuthCorrect$: Observable<boolean> = of(
      newPassword === 'newPassword'
    );
    return isAuthCorrect$.pipe(map((isCorrect: boolean) => isCorrect));
  }

  sendUserPassLink(): boolean {
    // KIRBY - Send users name and a link to the password set up screen
    // Exchange true for: returns true if the email is sent correctly, if not, returns false
    return true;
  }
}
