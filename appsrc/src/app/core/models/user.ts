import { Role } from './role';
import { ProjectProfile } from './project';

export interface User {
  id?: string;
  firstName?: string;
  surname?: string;
  username: string;
  registerDate?: Date;
  roles?: Role[];
  organization?: string;
  location?: string;
  email: string;
  status?: string;
  picture?: string;
  projectList?: number[];
  projects?: ProjectProfile[];
  lastLogin?: Date;
}
