export type versionStatus = 'Processing' | 'Processed' | 'Testing' | 'Tested' | 'Error';

export interface VersionNameId {
    versionId: string;
    versionName: string;
}

export interface Version {
    versionId: string;
    versionName: string;
    creationDate: Date;
    commits?: number;
    lastCommitId?: string;
    relatedArtifacts?: string[];
    versionStatus: versionStatus;
}
