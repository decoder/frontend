import { Role } from './role';

export interface PKMUser {
  email?: string;
  first_name?: string;
  last_name?: string;
  name?: string;
  phone?: string;
  roles?: Role[];
  // proto?: Object;
}
