import { PKMASFMDocArtifact } from 'src/app/plugins/project/models/asfm-document-artifact.model';
import { PKMAnnotation, PKMNerSrlResult, PKMCAnnotation } from './annotation';
import { PKMComment } from './comment';

export type ArtifactType =
  | 'Diagram'
  | 'Code'
  | 'Document'
  | 'ASFM Docs'
  | 'Annotation'
  | 'Log'
  | 'Comment'
  | 'TESTAR_State_Model'
  | 'UML Model'
  | 'NER'
  | 'SRL'
  | 'Binary Executable'
  | 'CVE';

export type StatusType = 'Status1' | 'Status2' | 'Status3';

export class PKMArtifact {
  id: string;

  name: string;
  type: ArtifactType;
  // tslint:disable-next-line: variable-name
  rel_path: string;
  sourceFile?: string;
  content: any; // must support many types of content
  format: string;
  encoding: string;
  // tslint:disable-next-line: variable-name
  mime_type: string;
  // tslint:disable-next-line: variable-name
  git_repository_url: string;
  // tslint:disable-next-line: variable-name
  git_commit_id: string;

  static fromUMLArtifact(umlArtifact: PKMUMLClassDiagramArtifact): PKMArtifact {
    // creates a PKMArtifact from a UML Artifact
    return {
      content: umlArtifact.diagram,
      type: umlArtifact.type,
      name: umlArtifact.name,
      id: umlArtifact.name,
      rel_path: umlArtifact.name,
      format: '',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromTESTARArtifact(testarArtifact: PKMTESTARArtifact): PKMArtifact {
    // creates a PKMArtifact from a TESTAR Artifact
    return {
      // content = testarArtifact.,
      type: testarArtifact.type,
      name: testarArtifact.stateModelDataStoreDB,
      id: testarArtifact._id,
      rel_path: '',
      format: '',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromDocumentArtifact(document: PKMDocumentArtifact): PKMArtifact {
    // creates a PKMArtifact from a Document Artifact
    return {
      content: '',
      type: document.type,
      name: document.rel_path,
      id: document.rel_path,
      rel_path: document.rel_path,
      format: document.format,
      encoding: document.encoding,
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromASFMDocArtifact(document: PKMASFMDocArtifact): PKMArtifact {
    // creates a PKMArtifact from a ASFM Document Artifact
    return {
      content: document,
      type: document.type,
      name: document.name,
      id: document.name,
      rel_path: document.sourceFile,
      format: 'json',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromAnnotationArtifact(annotation: PKMAnnotation): PKMArtifact {
    // if it has the tool property
    let appendText: string;
    if (annotation.tool) {
      appendText = '(jsonOpenJML)';
    } else {
      appendText = '';
    }
    return {
      id: annotation.sourceFile + appendText,
      name: annotation.sourceFile + appendText,
      type: annotation.type,
      content: annotation, // pass all the structure to the renderer component
      rel_path: annotation.sourceFile,
      format: annotation.fileFormat,
      encoding: annotation.fileEncoding,
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromNerSrlResultArtifact(annotation: PKMNerSrlResult): PKMArtifact {
    if (annotation.ner !== undefined) {
      return {
        id: annotation.path + annotation._id,
        name: annotation.path,
        type: 'NER',
        content: annotation, // pass all the structure to the renderer component
        rel_path: annotation.path,
        format: '',
        encoding: '',
        git_repository_url: '',
        git_commit_id: '',
      } as PKMArtifact;
    } else {
      return {
        id: annotation.path + annotation._id,
        name: annotation.path,
        type: 'SRL',
        content: annotation, // pass all the structure to the renderer component
        rel_path: annotation.path,
        format: '',
        encoding: '',
        git_repository_url: '',
        git_commit_id: '',
      } as PKMArtifact;
    }
  }

  static fromCAnnotationArtifact(annotation: PKMCAnnotation): PKMArtifact {
    // if it has the tool property
    return {
      id: annotation.sourceFile,
      name: annotation.sourceFile,
      type: annotation.type,
      content: annotation, // pass all the structure to the renderer component
      rel_path: annotation.sourceFile,
      format: '',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromCommentArtifact(comment: PKMComment): PKMArtifact {
    // creates a PKMArtifact from a Comment Artifact
    return {
      content: comment,
      type: comment.type,
      name: comment.sourceFile,
      id: comment.sourceFile,
      rel_path: comment.sourceFile,
      format: comment.fileFormat,
      encoding: comment.fileEncoding,
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromLogArtifact(log: PKMLogArtifact): PKMArtifact {
    // creates a PKMArtifact from a Log Artifact
    return {
      content: log,
      type: log.type,
      name: `(${log.tool} ${log['nature of report']})-${log.id}`,
      id: log.id,
      rel_path: log._id,
      format: 'json',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromBinaryExecutableArtifact(executable: PKMExecutableArtifact): PKMArtifact {
    // creates a PKMArtifact from a Binary Executable Artifact
    return {
      content: executable.content,
      type: executable.type,
      name: executable.rel_path,
      id: executable.rel_path,
      rel_path: executable.rel_path,
      format: executable.format,
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }

  static fromCVEArtifact(executable: PKMCVEArtifact): PKMArtifact {
    // creates a PKMArtifact from a CVE Artifact
    return {
      content: executable,
      type: 'CVE',
      name: executable.CVE_data_meta.ID,
      id: executable.CVE_data_meta.ID,
      rel_path: executable.CVE_data_meta.ID,
      format: 'json',
      encoding: '',
      git_repository_url: '',
      git_commit_id: '',
    } as PKMArtifact;
  }
}

export class PKMDocumentArtifact {
  // tslint:disable-next-line: variable-name
  rel_path: string;
  content: string;
  format: string;
  encoding: string;
  type: string;
  // tslint:disable-next-line: variable-name
  mime_type: string;
  // tslint:disable-next-line: variable-name
  git_repository_url: string;
  // tslint:disable-next-line: variable-name
  git_commit_id: string;
}

export class PKMExecutableArtifact {
  // tslint:disable-next-line: variable-name
  rel_path: string;
  content: string;
  format: string;
  encoding: string;
  type: string;
  // tslint:disable-next-line: variable-name
  mime_type: string;
  // tslint:disable-next-line: variable-name
  git_repository_url: string;
  // tslint:disable-next-line: variable-name
  git_commit_id: string;
}

export class PKMUMLClassDiagramArtifact {
  _id: string;
  name: string;
  type: ArtifactType;
  modules: any; // big structured thing here, but ignored in this case
  sourcefile: string;
  diagram: string;
}

export class PKMTESTARArtifact {
  _id: string;
  url: string;
  type: ArtifactType; // "TESTAR_State_Model"
  timestamp: string;
  sut: any; // big structured thing here, but ignored in this case
  tool: any; // data about the tool
  stateModelDataStore: string;
  stateModelDataStoreType: string;
  stateModelDataStoreServer: string;
  stateModelDataStoreDirectory: string;
  stateModelDataStoreDB: string;
  stateModelDataStoreUser: string;
  stateModelDataStorePassword: string;
  stateModelIdentifier: string;
  stateModelAppName: string;
  stateModelAppVersion: number;
  stateModelDifference: any;
  abstractionId: string;
  deterministic: boolean;
  unvisitedAbstractActions: number;
  numberAbstractStates: number;
  numberAbstractActions: number;
  numberConcreteStates: number;
  numberConcreteActions: number;
  storeWidgets: boolean;
  numberWidgets: number;
  numberTestSequences: number;
  testSequences: any; // structured thing here
}

export class PKMLogArtifact {
  _id: string;
  id: string;
  tool: string; // "code_summarization"
  'nature of report': string; // "Execution log",
  type: ArtifactType; // "Log"
  'start running time': string; // format: "2021-02-26T11:43:34.000Z"
  'end running time': string; // format: "2021-02-26T11:43:34.000Z",
  messages: string[]; // "DECODER_API.controller: Starting prediction for file Foo%Test.java inside collection mythaistar",
  warnings: string[];
  errors: string[];
  status: boolean;
}

interface CVEDataMeta {
    ASSIGNER: string;
    ID: string;
    STATE: string;
    UPDATED: string;
}

interface Version {
    version_value: string;
}

interface Product {
    versions: Version[];
}

interface Vendor {
    products: Product[];
    vendor_name: string;
}

interface Affected {
    vendors: Vendor[];
}

interface Description {
    lang: string;
    value: string;
}

interface Problemtypes {
    descriptions: any[][];
}

interface Reference {
    name: string;
    refsource: string;
    url: string;
}

interface CnaContainer {
    affected: Affected;
    descriptions: Description[];
    problemtypes: Problemtypes;
    references: Reference[];
}

export class PKMCVEArtifact {
  // tslint:disable-next-line: variable-name
  CVE_data_meta: CVEDataMeta;
  'cna-container': CnaContainer;
  // tslint:disable-next-line: variable-name
  data_format: string;
  // tslint:disable-next-line: variable-name
  data_type: string;
  // tslint:disable-next-line: variable-name
  data_version: string;
}
