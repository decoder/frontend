import { Role } from './role';

export interface MemberDetails {
    id?: string;
    memberId?: string;
    username: string;
    email: string;
    status?: string;
    roles?: Role[];
    owner?: boolean;
    requestDate?: Date;
}
