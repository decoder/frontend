export interface VersionChange {
  [key: string]: number;
}
