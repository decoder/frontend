export interface ProjectVersionInfo {
  name: string;
  versionId: string;
  versionName: string;
  projectId?: string;
}
