import { ReviewComment } from './reviewComment';

export interface Review {
  _id?: string;
  reviewID?: string;
  reviewTitle: string;
  reviewOpenDate: string;
  reviewCompletedDate?: string;
  reviewAuthor: string;
  reviewStatus: string;
  reviewCommets: ReviewComment[];
  reviewArtifacts: ReviewArtifacts[];
}

export interface ReviewHistory {
  number: number;
  user: string;
  date: string;
  comments?: string;
}

export interface ReviewArtifacts {
  path: string;
  type: string;
}
