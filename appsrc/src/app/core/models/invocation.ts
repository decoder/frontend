export interface Invocation {
  invocationID?: string;
  tool?: string;
  invocationConfiguration?: InvocationArtifact;
  user?: string;
  timestampRequest?: string;
  timestampStart?: string;
  timestampCompleted?: string;
  invocationArtifacts?: InvocationArtifact[];
  invocationStatus?: string;
  invocationResults?: InvocationArtifactResult[];
  message?: string;
}

export interface ToolConfiguration {
  generate?: string;
}

export interface InvocationArtifact {
  dbName?: string;
  project_id?: string;
  generate?: string;
  file?: string;
  sourceFileName?: string;
  source_file_paths?: string;
  path?: string;
  mode?: string;
  options?: string;

}

export interface InvocationArtifactResult {
  path: string;
  type: string;
}
