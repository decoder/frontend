export interface VersionComparison {
  projectId: string;
  baseVersionId: string;
  compareToVersionId: string;
}
