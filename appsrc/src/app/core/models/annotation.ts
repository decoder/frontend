import { ArtifactType } from './artifact';

export interface PKMAnnotation {
  _id: string;
  tool: any; // used for OpenJML Annotations only
  annotations: Annotation[];
  fileEncoding: string;
  fileFormat: string;
  fileMimeType: string;
  sourceFile: string;
  type: ArtifactType;
}

export interface PKMCAnnotation {
  annotations: PKMCAnnotation[];
  sourceFile: string;
  type: ArtifactType;
}
export interface Annotation {
  annotations: string[];
  commentInEnvironment?: string;
  annotationInEnvironmentSplited?: string[];
  loc: Loc;
}

export interface CAnnotation {
  CAnnotations: object[];
  loc: Loc;
}
interface Loc {
  pos_end: Pos;
  pos_start: Pos;
}

interface Pos {
  pos_cnum: number;
  pos_lnum: number;
}

export interface PKMNerSrlResult {
  _id: string;
  type: string;
  path: string;
  access: string;
  ner?: any[];
  srl?: any[];
}
