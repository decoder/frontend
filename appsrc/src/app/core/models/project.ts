import { MemberDetails } from './memberDetails';
import { Version } from './version';
import { Role } from './role';

export interface ProjectNameId {
  projectId: string;
  projectName: string;
  owner: boolean;
}
export interface ProjectProfile {
  id: string;
  name: string;
  roles: Role[];
  creationDate: Date;
}

export interface Project {
  projectId: string;
  projectName: string;
  projectToken: string;
  company: string;
  versions: Version[];
  repository: string;
  lastCommitDate: Date;
  creationDate: Date;
  userCreatorId: string;
  members: MemberDetails[];
}

export interface PKMProject {
  name: string;
  members: PKMProjectMember[];
}

export interface PKMProjectNameId {
  projectId: string;
  projectName: string;
  owner: boolean;
}

export interface PKMProjectMember {
  name: string;
  owner?: boolean;
  roles: string[];
}
