import { ArtifactType } from './artifact';

export interface PKMComment {
  _id: string;
  fileEncoding?: string;
  fileFormat?: string;
  fileMimeType?: string;
  sourceFile: string;
  type: ArtifactType;
  comments: CommentItem[];
}

export interface CommentItem {
  loc: Loc;
  comments: object[];
  global_kind?: string;
}

interface Loc {
  pos_end: Pos;
  pos_start: Pos;
}

interface Pos {
  pos_path: string;
  pos_cnum: number;
  pos_lnum: number;
}
