export interface ReviewComment {
  author: string;
  datetime?: string;
  comment: string;
}
