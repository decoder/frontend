import { ArtifactSearchItem } from './artifact-search-item';
import { Page } from 'src/app/plugins/dashboard/models/traceability-matrix/page';

export interface ArtifactSearch {
  results: ArtifactSearchItem[];
  pagination: Page;
}
