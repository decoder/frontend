import { Page } from 'src/app/plugins/dashboard/models/traceability-matrix/page';

// currently not in use
export interface ArtifactFilter {
  userId: string;
  name?: string;
  criteria?: string;
  type?: ('Diagram' | 'Code' | 'Document' | 'Annotation')[];
  projectIds?: string[];
  projectVersions?: string[];
  pagination?: Page;
}
