export interface ArtifactSearchItem {
  id: string;
  name: string;
  type: ('Diagram' | 'Code' | 'Document' | 'Annotation');
  projectName: string;
  projectId: string;
  version: string;
  versionId: string;
  artifactId: string;
  preview?: string;
}
