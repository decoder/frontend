import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JupyterComponent } from './components/jupyter/jupyter.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{ path: '', component: JupyterComponent}];

@NgModule({
  declarations: [JupyterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class JupyterModule { }
