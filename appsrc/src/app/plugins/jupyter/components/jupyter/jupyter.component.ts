import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { AppConfiguration } from 'src/config/app-configuration';

@Component({
  selector: 'app-jupyter',
  templateUrl: './jupyter.component.html',
  styleUrls: ['./jupyter.component.scss']
})
export class JupyterComponent {
  jupyterURL: SafeUrl;

  constructor(private sanitizer: DomSanitizer, private appConfig: AppConfiguration) {
    // Tells angular to trust this URL
    this.jupyterURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.appConfig.jupyterURL);
  }
}
