export const interfacePlugins: any = [
  {
    path: 'projects/:projectId/:versionId',
    icon: 'list',
    text: 'Navigation',
    moduleName: 'ProjectModule',
    importFunction: () => import('./project/project.module')
  },
  {
    path: 'projects/:projectId/:versionId/tools',
    icon: 'build',
    text: 'Tools',
    moduleName: 'ProjectModule',
    importFunction: () => import('./project/project.module')
  },
  {
    path: 'projects/:projectId/:versionId/results',
    icon: 'rule',
    text: 'Result',
    moduleName: 'ProjectModule',
    importFunction: () => import('./project/project.module')
  },
  {
    path: 'dashboard/:projectId/:versionId',
    icon: 'grid_on',
    text: 'Matrix',
    moduleName: 'DashboardModule',
    importFunction: () => import('./dashboard/dashboard.module')
  },
  {
    path: 'jupyter',
    icon: 'code',
    text: 'Jupyter',
    moduleName: 'JupyterModule',
    importFunction: () => import('./jupyter/jupyter.module')
  },
  // Append your pluggin here
];

export const rendererPlugins: any = {
  default: {
    componentName: 'DefaultRendererComponent',
    importFunction: () =>
      import('./renderers/default-renderer/default-renderer.component')
  },
  code: {
    componentName: 'CodeRendererComponent',
    importFunction: () =>
      import('./renderers/code-renderer/code-renderer.component')
  },
  txt: {
    componentName: 'TextRendererComponent',
    importFunction: () =>
      import('./renderers/text-renderer/text-renderer.component')
  },
  json: {
    componentName: 'JsonTreeRendererComponent',
    importFunction: () =>
      import('./renderers/json-tree-renderer/json-tree-renderer.component')
  },
  uml: {
    componentName: 'UmlRendererComponent',
    importFunction: () =>
      import('./renderers/uml-renderer/uml-renderer.component')
  },
  comment: {
    componentName: 'CommentRendererComponent',
    importFunction: () =>
      import('./renderers/comment-renderer/comment-renderer.component')
  },
  annotation: {
    componentName: 'AnnotationRendererComponent',
    importFunction: () =>
      import('./renderers/annotation-renderer/annotation-renderer.component')
  },
  ner: {
    componentName: 'NerRendererComponent',
    importFunction: () =>
      import('./renderers/ner-renderer/ner-renderer.component')
  },
  srl: {
    componentName: 'SrlRendererComponent',
    importFunction: () =>
      import('./renderers/srl-renderer/srl-renderer.component')
  },
  web_testar_statemodel: {
    componentName: 'StateModelVisualizerComponent',
    importFunction: () =>
      import('src/app/plugins/project/components/statemodel-visualizer/statemodel-visualizer.component')
  },
  document: {
    componentName: 'DocumentRendererComponent',
    importFunction: () =>
      import('./renderers/document-renderer/document-renderer.component')
  },
};
