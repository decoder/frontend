import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { projectMock } from 'src/app/mocks/core/fixtures/project.mock';
import { Project } from 'src/app/core/models/project';
import { ActivatedRoute } from '@angular/router';
import { versionMock } from 'src/app/mocks/core/fixtures/version.mock';
import { ReportsDialogService } from '../../services/reports-dialog/reports-dialog.service';

class ProjectsServiceStub {
  getProjectById(): Observable<Project> {
    return of({...projectMock[0], versions: [...versionMock]});
  }
}

class ActivatedRouteStub {
  snapshot: any;

  constructor() {
    this.snapshot = {
      paramMap: {
        get: () => '0'
      }
    };
  }
}

class ReportsDialogServiceStub {
  openSitemapDifferences(): Observable<any> {
    return of();
  }
}

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let reportsDialog: ReportsDialogService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [RouterTestingModule],
      providers: [
        { provide: ProjectsService, useClass: ProjectsServiceStub },
        { provide: ReportsDialogService, useClass: ReportsDialogServiceStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    reportsDialog = TestBed.inject(ReportsDialogService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open sitemap', () => {
    spyOn(reportsDialog, 'openSitemapDifferences');

    component.onOpenSitemap();
    expect(reportsDialog.openSitemapDifferences).toHaveBeenCalled();
  });
});
