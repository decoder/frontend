import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Project } from 'src/app/core/models/project';
import { ReportsDialogService } from '../../services/reports-dialog/reports-dialog.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  selectedProject: Project;
  urlParams: { projectId: string; versionId: string };
  currentVersionId: string;

  constructor(
    private route: ActivatedRoute,
    private reportsDialog: ReportsDialogService,
  ) { }

  ngOnInit(): void {
    this.setUrlParams();
  }

  onOpenSitemap(): void {
    this.reportsDialog.openSitemapDifferences();
  }

  private setUrlParams(): void {
    const params: ParamMap = this.route.snapshot.paramMap;
    const projectId: string = params.get('projectId');
    const versionId: string = params.get('versionId');
    this.currentVersionId = versionId;
    this.urlParams = { projectId, versionId };
  }

}
