import {
  Component,
  Input,
} from '@angular/core';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';

@Component({
  selector: 'app-version-comparison-matrix-view',
  templateUrl: './version-comparison-matrix-view.component.html',
  styleUrls: ['./version-comparison-matrix-view.component.scss'],
})
export class VersionComparisonMatrixViewComponent {

  @Input() projectId: string;

  show: boolean;
  toolTipDescription: string = `The traceability matrix shows the relationship between two sections of text from files
   (comments and annotations by the moment) of the same project. Relationships are identified by the arrow symbol.`;

  constructor(private projects: ProjectsService) {
    this.show = true;
  }

}
