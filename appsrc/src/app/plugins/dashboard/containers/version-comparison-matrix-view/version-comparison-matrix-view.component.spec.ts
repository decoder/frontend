import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';

import { VersionComparisonMatrixViewComponent } from './version-comparison-matrix-view.component';
import { Observable, asyncScheduler, scheduled, of } from 'rxjs';
import { VersionChange } from 'src/app/core/models/version-change';
import { versionChangesMock } from 'src/app/mocks/core/fixtures/version-change.mock';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';

class ProjectServiceStub {
  compareVersions(): Observable<VersionChange[]> {
    return scheduled(of(versionChangesMock), asyncScheduler);
  }
}

describe('VersionComparisonMatrixViewComponent', () => {
  let component: VersionComparisonMatrixViewComponent;
  let fixture: ComponentFixture<VersionComparisonMatrixViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VersionComparisonMatrixViewComponent],
      providers: [
        { provide: ProjectsService, useClass: ProjectServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionComparisonMatrixViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
