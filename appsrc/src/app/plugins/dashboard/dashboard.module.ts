import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { VersionComparisonMatrixViewComponent } from './containers/version-comparison-matrix-view/version-comparison-matrix-view.component';
import { Routes, RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TraceabilityMatrixComponent } from './components/traceability-matrix/traceability-matrix.component';
import { MaterialModule } from 'src/app/material.module';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { ReportsDialogComponent } from './containers/reports-dialog/reports-dialog.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { TraceabilityMatrixService } from './services/traceability-matrix/traceability-matrix.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { RenderersModule } from '../renderers/renderers.module';
import { TmatrixResultDetail } from './components/tmatrix-result-detail/tmatrix-result-detail.component';

const routes: Routes = [
  { path: '**', pathMatch: 'full', component: DashboardComponent },
];

@NgModule({
  declarations: [
    DashboardComponent,
    VersionComparisonMatrixViewComponent,
    TraceabilityMatrixComponent,
    ReportsDialogComponent,
    TmatrixResultDetail,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatSelectModule,
    MaterialModule,
    MatGridListModule,
    SharedModule,
    RouterModule.forChild(routes),
    RenderersModule,
  ],
  providers: [ProjectsService, TraceabilityMatrixService],
})
export class DashboardModule {}
