import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ReportsDialogComponent } from '../../containers/reports-dialog/reports-dialog.component';
import { Observable } from 'rxjs';

type ReportsDialog = MatDialogRef<ReportsDialogComponent, any>;

@Injectable({
  providedIn: 'root',
})
export class ReportsDialogService {
  constructor(private dialog: MatDialog) {}

  openSitemapDifferences(): Observable<any> {
    const dialogRef: ReportsDialog = this.dialog.open(ReportsDialogComponent, {
      width: '80%',
      data: 'difference-report/DifferenceReport.html',
      autoFocus: false,
    });

    return dialogRef.afterClosed();
  }
}
