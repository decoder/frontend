import { TestBed } from '@angular/core/testing';

import { ReportsDialogService } from './reports-dialog.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ReportsDialogComponent } from '../../containers/reports-dialog/reports-dialog.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ReportsDialogService', () => {
  let service: ReportsDialogService;
  let matDialog: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
      imports: [NoopAnimationsModule, MatDialogModule],
    });
    service = TestBed.inject(ReportsDialogService);
    matDialog = TestBed.inject(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should [openSitemapDifferences]', () => {
    spyOn(matDialog, 'open').and.callThrough();
    service.openSitemapDifferences();

    expect(matDialog.open).toHaveBeenCalledWith(
      ReportsDialogComponent,
      jasmine.anything()
    );
  });
});
