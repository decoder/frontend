import { TestBed, fakeAsync, flush } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { TraceabilityMatrixService } from './traceability-matrix.service';
import { VersionComparison } from 'src/app/core/models/version-comparison';
import { mockMatrix } from '../../../../mocks/dashboard/fixtures/traceability-matrix/matrix.mock';
import { PKMMatrixJSON } from './types/PKMMatrixJSON';
import { AppConfiguration } from 'src/config/app-configuration';

describe('TraceabilityMatrixService', () => {
  let service: TraceabilityMatrixService;
  let comparison: VersionComparison;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppConfiguration],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(TraceabilityMatrixService);
    http = TestBed.inject(HttpTestingController);

    comparison = {
      projectId: '1',
      baseVersionId: '2',
      compareToVersionId: '3',
    };
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a matrix', fakeAsync(() => {
    const response: PKMMatrixJSON = mockMatrix;
    let result: PKMMatrixJSON;

    service.getTraceabilityMatrixResults(comparison.projectId).subscribe((matrix: PKMMatrixJSON) => (result = matrix));

    configureResponse(response);
    flush();

    expect(result).toEqual(response);
  }));

  function configureResponse(response: PKMMatrixJSON): void {
    const url: string = `${service.toolBaseEndpoint}/traceability_matrix/${comparison.projectId}`;
    const req: TestRequest = http.expectOne(() => true);
    expect(req.request.method).toBe('GET');
    expect(req.request.url).toEqual(url);
    req.flush(response);
    http.verify();
  }
});
