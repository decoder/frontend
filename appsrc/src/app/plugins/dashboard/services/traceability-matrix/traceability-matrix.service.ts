import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PKMMatrixJSON } from './types/PKMMatrixJSON';
import { AppConfiguration } from 'src/config/app-configuration';

export interface VersionComparison {
  projectId: string;
  baseVersionId: string;
  compareToVersionId: string;
}

@Injectable({
  providedIn: 'root',
})
export class TraceabilityMatrixService {

  toolBaseEndpoint: string = this.appConfig.serverURL;

  constructor(private http: HttpClient, private appConfig: AppConfiguration) {}

  getTraceabilityMatrixResults(projectId: string): Observable<PKMMatrixJSON> {​​​​​​​
    const url: string = `${this.toolBaseEndpoint}/traceability_matrix/${projectId}`;
    return this.http.get<PKMMatrixJSON>(url);
  }​​​​​​​
}
