import { Matrix } from './matrix';
import { mockMatrix } from '../../../../../mocks/dashboard/fixtures/traceability-matrix/matrix.mock';
import {
  ArtifactRefCol,
  ArtifactRefRow,
} from '../../../models/traceability-matrix/artifact-ref';
import { MatrixElement } from '../../../models/traceability-matrix/matrix-element';

const COLUMNS: string = 'columns';
const ROWS: string = 'rows';
const ELEMENTS: string = 'elements';

const aRowId: string =
  // tslint:disable-next-line: max-line-length
  'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java-(generated ID)';
const aColId: string =
  // tslint:disable-next-line: max-line-length
  'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java-(generated ID)';

describe('Matrix class', () => {
  let matrix: Matrix;
  beforeEach(() => {
    matrix = new Matrix(mockMatrix);
  });

  it('should get column keys [getColumnIds]', () => {
    expect(matrix.getColumnIds()).toEqual(Object.keys(matrix[COLUMNS]));
  });

  it('should get column keys [getRowIds]', () => {
    expect(matrix.getRowIds()).toEqual(Object.keys(matrix[ROWS]));
  });

  it('should get column keys [getColumnValue]', () => {
    const refCol: ArtifactRefCol = matrix.getColumnValue(aColId);
    expect(refCol).toEqual(matrix[COLUMNS][aColId]);
  });

  it('should get row keys [getRowValue]', () => {
    const refRow: ArtifactRefRow = matrix.getRowValue(aRowId);
    expect(refRow).toEqual(matrix[ROWS][aRowId]);
  });

  it('should get the specified element on [getElement]', () => {
    const element: MatrixElement = matrix.getElement(aRowId, aColId);
    expect(element).toEqual(matrix[ELEMENTS][aRowId][aColId]);
  });
});
