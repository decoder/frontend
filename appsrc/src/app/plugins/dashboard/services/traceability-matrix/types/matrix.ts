import { MatrixElement } from '../../../models/traceability-matrix/matrix-element';
import {
  ArtifactRefCol,
  ArtifactRefRow,
} from '../../../models/traceability-matrix/artifact-ref';
import { PKMMatrixElemJSON, PKMMatrixJSON } from './PKMMatrixJSON';
import { artifactNameRetriever } from 'src/app/utils/utils';

interface RowMap {
  [rowId: string]: ArtifactRefRow;
}

interface ColumnMap {
  [columnId: string]: ArtifactRefCol;
}

interface ElementsMap {
  [rowId: string]: {
    [columnId: string]: MatrixElement;
  };
}

export class Matrix {
  private rows: RowMap;
  private columns: ColumnMap;
  private elements: ElementsMap;

  constructor(private matrixJson: PKMMatrixJSON) {
    this.elements = {};
    this.rows = {};
    this.columns = {};
    this.updateWithMatrix(this.matrixJson);
  }

  getColumnIds(): string[] {
    return Object.keys(this.columns);
  }

  getRowIds(): string[] {
    return Object.keys(this.rows);
  }

  getColumnValue(columnId: string): ArtifactRefCol {
    return this.columns[columnId];
  }

  getRowValue(rowId: string): ArtifactRefRow {
    return this.rows[rowId];
  }

  getElement(rowId: string, columnId: string): MatrixElement {
    const element: MatrixElement = this.elements[rowId][columnId];
    if (!element) {
      return this.defaultElement(rowId, columnId);
    }

    return element;
  }

  updateWithMatrix(sourceMatrix: PKMMatrixJSON): void {
    for (const element of sourceMatrix) {

      const rowId: string = this.getLastFileNameFrom(element, 'row');

      const columnId: string = this.getLastFileNameFrom(element, 'col');

      this.rows[rowId] = {
        id: rowId,
        changeType: rowId,
        location: rowId,
        name: rowId,
        src_path: element.src_path,
        src_access: element.src_access,
        src_text: element.src_text,
        similarity: element.similarity,
        invocationID: element.invocationID,
      } as ArtifactRefRow;
      this.columns[columnId] = {
        id: columnId,
        update: true,
        location: columnId,
        name: columnId,
        tgt_path: element.tgt_path,
        tgt_access: element.tgt_access,
        tgt_text: element.tgt_text,
        similarity: element.similarity,
        invocationID: element.invocationID,
      } as ArtifactRefCol;

      if (!this.elements[rowId]) {
        this.elements[rowId] = {};
      }

      const elem: MatrixElement = {
        origin: this.rows[rowId],
        destination: this.columns[columnId],
      };

      this.elements[rowId][columnId] = elem;
    }
  }

  getLastFileNameFrom(element: PKMMatrixElemJSON, type: string ): string {
    if (type === 'row') {
      return `${artifactNameRetriever(element.src_path, true)}-(${element.invocationID})`;
    } else {
      return `${artifactNameRetriever(element.tgt_path, true)}-(${element.invocationID})`;
    }
  }

  getIdFromAccess(access: string): string {
    const index: number = access.indexOf('%20%3D%3D%20%60');
    const index2: number = access.indexOf('%60%5D');

    return access.substring(index + 15, index2);
  }

  private defaultElement(rowId: string, columnId: string): MatrixElement {
    const origin: ArtifactRefRow = {
      id: rowId,
      location: '',
      name: '',
      changeType: '',
      src_path: '',
      src_access: '',
      src_text: '',
      similarity: 0,
      invocationID: ''
    };

    const destination: ArtifactRefCol = {
      id: columnId,
      location: '',
      name: '',
      update: false,
      tgt_path: '',
      tgt_access: '',
      tgt_text: '',
      similarity: 0,
      invocationID: ''
    };

    return { origin, destination };
  }
}
