
export type PKMMatrixJSON = PKMMatrixElemJSON[];

export interface PKMMatrixElemJSON {
    _id: string;
    src_path: string;
    src_access: string;
    src_text: string;
    tgt_path: string;
    tgt_access: string;
    tgt_text: string;
    similarity: number;
    role: string;
    invocationID: string;
}
