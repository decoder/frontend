import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatrixElement } from '../../models/traceability-matrix/matrix-element';

@Component({
  selector: 'app-invocation-info',
  templateUrl: './tmatrix-result-detail.component.html',
  styleUrls: ['./tmatrix-result-detail.component.scss'],
})
export class TmatrixResultDetail implements OnInit {

  matrixResultInformation: MatrixElement;

  constructor(
  public dialogRef: MatDialogRef<TmatrixResultDetail>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.matrixResultInformation = this.data;
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}
