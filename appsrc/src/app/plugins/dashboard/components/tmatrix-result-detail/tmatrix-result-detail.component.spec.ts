import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { mockMatrixElements } from 'src/app/mocks/dashboard/fixtures/traceability-matrix/matrix-elements.mock';
import { TimestampPipe } from 'src/app/shared/pipes/timestamp/timestamp.pipe';
import { AppConfiguration } from 'src/config/app-configuration';

import { TmatrixResultDetail } from './tmatrix-result-detail.component';

describe('TmatrixResultDetail', () => {
  let component: TmatrixResultDetail;
  let fixture: ComponentFixture<TmatrixResultDetail>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TmatrixResultDetail, TimestampPipe],
      imports: [MatDialogModule, HttpClientTestingModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmatrixResultDetail);
    component = fixture.componentInstance;
    component.data = {
      origin: mockMatrixElements[0],
      destination: mockMatrixElements[1]
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
