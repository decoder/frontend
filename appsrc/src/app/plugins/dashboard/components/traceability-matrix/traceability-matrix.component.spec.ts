import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';

import { TraceabilityMatrixComponent } from './traceability-matrix.component';
import { TraceabilityMatrixService } from '../../services/traceability-matrix/traceability-matrix.service';
import { Observable, of } from 'rxjs';
import { MatMenuModule } from '@angular/material/menu';
import { PKMMatrixJSON } from '../../services/traceability-matrix/types/PKMMatrixJSON';
import { mockMatrixElements } from 'src/app/mocks/dashboard/fixtures/traceability-matrix/matrix-elements.mock';
import { SharedModule } from 'src/app/shared/shared.module';

describe('TraceabilityMatrixComponent', () => {
  let component: TraceabilityMatrixComponent;
  let fixture: ComponentFixture<TraceabilityMatrixComponent>;

  class TraceabilityMatrixServiceStub {
    getTraceabilityMatrixResults(): Observable<PKMMatrixJSON> {
      return of(mockMatrixElements);
    }

    createRelation(): Observable<any> {
      return of(null);
    }

    deleteRelation(): Observable<any> {
      return of(null);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TraceabilityMatrixComponent],
      imports: [MatMenuModule, SharedModule],
      providers: [
        {
          provide: TraceabilityMatrixService,
          useClass: TraceabilityMatrixServiceStub,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraceabilityMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

});
