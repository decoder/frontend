import { Component, OnInit, Input } from '@angular/core';
import { TraceabilityMatrixService } from '../../services/traceability-matrix/traceability-matrix.service';
import { Matrix } from '../../services/traceability-matrix/types/matrix';
import { MatrixParams } from '../../types/traceability-matrix/matrix-params';
import { MatrixElement } from '../../models/traceability-matrix/matrix-element';
import { PKMMatrixJSON } from '../../services/traceability-matrix/types/PKMMatrixJSON';
import { MatDialog } from '@angular/material/dialog';
import { TmatrixResultDetail } from '../tmatrix-result-detail/tmatrix-result-detail.component';

@Component({
  selector: 'app-traceability-matrix',
  templateUrl: './traceability-matrix.component.html',
  styleUrls: ['./traceability-matrix.component.scss'],
})
export class TraceabilityMatrixComponent implements OnInit {
  private matrixParams: MatrixParams;
  @Input() projectId: string;

  matrix: Matrix;
  rows: string[];
  columns: string[];
  clickedCell: any = { row: '', column: '', relation: '', information: null };

  constructor(
    private matrixService: TraceabilityMatrixService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.matrixParams = {
      projectId: this.projectId,
      pageCol: 0,
      pageRow: 0,
    };

    this.loadMatrix();
  }

  onRightClick(event: MouseEvent, rowId: string, columnId: string): void {
    event.preventDefault();
    this.clickedCell.row = rowId;
    this.clickedCell.column = columnId;
    this.clickedCell.relation = this.relation(rowId, columnId);
    const information: MatrixElement = this.findMatrixClickedElement(rowId, columnId);
    this.dialog.open(TmatrixResultDetail, {
      data: information,
      height: '80vh',
      width: '120vh',
    });
  }

  onMouseOver(): void {
    const target: HTMLElement = document.getElementById(
      'cell' + '-' + this.clickedCell.row + '-' + this.clickedCell.column
    );
    target.style.backgroundColor = 'grey';
  }

  onMouseOut(): void {
    const target: HTMLElement = document.getElementById(
      'cell' + '-' + this.clickedCell.row + '-' + this.clickedCell.column
    );
    target.style.backgroundColor = 'white';
  }

  onNextColumnPage(): void {
    this.matrixParams.pageCol += 1;
    this.loadMatrix();
  }

  onNextRowPage(): void {
    this.matrixParams.pageRow += 1;
    this.loadMatrix();
  }

  resetClickedCell(): void {
    this.clickedCell = { row: '', column: '', relation: '', information: null };
  }

  relation(rowId: string, columnId: string): boolean {
    const element: MatrixElement = this.matrix.getElement(rowId, columnId);
    return element.destination.similarity > 0.85;
  }

  findMatrixClickedElement(rowId: string, columnId: string): MatrixElement {
    return this.matrix.getElement(rowId, columnId);
  }

  private loadMatrix(): void {

    this.matrixService.getTraceabilityMatrixResults(this.projectId).subscribe( (matrixJSON: PKMMatrixJSON) => {
      if (this.matrix) {
        this.matrix.updateWithMatrix(matrixJSON);

      } else {
        this.matrix = new Matrix(matrixJSON);
      }

      this.columns = this.matrix.getColumnIds();
      this.rows = this.matrix.getRowIds();

    });
  }
}
