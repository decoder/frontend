export interface ArtifactRefCol {
  id:	string;
  location:	string;
  name:	string;
  update:	boolean;
  tgt_path: string;
  tgt_access: string;
  tgt_text: string;
  similarity: number;
  invocationID: string;
}

export interface ArtifactRefRow {
  id:	string;
  location:	string;
  name:	string;
  changeType?:	string;
  src_path: string;
  src_access: string;
  src_text: string;
  similarity: number;
  invocationID: string;
}
