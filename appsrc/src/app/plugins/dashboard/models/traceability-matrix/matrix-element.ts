import { ArtifactRefRow, ArtifactRefCol } from './artifact-ref';

export interface MatrixElement {
  origin: ArtifactRefRow;
  destination: ArtifactRefCol;
}
