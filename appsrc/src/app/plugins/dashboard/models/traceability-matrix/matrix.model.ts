import { Page } from './page';
import { MatrixElement } from './matrix-element';

export interface MatrixJSON {
  array: MatrixElement[];
  pageRow: Page;
  pageCol: Page;
}
