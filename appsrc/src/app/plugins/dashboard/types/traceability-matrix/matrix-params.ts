export interface MatrixParams {
  projectId: string;
  pageRow: number;
  pageCol: number;
}
