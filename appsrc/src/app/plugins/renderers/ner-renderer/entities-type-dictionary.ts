export const entitiesTypesDictionary: { type: string }[] = [
  {
    type: 'Argument',
  },
  {
    type: 'AttackGoal',
  },
  {
    type: 'AttackMean',
  },
  {
    type: 'Attacker',
  },
  {
    type: 'AttributeName',
  },
  {
    type: 'Boolean',
  },
  {
    type: 'CVE',
  },
  {
    type: 'ClassName',
  },
  {
    type: 'Command',
  },
  {
    type: 'ComputerScienceConcept',
  },
  {
    type: 'Copyright',
  },
  {
    type: 'DataStructure',
  },
  {
    type: 'Date',
  },
  {
    type: 'ErrorString',
  },
  {
    type: 'FileFormat',
  },
  {
    type: 'FileName',
  },
  {
    type: 'FunctionName',
  },
  {
    type: 'Hardware',
  },
  {
    type: 'Identifier',
  },
  {
    type: 'IssueMention',
  },
  {
    type: 'IssueStatus',
  },
  {
    type: 'Legal',
  },
  {
    type: 'Library',
  },
  {
    type: 'License',
  },
  {
    type: 'Location',
  },
  {
    type: 'MemberName',
  },
  {
    type: 'Modifier',
  },
  {
    type: 'Norm',
  },
  {
    type: 'Numex',
  },
  {
    type: 'NUMBER',
  },
  {
    type: 'Unit',
  },
  {
    type: 'ORGANIZATION',
  },
  {
    type: 'OSName',
  },
  {
    type: 'Package',
  },
  {
    type: 'PackageName',
  },
  {
    type: 'ParameterName',
  },
  {
    type: 'PERSON',
  },
  {
    type: 'Predicate',
  },
  {
    type: 'ProductName',
  },
  {
    type: 'ProgrammingLanguageName',
  },
  {
    type: 'RawValue',
  },
  {
    type: 'Resource',
  },
  {
    type: 'SRL',
  },
  {
    type: 'SoftwareBundle',
  },
  {
    type: 'SoftwareName',
  },
  {
    type: 'String',
  },
  {
    type: 'TypeName',
  },
  {
    type: 'Url',
  },
  {
    type: 'UserId',
  },
  {
    type: 'UserMention',
  },
  {
    type: 'VariableName',
  },
  {
    type: 'VersionNumber',
  },
  {
    type: 'Vulnerability',
  }
];
