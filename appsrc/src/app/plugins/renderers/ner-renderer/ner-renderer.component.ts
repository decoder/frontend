import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { entitiesTypesDictionary } from './entities-type-dictionary';

interface NerEntityResult {
  type: string;
  text: string;
  pos: number;
  len: number;
}

interface NerResult {
  text: string;
  entities: NerEntityResult[];
}

@Component({
  selector: 'app-ner-renderer',
  templateUrl: './ner-renderer.component.html',
  styleUrls: ['./ner-renderer.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class NerRendererComponent implements OnInit {
  @Input() content: any;
  rawHtmlString: string[];

  entity_types: { type: string }[] = entitiesTypesDictionary;

  docData: NerResult = { text: '', entities: [] };
  entitiesDetected: string[] = [];

  ngOnInit(): void {
    this.docData.text = this.content.ner[0].text;
    this.docData.entities = this.content.ner[0].entities;
    this.rawHtmlString = this.convertEntitiesToFormatedHTML();
  }

  convertEntitiesToFormatedHTML(): string[] {
    const str: string[] = [];

    this.docData.entities.forEach((element: any) => {
      const entityType: any = this.entity_types.filter(
        (item: any) => item.type.toLowerCase() === element.type.toLowerCase()
      );

      const pre: string = this.docData.text.substring(0, element.pos);
      const post: string = this.docData.text.substring(
        element.len,
        this.docData.text.length
      );
      const phrase: string = this.docData.text.substring(
        element.pos,
        element.len
      );

      str.push(
        pre +
          `<span class="${entityType[0] !== undefined ? entityType[0].type.toLowerCase() : 'unknownentity' }" title="${
            element.type !== undefined ? element.type : 'UnknownEntity'
          }">${phrase}</span>` +
          post
      );

      if (!this.entitiesDetected.includes(element.type)) {
        this.entitiesDetected.push(element.type);
      }
    });

    return str;
  }
}
