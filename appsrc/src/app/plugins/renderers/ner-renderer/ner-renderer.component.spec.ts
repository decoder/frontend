import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NerRendererComponent } from './ner-renderer.component';

describe('NerRendererComponent', () => {
  let component: NerRendererComponent;
  let fixture: ComponentFixture<NerRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerRendererComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerRendererComponent);
    component = fixture.componentInstance;
    component.content = {
      ner: [{
        text: 'test',
        entities: []
      }]
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
