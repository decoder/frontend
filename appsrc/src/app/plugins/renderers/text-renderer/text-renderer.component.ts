import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-text-renderer',
  templateUrl: './text-renderer.component.html',
  styleUrls: ['./text-renderer.component.scss']
})
export class TextRendererComponent implements OnInit {
  @Input() content: string;
  contentUrl: SafeResourceUrl;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.contentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.content);
  }
}
