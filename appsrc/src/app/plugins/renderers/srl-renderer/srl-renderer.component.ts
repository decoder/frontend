import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

interface SRLPredicateResult {
  predicate: {
    text: string;
    pos: number;
    len: number;
  };
  roles: any;
}

interface SRLResult {
  text: string;
  predicates: SRLPredicateResult[];
}

@Component({
  selector: 'app-srl-renderer',
  templateUrl: './srl-renderer.component.html',
  styleUrls: ['./srl-renderer.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class SrlRendererComponent implements OnInit {
  @Input() content: any;
  docData: SRLResult[] = [];

  ngOnInit(): void {
    this.content.srl.forEach((item: SRLResult) => {
      const newSRLResult: SRLResult = {
        text: item.text,
        predicates: item.predicates
      };
      this.docData.push(newSRLResult);
    });
  }

  convertIntoArray(input: any): any {
    const rolesValues: any = Object.entries(input);
    const rolesEntries: string[] = Object.keys(input);
    const rolesResult: { key: string, text: string; }[] = [];
    rolesEntries.forEach((key: any, index: number) => {
      rolesResult.push({ key, text: rolesValues[index][1].text });
    });
    return rolesResult;
  }
}
