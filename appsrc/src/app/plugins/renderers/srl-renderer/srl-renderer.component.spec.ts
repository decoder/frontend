import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrlRendererComponent } from './srl-renderer.component';

describe('SrlRendererComponent', () => {
  let component: SrlRendererComponent;
  let fixture: ComponentFixture<SrlRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SrlRendererComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrlRendererComponent);
    component = fixture.componentInstance;
    component.content = {
      srl: [
        {
          text: 'test text',
          predicates: [
            {
              predicate: {
                text: 'test',
                pos: 1,
                len: 1,
              },
              roles: [],
            },
          ],
        },
      ],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
