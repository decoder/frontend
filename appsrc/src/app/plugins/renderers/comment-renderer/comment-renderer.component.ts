import { Component, OnInit, Input } from '@angular/core';
import {
  artifactNameRetriever,
  extractCommentsAnnotationsFromArtifact,
  obtainPathFromArtifact,
} from 'src/app/utils/utils';
import { CommentItem, PKMComment } from 'src/app/core/models/comment';
import { ArtifactsRetrieverService } from '../../project/services/artifacts-retriever/artifacts-retriever.service';
import { PKMArtifact } from 'src/app/core/models/artifact';

@Component({
  selector: 'app-comment-renderer',
  templateUrl: './comment-renderer.component.html',
  styleUrls: ['./comment-renderer.component.scss'],
})
export class CommentRendererComponent implements OnInit {
  @Input() content: PKMComment;
  @Input() projectName: string;
  formatedComments: CommentItem[];
  shorttenFileName: string;
  toolTipText: string;
  editorOptions: any;
  loading: boolean;
  pkmSourceArtifact: PKMArtifact;

  constructor(private artifactRetrieverService: ArtifactsRetrieverService) {
    this.toolTipText = '';
    this.shorttenFileName = '';
    this.formatedComments = [];
    this.loading = true;
    this.editorOptions = {
      theme: 'vs-dark',
      wordWrap: 'on',
    };
  }

  ngOnInit(): void {
    if (this.content) {
      this.toolTipText = `${obtainPathFromArtifact(this.content.sourceFile)}`;
      this.shorttenFileName = `../${artifactNameRetriever(
        this.content.sourceFile,
        false
      )}`;
      this.artifactRetrieverService
        .getPKMFileArtifactFromProjectByID(
          this.projectName,
          this.content.sourceFile
        )
        .subscribe((artifact: PKMArtifact) => {
          this.pkmSourceArtifact = artifact;
          this.fillComments(this.content.comments);
          this.loading = false;
        });
    }
  }

  fillComments(comments: CommentItem[]): void {
    comments.forEach((item: CommentItem) => {
      this.formatedComments.push({
        loc: item.loc
          ? item.loc
          : {
              pos_end: { pos_cnum: 0, pos_lnum: 0, pos_path: '' },
              pos_start: { pos_cnum: 0, pos_lnum: 0, pos_path: '' },
            },
        comments: extractCommentsAnnotationsFromArtifact(
          this.pkmSourceArtifact.content,
          item.loc,
          'Comment'
        ),
        global_kind: item.global_kind ? item.global_kind : '',
      });
    });
  }
}
