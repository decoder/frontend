import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CommentRendererComponent } from './comment-renderer.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

describe('CommentRendererComponent', () => {
  let component: CommentRendererComponent;
  let fixture: ComponentFixture<CommentRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentRendererComponent],
      imports: [HttpClientTestingModule],
      providers: [AppConfiguration]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
