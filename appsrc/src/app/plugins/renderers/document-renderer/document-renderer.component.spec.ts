import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { AppConfiguration } from 'src/config/app-configuration';

import { DocumentRendererComponent } from './document-renderer.component';

describe('DocumentRendererComponent', () => {
  let component: DocumentRendererComponent;
  let fixture: ComponentFixture<DocumentRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentRendererComponent ],
      providers: [ AppConfiguration ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentRendererComponent);
    component = fixture.componentInstance;
    component.artifact = pkmArtifactsMock[2];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
