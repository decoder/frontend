import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { allowedArtifactExtensions } from 'src/app/shared/constants/allowed-extensions';
import { AppConfiguration } from 'src/config/app-configuration';
@Component({
  selector: 'app-document-renderer',
  templateUrl: './document-renderer.component.html',
  styleUrls: ['./document-renderer.component.scss'], encapsulation: ViewEncapsulation.ShadowDom
})
export class DocumentRendererComponent implements OnInit {
  @Input() content: SafeUrl;
  @Input() artifact: PKMArtifact;
  @Input()  extension: string;
  @Input()  originalUrl: string;
  @Input()  projectName: string;

  extensionIsImage: boolean = false;
  extensionIsFormattedText: boolean = false;
  extensionIsBinaryDocument: boolean = false;

  safeDocumentUrl: SafeUrl;
  viewerOptions: object = {
    navbar: false,
    toolbar: {
      zoomIn: 4,
      zoomOut: 4,
      rotateLeft: 4,
      rotateRight: 4,
      flipHorizontal: 4,
      flipVertical: 4,
    }
  };

  constructor(private appConfig: AppConfiguration) { }

  ngOnInit(): void {
    this.checkExtensionType();
    this.getDocumentUrl();
  }

  getDocumentUrl(): void {
    const token: string = localStorage.getItem('access_token');
    const projectBaseURL: string = this.appConfig.serverURL;
    const url: string = `${projectBaseURL}/io/${this.projectName}/${encodeURIComponent(this.artifact.name)}`;
    this.safeDocumentUrl = (`${url}?key=${token}`) as SafeUrl;
  }

  checkExtensionType(): void {
    if (allowedArtifactExtensions
      .GET_ALL_BINARY_DOCUMENT_EXTENSIONS()
      .includes(this.extension)
    ) {
      this.extensionIsBinaryDocument = true;
    } else if (
      allowedArtifactExtensions
      .GET_ALL_TEXT_DOCUMENT_EXTENSIONS()
      .includes(this.extension)
    ) {
      this.extensionIsFormattedText = true;
    } else if (
      allowedArtifactExtensions
      .GET_ALL_BINARY_IMAGE_EXTENSIONS()
      .includes(this.extension)
    ) {
      this.extensionIsImage = true;
    }
  }
}
