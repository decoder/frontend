import { Component, Input, AfterViewInit } from '@angular/core';
import * as mermaid from 'mermaid';
import * as d3 from 'd3';

@Component({
  selector: 'app-uml-renderer',
  templateUrl: './uml-renderer.component.html',
  styleUrls: ['./uml-renderer.component.scss'],
})
export class UmlRendererComponent implements AfterViewInit {
  @Input() content: string;
  @Input() lang: string;
  date: string = this.makeid();

  ngAfterViewInit(): void {
    const config: object = {
      theme: 'forest',
      mermaid: {
        startOnLoad: false,
      },
      flowchart: {
        useMaxWidth: true,
        htmlLabels: true,
      },
    };
    mermaid.mermaidAPI.initialize(config);
    const element: any = document.getElementById(this.date);
    const graphDefinition: any = this.content;
    const mermaidId: string = this.makeid();
    mermaid.mermaidAPI.render(
      mermaidId,
      graphDefinition,
      (svgCode: any, bindFunctions: any) => {
        element.innerHTML = svgCode;
        bindFunctions(element);
      }
    );
    const svg: any = d3.select(`#${mermaidId}`);
    svg.call(
      d3.zoom().on('zoom', (event: any) => {
        svg.attr('transform', event.transform);
      })
    );
  }

  makeid(): string {
    const crypto: Crypto = window.crypto;
    const array: Uint32Array = new Uint32Array(1);
    crypto.getRandomValues(array);
    return `mermaid-${array.toString()}`;
  }
}
