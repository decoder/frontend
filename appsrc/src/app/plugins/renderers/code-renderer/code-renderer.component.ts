import { Component, OnInit, Input, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MonacoLanguages } from './monaco-languages/monaco-languages';
import { getFileExtension } from 'src/app/utils/utils';
import { DiffEditorModel, EditorComponent } from 'ngx-monaco-editor';
import { ArtifactsRetrieverService } from '../../project/services/artifacts-retriever/artifacts-retriever.service';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-code-renderer',
  templateUrl: './code-renderer.component.html',
  styleUrls: ['./code-renderer.component.scss'],
})
export class CodeRendererComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() content: string;
  @Input() artifact: PKMArtifact;
  @Input() projectName: string;
  @Input() lang: string;
  editorOptions: any;
  code: string;
  modifiedContent: string;
  $artifactRetrieverSubscription: Subscription;

  @ViewChild(EditorComponent) editorComponent: EditorComponent;

  originalModel: DiffEditorModel = {
    code: 'heLLo world!',
    language: 'text/plain',
  };

  modifiedModel: DiffEditorModel = {
    code: 'hello orlando!',
    language: 'text/plain',
  };

  constructor(private artifactsRetriever: ArtifactsRetrieverService) {
    this.content = '';
    this.editorOptions = {
      theme: 'vs-dark',
      language: 'java',
      wordWrap: 'on',
    };
  }

  ngOnInit(): void {
    if (this.artifact.type === 'Diagram') {
      this.$artifactRetrieverSubscription = this.artifactsRetriever
        .getPKMDiagramFilesFromProjectByName(
          this.projectName,
          this.artifact.name
        ).subscribe((result: PKMArtifact) => {
          this.setContentAndEditorConfig(result);
        });
    } else {
      this.$artifactRetrieverSubscription = this.artifactsRetriever
        .getPKMCodeFileFromProjectByName(this.projectName, this.artifact.name)
        .subscribe((result: PKMArtifact) => {
          this.setContentAndEditorConfig(result);
        });
    }
  }

  ngAfterViewInit(): void {
    this.editorComponent.onTouched = () => {
      return;
    };
    this.editorComponent.propagateChange = (event: string) => {
      this.modifiedContent = event;
    };
  }

  ngOnDestroy(): void {
    if (this.$artifactRetrieverSubscription) {
      this.$artifactRetrieverSubscription.unsubscribe();
    }
  }

  setContentAndEditorConfig(result: PKMArtifact): void {
    this.content = result.content;
    const language: string = this.getLangFromExtension();
    this.editorOptions = {
      ...this.editorOptions,
      language,
    };
    if (this.content) {
      this.getCodeData();
      this.getCodeDataModified();
    }
  }

  getLangFromExtension(): string {
    const monaco: MonacoLanguages = new MonacoLanguages();
    const fileExtension: string = getFileExtension(this.artifact.name);
    return monaco.language(fileExtension);
  }

  private getCodeData(): void {
    this.code = this.content;
    this.originalModel.code = this.content;
  }

  private getCodeDataModified(): void {
    this.modifiedModel.code = this.content;
  }
}
