export const MONACO_LANGUAGES: { [extension: string]: string } = {
  java: 'java',
  c: 'c',
  cpp: 'c',
  h: 'c',
  html: 'html',
  json: 'json',
  js: 'javascript',
  ts: 'javascript',
};

export class MonacoLanguages {
  constructor(private defaultLanguage: string = 'java') {}

  language(fileExtension: string): string {
    let language: string = MONACO_LANGUAGES[fileExtension];
    if (!language) {
      language = this.defaultLanguage;
    }

    return language;
  }
}
