import { MonacoLanguages } from './monaco-languages';

describe('MonacoExtension', () => {
  let monaco: MonacoLanguages;

  beforeEach(() => {
    monaco = new MonacoLanguages();
  });

  it('should return [javascript] for js files', () => {
    expect(monaco.language('js')).toEqual('javascript');
  });

  it('should return [java] as default', () => {
    const unexpectedExtension: string = 'abc';
    const extension: string = monaco.language(unexpectedExtension);
    expect(extension).toEqual('java');
  });

  it('should return [abc] as default', () => {
    const defaultLanguage: string = 'abc';
    const unexpectedExtension: string = '123';
    const defaultMonaco: MonacoLanguages = new MonacoLanguages(defaultLanguage);
    const extension: string = defaultMonaco.language(unexpectedExtension);
    expect(extension).toEqual(defaultLanguage);
  });
});
