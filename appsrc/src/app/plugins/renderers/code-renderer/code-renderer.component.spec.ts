import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CodeRendererComponent } from './code-renderer.component';
import { SafeValue, DomSanitizer } from '@angular/platform-browser';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';

class SanitizerStub {
  sanitize(_: any, value: string | SafeValue): string {
    return value.toString();
  }
}

describe('CodeRendererComponent', () => {
  let component: CodeRendererComponent;
  let fixture: ComponentFixture<CodeRendererComponent>;
  let artifact: PKMArtifact;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeRendererComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: DomSanitizer,
          useClass: SanitizerStub,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    setTimeout(() => {
      fixture = TestBed.createComponent(CodeRendererComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }, 1000);
  });

  it('should create', () => {
    setTimeout(() => {
      expect(component).toBeTruthy();
    }, 2000);
  });

  it('should [getLangFromExtension]', () => {
    artifact = pkmArtifactsMock[0];
    const lang: string = artifact.rel_path.split('.')[1];
    expect(lang).toEqual('java');
  });
});
