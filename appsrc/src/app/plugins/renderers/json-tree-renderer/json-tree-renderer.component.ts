import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PKMArtifact, PKMLogArtifact } from 'src/app/core/models/artifact';
import { PKMASFMDocArtifact } from '../../project/models/asfm-document-artifact.model';
import { ArtifactsRetrieverService } from '../../project/services/artifacts-retriever/artifacts-retriever.service';

@Component({
  selector: 'app-json-tree-renderer',
  templateUrl: './json-tree-renderer.component.html',
  styleUrls: ['./json-tree-renderer.component.scss'],
})
export class JsonTreeRendererComponent implements OnInit, OnDestroy {
  @Input() jsonObject: any;
  @Input() artifact: PKMArtifact;
  @Input() projectName: string;
  $artifactRetrieverSubscription: Subscription;

  jsonContent: object = {};

  constructor(private artifactsRetriever: ArtifactsRetrieverService) {}

  ngOnInit(): void {
    if (this.artifact === undefined) {
      this.jsonContent = this.jsonObject;
    } else {
      if (this.artifact.type === 'Log') {
        this.$artifactRetrieverSubscription = this.artifactsRetriever
          .getPKMLogFromAProjectById(this.projectName, this.artifact.id)
          .subscribe((result: PKMLogArtifact) => {
            this.jsonContent = result;
          });
      } else if (this.artifact.type === 'ASFM Docs') {
        this.$artifactRetrieverSubscription = this.artifactsRetriever
          .getASFMDocFromProjectByID(this.projectName, this.artifact.id)
          .subscribe((result: PKMASFMDocArtifact) => {
            this.jsonContent = result;
          });
      } else if (this.artifact.type === 'CVE') {
        this.jsonContent = this.artifact.content;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.$artifactRetrieverSubscription) {
      this.$artifactRetrieverSubscription.unsubscribe();
    }
  }
}
