import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JsonTreeRendererComponent } from './json-tree-renderer.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';
import { ArtifactsRetrieverService } from '../../project/services/artifacts-retriever/artifacts-retriever.service';
import { Observable, of } from 'rxjs';
import { pkmLogArtifacts } from 'src/app/mocks/core/fixtures/pkmLogArtifacts.mock';
import { PKMLogArtifact } from 'src/app/core/models/artifact';

class ArtifactsRetrieverServiceStub {
  getPKMLogFromAProjectById(projectName: string, logId: string): Observable<PKMLogArtifact> {
    return of(pkmLogArtifacts[0]);
  }
}

describe('JsonTreeRendererComponent', () => {
  let component: JsonTreeRendererComponent;
  let fixture: ComponentFixture<JsonTreeRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonTreeRendererComponent ],
      imports: [HttpClientTestingModule],
      providers: [
        AppConfiguration,
        {
          provide: ArtifactsRetrieverService,
          useClass: ArtifactsRetrieverServiceStub
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonTreeRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
