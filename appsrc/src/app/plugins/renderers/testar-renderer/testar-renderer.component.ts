import { Component, OnInit } from '@angular/core';
import sequences from '../../../../assets/mock-testar/sequences-result.json';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-testar-renderer',
  templateUrl: './testar-renderer.component.html',
  styleUrls: ['./testar-renderer.component.scss'],
})
export class TestarRendererComponent implements OnInit {
  htmlSequences: string[];
  htmlSequencesVerdicts: string[];
  htmlUrl: SafeUrl;
  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.getJsonDataAndInitIframe();
  }

  getJsonDataAndInitIframe(): void {
    this.htmlSequences = sequences.htmlsResult;
    this.htmlSequencesVerdicts = sequences.sequencesVerdicts;
    this.setHtml(this.htmlSequences[0]);
  }

  setHtml(name: string): void {
    const htmlPath: string = './assets//mock-testar/HTMLreports/' + name;
    this.htmlUrl = this.sanitizer.bypassSecurityTrustResourceUrl(htmlPath);
  }
}
