import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestarRendererComponent } from './testar-renderer.component';

describe('TestarRendererComponent', () => {
  let component: TestarRendererComponent;
  let fixture: ComponentFixture<TestarRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestarRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestarRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
