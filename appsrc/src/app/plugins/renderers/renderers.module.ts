import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextRendererComponent } from './text-renderer/text-renderer.component';
import { CodeRendererComponent } from './code-renderer/code-renderer.component';
import { DefaultRendererComponent } from './default-renderer/default-renderer.component';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { JsonTreeRendererComponent } from './json-tree-renderer/json-tree-renderer.component';
import { TestarRendererComponent } from './testar-renderer/testar-renderer.component';
import { MaterialModule } from 'src/app/material.module';
import { UmlRendererComponent } from './uml-renderer/uml-renderer.component';
import { CommentRendererComponent } from './comment-renderer/comment-renderer.component';
import { AnnotationRendererComponent } from './annotation-renderer/annotation-renderer.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { DocumentRendererComponent } from './document-renderer/document-renderer.component';
import { NerRendererComponent } from './ner-renderer/ner-renderer.component';
import { MarkdownModule } from 'ngx-markdown';
import { NgxViewerModule } from 'ngx-viewer';
import { SrlRendererComponent } from './srl-renderer/srl-renderer.component';

@NgModule({
  providers: [HttpClientModule],
  declarations: [
    CodeRendererComponent,
    DefaultRendererComponent,
    TextRendererComponent,
    JsonTreeRendererComponent,
    TestarRendererComponent,
    UmlRendererComponent,
    CommentRendererComponent,
    AnnotationRendererComponent,
    DocumentRendererComponent,
    NerRendererComponent,
    SrlRendererComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MonacoEditorModule,
    NgxJsonViewerModule,
    MaterialModule,
    NgxDocViewerModule,
    NgxViewerModule,
    MarkdownModule.forRoot(),
  ],
  exports: [TestarRendererComponent, JsonTreeRendererComponent],
})
export class RenderersModule {}
