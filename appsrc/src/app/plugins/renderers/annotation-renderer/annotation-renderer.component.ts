import { Component, OnInit, Input } from '@angular/core';
import {
  obtainPathFromArtifact,
  artifactNameRetriever,
  extractCommentsAnnotationsFromArtifact,
} from 'src/app/utils/utils';
import { PKMAnnotation, Annotation, CAnnotation } from 'src/app/core/models/annotation';
import { ArtifactsRetrieverService } from '../../project/services/artifacts-retriever/artifacts-retriever.service';
import { PKMArtifact } from 'src/app/core/models/artifact';

@Component({
  selector: 'app-annotation-renderer',
  templateUrl: './annotation-renderer.component.html',
  styleUrls: ['./annotation-renderer.component.scss'],
})
export class AnnotationRendererComponent implements OnInit {
  @Input() content: PKMAnnotation;
  @Input() projectName: string;
  annotations: Annotation[];
  shorttenFileName: string;
  toolTipText: string;
  loading: boolean;
  splitAnnotations: string[];
  posStart: string;
  posEnd: string;
  auxAnnotations: CAnnotation[];
  pkmSourceArtifact: PKMArtifact;
  cAnnotations: CAnnotation[];

  constructor(private artifactRetrieverService: ArtifactsRetrieverService) {
    this.toolTipText = '';
    this.shorttenFileName = '';
    this.annotations = [];
    this.cAnnotations = [];
    this.loading = true;
    this.splitAnnotations = [];
  }

  ngOnInit(): void {
    if (this.content) {
      this.toolTipText = `${obtainPathFromArtifact(this.content.sourceFile)}`;
      this.shorttenFileName = `../${artifactNameRetriever(
        this.content.sourceFile,
        false
      )}`;
      if (this.content.fileEncoding === undefined && this.content.fileFormat === undefined) {// Use case management .c comments
        this
          .artifactRetrieverService
          .getPKMFileArtifactFromProjectByID(this.projectName, this.content.sourceFile)
          .subscribe((artifact: PKMArtifact) => {
            this.pkmSourceArtifact = artifact;
            this.fillCAnnotations();
            this.loading = false;
          });
    } else { // Use case management for java comments
      this.fillAnnotations(this.content.annotations);
      this.loading = false;
    }
    }
  }
  returnAllAnnotations(annotations: string[]): string[] {
    return [annotations.join('\n')];
  }

  splitAnnotation(annotation: any): string[] {
    annotation.annotationInEnvironmentSplited = [];

    if (`${annotation.loc.pos_start.pos_lnum}`.length < 2) {
      this.posStart = '0' + annotation.loc.pos_start.pos_lnum;
    } else {
      this.posStart = annotation.loc.pos_start.pos_lnum.toString();
    }

    if (`${annotation.loc.pos_end.pos_lnum}`.length < 2) {
      this.posEnd = '0' + (annotation.loc.pos_end.pos_lnum + 1);
    } else {
      this.posEnd = (annotation.loc.pos_end.pos_lnum + 1).toString();
    }
    const ANNOTATION: string[] = annotation.commentInEnvironment
      .split(`${this.posStart}`)[1]
      .split(`${this.posEnd}`);

    annotation.annotationInEnvironmentSplited.push(
      annotation.commentInEnvironment.split(`${this.posStart}`)[0]
    );
    annotation.annotationInEnvironmentSplited.push(
      `${this.posStart}` + ANNOTATION[0]
    );
    annotation.annotationInEnvironmentSplited.push(
      `${this.posEnd}` + ANNOTATION[1]
    );

    return annotation.annotationInEnvironmentSplited;
  }

  fillAnnotations(annotations: any): void {
    annotations.forEach((item: Annotation) => {
      this.annotations.push({
        loc: item.loc,
        annotations: this.returnAllAnnotations(item.annotations),
        commentInEnvironment: `${item.commentInEnvironment.trim()}`,
        annotationInEnvironmentSplited: this.splitAnnotation(item),
      });
    });
  }

  fillCAnnotations(): void {
    this.content.annotations.forEach((item: Annotation) => {
      this.cAnnotations.push({
        loc: item.loc,
        CAnnotations: extractCommentsAnnotationsFromArtifact(this.pkmSourceArtifact.content, item.loc, 'Annotation'),
      });
    });
  }
}
