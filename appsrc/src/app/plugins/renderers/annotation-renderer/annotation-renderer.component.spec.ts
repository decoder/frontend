import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppConfiguration } from 'src/config/app-configuration';

import { AnnotationRendererComponent } from './annotation-renderer.component';

describe('AnnotationRendererComponent', () => {
  let component: AnnotationRendererComponent;
  let fixture: ComponentFixture<AnnotationRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnnotationRendererComponent],
      imports: [HttpClientTestingModule],
      providers: [AppConfiguration]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotationRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('returnAllAnnotations should return an array with a single string', () => {
    const result: string[] = component.returnAllAnnotations([
      '@String * > mock',
      '@Param(***)',
    ]);
    const expectedResult: string = ['@String * > mock', '@Param(***)'].join(
      '\n'
    );
    expect(result).toEqual([expectedResult]);
  });

  it('splitAnnotation should return an array with a single string', () => {
    const mockAnnotations: any = {
      loc: {
        pos_start: {
          pos_cnum: 13949,
          pos_lnum: 23,
        },
        pos_end: {
          pos_cnum: 14013,
          pos_lnum: 25,
        },
      },
      annotations: [
        'initialize, 023: validate invitedGuestEntity here if necessary *',
        'annotation',
        'another 025',
      ],
      commentInEnvironment:
        'initizalize 023: without example invitedGuestEntity here if necessary 025: after comment example',
    };
    spyOn(component, 'splitAnnotation');
    component.splitAnnotation(mockAnnotations);
    expect(component.splitAnnotation).toHaveBeenCalled();
    component.splitAnnotations = mockAnnotations;
    expect(component.splitAnnotations).toEqual(mockAnnotations);
  });

  it('fillAnnotations should filled up the array with the annotations', () => {
    const mockAnnotations: any = [
      {
        loc: {
          pos_start: {
            pos_cnum: 13949,
            pos_lnum: 378,
          },
          pos_end: {
            pos_cnum: 14013,
            pos_lnum: 379,
          },
        },
        annotations: ['@String * > mock'],
      },
    ];
    spyOn(component, 'fillAnnotations');
    component.fillAnnotations(mockAnnotations);
    expect(component.fillAnnotations).toHaveBeenCalled();
    component.annotations = mockAnnotations;
    expect(component.annotations).toEqual(mockAnnotations);
  });
});
