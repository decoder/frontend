import { interfacePlugins } from './plugins.config';
import { createRoutes } from './routes-creator';

describe('Create routes', () => {
  it('should create one route for each plugin in plugins.config.ts', () => {
    expect(createRoutes().length).toEqual(interfacePlugins.length);
  });
});
