import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectContainerComponent } from './containers/project-container/project-container.component';
import { Routes, RouterModule } from '@angular/router';
import { ArtifactsRetrieverService } from './services/artifacts-retriever/artifacts-retriever.service';
import { ArtifactTreeComponent } from './components/artifact-tree/artifact-tree.component';
import { RelatedArtifactsComponent } from './components/related-artifacts/related-artifacts.component';
import { ArtifactVisualizerComponent } from './components/artifact-visualizer/artifact-visualizer.component';
import { ArtifactNavigationComponent } from './containers/artifact-navigation/artifact-navigation.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TreeMapper } from './services/tree-mapper/tree-mapper.service';
import { QuickSearchBarComponent } from './components/quick-search-bar/quick-search-bar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RendererModule } from '../../renderer/renderer.module';
import { TabContentComponent } from './components/artifact-visualizer/tab-content/tab-content.component';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MethodologySupportComponent } from './components/methodology-support/methodology-support.component';
import { MethodologyGuidanceService } from './services/methodology-guidance/methodology-guidance.service';
import { MethodologyGuidanceComponent } from './containers/methodology-guidance/methodology-guidance.component';
import { ArtifactCommentsSelectorComponent } from './components/artifact-comments-selector/artifact-comments-selector.component';
import { ToolManagerComponent } from './components/tool-manager/tool-manager.component';
import { FramacComponent } from './components/tool-manager/tools/framac/framac.component';
import { JavaparserComponent } from './components/tool-manager/tools/javaparser/javaparser.component';
import { OpenjmlComponent } from './components/tool-manager/tools/openjml/openjml.component';
import { TestarComponent } from './components/tool-manager/tools/testar/testar.component';
import { JmlgenComponent } from './components/tool-manager/tools/jmlgen/jmlgen.component';
import { CodesumarizationComponent } from './components/tool-manager/tools/codesumarization/codesumarization.component';
import { NerComponent } from './components/tool-manager/tools/ner/ner.component';
import { SrlComponent } from './components/tool-manager/tools/srl/srl.component';
import { SemanticparserComponent } from './components/tool-manager/tools/semanticparser/semanticparser.component';
import { TracerecoveryComponent } from './components/tool-manager/tools/tracerecovery/tracerecovery.component';
import { RequestResultRendererComponent } from './components/tool-manager/common/request-result-renderer/request-result-renderer.component';
import { PeResultsViewerComponent } from './components/pe-results-viewer/pe-results-viewer.component';
import { FramacLangComponent } from './components/tool-manager/tools/framaclang/framaclang.component';
import { ArtifactSelectorComponent } from './components/tool-manager/common/artifact-selector/artifact-selector.component';
import { ConfigurationSelectorComponent } from './components/tool-manager/common/configuration-selector/configuration-selector.component';
import { VariablemisuseComponent } from './components/tool-manager/tools/variablemisuse/variablemisuse.component';
import { ClassModelTransformerComponent } from './components/tool-manager/tools/classmodeltransformer/class-model-transformer.component';
import { CodeToASFMComponent } from './components/tool-manager/tools/code-to-asfm/code-to-asfm.component';
import { DocToASFMComponent } from './components/tool-manager/tools/doc-to-asfm/doc-to-asfm.component';
import { AsfmToDocComponent } from './components/tool-manager/tools/asfm-to-doc/asfm-to-doc.component';
import { DefaultToolBaseComponent } from './components/tool-manager/tools/default-tool-base/default-tool-base-component';
import { ExcavatorComponent } from './components/tool-manager/tools/excavator/excavator.component';

export const routes: Routes = [
  {
    path: '',
    component: ProjectContainerComponent,
  },
];

@NgModule({
  providers: [
    ArtifactsRetrieverService,
    TreeMapper,
    ProjectsService,
    MethodologyGuidanceService,
  ],
  declarations: [
    ArtifactTreeComponent,
    RelatedArtifactsComponent,
    ArtifactVisualizerComponent,
    ArtifactNavigationComponent,
    ProjectContainerComponent,
    QuickSearchBarComponent,
    TabContentComponent,
    MethodologySupportComponent,
    MethodologyGuidanceComponent,
    ArtifactCommentsSelectorComponent,
    ToolManagerComponent,
    FramacComponent,
    JavaparserComponent,
    OpenjmlComponent,
    TestarComponent,
    JmlgenComponent,
    CodesumarizationComponent,
    NerComponent,
    SrlComponent,
    SemanticparserComponent,
    FramacLangComponent,
    TracerecoveryComponent,
    RequestResultRendererComponent,
    PeResultsViewerComponent,
    ArtifactSelectorComponent,
    ConfigurationSelectorComponent,
    VariablemisuseComponent,
    ClassModelTransformerComponent,
    CodeToASFMComponent,
    DocToASFMComponent,
    AsfmToDocComponent,
    DefaultToolBaseComponent,
    ExcavatorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    RendererModule,
    RouterModule.forChild(routes),
  ],
})
export class ProjectModule {}
