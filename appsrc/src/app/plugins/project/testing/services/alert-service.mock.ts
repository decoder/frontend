import { Observable, of } from 'rxjs';

export function getMockAlerts(): Observable<{}> {
    const mockAlerts: {} = {
        getAlertsByArtifactId: jasmine
            .createSpy('getAlertsByArtifactId')
            .and.returnValue([])
    };
    return of(mockAlerts);
}
