import { asyncScheduler, scheduled, of } from 'rxjs';
import { ArtifactsRetrieverService } from '../../services/artifacts-retriever/artifacts-retriever.service';

export function getMockRetriever(): Partial<ArtifactsRetrieverService> {
  return {
    getRelatedPKMArtifacts: jasmine
      .createSpy('getRelatedPKMArtifacts')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getAllPKMFilesArtifactsFromProject: jasmine
      .createSpy('getAllPKMFilesArtifactsFromProject')
      .and.returnValue(scheduled(of({}), asyncScheduler)),
    getPKMAnnotationsFromProject: jasmine
      .createSpy('getPKMAnnotationsFromProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMOpenJMLAnnotationsFromProject: jasmine
      .createSpy('getPKMOpenJMLAnnotationsFromProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMOpenJMLAnnotationsProjectByID: jasmine
    .createSpy('getPKMOpenJMLAnnotationsProjectByID')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMCommentsFromAProject: jasmine
    .createSpy('getPKMCommentsFromAProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMCommentsFromAProjectByID: jasmine
      .createSpy('getPKMCommentsFromAProjectByID')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMDocsFromAProject: jasmine
      .createSpy('getPKMDocsFromAProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMDocsFromAProjectByID: jasmine
      .createSpy('getPKMDocsFromAProjectByID')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getAllPKMFilesArtifactsFromProjectSimplified: jasmine
    .createSpy('getAllPKMFilesArtifactsFromProjectSimplified')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMLogsFromAProject : jasmine
      .createSpy('getPKMLogsFromAProject ')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMDiagramsFromAProject : jasmine
      .createSpy('getPKMDiagramsFromAProject ')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMTESTARStateModelFromAProject : jasmine
      .createSpy('getPKMTESTARStateModelFromAProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getASFMDocsFromProject : jasmine
      .createSpy('getASFMDocsFromProject')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    putPKMRawSourceCode : jasmine
      .createSpy('putPKMRawSourceCode')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getPKMAnnotationsResults: jasmine
      .createSpy('getPKMAnnotationsResults')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
  };
}
