export function getMockMapper(): {} {
  return {
    createTreeFromArtifact: jasmine
      .createSpy('createTreeFromArtifact')
      .and.returnValue([])
  };
}
