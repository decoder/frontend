import { Observable, of } from 'rxjs';
import { ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';

export class ProcessEngineServiceStub {
  processEngineToolInvocations(): Observable<ToolInvocationResponse> {
    return of(
      {
        invocationID: 'testIdGenerated',
        success: true,
      }
    );
  }
}
