import { asyncScheduler, scheduled, of } from 'rxjs';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';

export function getProjectMockRetriever(): Partial<ProjectsService> {
  return {
    getProjectsByUser: jasmine
      .createSpy('getProjectByUser')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
    getUserId: jasmine
      .createSpy('getUserId')
      .and.returnValue(scheduled(of([]), asyncScheduler)),

    getUserInfo: jasmine
      .createSpy('getUserInfo')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
  };
}
