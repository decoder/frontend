import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { TreeMapper } from '../../services/tree-mapper/tree-mapper.service';
import { PKMArtifact } from '../../../../core/models/artifact';
import { ArtifactNode } from '../../models/artifact-node.model';
import { ProjectVersionInfo } from 'src/app/core/models/project-version-info.model';

@Component({
  selector: 'app-artifact-navigation',
  templateUrl: './artifact-navigation.component.html',
  styleUrls: ['./artifact-navigation.component.scss'],
  host: {
    fxFill: '',
    fxLayout: 'row',
  },
})
export class ArtifactNavigationComponent implements OnChanges {
  @Input() quicksearchFiltered: boolean;
  @Input() projectInfo: ProjectVersionInfo;
  @Input() pkmArtifacts: PKMArtifact[];
  @Output() update: EventEmitter<any> = new EventEmitter();
  artifactNodesPKM: ArtifactNode[];
  selectedArtifact: PKMArtifact;
  selectedArtifactRelatedArtifacts: PKMArtifact[];
  displayMethodGuidance: boolean;
  loadingCollapsable: boolean;

  constructor(private treeMapper: TreeMapper) {
    this.selectedArtifactRelatedArtifacts = [];
    this.quicksearchFiltered = false;
    this.projectInfo = {
      name: '',
      versionName: '',
      versionId: '',
      projectId: '',
    };
    this.displayMethodGuidance = true;
    this.loadingCollapsable = true;
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.pkmArtifacts.isFirstChange()) {
      this.updateNodesPKM();
    }
  }

  onArtifactSelection(selectedArtifactNodeInformation: any): any {
    this.onHiddenComponent(false);
    this.determinateRetrieveBaseArtifactType(selectedArtifactNodeInformation);
    this.selectedArtifactRelatedArtifacts = this.pkmArtifacts.filter(
      (element: PKMArtifact) => {
        if (
          element === undefined ||
          element.id === undefined ||
          selectedArtifactNodeInformation === undefined ||
          selectedArtifactNodeInformation.id === undefined
        ) {
          return false;
        } else {
          return (
            element.id.split('.')[0] ===
            selectedArtifactNodeInformation.id.split('.')[0]
          );
        }
      }
    );
  }

  onKeyUpEvent(quickSearchInput: string): any { return; }

  onHiddenComponent(event: any): void {
    this.displayMethodGuidance = event;
  }

  refreshItemsFromPKM(): void {
    this.update.emit();
  }

  private updateNodesPKM(): void {
    this.artifactNodesPKM = this.treeMapper.createTreeFromPKMArtifact(
      this.pkmArtifacts
    );
  }

  private findArtifactByTypeAndId(type: string, id: string): PKMArtifact {
    if (!type || !id) {
      return new PKMArtifact();
    }
    return this.pkmArtifacts.find(
      (item: PKMArtifact) => item.type === type && item.id === id
    );
  }

  private determinateRetrieveBaseArtifactType(
    artifactNodeInformation: any
  ): void {
    this.selectedArtifact = this.findArtifactByTypeAndId(
      artifactNodeInformation.type,
      artifactNodeInformation.id
    );
  }
}
