import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArtifactNavigationComponent } from './artifact-navigation.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TreeMapper } from '../../services/tree-mapper/tree-mapper.service';
import { ArtifactsRetrieverService } from '../../services/artifacts-retriever/artifacts-retriever.service';
import { getMockMapper, getMockRetriever } from '../../testing/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ArtifactNavigationComponent', () => {
  let component: ArtifactNavigationComponent;
  let fixture: ComponentFixture<ArtifactNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ArtifactNavigationComponent],
      providers: [
        {
          provide: TreeMapper,
          useValue: getMockMapper(),
        },
        {
          provide: ArtifactsRetrieverService,
          useValue: getMockRetriever(),
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactNavigationComponent);
    component = fixture.componentInstance;
    component.pkmArtifacts = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
