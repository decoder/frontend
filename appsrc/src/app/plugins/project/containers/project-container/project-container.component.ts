import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { PKMNerSrlResult, PKMAnnotation, PKMCAnnotation } from 'src/app/core/models/annotation';
import { PKMArtifact,
  PKMCVEArtifact,
  PKMDocumentArtifact,
  PKMExecutableArtifact,
  PKMLogArtifact,
  PKMTESTARArtifact,
  PKMUMLClassDiagramArtifact
} from 'src/app/core/models/artifact';
import { PKMComment } from 'src/app/core/models/comment';
import { Invocation } from 'src/app/core/models/invocation';
import { ProjectVersionInfo } from 'src/app/core/models/project-version-info.model';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MethodologySupportComponent, MethodologyStep } from '../../components/methodology-support/methodology-support.component';
import { PKMASFMDocArtifact } from '../../models/asfm-document-artifact.model';
import { ArtifactsRetrieverService } from '../../services/artifacts-retriever/artifacts-retriever.service';

@Component({
  selector: 'app-project-container',
  templateUrl: './project-container.component.html',
  styleUrls: ['./project-container.component.scss'],
})
export class ProjectContainerComponent implements OnInit {
  projectInfo: ProjectVersionInfo;
  invocations: Invocation[];
  endswithDefault: boolean = false;
  endsWithResults: boolean = false;
  endsWithTools: boolean = false;

  pkmArtifacts: PKMArtifact[];
  displayMethodGuidance: boolean;
  loadingCollapsable: boolean;

  @ViewChild('checkboxTree')
  checkboxTree: MethodologySupportComponent;
  modal: any = {
    title: '',
    step: null,
    parent: null,
    child: null,
  };

  constructor(
    private route: ActivatedRoute,
    private peEngineService: ProcessEngineService,
    private artifactsRetriever: ArtifactsRetrieverService,
    private router: Router
    ) {
    this.projectInfo = {
      name: 'Unknown',
      versionId: 'Unknown',
      versionName: 'Unknown',
      projectId: 'Unknown',
    };
  }

  ngOnInit(): void {
    this.getProject();
    this.loadInvocations();
    this.getPKMArtifacts();
  }

  getProject(): void {
    const params: ParamMap = this.route.snapshot.paramMap;

    this.projectInfo.name = params.get('projectId');
    this.projectInfo.versionName = params.get('versionId');

    const currentPath: string = this.router.url;
    this.endswithDefault = currentPath.endsWith(`projects/${this.projectInfo.name}/${this.projectInfo.versionName}`);
    this.endsWithResults = currentPath.endsWith(`projects/${this.projectInfo.name}/${this.projectInfo.versionName}/results`);
    this.endsWithTools = currentPath.endsWith(`projects/${this.projectInfo.name}/${this.projectInfo.versionName}/tools`);
  }

  loadInvocations(): void {
    this.peEngineService
      .getInvocations(this.projectInfo.name)
      .subscribe((invocations: Invocation[]) => {
        this.invocations = invocations;
      });
  }

  onCheckboxSelection(step: MethodologyStep, substep: MethodologyStep): void {
    this.modal = {
      title: step.description,
      step: substep.name,
      parent: step,
      child: substep
    };
    this.displayMethodGuidance = true;
  }

  onStepCompletion(): void {
    this.checkboxTree.updateSteps();
  }

  onHiddenComponent(event: any): void {
    this.displayMethodGuidance = event;
  }

  getPKMArtifacts(): void {
    this.pkmArtifacts = [];
    this.loadingCollapsable = true;
    forkJoin([
      this.artifactsRetriever.getAllPKMCodeFilesFromProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getAllPKMDiagramFilesFromProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getAllPKMDocFilesFromProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getPKMSimplifiedExecutablesFromAProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getPKMAnnotationsFromProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getPKMCommentsFromAProject(this.projectInfo.name),
      this.artifactsRetriever.getASFMDocsFromProject(this.projectInfo.name),
      this.artifactsRetriever.getPKMLogsFromAProject(this.projectInfo.name),
      this.artifactsRetriever.getPKMDiagramsFromAProject(this.projectInfo.name),
      this.artifactsRetriever.getPKMTESTARStateModelFromAProject(
        this.projectInfo.name
      ),
      this.artifactsRetriever.getPKMAnnotationsResults(this.projectInfo.name),
      this.artifactsRetriever.getPKMCVEArtifactFromAProject(this.projectInfo.name),
    ]).subscribe(
      (results: any) => {
        const coll: any[] = results.flat(1);
        this.pkmArtifacts = coll.map((elem: any) =>
          this.convertToPKMArtifact(elem)
        );
      },
      (err: any) => console.error(err)
    );
  }

  /**
   * This function converts a specific artifact returned by a service
   * into the standard PKMArtifact.
   *
   * @param artifact the annotation, log, UML, code or Document artifact to convert
   * @returns PKMArtifact
   */
   private convertToPKMArtifact(artifact: any): PKMArtifact {
    switch (artifact.type) {
      case 'Annotation': {
        if (artifact.fileFormat === undefined && artifact.fileMimeType === undefined && artifact.sourceFile === undefined) {
          const specificArtifact: PKMNerSrlResult = artifact as PKMNerSrlResult;
          return PKMArtifact.fromNerSrlResultArtifact(specificArtifact);
        } else {
          if (artifact.sourceFile.includes('.hpp') || artifact.sourceFile.includes('.cpp')) {
            const specificCArtifact: PKMCAnnotation = artifact as PKMCAnnotation;
            return PKMArtifact.fromCAnnotationArtifact(specificCArtifact);
          }
          const specificArtifact: PKMAnnotation = artifact as PKMAnnotation;
          return PKMArtifact.fromAnnotationArtifact(specificArtifact);
        }
      }
      case 'Comment': {
        const specificArtifact: PKMComment = artifact as PKMComment;
        return PKMArtifact.fromCommentArtifact(specificArtifact);
      }
      case 'Log': {
        const speficifArtifact: PKMLogArtifact = artifact as PKMLogArtifact;
        return PKMArtifact.fromLogArtifact(speficifArtifact);
      }
      case 'UML Model': {
        const specificArtifact: PKMUMLClassDiagramArtifact = artifact as PKMUMLClassDiagramArtifact;
        return PKMArtifact.fromUMLArtifact(specificArtifact);
      }
      case 'TESTAR_State_Model': {
        const specificArtifact: PKMTESTARArtifact = artifact as PKMTESTARArtifact;
        return PKMArtifact.fromTESTARArtifact(specificArtifact);
      }
      case 'Document': {
        const specificArtifact: PKMDocumentArtifact = artifact as PKMDocumentArtifact;
        return PKMArtifact.fromDocumentArtifact(specificArtifact);
      }
      case 'ASFM Docs': {
        const specificArtifact: PKMASFMDocArtifact = artifact as PKMASFMDocArtifact;
        return PKMArtifact.fromASFMDocArtifact(specificArtifact);
      }
      case 'Binary Executable': {
        const specificArtifact: PKMExecutableArtifact = artifact as PKMExecutableArtifact;
        return PKMArtifact.fromBinaryExecutableArtifact(specificArtifact);
      }
      case 'CVE': {
        const specificArtifact: PKMCVEArtifact = artifact as PKMCVEArtifact;
        return PKMArtifact.fromCVEArtifact(specificArtifact);
      }
      case 'Code':
      case 'Diagram': {
        const specificArtifact: PKMArtifact = artifact as PKMArtifact;
        specificArtifact.id = specificArtifact.rel_path;
        specificArtifact.name = specificArtifact.rel_path;
        return specificArtifact;
      }
      default: {
        console.error(
          `Could not convert to PKMArtifact TYPE: ${artifact.type} not supported yet by the GUI`
        );
        return {
          ...artifact,
          name: `${artifact.name} - not supported yet`,
          type: `Not Supported`,
        };
      }
    }
  }
}
