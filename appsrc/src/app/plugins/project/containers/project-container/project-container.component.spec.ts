import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectContainerComponent } from './project-container.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

class ProjectsServiceStub {}

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('ProjectContainerComponent', () => {
  let component: ProjectContainerComponent;
  let fixture: ComponentFixture<ProjectContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectContainerComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        { provide: ProjectsService, useClass: ProjectsServiceStub },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        AppConfiguration
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
