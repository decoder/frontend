import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { STORAGE_ITEMS } from 'src/app/core/services/login/login.service';
import { ProcessEngineService, StepUpdatedInformation } from 'src/app/core/services/process-engine/process-engine.service';
import { ToolsVideoDialogComponent } from 'src/app/home/projects-list/components/tools-video-dialog/tools-video-dialog.component';
import { ToolManagerComponent } from '../../components/tool-manager/tool-manager.component';

@Component({
  selector: 'app-methodology-guidance',
  templateUrl: './methodology-guidance.component.html',
  styleUrls: ['./methodology-guidance.component.scss'],
})
export class MethodologyGuidanceComponent implements OnInit, OnChanges {
  @Input() title: any;
  @Input() projectName: string;
  @Input() searchableArtifacts: PKMArtifact[];
  @Output() stepCompletion: EventEmitter<never>;
  @Output() hiddenComponent: EventEmitter<boolean>;
  selectedArtifacts: PKMArtifact[];
  availableCommentsAndAnnotations: PKMArtifact[];
  availableTools: any;
  displayComponent: boolean;
  explanationViewed: boolean;
  toolsVideoDialog: any;
  viewTools: boolean;
  @ViewChild(ToolManagerComponent) toolManagerComponent: ToolManagerComponent;

  constructor(
    public dialog: MatDialog,
    private processEngineService: ProcessEngineService
  ) {
    this.stepCompletion = new EventEmitter<never>();
    this.searchableArtifacts = [];
    this.selectedArtifacts = [];
    this.projectName = '';
    this.hiddenComponent = new EventEmitter<boolean>(true);
    this.displayComponent = true;
    this.availableCommentsAndAnnotations = [];
    this.explanationViewed = false;
    this.viewTools = true;
    this.title = { title: '', step: '' };
  }

  ngOnInit(): void {
    this.searchableArtifacts = this.searchableArtifacts.filter(
      (element: PKMArtifact) =>
        ['Code', 'Annotation', 'Comment', 'Diagram', 'Document', 'ASFM Docs', 'Binary Executable'].some(
          (value: string) => element.type === value
        )
    );

    this.availableCommentsAndAnnotations = this.searchableArtifacts.filter(
      (element: PKMArtifact) =>
        ['Comment', 'Annotation'].some(
          (value: string) => element.type === value
        )
    );

    if (!localStorage.getItem('toolsExplanationViewed')) {
      this.openExplanationDialog();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.toolManagerComponent?.viewTools === false) {
      this.toolManagerComponent.viewTools = true;
    }
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName) && propName === 'title') {
        if (this.toolManagerComponent !== undefined) {
          this.toolManagerComponent.retrieveAvailableTools();
        }
      }
    }
  }

  openExplanationDialog(): void {
    this.toolsVideoDialog = this.dialog.open(ToolsVideoDialogComponent, {
      disableClose: true,
      width: '60%',
      height: '75%',
      data: {
        title: `Decoder tools explanation `,
        description:
          'This video will be changed as soon as possible for the tutorial one',
      },
    });

    this.toolsVideoDialog.afterClosed().subscribe((result: string) => {
      localStorage.setItem(STORAGE_ITEMS.toolsExplanationViewed, 'true');
    });
  }

  goBackNavigation(): void {
    this.hiddenComponent.emit(!this.displayComponent);
  }

  toggleView(): void {
    this.viewTools = !this.viewTools;
  }

  updateStepInfo(): void {
    const stepUpdatedInfo: StepUpdatedInformation = {
      taskID: this.title.child.id,
      completed: !this.title.child.completed,
    };

    this.processEngineService
      .updateStepInformation(this.projectName, stepUpdatedInfo)
      .subscribe((result: any) => {
        if (result) {
          this.stepCompletion.emit();
          this.goBackNavigation();
        }
      });
  }
}
