import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodologyGuidanceComponent } from './methodology-guidance.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('MethodologyGuidanceComponent', () => {
  let component: MethodologyGuidanceComponent;
  let fixture: ComponentFixture<MethodologyGuidanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, BrowserAnimationsModule, HttpClientTestingModule],
      declarations: [ MethodologyGuidanceComponent ],
      providers: [
        ProcessEngineService,
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        AppConfiguration
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodologyGuidanceComponent);
    component = fixture.componentInstance;
    component.title = {
      title: '',
      step: '',
      parent: {},
      child: {
        taskNumber: 1,
        name: 'test',
        id: 'test',
        completed: false
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit events', () => {
    spyOn(component.hiddenComponent , 'emit');
    component.goBackNavigation();
    expect(component.hiddenComponent.emit).toHaveBeenCalled();
  });
});
