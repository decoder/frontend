import { TestBed } from '@angular/core/testing';
import { ArtifactsRetrieverService } from './artifacts-retriever.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest,
} from '@angular/common/http/testing';
import {
  PKMArtifact,
  PKMDocumentArtifact,
  PKMLogArtifact,
  PKMUMLClassDiagramArtifact,
  PKMTESTARArtifact,
} from 'src/app/core/models/artifact';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { urlEncodeArtifactName } from 'src/app/utils/utils';
import { PKMAnnotation } from 'src/app/core/models/annotation';
import { pkmAnnotationsMock } from 'src/app/mocks/core/fixtures/pkmAnnotations.mock';
import { PKMComment } from 'src/app/core/models/comment';
import { pkmCommentsMock } from 'src/app/mocks/core/fixtures/pkmComments.mock';
import { PKMASFMDocArtifact } from '../../models/asfm-document-artifact.model';
import { pkmLogArtifacts } from 'src/app/mocks/core/fixtures/pkmLogArtifacts.mock';
import { pkmDocumentArtifactsmock } from 'src/app/mocks/core/fixtures/pkmDocumentArtifact.mock';
import { pkmUMLClassDiagramArtifactsMock } from 'src/app/mocks/core/fixtures/pkmUMLClassDiagramArtifacts.mock';
import { pkmESTARArtifactsMock } from 'src/app/mocks/core/fixtures/pkmESTARArtifacts.mock';
import { asfmDocumentArtifactsMock } from 'src/app/mocks/core/fixtures/asfmDocumentArtifacts.mock';
import { AppConfiguration } from 'src/config/app-configuration';

describe('ArtifactsRetrieverService', () => {
  let service: ArtifactsRetrieverService;
  let httpMock: HttpTestingController;
  let mockPKMArtifacts: PKMArtifact[];
  let mockPKMAnnotations: PKMAnnotation[];
  let mockPKMComments: PKMComment[];
  let mockPKMDocumentArtifacts: PKMDocumentArtifact[];
  let mockAsfmDocumentArtifacts: PKMASFMDocArtifact[];
  let mockPKMLogArtifact: PKMLogArtifact[];
  let mockPKMUMLClassDiagramArtifact: PKMUMLClassDiagramArtifact[];
  let mockPKMTESTARtifact: PKMTESTARArtifact[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppConfiguration]
    });
    service = TestBed.inject(ArtifactsRetrieverService);
    httpMock = TestBed.inject(HttpTestingController);
    mockPKMArtifacts = pkmArtifactsMock;
    mockPKMAnnotations = pkmAnnotationsMock;
    mockPKMComments = pkmCommentsMock;
    mockPKMDocumentArtifacts = pkmDocumentArtifactsmock;
    mockAsfmDocumentArtifacts = asfmDocumentArtifactsMock;
    mockPKMLogArtifact = pkmLogArtifacts;
    mockPKMUMLClassDiagramArtifact = pkmUMLClassDiagramArtifactsMock;
    mockPKMTESTARtifact = pkmESTARArtifactsMock;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // Testing getRelatedPKMArtifacts
  it('should return pkm artifacts when getRelatedPKMArtifacts', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/code/rawsourcecode/${projectId}`;

    service.getRelatedPKMArtifacts(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMArtifacts);
    httpMock.verify();
  });

  // Testing getRelatedPKMArtifactsByID
  it('should get artifacts with id when getRelatedPKMArtifactsById', () => {
    const projectId: string = '0';
    const artifactId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/code/rawsourcecode/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    const pkmArtifact: PKMArtifact = mockPKMArtifacts[0];

    service.getRelatedPKMArtifactsByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(pkmArtifact);
    httpMock.verify();
  });

  // Testing getAllPKMFilesArtifactsFromProject
  it('should return projects with project id when getAllPKMFilesArtifactsFromProject', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/files/${projectId}`;

    service.getAllPKMFilesArtifactsFromProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMArtifacts);
    httpMock.verify();
  });

  // Testing getAllPKMFilesArtifactsFromProjectSimplified
  it('should return simplify artifacts when getAllPKMFilesArtifactsFromProjectsSimplified', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/files/${projectId}?abbrev=true`;

    service.getAllPKMFilesArtifactsFromProjectSimplified(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMArtifacts);
    httpMock.verify();
  });

  // Testing getPKMFileArtifactFromProjectByID
  it('should return file artifact when getPKMFileArtifactFromProjectByID', () => {
    const projectId: string = '0';
    const artifactId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/files/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;

    service
      .getPKMFileArtifactFromProjectByID(projectId, artifactId)
      .subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMArtifacts[0]);
    httpMock.verify();
  });

  // Testing getPKMAnnotationsFromProject
  it('should pkm annotations when getPKMAnnotationsFromProject is called', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/code/annotations/${projectId}`;

    service.getPKMAnnotationsFromProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMAnnotations);
    httpMock.verify();
  });

  // Testing getPKMAnnotationsFromProjectByID
  it('should return annotations of project when getPKMAnnotationsFromProjectByID is called', () => {
    const projectId: string = '0';
    const artifactId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/code/annotations/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;

    service.getPKMAnnotationsFromProjectByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMAnnotations);
    httpMock.verify();
  });

  // Testing getPKMOpenJMLAnnotationsFromProject
  it('should return jml annotations when getPKMOpenJMLAnnotationsFromProject is called', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/annotations/openjml/analysis/${projectId}`;

    service.getPKMOpenJMLAnnotationsFromProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMAnnotations);
    httpMock.verify();
  });

  // Testing getPKMCommentsFromAProject
  it('should return pkm comments when getPKMCommentsFromAProject is called', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/code/comments/${projectId}`;

    service.getPKMCommentsFromAProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMComments);
    httpMock.verify();
  });

  // Testing getPKMDocsFromAProject
  it('should return pkm docs when getPKMDocsFromAProject is called', () => {
    const projectId: string = '0';
    const mockUrl: string = `${service.projectBaseURL}/doc/rawdoc/${projectId}`;

    service.getPKMDocsFromAProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMDocumentArtifacts);
    httpMock.verify();
  });

  // Testing getPKMDocsFromAProjectByID
  it('should return docs when getPKMDocsFromAProjectByID', () => {
    const projectId: string = '32';
    const artifactId: string = '23';
    const mockUrl: string = `${service.projectBaseURL}/doc/rawdoc/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;

    service.getPKMDocsFromAProjectByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMDocumentArtifacts[0]);
    httpMock.verify();
  });

  // Testing getASFMDocsFromProject
  it('should return asfm documents when getASMFDocsFromProject is called', () => {
    const projectId: string = '3';
    const mockUrl: string = `${service.projectBaseURL}/doc/asfm/docs/${projectId}`;

    service.getASFMDocsFromProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockAsfmDocumentArtifacts);
    httpMock.verify();
  });

  // Testing getASFMDocFromProjectByID
  it('should return asfm docs when getASFMDocsFromProjectByID is called', () => {
    const projectId: string = '3';
    const artifactId: string = '12';
    const mockUrl: string = `${service.projectBaseURL}/doc/asfm/docs/${projectId}?doc=${urlEncodeArtifactName(
      artifactId
    )}`;

    service.getASFMDocFromProjectByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockAsfmDocumentArtifacts[0]);
    httpMock.verify();
  });

  // Testing getPKMLogsFromAProject
  it('should return logs when getPKMLogsFromAProject is called', () => {
    const projectId: string = '3';
    const mockUrl: string = `${service.projectBaseURL}/log/${projectId}?abbrev=true`;

    service.getPKMLogsFromAProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMLogArtifact);
    httpMock.verify();
  });

  // Testing getPKMLogsFromAProjectByID
  it('should return logs when getPKMLogsFromAProjectByID is called', () => {
    const projectId: string = '3';
    const artifactId: string = '23';
    const mockUrl: string = `${service.projectBaseURL}/log/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;

    service.getPKMLogsFromAProjectByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMLogArtifact[0]);
    httpMock.verify();
  });

  // Testing getPKMDiagramsFromAProject
  it('should return diagrams when getPKMDiagramsFromAProject is called ', () => {
    const projectId: string = '34';
    const mockUrl: string = `${service.projectBaseURL}/uml/uml_class/${projectId}`;

    service.getPKMDiagramsFromAProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMUMLClassDiagramArtifact);
    httpMock.verify();
  });

  // Testing getPKMDiagramsFromAProjectByID
  it('should return diagrams when getPKMDiagramsFromAProjectByID is called ', () => {
    const projectId: string = '34';
    const artifactId: string = '21';
    const mockUrl: string = `${service.projectBaseURL}/uml/uml_class/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;

    service.getPKMDiagramsFromAProjectByID(projectId, artifactId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMUMLClassDiagramArtifact);
    httpMock.verify();
  });

  // Testing getPKMTESTARStateModelFromAProject
  it('should return pkmstar state model when getPKMESTARStateModelFromAProject is called ', () => {
    const projectId: string = '34';
    const mockUrl: string = `${service.projectBaseURL}/testar/state_model/${projectId}`;

    service.getPKMTESTARStateModelFromAProject(projectId).subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMTESTARtifact);
    httpMock.verify();
  });

  // Testing getPKMTESTARStateModelFromAProjectByID
  it('should return pkmestar state model when getPKMTESTARStateModelFromAProjectByID is called ', () => {
    const projectId: string = '34';
    const artifactId: string = '12';
    const mockUrl: string = `${service.projectBaseURL}/testar/state_model/${projectId}/${artifactId}`;

    service
      .getPKMTESTARStateModelFromAProjectByID(projectId, artifactId)
      .subscribe();
    const req: TestRequest = httpMock.expectOne(mockUrl);

    expect(req.request.method).toBe('GET');
    req.flush(mockPKMTESTARtifact[0]);
    httpMock.verify();
  });
});
