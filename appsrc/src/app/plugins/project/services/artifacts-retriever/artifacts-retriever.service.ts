import { Injectable } from '@angular/core';
import {
  PKMArtifact,
  PKMCVEArtifact,
  PKMDocumentArtifact,
  PKMExecutableArtifact,
  PKMLogArtifact,
  PKMTESTARArtifact,
  PKMUMLClassDiagramArtifact,
} from 'src/app/core/models/artifact';
import { Observable, of } from 'rxjs';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { urlEncodeArtifactName } from '../../../../utils/utils';
import { PKMAnnotation } from 'src/app/core/models/annotation';
import { PKMComment } from 'src/app/core/models/comment';
import { AppConfiguration } from 'src/config/app-configuration';
import { PKMASFMDocArtifact } from '../../models/asfm-document-artifact.model';

@Injectable({
  providedIn: 'root',
})
export class ArtifactsRetrieverService {

  projectBaseURL: string = this.appConfig.serverURL;

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(private readonly http: HttpClient, private appConfig: AppConfiguration) { }

  getRelatedPKMArtifacts(projectId: string): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/code/rawsourcecode/${projectId}`;
    return this.http.get<PKMArtifact[]>(url).pipe(catchError(() => [])); // ignore errors
  }

  // unused service by now:
  getRelatedPKMArtifactsByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMArtifact> {
    const url: string = `${this.projectBaseURL}/code/rawsourcecode/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMArtifact>(url); // if any errors occur, then propagate automatically
  }

  getAllPKMFilesArtifactsFromProject(
    projectId: string
  ): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/files/${projectId}`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getAllPKMCodeFilesFromProject(
    projectId: string
  ): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/code/rawsourcecode/${projectId}?abbrev=true`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getPKMCodeFileFromProjectByName(
    projectId: string,
    artifactName: string,
  ): Observable<PKMArtifact> {
    const url: string = `${this.projectBaseURL}/code/rawsourcecode/${projectId}/${urlEncodeArtifactName(artifactName)}`;
    return (
      this.http
        .get<PKMArtifact>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of(null);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getAllPKMDiagramFilesFromProject(
    projectId: string
  ): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/uml/rawuml/${projectId}`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getPKMDiagramFilesFromProjectByName(
    projectId: string,
    artifactName: string,
  ): Observable<PKMArtifact> {
    const url: string = `${this.projectBaseURL}/uml/rawuml/${projectId}/${urlEncodeArtifactName(artifactName)}`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of(null);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getAllPKMDocFilesFromProject(
    projectId: string
  ): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/doc/rawdoc/${projectId}`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getAllPKMFilesArtifactsFromProjectSimplified(
    projectId: string
  ): Observable<PKMArtifact[]> {
    const url: string = `${this.projectBaseURL}/files/${projectId}?abbrev=true`;
    return (
      this.http
        .get<PKMArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              // propagate error
              throw err;
            }
          })
        )
    );
  }

  getPKMFileArtifactFromProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMArtifact> {
    const url: string = `${this.projectBaseURL}/files/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMArtifact>(url); // in case of error automatically propagate (also 404 Not Found error)
  }

  getPKMAnnotationsFromProject(projectId: string): Observable<PKMAnnotation[]> {
    const url: string = `${this.projectBaseURL}/code/annotations/${projectId}`;
    return this.http.get<PKMAnnotation[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMCAnnotationsFromProject(projectId: string): Observable<PKMAnnotation[]> {
    const url: string = `${this.projectBaseURL}/code/c/annotations/${projectId}`;
    return this.http.get<PKMAnnotation[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  // Retrieves ner and srl results from the PKM
  getPKMAnnotationsResults(projectId: string): Observable<any[]> {
    const url: string = `${this.projectBaseURL}/annotations/${projectId}`;
    return this.http.get<any[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMAnnotationsFromProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMAnnotation[]> {
    const url: string = `${this.projectBaseURL}/code/annotations/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMAnnotation[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMOpenJMLAnnotationsFromProject(
    projectId: string
  ): Observable<PKMAnnotation[]> {
    const url: string = `${this.projectBaseURL}/annotations/openjml/analysis/${projectId}`;
    return this.http.get<PKMAnnotation[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMOpenJMLAnnotationsProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMAnnotation[]> {
    const url: string = `${this.projectBaseURL}/annotations/openjml/analysis/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMAnnotation[]>(url).pipe(
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse && err.status === 404) {
          return of([]);
        } else {
          throw err;
        }
      })
    );
  }

  getPKMCommentsFromAProject(projectId: string): Observable<PKMComment[]> {
    const url: string = `${this.projectBaseURL}/code/comments/${projectId}`;
    return this.http.get<PKMComment[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMCommentsFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMComment[]> {
    const url: string = `${this.projectBaseURL}/code/comments/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMComment[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMDocsFromAProject(projectId: string): Observable<PKMDocumentArtifact[]> {
    const url: string = `${this.projectBaseURL}/doc/rawdoc/${projectId}`;
    return this.http.get<PKMDocumentArtifact[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMDocsFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMDocumentArtifact> {
    const url: string = `${this.projectBaseURL}/doc/rawdoc/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMDocumentArtifact>(url); // propagate all kinds of errors (also for 404 Not Found), so all responses are not null
  }

  getASFMDocsFromProject(
    projectId: string
  ): Observable<PKMASFMDocArtifact[]> {
    const url: string = `${this.projectBaseURL}/doc/asfm/docs/${urlEncodeArtifactName(projectId)}`;
    return (
      this.http
        .get<PKMASFMDocArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            if (err instanceof HttpErrorResponse && err.status === 404) {
              return of([]);
            } else {
              throw err;
            }
          })
        )
    );
  }

  getASFMDocFromProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMASFMDocArtifact> {
    const url: string = `${this.projectBaseURL}/doc/asfm/docs/${projectId}?doc=${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http
      .get<PKMASFMDocArtifact[]>(url) // propagate all kinds of errors (also for 404 Not Found), so all responses are not null
      .pipe(map((elems: PKMASFMDocArtifact[]) => elems[0])); // take first element because there is only one with that name
  }

  getPKMLogsFromAProject(projectId: string): Observable<PKMLogArtifact[]> {
    const url: string = `${this.projectBaseURL}/log/${projectId}?abbrev=true`;
    return this.http.get<PKMLogArtifact[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMLogFromAProjectById(projectId: string, logId: string): Observable<PKMLogArtifact> {
    const url: string = `${this.projectBaseURL}/log/${projectId}/${logId}`;
    return this.http.get<PKMLogArtifact>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of(null);
      })
    );
  }

  getPKMLogsFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMLogArtifact> {
    const url: string = `${this.projectBaseURL}/log/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMLogArtifact>(url); // propagate all kinds of errors (also for 404 Not Found), so all responses are not null
  }

  getArtfifactFromAProjectByID(
    path: string
  ): Observable<PKMLogArtifact | boolean> {
    const url: string = `${this.projectBaseURL}${path}`;
    return this.http.get<PKMLogArtifact>(url).pipe(
      catchError((err: any) => {
        return of(err.ok);
      })
    ); // propagate all kinds of errors (also for 404 Not Found), so all responses are not null
  }

  getPKMExecutableFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMExecutableArtifact> {
    const url: string = `${this.projectBaseURL}/bin/executable/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMExecutableArtifact>(url);
  }

  getPKMSimplifiedExecutablesFromAProject(
    projectId: string
  ): Observable<PKMExecutableArtifact[]> {
    const url: string = `${this.projectBaseURL}/bin/executable/${projectId}?abbrev=true`;
    return this.http.get<PKMExecutableArtifact[]>(url);
  }

  getPKMDiagramsFromAProject(
    projectId: string
  ): Observable<PKMUMLClassDiagramArtifact[]> {
    const url: string = `${this.projectBaseURL}/uml/uml_class/${projectId}`;
    return (
      this.http
        .get<PKMUMLClassDiagramArtifact[]>(url)
        .pipe(
          catchError((err: any) => {
            console.error(err);
            return of([]);
          })
        )
    );
  }

  getPKMDiagramsFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMUMLClassDiagramArtifact[]> {
    const url: string = `${this.projectBaseURL}/uml/uml_class/${projectId}/${urlEncodeArtifactName(
      artifactId
    )}`;
    return this.http.get<PKMUMLClassDiagramArtifact[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMTESTARStateModelFromAProject(
    projectId: string
  ): Observable<PKMTESTARArtifact[]> {
    const url: string = `${this.projectBaseURL}/testar/state_model/${projectId}`;
    return this.http.get<PKMTESTARArtifact[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMCVEArtifactFromAProject(
    projectId: string
  ): Observable<PKMCVEArtifact[]> {
    const url: string = `${this.projectBaseURL}/cve/${projectId}`;
    return this.http.get<PKMCVEArtifact[]>(url).pipe(
      catchError((err: any) => {
        console.error(err);
        return of([]);
      })
    );
  }

  getPKMCVEArtifactFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMCVEArtifact> {
    const url: string = `${this.projectBaseURL}/cve/${projectId}/${artifactId}`;
    return this.http.get<PKMCVEArtifact>(url); // propagate all kinds of errors (also 404 Not Found), not to return an empty result
  }

  getPKMTESTARStateModelFromAProjectByID(
    projectId: string,
    artifactId: string
  ): Observable<PKMTESTARArtifact> {
    const url: string = `${this.projectBaseURL}/testar/state_model/${projectId}/${artifactId}`;
    return this.http.get<PKMTESTARArtifact>(url); // propagate all kinds of errors (also 404 Not Found), not to return an empty result
  }

  getTestarConfig(dbName: string): Observable<any> {
    return this.http.get<any>(`${this.projectBaseURL}/testar/settings/${dbName}`)
      .pipe(
        catchError((err: any) => {
          console.error('error :', err);
          return of({});
        })
      );
  }

  deleteArtifactByIdAndType(
    projectId: string,
    artifact: PKMArtifact,
  ): Observable<any> {
    let artifactName: string = '';
    if (artifact.type === 'NER' || artifact.type === 'SRL') {
      artifactName = artifact.content._id;
    } else if (artifact.type === 'Log' || artifact.type === 'CVE') {
      artifactName = artifact.id;
    } else {
      artifactName = artifact.name;
    }
    const url: string = `${this.projectBaseURL}/${this.retrieveEndpointFromArtifactType(
      projectId,
      artifactName,
      artifact.type
    )}`;
    return this.http.delete(url);
  }

  putPKMRawSourceCode(projectId: string, artifact: PKMArtifact): Observable<any> {
    const url: string = `${this.projectBaseURL}/code/rawsourcecode/${projectId}`;
    return this.http
      .put(url, [{ ...artifact }]);
  }

  private retrieveEndpointFromArtifactType(projectId: string, artifactId: string, artifactType: string): string {
    switch (artifactType) {
      case 'Code': {
        return `code/rawsourcecode/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      case 'Annotation': {
        return `code/annotations/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      case 'Comment': {
        return `code/comments/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      case 'Log': {
        return `log/${projectId}/${urlEncodeArtifactName(artifactId)}`;
      }
      case 'Diagram': {
        return `file/${projectId}/${urlEncodeArtifactName(artifactId)}`;
      }
      case 'Binary Executable': {
        return `bin/executable/${projectId}/${urlEncodeArtifactName(artifactId)}`;
      }
      case 'UML Model': {
        return `uml/uml_class/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      case 'Document': {
        return `doc/rawdoc/${projectId}/${urlEncodeArtifactName(artifactId)}`;
      }
      case 'NER':
      case 'SRL': {
        return `annotations/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      case 'CVE': {
        return `cve/${projectId}/${urlEncodeArtifactName(
          artifactId
        )}`;
      }
      default: {
        return '';
      }
    }
  }
}
