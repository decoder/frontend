import { Injectable } from '@angular/core';
import { AppConfiguration } from 'src/config/app-configuration';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { Review } from 'src/app/core/models/review';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  projectBaseURL: string = this.appConfig.serverURL;

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(private readonly http: HttpClient, private appConfig: AppConfiguration) { }

  getProjectReviews(projectId: string): Observable<Review[]> {
    const url: string = `${this.projectBaseURL}/reviews/${projectId}`;
    return this.http.get<Review[]>(url).pipe(catchError(() => []));
  }

  addProjectReview(projectId: string, review: Review[]): Observable<Review[]> {
    const url: string = `${this.projectBaseURL}/reviews/${projectId}`;
    return this.http.post(url, review).pipe(catchError(() => []));
  }

  updateProjectReview(projectId: string, review: Review[]): Observable<Review[]> {
    const url: string = `${this.projectBaseURL}/reviews/${projectId}`;
    return this.http.put(url, review).pipe(catchError(() => []));
  }

  deleteProjectReview(projectId: string, reviewId: string): Observable<Review[]> {
    const url: string = `${this.projectBaseURL}/reviews/${projectId}`;
    return this.http.delete(url,
      {
        headers: this.headers,
        params: {reviewID: reviewId}
      }, ).pipe(catchError(() => []));
  }
}
