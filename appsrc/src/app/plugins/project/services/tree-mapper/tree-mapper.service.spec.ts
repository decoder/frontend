import { TestBed } from '@angular/core/testing';
import { TreeMapper } from './tree-mapper.service';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { ArtifactNode } from '../../models/artifact-node.model';

describe('TreeService', () => {
  let service: TreeMapper;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TreeMapper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('it should transform an array of artifacts to a tree', () => {
    const resultTree: ArtifactNode[] = [
      {
        name: 'Code',
        children: [{
          name: 'TestArtifact.java',
          id: 'TestArtifact.java',
          icon: 'code',
          type: 'Code',
          children: [],
        }],
      },
    ];

    const sourceArtifacts: PKMArtifact[] = [{
      id: 'TestArtifact.java',
      name: 'TestArtifact.java',
      content: '',
      rel_path: 'TestArtifact.java',
      git_commit_id: '0332',
      git_repository_url: 'version-control.domain.url/TestArtifact.java',
      type: 'Code',
      format: '',
      encoding: '',
      mime_type: '',
    }];

    expect(service.createTreeFromPKMArtifact(sourceArtifacts)).toEqual(resultTree);
  });
});
