import { Injectable } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { artifactIconByType } from '../../../../shared/constants/utils';
import { ArtifactNode } from '../../models/artifact-node.model';

@Injectable({
  providedIn: 'root'
})
export class TreeMapper {
  private pkmArtifacts: PKMArtifact[];
  private pkmArtifactTypes: string[];

  constructor() {
    this.pkmArtifactTypes = [];
    this.pkmArtifacts = [];
  }

  createTreeFromPKMArtifact(artifacts: PKMArtifact[]): ArtifactNode[] {
    this.pkmArtifacts = artifacts;
    this.setUsedTypesPKM();
    return this.makeTreeByTypePKM();
  }

  setUsedTypesPKM(): void {
    const types: string[] = this.pkmArtifacts.map(
      (artifact: PKMArtifact) => artifact.type
    );
    this.pkmArtifactTypes = [...new Set(types)];
  }

  private makeTreeByTypePKM(): ArtifactNode[] {
    const nonEmptyNodes: ArtifactNode[] = [];

    this.pkmArtifactTypes.forEach((type: string) => {
      const artifactNode: ArtifactNode = this.getArtifactNodeByTypePKM(type);

      if (!this.isEmptyNode(artifactNode)) {
        nonEmptyNodes.push(artifactNode);
      }
    });

    return nonEmptyNodes;
  }

  private getArtifactNodeByTypePKM(type: string): ArtifactNode {
    const groupOfArtifacts: PKMArtifact[] = this.getArtifactsByTypePKM(type);

    return this.artifactsToNodePKM(groupOfArtifacts, type);
  }

  private getArtifactsByTypePKM(type: string): PKMArtifact[] {
    return this.pkmArtifacts.filter((art: PKMArtifact) => art.type === type);
  }

  private artifactsToNodePKM(artifacts: PKMArtifact[], type: string = 'Code'): ArtifactNode {
    const icon: string = artifactIconByType(type);
    return {
      name: type,
      children: this.artifactsToLeafPKM(artifacts, icon)
    };
  }

  private artifactsToLeafPKM(artifacts: PKMArtifact[], icon: string): ArtifactNode[] {
    return artifacts.map((artifact: PKMArtifact) => {
        const { name, type, id } = artifact;
        return { name, id, icon, type, children: [] };
    });
  }

  private isEmptyNode(artifactNode: ArtifactNode): boolean {
    const hasChildrenField: boolean = !!artifactNode.children;
    const hasChildren: boolean = artifactNode.children.length > 0;

    return !!hasChildrenField && !hasChildren;
  }
}
