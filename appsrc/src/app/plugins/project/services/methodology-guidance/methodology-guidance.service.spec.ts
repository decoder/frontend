import { TestBed } from '@angular/core/testing';
import { MethodologyGuidanceService } from './methodology-guidance.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

describe('MethodologyGuidanceService', () => {
  let service: MethodologyGuidanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppConfiguration]
    });
    service = TestBed.inject(MethodologyGuidanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
