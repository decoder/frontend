import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MethodologyStep } from '../../components/methodology-support/methodology-support.component';
import { Observable } from 'rxjs/internal/Observable';
import { urlEncodeArtifactName } from './../../../../utils/utils';
import { AppConfiguration } from 'src/config/app-configuration';

const CODE_TYPE: string = 'Code';
const ANNOTATION_TYPE: string = 'Annotation';
const COMMENTS_TYPE: string = 'Comment';

export interface AvailableToolSpecification {
  toolID: string;
  toolName: string;
  description: string;
  phases: string[];
  tasks: string[];
  server: string;
  endpoint: RequestInformation;
}

interface RequestInformation {
  path: string;
  method: string;
  pathFields: {
    name: string;
    description: string;
    required: boolean;
    type: string;
    isDBName?: boolean;
    artifactDesc?: ArtifactDesciption;
  };
  queryParameters?: QueryParameters[];
}

interface ArtifactDesciption {
  type: string;
  allowedExtentions: string[];
}

interface QueryParameters {
  name: string;
  description: string;
  type: string;
  required: boolean;
  allowedValues: string[];
}

@Injectable({
  providedIn: 'root',
})
export class MethodologyGuidanceService {

  availableTools$: Observable<AvailableToolSpecification[]>;
  availableSteps$: Observable<MethodologyStep[]>;

  constructor(private http: HttpClient, private appConfig: AppConfiguration) { }

  obtainSteps(projectName: string): void {
    this.availableSteps$ = this.http
      .get<MethodologyStep[]>(`${this.appConfig.toolsServerURL}/decoder/pe/process/${projectName}/status`);
  }

  getSteps(): Observable<MethodologyStep[]> {
    return this.availableSteps$;
  }

  obtainToolsByPhaseAndTask(projectName: string, step: MethodologyStep, substep: MethodologyStep): void {
    this.availableTools$ = this.http
      .get<AvailableToolSpecification[]>(
        // tslint:disable-next-line:max-line-length
        `${this.appConfig.toolsServerURL}/decoder/pe/tools/${projectName}?processTask=${encodeURI(substep.name)}&processPhase=${encodeURI(step.name)}
      `);
  }

  obtainAvailableTools(): Observable<AvailableToolSpecification[]> {
    return this.availableTools$;
  }

  nerAndSrlSingleRequest(
    projectId: string,
    artifactId: string,
    toolEndpoint: string,
    artifactType: string,
    index: string
  ): Observable<any> {
    const httpOptions: any = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }),
    };
    const url: string = `${toolEndpoint}`;
    const body: any = {
      project_id: projectId,
      path: this.retrievePathFromArtifactType(
        artifactType,
        projectId,
        artifactId
      ),
      access: this.buildAccessFieldContent(artifactType, artifactId, index),
    };
    return this.http.post(url, body, httpOptions);
  }

  retrievePathFromArtifactType(
    artifactType: string,
    projectId: string,
    artifactId: string
  ): string {
    if (artifactType === CODE_TYPE) {
      return urlEncodeArtifactName(
        `code/rawsourcecode/${projectId}/${urlEncodeArtifactName(artifactId)}`
      );
    } else if (artifactType === ANNOTATION_TYPE) {
      return urlEncodeArtifactName(
        `code/annotations/${projectId}/${urlEncodeArtifactName(artifactId)}`
      );
    } else if (artifactType === COMMENTS_TYPE) {
      return urlEncodeArtifactName(
        `code/comments/${projectId}/${urlEncodeArtifactName(artifactId)}`
      );
    }
    return '';
  }

  buildAccessFieldContent(
    artifactType: string,
    artifactId: string,
    subSelectedItem?: any
  ): string {
    if (
      artifactId.indexOf('.java') !== -1 &&
      artifactId.split('.').pop() === 'java'
    ) {
      if (artifactType === COMMENTS_TYPE) {
        return urlEncodeArtifactName(
          `[0].comments[?loc.pos_start.pos_cnum == \`${subSelectedItem[0].loc.pos_start.pos_cnum}\`].comments[]`
        );
      } else if (artifactType === ANNOTATION_TYPE) {
        return urlEncodeArtifactName(
          `[0].annotations[?loc.pos_start.pos_cnum == \`${subSelectedItem[0].loc.pos_start.pos_cnum}\`].annotations[]`
        );
      }
    }
    if (
      (artifactId.indexOf('.c') !== -1 && artifactId.split('.').pop() === 'c') ||
      (artifactId.indexOf('.h') !== -1 && artifactId.split('.').pop() === 'h') ||
      (artifactId.indexOf('.c') !== -1 && artifactId.split('.').pop() === 'cpp') ||
      (artifactId.indexOf('.h') !== -1 && artifactId.split('.').pop() === 'hpp')
    ) {
      if (artifactType === COMMENTS_TYPE) {
        if (subSelectedItem[0].global_kind) {
          // tslint:disable-next-line
          return `[0].comments | [?global_kind == \'${subSelectedItem[0].global_kind}\'] | [?loc.pos_start.pos_path == \'${artifactId}\'].comments | [${subSelectedItem[1]}]`;
        } else {
          return `[0].comments | [?loc.pos_start.pos_path == \'${artifactId}\'].comments | [${subSelectedItem[1]}]`;
        }
      } else if (artifactType === ANNOTATION_TYPE) {
        return `[0].annotations | [?loc.pos_start.pos_path == \'${artifactId}\'].annotations | [${subSelectedItem[1]}]`;
      }
    }
    return '';
  }
}
