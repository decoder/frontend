import { SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { PKMArtifact } from 'src/app/core/models/artifact';

export class VisualizableArtifact {
    artifact: PKMArtifact;
    extension: string;
    content: SafeResourceUrl;
    originalUrl: string | SafeUrl;

    constructor(
        artifact: PKMArtifact,
        extension: string,
        content: SafeResourceUrl,
        originalUrl: string | SafeUrl
    ) {
        this.artifact = artifact;
        this.extension = extension;
        this.content = content;
        this.originalUrl = originalUrl;
    }

    getType(): string {
        return this.artifact.type;
    }

    getId(): string {
        return this.artifact.id;
    }
}
