/**
 * This is the structure returned by the endpoint:
 *
 *  https://decoder-tool.ow2.org/pkm/doc/rawdoc/{project-name}/{artifactname}
 */
 export class BinaryDocumentArtifact {
    content: string;
    format: string;
    mime_type: string;
    rel_path: string;
    type: string;
}
