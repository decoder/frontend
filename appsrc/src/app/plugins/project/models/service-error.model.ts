
export interface ServiceError {
    isError: boolean;
    error: any;
}
