export interface ArtifactNode {
  name: string;
  id?: string;
  icon?: string;
  type?: string;
  children: ArtifactNode[];
}
