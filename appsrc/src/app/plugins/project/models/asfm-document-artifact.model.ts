/**
 * This is the structure returned by the endpoint:
 *
 *  https://decoder-tool.ow2.org/pkm/doc/asfm/docs/{project-name}?filename={filename.docx}
 */
export class PKMASFMDocArtifact {
        _id: string;
        name: string;
        type: string;
        sourceFile: string;
        units: UnitArtifact[];
}

export interface UnitArtifact {
        id: number;
        name: string;
        classes: ClassArtifact[];
}

export interface ClassArtifact {
        id: number;
        name: string;
        fields?: ClassElem[];
        types?: ClassElem[];
        methods?: ClassElem[];
}

export interface ClassElem {
        id: number;
        name: string;
}
