import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { VisualizableArtifact } from './visualizable-artifact.model';

describe('VisualizableArtifact', () => {
  it('should create one visualizable artifact from scracth', () => {
    const visualizableArtifact: VisualizableArtifact = new VisualizableArtifact(
      pkmArtifactsMock[0],
      'java',
      '',
      'domain.test'
    );
    expect(visualizableArtifact.extension).toBe('java');
    expect(visualizableArtifact.getId()).toBe('artifactTest.java');
    expect(visualizableArtifact.getType()).toBe('Code');
  });
});
