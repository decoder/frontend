import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { InvocationInfoComponent } from 'src/app/core/components/invocation-info/invocation-info.component';
import { Invocation } from 'src/app/core/models/invocation';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';

export interface IFilterValue {
  value: string;
  option: string;
}

@Component({
  selector: 'app-pe-results-viewer',
  templateUrl: './pe-results-viewer.component.html',
  styleUrls: ['./pe-results-viewer.component.scss'],
})
export class PeResultsViewerComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() invocations: Invocation[];
  @Input() projectName: string;
  @Output() invocationsEvent: EventEmitter<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  optionFilter: string = '';
  filterSelected: string;
  filters: IFilterValue[];
  withFilter: boolean = false;
  displayedColumns: string[] = [
    'Tool',
    'User',
    'Status',
    'ID',
    'detailed',
    'delete',
  ];
  dataSource: MatTableDataSource<Invocation>;
  filterForm: FormGroup;
  invocationsAreLoaded: boolean = false;

  constructor(
    public dialog: MatDialog,
    private readonly formBuilder: FormBuilder,
    private readonly processEngineService: ProcessEngineService,
    private snackbar: SnackbarService,
  ) {
    this.invocationsEvent = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      filter: ['', [Validators.required, Validators.email]],
    });
    this.filters = [
      {
        value: 'tool',
        option: 'Tool',
      },
      {
        value: 'user',
        option: 'User',
      },
      {
        value: 'invocationStatus',
        option: 'Status',
      },
      {
        value: 'invocationID',
        option: 'ID',
      },
    ];
    this.udpateInvocations();
    this.dataSource = new MatTableDataSource<Invocation>(this.invocations);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const invocations: SimpleChange = changes.invocations;

    if (invocations.currentValue !== invocations.previousValue) {
      if (this.dataSource) {
        this.dataSource.data = this.invocations;
      }
      this.invocationsAreLoaded = true;
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  openResultDialog(data: Invocation): void {
    const dialogRef: MatDialogRef<any> = this.dialog.open(InvocationInfoComponent, {
      data: {
        data,
        projectName: this.projectName
      },
      height: '80vh',
      width: '120vh',
    });

    dialogRef.afterClosed().subscribe(() => {
      this.invocationsEvent.emit();
    });
  }

  udpateInvocations(): void {
    this.invocationsEvent.emit();
  }

  changeOptionFilter(event: any): void {
    this.optionFilter = event.value;
  }

  filterButton(): void {
    const value: string = this.filterForm.get('filter').value;
    if (value === '') {
      this.dataSource = new MatTableDataSource<Invocation>(this.invocations);
    }
    if (this.optionFilter === '') {
      window.alert('You should select a option');
    }
    this.filterTools(this.optionFilter, value);
  }

  filterTools(option: string, value: string): void {
    let invocationsFiltered: Invocation[] = [];
    switch (option) {
      case 'tool':
        invocationsFiltered = this.invocations.filter(
          (invocation: Invocation) =>
            invocation.tool.toLowerCase().includes(value.toLowerCase())
        );
        break;
      case 'user':
        invocationsFiltered = this.invocations.filter(
          (invocation: Invocation) =>
            invocation.user.toLowerCase().includes(value.toLowerCase())
        );
        break;
      case 'invocationStatus':
        invocationsFiltered = this.invocations.filter(
          (invocation: Invocation) =>
            invocation.invocationStatus
              .toLowerCase()
              .includes(value.toLowerCase())
        );
        break;
      case 'invocationID':
        invocationsFiltered = this.invocations.filter(
          (invocation: Invocation) =>
            invocation.invocationID.toLowerCase().includes(value.toLowerCase())
        );
        break;
      default:
        this.dataSource = new MatTableDataSource<Invocation>(this.invocations);
        break;
    }
    this.withFilter = true;
    this.dataSource = new MatTableDataSource<Invocation>(invocationsFiltered);
    this.dataSource.paginator = this.paginator;
  }

  showAllTools(): void {
    this.withFilter = false;
    this.filterForm.setValue({ filter: '' });
    this.dataSource = new MatTableDataSource<Invocation>(this.invocations);
    this.dataSource.paginator = this.paginator;
  }

  deleteInvocation(invocation: Invocation): void {
    this.processEngineService
      .deleteInvocation(
        this.projectName,
        invocation.invocationID
      )
      .subscribe(
        (result: any) => {
          if (typeof result.failed !== 'undefined') {
            this.snackbar.openSnackBar(result.errorMessage, 'error');
          } else {
            this.snackbar.openSnackBar('Invocation result deleted successfully!', 'check_circle');
            this.udpateInvocations();
          }
        }
      );
  }
}
