import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfiguration } from 'src/config/app-configuration';

import { PeResultsViewerComponent } from './pe-results-viewer.component';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('PeResultsViewerComponent', () => {
  let component: PeResultsViewerComponent;
  let fixture: ComponentFixture<PeResultsViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatDialogModule, HttpClientModule, FormsModule, ReactiveFormsModule],
      declarations: [PeResultsViewerComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeResultsViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit events', () => {
    spyOn(component.invocationsEvent, 'emit');

    component.udpateInvocations();
    expect(component.invocationsEvent.emit).toHaveBeenCalled();
  });
});
