import { ChangeDetectorRef } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';

import { ToolManagerComponent } from './tool-manager.component';
import { Observable, of } from 'rxjs';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { Invocation } from 'src/app/core/models/invocation';
import { invocationMock } from 'src/app/mocks/developer/invocation.mock';
import { PeResultsViewerComponent } from '../pe-results-viewer/pe-results-viewer.component';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatDialogModule } from '@angular/material/dialog';
import { AvailableToolSpecification, MethodologyGuidanceService } from '../../services/methodology-guidance/methodology-guidance.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

class PeEngineServiceStub {
  getInvocations(): Observable<Invocation[]> {
    return of(invocationMock);
  }
}

class MethodologyGuidanceServiceStub {
  obtainAvailableTools(): Observable<AvailableToolSpecification[]> {
    return of([{
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    }]);
  }
}

describe('ToolManagerComponent', () => {
  let component: ToolManagerComponent;
  let fixture: ComponentFixture<ToolManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, MatDialogModule, FormsModule, ReactiveFormsModule],
      declarations: [ToolManagerComponent, PeResultsViewerComponent],
      providers: [
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        ChangeDetectorRef,
        { provide: ProcessEngineService, useClass: PeEngineServiceStub },
        { provide: MethodologyGuidanceService, useClass: MethodologyGuidanceServiceStub },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load invocations component', () => {
    component.invocations = null;

    component.loadInvocations();

    expect(component.invocations).toBeTruthy();
  });

});
