import { Component, OnInit, Input, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { MatListOption } from '@angular/material/list';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { Invocation } from 'src/app/core/models/invocation';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { AvailableToolSpecification, MethodologyGuidanceService } from '../../services/methodology-guidance/methodology-guidance.service';

@Component({
  selector: 'app-tool-manager',
  templateUrl: './tool-manager.component.html',
  styleUrls: ['./tool-manager.component.scss'],
})
export class ToolManagerComponent implements OnInit, OnChanges {
  @Input() searchableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() viewTools: boolean;

  selectedTool: AvailableToolSpecification;
  showSidebar: boolean = true;
  invocations: Invocation[];

  availableToolsCollection: AvailableToolSpecification[] = [];
  hideTool: string;

  constructor(
    private ref: ChangeDetectorRef,
    private peEngineService: ProcessEngineService,
    private methodologyGuidanceService: MethodologyGuidanceService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.viewTools.currentValue === true) {
      this.selectedTool = undefined;
    }
  }

  ngOnInit(): void {
    this.ref.detectChanges();
    this.loadInvocations();
    this.retrieveAvailableTools();
  }

  onGroupsChangeTools(options: MatListOption[]): void {
    const clickedToolName: string = options.map((o: any) => o.value)[0];
    this.selectedTool = this.availableToolsCollection.find(
      (element: AvailableToolSpecification) => element.toolName === clickedToolName
    );
    if (this.selectedTool) {
      this.viewTools = false;
    }
  }

  loadInvocations(): void {
    this.peEngineService
      .getInvocations(this.projectName)
      .subscribe((invocations: Invocation[]) => {
        this.invocations = invocations;
      });
  }

  retrieveAvailableTools(): void {
    this.methodologyGuidanceService.obtainAvailableTools()
      .subscribe((tools: AvailableToolSpecification[]) => {
        this.availableToolsCollection = tools;
      });
  }

  goBackNavigation(): void {
    this.viewTools = true;
  }
}
