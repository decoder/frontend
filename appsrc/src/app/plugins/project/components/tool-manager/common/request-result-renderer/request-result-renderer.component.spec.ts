import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestResultRendererComponent } from './request-result-renderer.component';

describe('RequestResultRendererComponent', () => {
  let component: RequestResultRendererComponent;
  let fixture: ComponentFixture<RequestResultRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestResultRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestResultRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
