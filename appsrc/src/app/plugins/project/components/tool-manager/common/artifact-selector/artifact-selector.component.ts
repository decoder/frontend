import { EventEmitter, ViewChild, Component, OnInit, Input, Output } from '@angular/core';
import { MatListOption, MatSelectionList } from '@angular/material/list';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { getArtifactsIdByTool } from 'src/app/utils/utils';

@Component({
  selector: 'app-artifact-selector',
  templateUrl: './artifact-selector.component.html',
  styleUrls: ['./artifact-selector.component.scss'],
})
export class ArtifactSelectorComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() toolName: string;
  @Input() type?: string;
  @Input() extension?: string;
  @Input() allowMultiple?: boolean = true;
  @Output() selectedItems: EventEmitter<string[]> = new EventEmitter();
  @ViewChild('artifacts') artifactSelector: MatSelectionList;

  allSelected: boolean = false;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];

  constructor() {
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, this.toolName);
  }

  onArtifactSelection(options: MatListOption[]): void {
    this.selectedArtifacts = options.map((o: MatListOption) => o.value);
    this.selectedItems.emit([ ...this.selectedArtifacts ]);
    if (this.selectableArtifactsId.length === this.selectedArtifacts.length) {
      this.allSelected = true;
    } else {
      this.allSelected = false;
    }
  }

  toggleAllSelection(): void {
    this.allSelected = !this.allSelected;
    if (this.allSelected) {
      this.artifactSelector.options.forEach((item: MatListOption) => item.selected = true);
      this.onArtifactSelection([...this.artifactSelector.options]);

    } else {
      this.artifactSelector.options.forEach((item: MatListOption) => item.selected = false);
      this.selectedArtifacts = [];
      this.onArtifactSelection([]);
    }
  }
}
