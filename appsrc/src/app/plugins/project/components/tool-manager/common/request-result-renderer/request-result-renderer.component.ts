import { Component, Input, OnChanges } from '@angular/core';
import { ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';

@Component({
  selector: 'app-request-result-renderer',
  templateUrl: './request-result-renderer.component.html',
  styleUrls: ['./request-result-renderer.component.scss']
})
export class RequestResultRendererComponent implements OnChanges {

  @Input() requestResponse: ToolInvocationResponse | ToolInvocationResponse[];
  @Input() loading: boolean;

  displayResponse: ToolInvocationResponse[];

  ngOnChanges(): void {
    if (!Array.isArray(this.requestResponse)) {
      this.displayResponse = [this.requestResponse];
    } else {
      this.displayResponse = this.requestResponse;
    }
  }
}
