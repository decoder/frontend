import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatListOption } from '@angular/material/list';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';

@Component({
  selector: 'app-configuration-selector',
  templateUrl: './configuration-selector.component.html',
})
export class ConfigurationSelectorComponent implements OnInit {

  @Input() toolConfig: AvailableToolSpecification;
  @Output() selectedExecutionMode: EventEmitter<string[]> = new EventEmitter<string[]>();
  paramsOptions: string[];
  paramSelected: string[];

  ngOnInit(): void {
    this.paramsOptions = this.toolConfig.endpoint.queryParameters[0].allowedValues;
  }

  onExecutionModeSelection(options: MatListOption[]): void {
    this.paramSelected = options.map((o: MatListOption) => o.value);
    this.selectedExecutionMode.emit([ ...this.paramSelected ]);
  }
}
