import { Component, OnInit, Input } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { MatListOption } from '@angular/material/list';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import {
  ProcessEngineService,
  ToolInvocationBody,
  ToolInvocationResponse,
} from 'src/app/core/services/process-engine/process-engine.service';
import { Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

const FRAMAC_MODES: string[] = ['parser', 'eva', 'wp'];

interface Mode {
  parser: boolean;
  eva: boolean;
  wp: boolean;
}

interface ToolInvocationBodyFramaC extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    source_file_paths: string[];
    mode: Mode;
    options?: {
      args: string[];
      eva: {
        main: string;
      };
      wp: {
        wp_fct: string[];
      };
    };
  };
}

@Component({
  selector: 'app-framac-dashboard',
  templateUrl: './framac.component.html',
  styleUrls: ['./framac.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class FramacComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  modes: string[] = FRAMAC_MODES;
  selectedModes: string[];
  evaOption: string;
  wpOption: string[];
  argsOption: string[];
  loading: boolean;

  framaCInvocationResult: ToolInvocationResponse;

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.modes = FRAMAC_MODES;
    this.selectedModes = [];
    this.wpOption = [];
    this.evaOption = 'main';
    this.argsOption = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(
      this.selectableArtifacts,
      'FramaC'
    );
  }

  onModeSelect(options: MatListOption[]): void {
    this.selectedModes = options.map((o: MatListOption) => o.value);
  }

  isEvaSelected(): boolean {
    return this.selectedModes.some((mode: string) => mode === 'eva');
  }

  isWpaSelected(): boolean {
    return this.selectedModes.some((mode: string) => mode === 'wp');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;

    this._callProcessEngine(this.selectedArtifacts).subscribe(
      (framaCResponses: any) => {
        this.framaCInvocationResult = framaCResponses;
      }
    );

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  obtainObjectFromSelectedModes(): Mode {
    const formatedObject: Mode = {
      parser: false,
      eva: false,
      wp: false,
    };
    this.selectedModes.forEach((element: string) => {
      formatedObject[element] = true;
    });
    return formatedObject;
  }

  calculateStyle(): any {
    return this.selectableArtifactsId.length === 0
      ? { 'artifact-selector-empty': true }
      : { 'artifact-selector': true };
  }

  private _callProcessEngine(
    artifactId: string[]
  ): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyFramaC = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        mode: this.obtainObjectFromSelectedModes(),
        source_file_paths: artifactId,
        options: {
          args: this.parseTextContentToArray(this.argsOption.toString()),
          eva: {
            main: this.evaOption,
          },
          wp: {
            wp_fct: this.parseTextContentToArray(this.wpOption.toString()),
          },
        },
      },
    };

    // make call
    return this.processEngineService.processEngineToolInvocations(
      this.projectName,
      configuration
    );
  }

  private parseTextContentToArray(textContent: string): string[] {
    if (textContent === '') {
      return [];
    } else {
      return textContent.split(',');
    }
  }
}
