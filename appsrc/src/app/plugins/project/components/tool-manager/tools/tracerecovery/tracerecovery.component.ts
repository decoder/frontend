import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ArtifactCommentsSelectorComponent } from '../../../artifact-comments-selector/artifact-comments-selector.component';
import { PKMArtifact } from 'src/app/core/models/artifact';
import {
  AvailableToolSpecification,
  MethodologyGuidanceService
} from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';

interface ToolInvocationBodyTraceRecovery extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    project_id: string;
    src_path: string;
    src_access: string;
    tgt_path: string;
    tgt_access: string
  };
}

@Component({
  selector: 'app-tracerecovery',
  templateUrl: './tracerecovery.component.html',
  styleUrls: ['./tracerecovery.component.scss'],
})
export class TracerecoveryComponent implements OnInit {
  @Input() selectableArtifacts: any;
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  @Output() resetEvent: EventEmitter<any> = new EventEmitter();
  loading: boolean;

  traceRecoveryInvocationResult: ToolInvocationResponse;

  @ViewChild('commentsSelector')
  commentsSelectorComponent: ArtifactCommentsSelectorComponent;

  constructor(
    private methologyGuidanceService: MethodologyGuidanceService,
    private processEngineService: ProcessEngineService
  ) {
    this.selectableArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifacts = this.selectableArtifacts.filter(
      (element: PKMArtifact) =>
        ['Comment', 'Annotation'].some(
          (value: string) => element.type === value
        )
    ) as PKMArtifact;
  }

  onResetEvent(): void {
    this.resetEvent.emit();
  }

  executeTool(): void {
    this.loading = true;

    const configuration: ToolInvocationBodyTraceRecovery = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        project_id: this.projectName,
        src_path: this.methologyGuidanceService.retrievePathFromArtifactType(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.projectName,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
        ),
        src_access: this.methologyGuidanceService.buildAccessFieldContent(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
          this.commentsSelectorComponent.srcSubselected
        ),
        tgt_path: this.methologyGuidanceService.retrievePathFromArtifactType(
          this.commentsSelectorComponent.targetSelectedCommentAnnotation.type,
          this.projectName,
          this.commentsSelectorComponent.targetSelectedCommentAnnotation.name,
        ),
        tgt_access: this.methologyGuidanceService.buildAccessFieldContent(
          this.commentsSelectorComponent.targetSelectedCommentAnnotation.type,
          this.commentsSelectorComponent.targetSelectedCommentAnnotation.name,
          this.commentsSelectorComponent.targetSubselected
        )
      }
    };

    this.loading = true;
    this.processEngineService.processEngineToolInvocations(this.projectName, configuration).subscribe((response: ToolInvocationResponse) => {
      this.traceRecoveryInvocationResult = response;
      this.loading = false;
    });
  }
}
