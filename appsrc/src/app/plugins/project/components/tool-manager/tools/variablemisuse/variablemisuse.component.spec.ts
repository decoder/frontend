import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariablemisuseComponent } from './variablemisuse.component';
import {
  ProcessEngineService,
  ToolInvocationResponse
} from '../../../../../../core/services/process-engine/process-engine.service';
import { Observable, of } from 'rxjs';

class ProcessEngineServiceStub {
  processEngineToolInvocations(): Observable<ToolInvocationResponse> {
    return of(
      {
        invocationID: 'testIdGenerated',
        success: true,
      }
    );
  }
}

describe('VariablemisuseComponent', () => {
  let component: VariablemisuseComponent;
  let fixture: ComponentFixture<VariablemisuseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariablemisuseComponent ],
      providers: [
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariablemisuseComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
