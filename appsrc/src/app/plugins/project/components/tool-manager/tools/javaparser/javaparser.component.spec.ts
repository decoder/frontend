import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JavaparserComponent } from './javaparser.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';

describe('JavaparserComponent', () => {
  let component: JavaparserComponent;
  let fixture: ComponentFixture<JavaparserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JavaparserComponent ],
      providers: [
        {
          provide: MethodologyGuidanceService,
        },
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JavaparserComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: 'JavaParser'
    };
    component.selectableArtifacts = pkmArtifactsMock;
    component.executionParam = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve the available artifacts for javaparser on init (.java files)', () => {
    component.ngOnInit();
    expect(component.selectableArtifactsId.length).toBe(1);
    expect(component.selectableArtifactsId[0]).toBe(pkmArtifactsMock[0].sourceFile);
  });

  it('should make the call to the service when item is selected', () => {
    const selectedMode: string = 'all';
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];
    component.executionParam = selectedMode;

    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.javaParserInvocationResult.length).toBe(1);
    expect(component.javaParserInvocationResult[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

  it('should change selectedArtifacts on selection', () => {
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;

    component.checkSelectedItems([selectedArtifactId]);
    expect(component.selectedArtifacts).toEqual([selectedArtifactId]);
  });

  it('should execute tool when selecting mode', () => {
    const selectedMode: string = 'ast';
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];

    spyOn(component, 'nextStep');
    component.checkSelectionExecutionMode(selectedMode, new MatHorizontalStepper(null, null, null, null));
    expect(component.javaParserInvocationResult.length).toBe(1);
    expect(component.javaParserInvocationResult[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

});
