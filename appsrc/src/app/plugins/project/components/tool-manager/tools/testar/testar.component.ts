import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input, OnInit } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import {
  AvailableToolSpecification
} from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

@Component({
  selector: 'app-testar',
  templateUrl: './testar.component.html',
  styleUrls: ['./testar.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class TestarComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  loading: boolean;

  testarInvocationResult: ToolInvocationResponse;
  testarConfig: object = {};

  constructor(
    private processEngineService: ProcessEngineService,
    private artifactRetrieverService: ArtifactsRetrieverService
  ) {
    super();
    this.loading = false;
  }

  ngOnInit(): void {
    this.artifactRetrieverService
      .getTestarConfig(this.projectName)
      .subscribe((config: object) => (this.testarConfig = config));
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const configuration: any = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        ...this.testarConfig,
        PKMkey: localStorage.getItem('access_token'),
      },
    };
    // make call
    this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration)
      .subscribe(
        (result: ToolInvocationResponse) =>
          (this.testarInvocationResult = result)
      );

    this.loading = false;
    this.nextStep(stepper);
  }

}
