import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenjmlComponent } from './openjml.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';

describe('OpenjmlComponent', () => {
  let component: OpenjmlComponent;
  let fixture: ComponentFixture<OpenjmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenjmlComponent ],
      providers: [
        {
          provide: MethodologyGuidanceService,
        },
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenjmlComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: 'OpenJML'
    };
    component.selectableArtifacts = pkmArtifactsMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve the available artifacts for openjml on init (.java files)', () => {
    component.ngOnInit();
    expect(component.selectableArtifactsId.length).toBe(1);
    expect(component.selectableArtifactsId[0]).toBe(pkmArtifactsMock[0].sourceFile);
  });

  it('should make the call to the service when item is selected', () => {
    const selectedMode: string = 'typeChecking';
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];
    component.executionParam = selectedMode;

    spyOn(component, 'nextStep');
    component.executeTool(new MatHorizontalStepper(null, null, null, null));
    expect(component.openJMLInvocationResult.length).toBe(1);
    expect(component.openJMLInvocationResult[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

  it('should change selectedArtifacts on selection', () => {
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;

    component.checkSelectedItems([selectedArtifactId]);
    expect(component.selectedArtifacts).toEqual([selectedArtifactId]);
  });

  it('should execute tool when selecting mode', () => {
    const selectedMode: string = 'typeChecking';
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];

    spyOn(component, 'nextStep');
    component.checkSelectionExecutionMode(selectedMode, new MatHorizontalStepper(null, null, null, null));
    expect(component.openJMLInvocationResult.length).toBe(1);
    expect(component.openJMLInvocationResult[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });
});
