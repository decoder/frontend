import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JmlgenComponent } from './jmlgen.component';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';

describe('JmlgenComponent', () => {
  let component: JmlgenComponent;
  let fixture: ComponentFixture<JmlgenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JmlgenComponent ],
      providers: [
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JmlgenComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should execute tool', () => {
    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.jmlGenInvocationResult).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

  it('should execute executeTool function when "Execute tool" is clicked', async () => {
    spyOn(component, 'nextStep');
    spyOn(component, 'executeTool');
    const button: HTMLElement = fixture.debugElement.nativeElement.querySelector('.header-button');
    button.click();
    expect(component.executeTool).toHaveBeenCalled();
  });

});
