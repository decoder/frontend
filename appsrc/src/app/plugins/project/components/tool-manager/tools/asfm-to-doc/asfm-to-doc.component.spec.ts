import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsfmToDocComponent } from './asfm-to-doc.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of } from 'rxjs';

class ProcessEngineServiceStub {
  processEngineToolInvocations(): Observable<ToolInvocationResponse> {
    return of(
      {
        invocationID: 'testIdGenerated',
        success: true,
      }
    );
  }
}

describe('DocToASFMComponent', () => {
  let component: AsfmToDocComponent;
  let fixture: ComponentFixture<AsfmToDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsfmToDocComponent ],
      providers: [
        {
          provide: MethodologyGuidanceService,
        },
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsfmToDocComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    component.executionParam = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
