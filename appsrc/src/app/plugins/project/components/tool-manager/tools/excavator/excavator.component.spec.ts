import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AppConfiguration } from 'src/config/app-configuration';

import { ExcavatorComponent } from './excavator.component';

describe('ExcavatorComponent', () => {
  let component: ExcavatorComponent;
  let fixture: ComponentFixture<ExcavatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatSnackBarModule, HttpClientTestingModule ],
      declarations: [ ExcavatorComponent ],
      providers: [ AppConfiguration ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcavatorComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
