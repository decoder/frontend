import { Component, Input } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyExcavator extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    binary: string;
    sources: string[];
    suppress_types: string[];
    suppress_functions: string[];
    output_dir: string;
  };
}

@Component({
  selector: 'app-excavator',
  templateUrl: './excavator.component.html',
  styleUrls: ['./excavator.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class ExcavatorComponent extends DefaultToolBaseComponent {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedExecutableFile: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;

  sources: string;
  suppress_types: string;
  suppress_functions: string;
  output_dir: string;
  excavatorInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedExecutableFile = [];
    this.loading = false;
    this.sources = '';
    this.suppress_types = '';
    this.suppress_functions = '';
    this.output_dir = '';
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const excavatorObservables: Observable<ToolInvocationResponse>[] =
    this.selectedExecutableFile.map((file: string) => {
      return this._callProcessEngine(file);
    });

    forkJoin(excavatorObservables).subscribe((excavatorResponses: any) => {
      this.excavatorInvocationResult = excavatorResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedExecutableFile = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private formatInput(input: string): string[]  {
    // remove apostrophes and blank spaces
    return input.replace(/("|\')/g, '').replace(/(\n|\r|\t|\s)/gm, '').split(',');
  }

  private _callProcessEngine(file: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyExcavator = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        binary: file,
        sources: this.formatInput(this.sources),
        suppress_types: this.formatInput(this.suppress_types),
        suppress_functions: this.formatInput(this.suppress_functions),
        output_dir: this.output_dir
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }
}
