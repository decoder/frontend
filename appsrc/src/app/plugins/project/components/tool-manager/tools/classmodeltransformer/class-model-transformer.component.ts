import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input, OnInit } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { forkJoin, Observable } from 'rxjs';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { ProcessEngineService, ToolInvocationBody, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyClassModelTransformer extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string,
    sourceFileName: string,
  };
}
@Component({
  selector: 'app-class-model-transformer',
  templateUrl: './class-model-transformer.component.html',
  styleUrls: ['./class-model-transformer.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class ClassModelTransformerComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  classModelTransformerResponses: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'ClassModelTransformer');
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const classModelTransformerObservables: Observable<ToolInvocationResponse>[] =
    this.selectedArtifacts.map((file: string) => {
      return this._callProcessEngine(file);
    });

    forkJoin(classModelTransformerObservables).subscribe((classModelTransformerResponses: any) => {
      this.classModelTransformerResponses = classModelTransformerResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  private _callProcessEngine(file: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyClassModelTransformer = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        sourceFileName: file,
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }
}
