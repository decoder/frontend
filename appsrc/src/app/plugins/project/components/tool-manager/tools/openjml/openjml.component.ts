import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ProcessEngineService, ToolInvocationBody, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyOpenJML extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    sourceFileName: string;
    tool: string;
  };
}

@Component({
  selector: 'app-openjml',
  templateUrl: './openjml.component.html',
  styleUrls: ['./openjml.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})

export class OpenjmlComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;

  openJMLInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'OpenJML');
  }

  executeTool(stepper: MatHorizontalStepper): void {
    this.loading = true;
    const openJmlObservables: Observable<ToolInvocationResponse>[] =
    this.selectedArtifacts.map((file: string) => {
      return this._callProcessEngine(file);
    });

    forkJoin(openJmlObservables).subscribe((openJmlResponses: any) => {
      this.openJMLInvocationResult = openJmlResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private _callProcessEngine(file: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyOpenJML = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        sourceFileName: file,
        tool: this.executionParam[0]
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }
}
