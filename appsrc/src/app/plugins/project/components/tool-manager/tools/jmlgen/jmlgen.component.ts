import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService, ToolInvocationBody, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyJMLGen extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
  };
}

@Component({
  selector: 'app-jmlgen',
  templateUrl: './jmlgen.component.html',
  styleUrls: ['./jmlgen.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class JmlgenComponent extends DefaultToolBaseComponent {
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  loading: boolean;

  jmlGenInvocationResult: ToolInvocationResponse;

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.loading = false;
  }

  executeTool(stepper: MatStepper): void {
    const configuration: ToolInvocationBodyJMLGen = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
      }
    };
    // make call to process engine
    this.loading = true;
    this.processEngineService.processEngineToolInvocations(this.projectName, configuration).subscribe((response: ToolInvocationResponse) => {
      this.jmlGenInvocationResult = response;
      this.loading = false;
    });
    this.nextStep(stepper);
  }

}
