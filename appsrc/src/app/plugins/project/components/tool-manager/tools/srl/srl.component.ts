import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ArtifactCommentsSelectorComponent } from '../../../artifact-comments-selector/artifact-comments-selector.component';
import { PKMArtifact } from 'src/app/core/models/artifact';
import {
  AvailableToolSpecification,
  MethodologyGuidanceService
} from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';

interface ToolInvocationBodySRL extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    project_id: string;
    path: string;
    access: string;
  };
}

@Component({
  selector: 'app-srl',
  templateUrl: './srl.component.html',
  styleUrls: ['./srl.component.scss'],
})
export class SrlComponent implements OnInit {
  @Input() selectableArtifacts: any;
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  @Output() resetEvent: EventEmitter<any> = new EventEmitter();
  loading: boolean;

  srlInvocationResult: ToolInvocationResponse;

  @ViewChild('commentsSelector')
  commentsSelectorComponent: ArtifactCommentsSelectorComponent;

  constructor(
    private methologyGuidanceService: MethodologyGuidanceService,
    private processEngineService: ProcessEngineService) {
    this.selectableArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifacts = this.selectableArtifacts.filter(
      (element: PKMArtifact) =>
        ['Comment', 'Annotation'].some(
          (value: string) => element.type === value
        )
    ) as PKMArtifact;
  }

  onResetEvent(): void {
    this.resetEvent.emit();
  }

  executeTool(): void {
    this.loading = true;
    const configuration: ToolInvocationBodySRL = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        project_id: this.projectName,
        path: this.methologyGuidanceService.retrievePathFromArtifactType(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.projectName,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
        ),
        access: this.methologyGuidanceService.buildAccessFieldContent(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
          this.commentsSelectorComponent.srcSubselected
        ),
      }
    };

    this.loading = true;
    this.processEngineService.processEngineToolInvocations(this.projectName, configuration).subscribe((response: ToolInvocationResponse) => {
      this.srlInvocationResult = response;
      this.loading = false;
    });
  }
}
