import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppConfiguration } from 'src/config/app-configuration';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ClassModelTransformerComponent } from './class-model-transformer.component';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';
import { MatStepper } from '@angular/material/stepper';

describe('ClassModelTransformerComponent', () => {
  let component: ClassModelTransformerComponent;
  let fixture: ComponentFixture<ClassModelTransformerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClassModelTransformerComponent],
      providers: [
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub },
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassModelTransformerComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: '',
        },
        queryParameters: [
          {
            allowedValues: [''],
            description: '',
            name: 'test',
            required: false,
            type: '',
          },
        ],
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: 'ClassModelTransformer',
    };
    component.selectableArtifacts = [
      {
        id: 'artifactTest.uml',
        content: '',
        encoding: 'utf-8',
        format: '',
        git_commit_id: '',
        mime_type: '',
        git_repository_url: '',
        name: 'artifactTest.uml',
        rel_path: 'artifactTest.uml',
        sourceFile: 'artifactTest.uml',
        type: 'Diagram',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve the available artifacts for ClassModelXmiToJson on init (Diagram files)', () => {
    component.ngOnInit();
    expect(component.selectableArtifactsId.length).toBe(1);
    expect(component.selectableArtifactsId[0]).toBe('artifactTest.uml');
  });

  it('should make the call to the service when item is selected', () => {
    const selectedArtifactId: string = 'artifactTest.uml';
    component.selectedArtifacts = [selectedArtifactId];

    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.classModelTransformerResponses.length).toBe(1);
    expect(component.classModelTransformerResponses[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });
});
