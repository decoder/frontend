import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NerComponent } from './ner.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { asyncScheduler, scheduled, of } from 'rxjs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

export function getMockMethodologyGuidance(): Partial<MethodologyGuidanceService> {
  return {
    nerAndSrlSingleRequest: jasmine
      .createSpy('nerAndSrlSingleRequest')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
  };
}

describe('NerComponent', () => {
  let component: NerComponent;
  let fixture: ComponentFixture<NerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NerComponent ],
      providers: [
        {
          provide: MethodologyGuidanceService,
          useValue: getMockMethodologyGuidance(),
        },
        AppConfiguration
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    component.selectableArtifacts = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
