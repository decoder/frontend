import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { Observable, forkJoin } from 'rxjs';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService, ToolInvocationBody, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodySemanticParser extends ToolInvocationBody {
  invocationConfiguration: {
    file: string,
    source_language: string,
    target_language: string,
    project_id: string,
  };
}

@Component({
  selector: 'app-semanticparser',
  templateUrl: './semanticparser.component.html',
  styleUrls: ['./semanticparser.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class SemanticparserComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;

  semanticParserResponses: ToolInvocationResponse[];

  constructor(private appConfig: AppConfiguration, private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'SemanticParser');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const semanticParserObservables: Observable<ToolInvocationResponse>[] =
    this.selectedArtifacts.map((file: string) => {
      const extension: string = file.split('.').pop();
      if (extension === 'c') {
        return this._callProcessEngine(file, extension, 'acsl');
      } else if (extension === 'cpp') {
        return this._callProcessEngine(file, extension, 'acslpp');
      } else if (extension === 'java') {
        return this._callProcessEngine(file, extension, 'jml');
      }
    });

    forkJoin(semanticParserObservables).subscribe((semanticParserResponses: any) => {
      this.semanticParserResponses = semanticParserResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  private _callProcessEngine(file: string, srcLang: string, trgLang: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodySemanticParser = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        file,
        source_language: srcLang,
        target_language: trgLang,
        project_id: this.projectName,
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }
}
