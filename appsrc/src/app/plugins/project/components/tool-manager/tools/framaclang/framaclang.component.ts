import { Component, OnInit, Input } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ToolInvocationResponse, ToolInvocationBody, ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyFramaCLang extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    source_file_paths: string[];
  };
}

@Component({
  selector: 'app-framaclang',
  templateUrl: './framaclang.component.html',
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class FramacLangComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;

  framaCLangInvocationResult: ToolInvocationResponse;

  constructor(
    private appConfig: AppConfiguration,
    private processEngineService: ProcessEngineService
  ) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'FramaCLang');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const configuration: ToolInvocationBodyFramaCLang = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        source_file_paths: [...this.selectedArtifacts],
      }
    };

    this.loading = true;
    this.processEngineService.processEngineToolInvocations(this.projectName, configuration).subscribe((response: ToolInvocationResponse) => {
      this.framaCLangInvocationResult = response;
      this.loading = false;
    });

    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  calculateStyle(): any {
    return this.selectableArtifactsId.length === 0
      ? { 'artifact-selector-empty': true }
      : { 'artifact-selector': true };
  }

}
