import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodesumarizationComponent } from './codesumarization.component';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';

describe('CodesumarizationComponent', () => {
  let component: CodesumarizationComponent;
  let fixture: ComponentFixture<CodesumarizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodesumarizationComponent ],
      providers: [
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodesumarizationComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: 'code-summarization-file',
      toolName: 'CodeSumarization'
    };
    component.selectableArtifacts = pkmArtifactsMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should make the call to the service when item is selected', () => {
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];

    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.codeSumarizationInvocationResult.length).toBe(1);
    expect(component.codeSumarizationInvocationResult[0]).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

  it('should change selectedArtifacts on selection', () => {
    const selectedArtifactId: string = pkmArtifactsMock[0].sourceFile;

    component.checkSelectedItems([selectedArtifactId]);
    expect(component.selectedArtifacts).toEqual([selectedArtifactId]);
  });

});
