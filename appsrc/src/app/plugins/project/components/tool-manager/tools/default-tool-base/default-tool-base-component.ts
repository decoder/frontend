import { Component, EventEmitter, Output } from '@angular/core';
import { CdkStep } from '@angular/cdk/stepper';
import { MatStep, MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-default-tool-base',
  template: ''
})
export class DefaultToolBaseComponent {
  @Output() resetEvent: EventEmitter<any> = new EventEmitter();

  nextStep(stepper: MatStepper): void {
    const steps: CdkStep[] = stepper.steps.toArray();

    // completes previous step
    const previousStep: CdkStep = stepper.selected;
    previousStep.completed = true;

    // updates step
    stepper.next();
    const currentStep: CdkStep = stepper.selected;
    const currentIndex: number = stepper.selectedIndex;

    // if last step: set state to all-done and steps are no longer editable
    if (currentIndex === steps.length - 1) {
      currentStep.state = 'all-done';
      steps.forEach( (step: CdkStep) => step.editable = false );
    } else {
      // number state: touched but not completed yet.
      currentStep.state = 'number';
      // consecutive steps will be reset.
      for (let i: number = currentIndex; i < steps.length - 1; i++) {
        steps[i + 1].completed = false;
        steps[i + 1].state = 'null';
      }
    }
  }

  reset(stepper: MatStepper): void {
    const allSteps: MatStep[] = stepper._steps.toArray();
    allSteps.forEach((step: MatStep) => {
      step.completed = false;
      step.editable = true;
      step.state = 'null';
    });
    stepper.reset();
    this.resetEvent.emit();
  }
}
