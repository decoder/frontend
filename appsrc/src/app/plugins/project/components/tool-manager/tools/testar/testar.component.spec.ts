import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestarComponent } from './testar.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { Observable, of } from 'rxjs';

class ArtifactsRetrieverServiceStub {
  getTestarConfig(projectName: string): Observable<any> {
    return of({PKMKey: '', PKMDatabase: 'testDatabase', PKMUser: 'testUser'});
  }
}

describe('TestarComponent', () => {
  let component: TestarComponent;
  let fixture: ComponentFixture<TestarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestarComponent ],
      providers: [
        MethodologyGuidanceService,
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub },
        { provide: ArtifactsRetrieverService, useClass: ArtifactsRetrieverServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestarComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: 'Testar'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve testar configuration on initialization of the component (on init)', () => {
    component.ngOnInit();
    expect(component.testarConfig).toEqual({
      PKMKey: '',
      PKMDatabase: 'testDatabase',
      PKMUser: 'testUser'
    });
  });

  it('should execute tool', () => {
    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.testarInvocationResult).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });
});
