import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyASFMToDoc extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    docName: string;
  };
}

@Component({
  selector: 'app-asfm-to-doc',
  templateUrl: './asfm-to-doc.component.html',
  styleUrls: ['./asfm-to-doc.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class AsfmToDocComponent extends DefaultToolBaseComponent implements OnInit {

  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;

  asfmToDocInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'ASFMToDoc');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const asfmoDocObservables: Observable<ToolInvocationResponse>[] =
    this.selectedArtifacts.map((file: string) => {
      return this._callProcessEngine(file);
    });

    forkJoin(asfmoDocObservables).subscribe((asfmToDocResponses: any) => {
      this.asfmToDocInvocationResult = asfmToDocResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private _callProcessEngine(file: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyASFMToDoc = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        docName: file,
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }

}
