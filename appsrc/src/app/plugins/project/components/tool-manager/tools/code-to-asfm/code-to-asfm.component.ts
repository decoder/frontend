import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodycodeToASFM extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
  };
}
@Component({
  selector: 'app-code-to-asfm',
  templateUrl: './code-to-asfm.component.html',
  styleUrls: ['./code-to-asfm.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class CodeToASFMComponent extends DefaultToolBaseComponent implements OnInit {

  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;
  haveCFiles: boolean = false;

  codeToASFMInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'codeToASFM');
    if (this.selectableArtifactsId.length > 0) {
      this.haveCFiles = true;
    }
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const codeToASFMObservables: Observable<ToolInvocationResponse> =
     this._callProcessEngine();

    codeToASFMObservables.subscribe((codeToASFMResponses: any) => {
      this.codeToASFMInvocationResult = codeToASFMResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private _callProcessEngine(): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodycodeToASFM = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }

}
