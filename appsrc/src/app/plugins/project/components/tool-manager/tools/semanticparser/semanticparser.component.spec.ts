import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticparserComponent } from './semanticparser.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('SemanticparserComponent', () => {
  let component: SemanticparserComponent;
  let fixture: ComponentFixture<SemanticparserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticparserComponent ],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: MethodologyGuidanceService,
        },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        AppConfiguration
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticparserComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
