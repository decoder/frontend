import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TracerecoveryComponent } from './tracerecovery.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { of, Observable } from 'rxjs';
import { AppConfiguration } from 'src/config/app-configuration';
import { ToolInvocationResponse, ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

class ProcessEngineServiceStub {
  processEngineToolInvocations(): Observable<ToolInvocationResponse> {
    return of(
      {
        invocationID: 'testIdGenerated',
        success: true,
      }
    );
  }
}

describe('TracerecoveryComponent', () => {
  let component: TracerecoveryComponent;
  let fixture: ComponentFixture<TracerecoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TracerecoveryComponent ],
      providers: [
        MethodologyGuidanceService,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub },
        AppConfiguration
      ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TracerecoveryComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
