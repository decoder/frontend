import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrlComponent } from './srl.component';
import { MethodologyGuidanceService } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { asyncScheduler, scheduled, of } from 'rxjs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

export function getMockMethodologyGuidance(): Partial<MethodologyGuidanceService> {
  return {
    nerAndSrlSingleRequest: jasmine
      .createSpy('nerAndSrlSingleRequest')
      .and.returnValue(scheduled(of([]), asyncScheduler)),
  };
}

describe('SrlComponent', () => {
  let component: SrlComponent;
  let fixture: ComponentFixture<SrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrlComponent ],
      providers: [
        {
          provide: MethodologyGuidanceService,
          useValue: getMockMethodologyGuidance(),
        },
        AppConfiguration
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrlComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        },
        queryParameters: [{
          allowedValues: [''],
          description: '',
          name: 'test',
          required: false,
          type: ''
        }]
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
