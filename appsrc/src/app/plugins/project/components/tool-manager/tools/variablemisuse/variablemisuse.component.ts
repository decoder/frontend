import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input, OnInit } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { forkJoin, Observable } from 'rxjs';
import { PKMArtifact } from 'src/app/core/models/artifact';
import {
  ProcessEngineService,
  ToolInvocationBody,
  ToolInvocationResponse,
} from 'src/app/core/services/process-engine/process-engine.service';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyVariableMisuse extends ToolInvocationBody {
  invocationConfiguration: {
    collectionId: string;
    artefactID?: string;
  };
}

@Component({
  selector: 'app-variablemisuse',
  templateUrl: './variablemisuse.component.html',
  styleUrls: ['./variablemisuse.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class VariablemisuseComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  executionParam: string;

  variableMisuseInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(
      this.selectableArtifacts,
      'VariableMisuse'
    );
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    if (this.selectedTool.toolID.includes('file')) {
      const variableMisuseObservables: Observable<ToolInvocationResponse>[] =
        this.selectedArtifacts.map((file: string) => {
          return this._callProcessEngine(file);
        });
      forkJoin(variableMisuseObservables).subscribe((responses: any) => {
        this.variableMisuseInvocationResult = responses;
      });
    } else {
      this._callProcessEngine().subscribe((responses: any) => {
        this.variableMisuseInvocationResult = responses;
      });
    }
    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  calculateStyle(): any {
    return this.selectableArtifactsId.length === 0
      ? { 'artifact-selector-empty': true }
      : { 'artifact-selector': true };
  }

  private _callProcessEngine(
    file?: string
  ): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyVariableMisuse = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: this.selectedTool.toolID.includes('file')
        ? {
            collectionId: this.projectName,
            artefactID: file,
          }
        : {
            collectionId: this.projectName,
          },
    };

    // make call
    return this.processEngineService.processEngineToolInvocations(
      this.projectName,
      configuration
    );
  }
}
