import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { ArtifactCommentsSelectorComponent } from '../../../artifact-comments-selector/artifact-comments-selector.component';
import {
  AvailableToolSpecification,
  MethodologyGuidanceService
} from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { ToolInvocationBody, ToolInvocationResponse, ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';

interface ToolInvocationBodyNER extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    project_id: string;
    path: string;
    access: string;
    model?: string;
  };
}

@Component({
  selector: 'app-ner',
  templateUrl: './ner.component.html',
  styleUrls: ['./ner.component.scss'],
})
export class NerComponent implements OnInit {
  @Input() selectableArtifacts: any;
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  loading: boolean;
  @Output() resetEvent: EventEmitter<any> = new EventEmitter();

  nerInvocationResult: ToolInvocationResponse;

  @ViewChild('commentsSelector')
  commentsSelectorComponent: ArtifactCommentsSelectorComponent;

  constructor(
    private methologyGuidanceService: MethodologyGuidanceService,
    private processEngineService: ProcessEngineService
    ) {
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifacts = this.selectableArtifacts.filter(
      (element: PKMArtifact) =>
        ['Comment', 'Annotation'].some(
          (value: string) => element.type === value
        )
    ) as PKMArtifact;
  }

  onResetEvent(): void {
    this.resetEvent.emit();
  }

  executeTool(): void {
    this.loading = true;
    const configuration: ToolInvocationBodyNER =
    this.commentsSelectorComponent.selectedModel ? {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        project_id: this.projectName,
        path: this.methologyGuidanceService.retrievePathFromArtifactType(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.projectName,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
        ),
        access: this.methologyGuidanceService.buildAccessFieldContent(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
          this.commentsSelectorComponent.srcSubselected
        ),
        model: this.commentsSelectorComponent.selectedModel
      }
    } : {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        project_id: this.projectName,
        path: this.methologyGuidanceService.retrievePathFromArtifactType(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.projectName,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
        ),
        access: this.methologyGuidanceService.buildAccessFieldContent(
          this.commentsSelectorComponent.selectedCommentAnnotation.type,
          this.commentsSelectorComponent.selectedCommentAnnotation.name,
          this.commentsSelectorComponent.srcSubselected
        )
      }
    };

    this.processEngineService.processEngineToolInvocations(this.projectName, configuration).subscribe((response: ToolInvocationResponse) => {
      this.nerInvocationResult = response;
      this.loading = false;
    });
  }
}
