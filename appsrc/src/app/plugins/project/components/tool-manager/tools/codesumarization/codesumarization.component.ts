import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import {
  ProcessEngineService,
  ToolInvocationBody,
  ToolInvocationResponse,
} from 'src/app/core/services/process-engine/process-engine.service';
import { MatStepper } from '@angular/material/stepper';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyCodeSumarization extends ToolInvocationBody {
  invocationConfiguration: {
    collectionId: string;
    artefactID?: string;
  };
}

@Component({
  selector: 'app-codesumarization',
  templateUrl: './codesumarization.component.html',
  styleUrls: ['./codesumarization.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class CodesumarizationComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;
  @Input() toolMode: string;
  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  executionParam: string;

  codeSumarizationInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(
      this.selectableArtifacts,
      'CodeSumarization'
    );
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    if (this.selectedTool.toolID.includes('file')) {
      const sumarizationObservables: Observable<ToolInvocationResponse>[] =
        this.selectedArtifacts.map((file: string) => {
          return this._callProcessEngine(file);
        });
      forkJoin(sumarizationObservables).subscribe((responses: any) => {
        this.codeSumarizationInvocationResult = responses;
      });
    } else {
      this._callProcessEngine().subscribe((responses: any) => {
        this.codeSumarizationInvocationResult = responses;
      });
    }
    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  calculateStyle(): any {
    return this.selectableArtifactsId.length === 0
      ? { 'artifact-selector-empty': true }
      : { 'artifact-selector': true };
  }

  private _callProcessEngine(
    file?: string
  ): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyCodeSumarization = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: this.selectedTool.toolID.includes('file')
        ? {
            collectionId: this.projectName,
            artefactID: file,
          }
        : {
            collectionId: this.projectName,
          },
    };

    // make call
    return this.processEngineService.processEngineToolInvocations(
      this.projectName,
      configuration
    );
  }
}
