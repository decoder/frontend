import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodydocToASFM extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    file_path: string;
  };
}

@Component({
  selector: 'app-doc-to-asfm',
  templateUrl: './doc-to-asfm.component.html',
  styleUrls: ['./doc-to-asfm.component.scss']
})
export class DocToASFMComponent extends DefaultToolBaseComponent implements OnInit {

  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;

  docToASFMInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'docToASFM');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const docToASFMObservables: Observable<ToolInvocationResponse>[] =
    this.selectedArtifacts.map((file: string) => {
      return this._callProcessEngine(file);
    });

    forkJoin(docToASFMObservables).subscribe((docToASFMResponses: any) => {
      this.docToASFMInvocationResult = docToASFMResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private _callProcessEngine(file: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodydocToASFM = {
      tool: this.selectedTool.toolID,
      user: localStorage.getItem('currentUser'),
      invocationConfiguration: {
        dbName: this.projectName,
        file_path: file,
      }
    };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }

}
