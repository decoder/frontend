import { Component, Input, OnInit } from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { AvailableToolSpecification } from 'src/app/plugins/project/services/methodology-guidance/methodology-guidance.service';
import { MatHorizontalStepper, MatStepper } from '@angular/material/stepper';
import { getArtifactsIdByTool } from 'src/app/utils/utils';
import { ToolInvocationBody, ProcessEngineService, ToolInvocationResponse } from 'src/app/core/services/process-engine/process-engine.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { forkJoin, Observable } from 'rxjs';
import { DefaultToolBaseComponent } from '../default-tool-base/default-tool-base-component';

interface ToolInvocationBodyJavaParser extends ToolInvocationBody {
  invocationConfiguration: {
    dbName: string;
    sourceFileName?: string;
    generate: string;
  };
}

@Component({
  selector: 'app-javaparser',
  templateUrl: './javaparser.component.html',
  styleUrls: ['./javaparser.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class JavaparserComponent extends DefaultToolBaseComponent implements OnInit {
  @Input() selectableArtifacts: PKMArtifact[];
  @Input() projectName: string;
  @Input() selectedTool: AvailableToolSpecification;

  selectableArtifactsId: string[];
  selectedArtifacts: string[];
  loading: boolean;
  selectedModeValue: string;
  executionParam: string;

  javaParserInvocationResult: ToolInvocationResponse[];

  constructor(private processEngineService: ProcessEngineService) {
    super();
    this.selectableArtifacts = [];
    this.selectableArtifactsId = [];
    this.selectedArtifacts = [];
    this.loading = false;
  }

  ngOnInit(): void {
    this.selectableArtifactsId = getArtifactsIdByTool(this.selectableArtifacts, 'Javaparser');
  }

  executeTool(stepper: MatStepper): void {
    this.loading = true;
    const javaParserObservables: Observable<ToolInvocationResponse>[] =
      this.selectedTool.toolID === 'javaParser' ?
        this.selectedArtifacts.map((file: string) => {
          return this._callProcessEngine(file);
        })
      : [this._callProcessEngine()];

    forkJoin(javaParserObservables).subscribe((javaParserResponses: any) => {
      this.javaParserInvocationResult = javaParserResponses;
    });

    this.loading = false;
    this.nextStep(stepper);
  }

  checkSelectedItems(event: any): void {
    this.selectedArtifacts = event;
  }

  checkSelectionExecutionMode(event: any, stepper: MatHorizontalStepper): void {
    this.executionParam = event;
    this.executeTool(stepper);
  }

  private _callProcessEngine(file?: string): Observable<ToolInvocationResponse> {
    const configuration: ToolInvocationBodyJavaParser =
      this.selectedTool.toolID === 'javaParser' ? {
        tool: this.selectedTool.toolID,
        user: localStorage.getItem('currentUser'),
        invocationConfiguration: {
          dbName: this.projectName,
          sourceFileName: file,
          generate: this.executionParam[0]
        }
      } : {
        tool: this.selectedTool.toolID,
        user: localStorage.getItem('currentUser'),
        invocationConfiguration: {
          dbName: this.projectName,
          generate: this.executionParam[0]
        }
      };
    // make call
    return this.processEngineService
      .processEngineToolInvocations(this.projectName, configuration);
  }
}
