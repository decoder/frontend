import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FramacLangComponent } from './framaclang.component';
import { AppConfiguration } from 'src/config/app-configuration';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FramacLangComponent', () => {
  let component: FramacLangComponent;
  let fixture: ComponentFixture<FramacLangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FramacLangComponent ],
      providers: [
        ProcessEngineService,
        AppConfiguration
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FramacLangComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
