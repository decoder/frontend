import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FramacComponent } from './framac.component';
import { AppConfiguration } from 'src/config/app-configuration';
import { ProcessEngineService } from 'src/app/core/services/process-engine/process-engine.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { MatStepper } from '@angular/material/stepper';
import { ProcessEngineServiceStub } from 'src/app/plugins/project/testing/services/process-engine.service.mock';

describe('FramacComponent', () => {
  let component: FramacComponent;
  let fixture: ComponentFixture<FramacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FramacComponent ],
      providers: [
        ProcessEngineService,
        AppConfiguration,
        { provide: ProcessEngineService, useClass: ProcessEngineServiceStub }
      ],
      imports: [MatSnackBarModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FramacComponent);
    component = fixture.componentInstance;
    component.selectedTool = {
      description: '',
      endpoint: {
        method: '',
        path: '',
        pathFields: {
          description: '',
          name: '',
          required: true,
          type: ''
        }
      },
      phases: [],
      server: '',
      tasks: [],
      toolID: '',
      toolName: 'FramaC'
    };
    component.selectableArtifacts = pkmArtifactsMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve the available artifacts for framac on init (.c files)', () => {
    component.ngOnInit();
    expect(component.selectableArtifactsId.length).toBe(1);
    expect(component.selectableArtifactsId[0]).toBe(pkmArtifactsMock[1].sourceFile);
  });

  it('should make the call to the service when item is selected', () => {
    const evaOption: string = 'main';
    const wpOption: string[] = ['testFunction'];
    const selectedArtifactId: string = pkmArtifactsMock[1].sourceFile;
    component.selectedArtifacts = [selectedArtifactId];
    component.evaOption = evaOption;
    component.wpOption = wpOption;

    spyOn(component, 'nextStep');
    component.executeTool(new MatStepper(null, null, null, null));
    expect(component.framaCInvocationResult).toEqual({
      invocationID: 'testIdGenerated',
      success: true,
    });
  });

});
