import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ArtifactTreeComponent } from './artifact-tree.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ArtifactNode } from '../../models/artifact-node.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ArtifactTreeComponent', () => {
  let component: ArtifactTreeComponent;
  let fixture: ComponentFixture<ArtifactTreeComponent>;

  beforeEach(async(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 11000;
    TestBed.configureTestingModule({
      declarations: [ArtifactTreeComponent],
      imports: [MatTreeModule, MatButtonModule, MatIconModule, HttpClientTestingModule]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ArtifactTreeComponent);
        component = fixture.componentInstance;
        component.timeOutSpinnerLoader = () => { return; };
        fixture.detectChanges();

      });
  }));

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should set the selected item when clicked', fakeAsync(() => {
    const selected: string = '1';
    component.setSelected(selected);
    expect(component.isSelected(selected)).toBeTruthy();
  }));

  it('should check child existence', fakeAsync(() => {
    const completeTree: ArtifactNode = {
      name: 'Diagram',
      children: [
        {
          name: 'artifact-tst',
          id: '1',
          icon: 'device_hub',
          children: [
            {
              name: 'Code',
              children: []
            }
          ]
        }
      ]
    };

    const noChildrenTree: ArtifactNode = {
      name: 'Diagram',
      children: null,
    };

    expect(component.hasChild(0, completeTree)).toBeTruthy();
    expect(component.hasChild(0, noChildrenTree)).toBeFalsy();
  }));

  it('should emit when selected', fakeAsync(() => {
    interface EmitedNode {id: number; type: string; }
    const emitedNode: EmitedNode = ({id: 1, type: 'Document'});
    spyOn(component.artifactSelected, 'emit');
    component.onSelection(emitedNode);
    expect(component.artifactSelected.emit).toHaveBeenCalled();
  }));

});
