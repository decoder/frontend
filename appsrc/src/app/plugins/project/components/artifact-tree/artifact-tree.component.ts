import { NestedTreeControl } from '@angular/cdk/tree';
import {
  Component,
  Input,
  EventEmitter,
  Output,
  SimpleChanges,
  OnInit,
  OnDestroy
} from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ArtifactNode } from '../../models/artifact-node.model';

@Component({
  selector: 'app-artifact-tree',
  templateUrl: './artifact-tree.component.html',
  styleUrls: ['./artifact-tree.component.scss'],
})
export class ArtifactTreeComponent implements OnInit, OnDestroy {
  private _treeData: ArtifactNode[];
  treeControl: NestedTreeControl<ArtifactNode>;
  dataSource: MatTreeNestedDataSource<ArtifactNode>;
  @Input() selectedArtifactId: string;
  @Input() emptyArtifactsMessage: boolean;
  @Output() artifactSelected: EventEmitter<any>;

  selectedArtifactType: string;
  spinnerDisplayed: boolean = true;
  timer: any;

  @Input() set treeData(value: ArtifactNode[]) {
    this._treeData = value;
    this.dataSource.data = this._treeData;
  }

  constructor() {
    this.artifactSelected = new EventEmitter();
    this.treeControl = new NestedTreeControl<ArtifactNode>(
      (node: ArtifactNode) => node.children
    );
    this.dataSource = new MatTreeNestedDataSource<ArtifactNode>();
  }

  ngOnInit(): void {
    this.timeOutSpinnerLoader();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['selectedArtifactId'] && !!this.selectedArtifactId) {
      const artifactSelection: ArtifactNode = this.dataSource.data.find(
        (artifactNode: ArtifactNode) =>
          artifactNode.children.find(
            (artifactChildren: ArtifactNode) =>
              artifactChildren.id === this.selectedArtifactId &&
              artifactChildren.type === this.selectedArtifactType
          )
      );

      this.treeControl.expandDescendants(artifactSelection);
    }
  }

  onSelection(node: any): void {
    this.setSelected({ id: node.id, type: node.type });
    this.emitSelected({ id: node.id, type: node.type });
  }

  setSelected(node: any): void {
    this.selectedArtifactId = node.id;
    this.selectedArtifactType = node.type;
  }

  emitSelected(node: any): void {
    this.artifactSelected.emit(node);
  }

  isSelected(node: any): boolean {
    return (
      this.selectedArtifactId === node.id &&
      this.selectedArtifactType === node.type
    );
  }

  hasChild(_: number, node: ArtifactNode): boolean {
    return !!node.children && node.children.length > 0;
  }

  getLastFileNameFrom(str: string): string {
    if (str !== undefined) {
      const arr: string[] = str.split('/');
      const name: string = arr[arr.length - 1];
      return decodeURIComponent(name);
    } else {
      return '';
    }
  }

  decodeName(name: string): string {
    return decodeURIComponent(name);
  }

  timeOutSpinnerLoader(): void {
    this.timer = setTimeout(() => { this.spinnerDisplayed = false; }, 20000);
  }

  ngOnDestroy(): void {
    clearTimeout(this.timer);
  }
}
