import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentComponent } from './tab-content.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { AppConfiguration } from 'src/config/app-configuration';

describe('TabContentComponent', () => {
  let component: TabContentComponent;
  let fixture: ComponentFixture<TabContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ AppConfiguration ],
      declarations: [ TabContentComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentComponent);
    component = fixture.componentInstance;
    component.renderingConfig = {
      artifact: pkmArtifactsMock[0],
      content: '',
      extension: '',
      originalUrl: '',
    };
    component.renderingConfig.artifact = pkmArtifactsMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
