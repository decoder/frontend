import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { RenderingConfiguration } from 'src/app/renderer/models/rendering-configuration.model';
import { RendererComponent } from 'src/app/renderer/components/renderer/renderer.component';
import { SafeUrl } from '@angular/platform-browser';
import { AppConfiguration } from 'src/config/app-configuration';

@Component({
  selector: 'app-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.scss']
})
export class TabContentComponent implements OnInit {
  private _renderingConfig: RenderingConfiguration;
  @ViewChild(RendererComponent) private rendererComponent: RendererComponent;
  @Input() projectName: string;
  @Input() set renderingConfig(config: RenderingConfiguration) {
    this._renderingConfig = config;
  }
  get renderingConfig(): RenderingConfiguration {
    return this._renderingConfig;
  }
  @Output() deleteEventDispatch: EventEmitter<any> = new EventEmitter();
  @Output() editRawSourceCode: EventEmitter<never>;
  safeDocumentUrl: SafeUrl;

  constructor(private appConfig: AppConfiguration) {
    this.deleteEventDispatch = new EventEmitter();
    this.editRawSourceCode = new EventEmitter();
  }

  ngOnInit(): void {
    const token: string = localStorage.getItem('access_token');
    const projectBaseURL: string = this.appConfig.serverURL;
    const url: string = `${projectBaseURL}/io/${this.projectName}/${encodeURIComponent(this._renderingConfig.artifact.name)}`;
    this.safeDocumentUrl = (`${url}?key=${token}`) as SafeUrl;
  }

  dispatchDeleteEvent(): void {
    this.deleteEventDispatch.emit();
  }

  saveClicked(): void {
    this.rendererComponent.onSaveModifiedCode();
  }

  checkArtifactTypeEditable(): boolean {
    return this.renderingConfig.artifact.type === 'Code';
  }

  checkArtifactTypeDownloadable(): boolean {
    return (
      this.renderingConfig.artifact.type === 'Document'
      || this.renderingConfig.artifact.type === 'Binary Executable'
    );
  }

  onEditRawSourceCode(): void {
    console.log('tab content componente [editRawSourceCode]');
    this.editRawSourceCode.emit();
  }

}
