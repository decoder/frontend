import {
  Component,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { VisualizableArtifact } from '../../models/visualizable-artifact.model';
import { artifactNameRetriever } from '../../../../utils/utils';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/home/projects-list/components/delete-dialog/delete-dialog.component';
import { ArtifactsRetrieverService } from '../../services/artifacts-retriever/artifacts-retriever.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BinaryDocumentArtifact } from '../../models/binary-document-artifact.model';
import { DomSanitizer } from '@angular/platform-browser';
import { PKMASFMDocArtifact } from '../../models/asfm-document-artifact.model';

@Component({
  selector: 'app-artifact-visualizer',
  templateUrl: './artifact-visualizer.component.html',
  styleUrls: ['./artifact-visualizer.component.scss'],
})
export class ArtifactVisualizerComponent {
  private _visualizableStore: VisualizableArtifact[] = [];
  @Input() selectedArtifact: PKMArtifact;
  @Input() projectName: string;
  @Output() artifactSelected: EventEmitter<any> = new EventEmitter();
  @Output() refreshItemsAfterDelete: EventEmitter<any> = new EventEmitter();
  @Output() editRawSourceCode: EventEmitter<never>;
  selectedTab: number = 0;
  deleteDialog: any;

  constructor(
    public dialog: MatDialog,
    private artifactRetrieverService: ArtifactsRetrieverService,
    private domSanitizer: DomSanitizer
  ) {
    this.editRawSourceCode = new EventEmitter();
  }

  get visualizableStore(): VisualizableArtifact[] {
    return this._visualizableStore;
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Event is not triggered when store is empty
    if (
      !changes.selectedArtifact.firstChange &&
      changes['selectedArtifact'] &&
      Object.keys(this.selectedArtifact).length !== 0
    ) {
      this.storeSelectedArtifact();
    }
  }

  onTabClicked(index: number): void {
    this.selectedArtifact = this._visualizableStore[index].artifact;
    this.selectedTab = index;
    this.artifactSelected.emit(this.selectedArtifact);
  }

  onDeleteTab(index: number): void {
    this._visualizableStore.splice(index, 1);
    if (this.visualizableStore.length === 0) {
      this.artifactSelected.emit('');
    } else {
      if (
        this.visualizableStore[index] !== undefined &&
        this.visualizableStore[index].artifact !== undefined
      ) {
        const pkm: PKMArtifact = this.visualizableStore[index].artifact;
        this.artifactSelected.emit(pkm.id);
      }
    }
  }

  onDeleteArtifact(index: number): void {
    this._visualizableStore.splice(index, 1);
    this.refreshItemsAfterDelete.emit();
  }

  openDeleteDialog(index: number): void {
    this.deleteDialog = this.dialog.open(DeleteDialogComponent, {
      width: '650px',
      height: '400px',
      data: {
        title: `Delete artifact (${this.selectedArtifact.type}) - "${artifactNameRetriever(this.selectedArtifact.name, false)}"`,
        description:
          'If you delete the artifact you will lose all your data. This action is irreversible.',
        accept: 'DELETE ARTIFACT',
      },
    });

    this.deleteDialog.afterClosed().subscribe((result: string) => {
      if (result === 'delete') {
        this.artifactRetrieverService
          .deleteArtifactByIdAndType(this.projectName, this.selectedArtifact)
          .subscribe(() => {
            this.onDeleteArtifact(index);
          });
      }
    });
  }

  onEditRawSourceCode(): void {
    this.editRawSourceCode.emit();
  }

  private storeSelectedArtifact(): void {
    const isStored: boolean = this.isSelectedArtifactStored();
    if (!isStored) {
        this.selectedToVisualizable(this.selectedArtifact).subscribe( (visualizable: VisualizableArtifact) => {
          this._visualizableStore = [...this._visualizableStore, visualizable];
          this.selectTab();
        });
    } else {
      this.selectTab();
    }
  }

  private selectedToVisualizable(selected: PKMArtifact): Observable<VisualizableArtifact> {
    if (!selected || !selected.type) {
      return;
    }
    switch (selected.type) {
      // get method fetches content, but it's not being used. Api can be optimized with abbrevv param.
      case 'Binary Executable': {
        return this.artifactRetrieverService.getPKMExecutableFromAProjectByID(
          this.projectName, selected.name
        )
          .pipe(
            map((binaryArtifact: BinaryDocumentArtifact) => {
              // convierto el artefacto binario del servicio en uno Visualizable
              const splittedArray: string[] = binaryArtifact.rel_path.split('.');
              const hasExtension: boolean = splittedArray.length > 1;
              return new VisualizableArtifact(
                selected,
                hasExtension ? splittedArray.pop() : '',
                binaryArtifact.content,
                this.domSanitizer.bypassSecurityTrustUrl(
                  `data:${binaryArtifact.mime_type};base64,` + binaryArtifact.content
                )
              );
            })
          );
      }
      case 'Document': {
        return this.artifactRetrieverService.getPKMDocsFromAProjectByID(
          this.projectName, selected.name
        )
          .pipe(
            map((binaryArtifact: BinaryDocumentArtifact) =>
              // convierto el artefacto binario del servicio en uno Visualizable
              new VisualizableArtifact(
                selected,
                binaryArtifact.rel_path.split('.').pop(),
                binaryArtifact.content,
                this.domSanitizer.bypassSecurityTrustUrl(
                  binaryArtifact.format === 'binary' ?
                    `data:${binaryArtifact.mime_type};base64,` + String(binaryArtifact.content)
                  : `data:${binaryArtifact.mime_type},` + String(binaryArtifact.content)
                )
              ),
            )
          );
      }

      case 'ASFM Docs': {
        return this.artifactRetrieverService.getASFMDocFromProjectByID(this.projectName, selected.name)
          .pipe(
            map((asfmArtifact: PKMASFMDocArtifact) =>
              new VisualizableArtifact(
                selected,
                'json',
                asfmArtifact,
                selected.rel_path
              ),
            )
          );
      }
      case 'CVE':
      case 'Log': {
        return of(
          new VisualizableArtifact(
            selected,
            'json',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'Annotation': {
        return of(
          new VisualizableArtifact(
            selected,
            'annotation',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'NER': {
        return of(
          new VisualizableArtifact(
            selected,
            'ner',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'SRL': {
        return of(
          new VisualizableArtifact(
            selected,
            'srl',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'Comment': {
        return of(
          new VisualizableArtifact(
            selected,
            'comment',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'Diagram': {
        // Extension is java because it should visualized with Monaco code plugin
        return of(
          new VisualizableArtifact(
            selected,
            'java',
            selected.content,
            selected.git_repository_url
          )
        );
      }
      case 'TESTAR_State_Model': {
        return of(
          new VisualizableArtifact(selected, 'web_testar_statemodel', '', '')
        );
      }
      default: {
        return of(
          new VisualizableArtifact(
            selected,
            artifactNameRetriever(selected.rel_path, false)
              .split('.')
              .slice(-1)
              .toString(),
            selected.content,
            selected.git_repository_url
          )
        );
      }
    }
  }

  private isSelectedArtifactStored(): boolean {
    const newId: string = this.selectedArtifact.id;
    const type: string = this.selectedArtifact.type;
    return !!this._visualizableStore.find(
      this.isEqualToId(newId, type)
    );
  }

  private selectTab(): void {
    const newId: string = this.selectedArtifact.id;
    const type: string = this.selectedArtifact.type;
    const index: number = this._visualizableStore.findIndex(
      this.isEqualToId(newId, type)
    );
    this.selectedTab = index;
  }

  private isEqualToId(
    id: string,
    type: string,
  ): (visualizable: VisualizableArtifact) => boolean {
    return (visualizable: VisualizableArtifact) => {
      return visualizable.getId() === id && visualizable.getType() === type;
    };
  }
}
