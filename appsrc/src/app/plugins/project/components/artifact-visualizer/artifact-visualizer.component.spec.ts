import {
  async,
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync
} from '@angular/core/testing';
import { ArtifactVisualizerComponent } from './artifact-visualizer.component';
import {
  CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { DomSanitizer } from '@angular/platform-browser';
import { VisualizableArtifact } from '../../models/visualizable-artifact.model';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';
import { AppConfiguration } from 'src/config/app-configuration';

describe('ArtifactVisualizerComponent', () => {
  let component: ArtifactVisualizerComponent;
  let fixture: ComponentFixture<ArtifactVisualizerComponent>;
  let artifactTest: PKMArtifact;
  let testStore: VisualizableArtifact[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArtifactVisualizerComponent],
      providers: [{ provide: DomSanitizer}, AppConfiguration],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [MatDialogModule, HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactVisualizerComponent);
    component = fixture.componentInstance;
    artifactTest = { ...pkmArtifactsMock[0] };
    testStore = [
      {
        artifact: artifactTest,
        extension: artifactTest.rel_path.split('.')[1],
        content: artifactTest.content,
        originalUrl: artifactTest.git_repository_url,
        getId: null,
        getType: null,
      }
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the selected artifact and emit its id [onTabClicked]', fakeAsync(() => {
    component['_visualizableStore'] = testStore;
    fixture.detectChanges();
    component.onTabClicked(0);
    tick();

    expect(component.selectedArtifact.id).toEqual(artifactTest.id);
    component.artifactSelected.subscribe((id: string) => {
      expect(id).toEqual(artifactTest.id);
    });
  }));

  it('should delete the specified tab [onDeleteTab]', () => {
    component['_visualizableStore'] = [...testStore];
    fixture.detectChanges();

    component.onDeleteTab(0);

    expect(component.visualizableStore.length).not.toEqual(testStore.length);
  });
});
