import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {
  MethodologySupportComponent,
  MethodologyStep,
  MethodologyStepFlat,
} from './methodology-support.component';
import { MethodologyGuidanceService } from '../../services/methodology-guidance/methodology-guidance.service';
import { Observable, of } from 'rxjs';
import { methodologyStepsMock } from 'src/app/mocks/project/methodology-step.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

class MethodologyGuidanceServiceStub {
  availableSteps$: Observable<MethodologyStep[]>;

  getSteps(): Observable<MethodologyStep[]> {
    return this.availableSteps$;
  }

  obtainSteps(projectName: string): void {
    this.availableSteps$ = of(methodologyStepsMock);
  }
}

describe('MethodologySupportComponent', () => {
  let component: MethodologySupportComponent;
  let fixture: ComponentFixture<MethodologySupportComponent>;
  let step: MethodologyStep;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MethodologySupportComponent],
      providers: [
        {
          provide: MethodologyGuidanceService,
          useClass: MethodologyGuidanceServiceStub,
        },
      ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodologySupportComponent);
    component = fixture.componentInstance;
    component.dataSource.data = methodologyStepsMock;
    step = component.dataSource.data[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check if a node is expandable on [hasChild]', () => {
    const expandable: MethodologyStepFlat = new MethodologyStepFlat();
    const nonExpandable: MethodologyStepFlat = new MethodologyStepFlat();
    expandable.expandable = true;
    nonExpandable.expandable = false;
    expect(component.hasChild(undefined, expandable)).toBe(true);
    expect(component.hasChild(undefined, nonExpandable)).toBe(false);
  });

  it('should cancel common click behaviour [onCheckboxClick]', () => {
    const clickEvent: any = {
      stopPropagation: () => null,
      preventDefault: () => null,
    };
    spyOn(clickEvent, 'preventDefault');
    component.onCheckboxClick(clickEvent);
    expect(clickEvent.preventDefault).toHaveBeenCalled();
  });

  it('should check whether [descendantsAllSelected]', () => {
    step.tasks.forEach((substep: MethodologyStep) => {
      component.selectNode(substep);
    });
    const flat: MethodologyStepFlat = component.nestedNodeMap.get(step);
    expect(component.descendantsAllSelected(flat)).toBe(true);
    expect(component.descendantsPartiallySelected(flat)).toBe(false);
  });

  it('should check whether [descendantsPartiallySelected]', () => {
    component.selectNode(step.tasks[0]);
    const flat: MethodologyStepFlat = component.nestedNodeMap.get(step);
    expect(component.descendantsAllSelected(flat)).toBe(false);
    expect(component.descendantsPartiallySelected(flat)).toBe(true);
  });

  it('should [getParentNode] of a node', () => {
    const flatStep: MethodologyStepFlat = new MethodologyStepFlat();
    flatStep.level = 0;
    expect(component['getParentNode'](flatStep)).toBe(null);
    flatStep.level = 0;
    expect(component['getParentNode'](flatStep)).toBe(null);

    const dataNodes: MethodologyStepFlat[] = component.treeControl.dataNodes;
    const lastStep: MethodologyStepFlat = dataNodes[dataNodes.length - 1];
    expect(component['getParentNode'](lastStep)).toBeTruthy();
  });

  it('should [getLevel] of a node', () => {
    const flatStep: MethodologyStepFlat = new MethodologyStepFlat();
    flatStep.level = 0;
    expect(component['getLevel'](flatStep)).toEqual(0);
  });

  it('should check whether a node [isExpandable]', () => {
    const flatStep: MethodologyStepFlat = new MethodologyStepFlat();
    flatStep.expandable = true;
    expect(component['isExpandable'](flatStep)).toEqual(true);
    flatStep.expandable = false;
    expect(component['isExpandable'](flatStep)).toEqual(false);
  });

  it('should [getChildren]', () => {
    expect(component['getChildren'](step)).toEqual(step.tasks);
  });

  it('should turn a node into a flat node using the [transformer]', () => {
    const flat: MethodologyStepFlat = component['transformer'](step, 0);
    expect(flat.level).toEqual(0);
    expect(flat.expandable).toBe(true);
    expect(flat.name).toEqual(step.name);
  });
});
