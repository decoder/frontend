import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  MatTreeFlattener,
  MatTreeFlatDataSource,
} from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { MethodologyGuidanceService } from '../../services/methodology-guidance/methodology-guidance.service';

export interface MethodologyStep {
  phaseNumber?: number;
  name: string;
  description?: string;
  completed: boolean;
  tasks?: MethodologyStep[];
  taskNumber?: number;
}

export class MethodologyStepFlat {
  taskNumber: number;
  name: string;
  completed: boolean;
  level: number;
  expandable: boolean;
}

class StepEmitter extends EventEmitter<{
  step: MethodologyStep;
  substep: MethodologyStep;
}> {
  constructor() {
    super();
  }
}

@Component({
  selector: 'app-methodology-support',
  templateUrl: './methodology-support.component.html',
  styleUrls: ['./methodology-support.component.scss'],
})
export class MethodologySupportComponent implements OnInit {
  checklistSelection: SelectionModel<MethodologyStepFlat>;
  @Output() selection: StepEmitter;
  @Output() deselection: EventEmitter<MethodologyStep>;
  @Input() projectName: string;

  flatNodeMap: Map<MethodologyStepFlat, MethodologyStep>;
  nestedNodeMap: Map<MethodologyStep, MethodologyStepFlat>;
  selectedParent: MethodologyStepFlat | null = null;

  treeControl: FlatTreeControl<MethodologyStepFlat>;

  treeFlattener: MatTreeFlattener<MethodologyStep, MethodologyStepFlat>;

  dataSource: MatTreeFlatDataSource<MethodologyStep, MethodologyStepFlat>;

  constructor(
    private guidance: MethodologyGuidanceService,
  ) {
    this.flatNodeMap = new Map<MethodologyStepFlat, MethodologyStep>();
    this.nestedNodeMap = new Map<MethodologyStep, MethodologyStepFlat>();
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      this.getChildren
    );
    this.treeControl = new FlatTreeControl<MethodologyStepFlat>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new MatTreeFlatDataSource(
      this.treeControl,
      this.treeFlattener
    );
    this.checklistSelection = new SelectionModel<MethodologyStepFlat>(true);
    this.dataSource.data = [];
    this.selection = new StepEmitter();
    this.deselection = new EventEmitter();
  }

  ngOnInit(): void {
    this.updateSteps();
  }

  hasChild = (_: number, node: MethodologyStepFlat): boolean => node.expandable;

  onCheckboxClick(e: any): void {
    e.preventDefault();
  }

  updateSteps(): void {
    this.guidance.obtainSteps(this.projectName);
    this.guidance.getSteps()
      .subscribe((steps: MethodologyStep[]) => {
      this.checklistSelection = new SelectionModel<MethodologyStepFlat>(true);
      this.dataSource.data = steps;
    });
  }

  descendantsAllSelected(node: MethodologyStepFlat): boolean {
    const descendants: MethodologyStepFlat[] = this.treeControl.getDescendants(
      node
    );
    return descendants.every(
      (child: MethodologyStepFlat) => this.checklistSelection.isSelected(child)
    );
  }

  descendantsPartiallySelected(node: MethodologyStepFlat): boolean {
    const descendants: MethodologyStepFlat[] = this.treeControl.getDescendants(
      node
    );
    const result: boolean = descendants.some((child: MethodologyStepFlat) =>
      this.checklistSelection.isSelected(child)
    );
    return result && !this.descendantsAllSelected(node);
  }

  toggleLeafNode(flatStep: MethodologyStepFlat, event: any): void {
    this.onCheckboxClick(event);
    const parentStepFlat: MethodologyStepFlat = this.getParentNode(flatStep);
    const parentStep: MethodologyStep = this.flatNodeMap.get(parentStepFlat);
    const step: MethodologyStep = this.flatNodeMap.get(flatStep);

    // user clicks on already selected node
    if (this.checklistSelection.isSelected(flatStep)) {
      this.deselection.emit(step);
    } else {
      // deselect previous (Array of values)
      this.checklistSelection.deselect(this.checklistSelection.selected[0]);
      // select new node
      this.checklistSelection.select(flatStep);
      this.selection.emit({ step: parentStep, substep: step });
      this.guidance.obtainToolsByPhaseAndTask(this.projectName, parentStep, step);
    }
  }

  selectNode(node: MethodologyStep): void {
    const flatNode: MethodologyStepFlat = this.nestedNodeMap.get(node);
    this.checklistSelection.select(flatNode);
  }

  deselectNode(node: MethodologyStep): void {
    const flatNode: MethodologyStepFlat = this.nestedNodeMap.get(node);
    this.checklistSelection.deselect(flatNode);
  }

  private getParentNode(node: MethodologyStepFlat): MethodologyStepFlat | null {
    const currentLevel: number = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex: number = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i: number = startIndex; i >= 0; i--) {
      const currentNode: MethodologyStepFlat = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  private getLevel = (node: MethodologyStepFlat) => node.level;

  private isExpandable = (node: MethodologyStepFlat) => node.expandable;

  private getChildren = (node: MethodologyStep): MethodologyStep[] => {
    return node.tasks.sort(((a: MethodologyStep, b: MethodologyStep) => (a.taskNumber > b.taskNumber) ? 1 : -1));
  }

  private transformer = (node: MethodologyStep, level: number) => {
    const existingNode: MethodologyStepFlat = this.nestedNodeMap.get(node);
    const flatNode: MethodologyStepFlat =
      existingNode && existingNode.name === node.name
        ? existingNode
        : new MethodologyStepFlat();
    flatNode.name = node.name;
    flatNode.completed = node.completed;
    flatNode.taskNumber = node.taskNumber;
    flatNode.level = level;
    flatNode.expandable = !!node.tasks;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }
}
