import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-statemodel-visualizer',
  templateUrl: './statemodel-visualizer.component.html',
  styleUrls: ['./statemodel-visualizer.component.scss'],
})
export class StateModelVisualizerComponent implements OnInit {
  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  htmlUrl: SafeUrl;

  constructor(private readonly http: HttpClient, private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    // Activate TESTAR State Model web feature sending a GET request
    this.http.get(
      'https://decoder-tool.ow2.org/testar/testar/analysis/MyThaiStar',
      {
        headers: this.headers,
      }).subscribe((x: any) => console.log(x));

    this.htmlUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://decoder-tool.ow2.org/models/models');
  }
}
