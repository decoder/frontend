import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RelatedArtifactsComponent } from './related-artifacts.component';
import { ArtifactType } from '../../../../core/models/artifact';
import { constants } from '../../../../shared/constants/constants';

describe('RelatedArtifactsComponent', () => {
  let component: RelatedArtifactsComponent;
  let fixture: ComponentFixture<RelatedArtifactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RelatedArtifactsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedArtifactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit artifact id', () => {
    interface EmitedArtifact {id: string; type: string; }
    const emitedArtifactId: string = '1';
    const emitedArtifactType: string = 'Code';
    const emitedArtifact: EmitedArtifact = ({id: emitedArtifactId, type: emitedArtifactType});

    component.artifactSelected.subscribe((artifact: EmitedArtifact) =>
      expect(artifact).toEqual(emitedArtifact)
    );
    component.onSelectedArtifact(emitedArtifact);
  });

  it('should return a correct icon with artifact type', () => {
    const artifactTypeDiagram: ArtifactType = 'Diagram';
    const artifactTypeCode: ArtifactType = 'Code';
    const artifactTypeDoc: ArtifactType = 'Document';
    const artifactTypeAnnot: ArtifactType = 'Annotation';
    const artifactTypeTestar: ArtifactType = 'TESTAR_State_Model';

    expect(component.getIcon(artifactTypeDiagram)).toBe(
      constants.icons.DIAGRAM
    );
    expect(component.getIcon(artifactTypeCode)).toBe(constants.icons.CODE);
    expect(component.getIcon(artifactTypeDoc)).toBe(constants.icons.DOCUMENT);
    expect(component.getIcon(artifactTypeTestar)).toBe(constants.icons.TESTAR_STATE_MODEL);
    expect(component.getIcon(artifactTypeAnnot)).toBe(
      constants.icons.ANNOTATION
    );
  });

  it('should return last filename correctly', () => {
    const path: string = 'app/src/example/wef/plugins/compo/Booking.uml';
    expect(component.getLastFileNameFrom(path)).toBe('Booking.uml');
  });

  it('should emit event when artifact is selected', () => {
    const artifact: string = 'artifact';
    spyOn(component.artifactSelected, 'emit');
    component.onSelectedArtifact(artifact);

    expect(component.artifactSelected.emit).toHaveBeenCalled();
  });

});
