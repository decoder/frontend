import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ArtifactType, PKMArtifact } from '../../../../core/models/artifact';
import { artifactIconByType } from '../../../../shared/constants/utils';

@Component({
  selector: 'app-related-artifacts',
  templateUrl: './related-artifacts.component.html',
  styleUrls: ['./related-artifacts.component.scss'],
})
export class RelatedArtifactsComponent {
  @Input() selectedArtifact: PKMArtifact;
  @Input() relatedArtifacts: PKMArtifact[];
  @Output() artifactSelected: EventEmitter<any> = new EventEmitter<any>();

  /**
   * Function to emit artifactSelected to the container, artifact-navigation-component
   */
  onSelectedArtifact(artifact: any): void {
    this.artifactSelected.emit({ id: artifact.id, type: artifact.type });
  }

  getIcon(type: ArtifactType): string {
    return artifactIconByType(type);
  }

  getLastFileNameFrom(str: string): string {
    if (str !== undefined) {
      const arr: string[] = str.split('/');
      let name: string = arr[arr.length - 1];
      const max: number = 30;
      if (name.length > max) {
        name = decodeURIComponent(name).substr(0, max) + '...';
      }
      return decodeURIComponent(name);
    } else {
      return '';
    }
  }
}
