import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { PKMArtifact } from 'src/app/core/models/artifact';

interface OptionInterface {
  name: string;
  type: string;
  id: string;
}

@Component({
  selector: 'app-quick-search-bar',
  templateUrl: './quick-search-bar.component.html',
  styleUrls: ['./quick-search-bar.component.scss']
})
export class QuickSearchBarComponent implements OnInit {
  @Output() emitQuickSearchInput: EventEmitter<OptionInterface> = new EventEmitter<OptionInterface>();
  @Output() emitQuickSearchArtifactId: EventEmitter<PKMArtifact> = new EventEmitter<PKMArtifact>();
  options: OptionInterface[];
  myControl: FormControl = new FormControl();
  filteredOptions: Observable<OptionInterface[]>;
  artifacts: PKMArtifact[];

  @Input() set searchableArtifacts(artifacts: PKMArtifact[]) {
    this.artifacts = artifacts;
    this.options = artifacts.map((artifact: PKMArtifact) =>  {
      return { name: artifact.name, type: artifact.type, id: artifact.id };
    });
  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map((value: string) => this._filter(value))
      );
  }

  onKeyUp(): void {
    this.emitQuickSearchInput.emit(this.myControl.value);
  }

  onMouseSelection(option: OptionInterface): void {
    this.emitQuickSearchInput.emit(option);
    if (this.myControl.value !== undefined) {
      const result: PKMArtifact =
        this.artifacts.filter((element: PKMArtifact) => (element.name === this.myControl.value && element.type === option.type))[0];
      this.emitQuickSearchArtifactId.emit(result);
    }
  }

  private _filter(value: any): OptionInterface[] {
    if (value === '') { return null; }
    return this.options.filter((option: OptionInterface) => {
      if (option.name === undefined ) { return false; }
      if (option.name.toLowerCase().includes(value.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    });
  }

}
