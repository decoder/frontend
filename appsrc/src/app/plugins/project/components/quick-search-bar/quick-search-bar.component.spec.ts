import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectModule } from '../../project.module';
import { QuickSearchBarComponent } from './quick-search-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { pkmArtifactsMock } from 'src/app/mocks/core/fixtures/artifacts/pkmArtifact.mock';

interface OptionInterface {
  name: string;
  type: string;
  id: string;
}

describe('QuickSearchBarComponent', () => {
  let component: QuickSearchBarComponent;
  let fixture: ComponentFixture<QuickSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ProjectModule, BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit input value on keyup event', () => {
    spyOn(component.emitQuickSearchInput, 'emit');
    component.onKeyUp();
    expect(component.emitQuickSearchInput.emit).toHaveBeenCalledWith(component.myControl.value);
  });

  it('should emit event on mouse selection', () => {
    const emitedInput: OptionInterface = {name: '1', type: '1', id: '1'};
    component.artifacts = pkmArtifactsMock;
    fixture.detectChanges();

    spyOn(component.emitQuickSearchInput, 'emit');
    component.onMouseSelection(emitedInput);
    expect(component.emitQuickSearchInput.emit).toHaveBeenCalledWith(emitedInput);
  });
});
