import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtifactCommentsSelectorComponent } from './artifact-comments-selector.component';

describe('ArtifactCommentsSelectorComponent', () => {
  let component: ArtifactCommentsSelectorComponent;
  let fixture: ComponentFixture<ArtifactCommentsSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtifactCommentsSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtifactCommentsSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
