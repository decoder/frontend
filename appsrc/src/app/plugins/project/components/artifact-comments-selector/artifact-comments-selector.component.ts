import { CdkStep, STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { MatStep, MatStepper } from '@angular/material/stepper';
import { PKMArtifact } from 'src/app/core/models/artifact';

const MODEL_VALUES: string[] = [
  'Decoder',
  'DecoderOpenCV',
  'DecoderCVE',
  'DecoderJava'
];
@Component({
  selector: 'app-artifact-comments-selector',
  templateUrl: './artifact-comments-selector.component.html',
  styleUrls: ['./artifact-comments-selector.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class ArtifactCommentsSelectorComponent {

  selectedCommentAnnotation: PKMArtifact;
  srcSelected: any;
  srcSubselected: any;

  targetSelectedCommentAnnotation: PKMArtifact;
  targetSelected: any;
  targetSubselected: any;

  models: string[];
  selectedModel: string;

  @Input() availableCommentsAndAnnotations: PKMArtifact[];
  @Input() source: boolean;
  @Input() target: boolean;
  @Input() loading: boolean;
  @Input() invocationResult: any;
  @Input() toolIsNER: boolean;

  @Output() executeTool: EventEmitter<any>;
  @Output() resetEvent: EventEmitter<any>;

  constructor() {
    this.srcSelected = [];
    this.srcSubselected = [];
    this.targetSelected = [];
    this.targetSubselected = [];
    this.availableCommentsAndAnnotations = [];
    this.executeTool = new EventEmitter<boolean>(false);
    this.resetEvent = new EventEmitter<any>();
    this.models = MODEL_VALUES;
  }

  onSubChangeSrc(sub: any): void {
    this.srcSubselected =
      this.srcSubselected === sub.selectedOptions.selected[0].value
        ? null
        : sub.selectedOptions.selected[0].value;
    if (!this.srcSubselected) {
      sub.deselectAll();
    }
  }

  onChangeSrc(e: any): void {
    this.selectedCommentAnnotation = e.selectedOptions.selected[0].value;
    if (this.selectedCommentAnnotation.type === 'Comment') {
      this.srcSelected =
        this.srcSelected ===
        e.selectedOptions.selected[0].value.content.comments
          ? null
          : e.selectedOptions.selected[0].value.content.comments;
    } else if (this.selectedCommentAnnotation.type === 'Annotation') {
      this.srcSelected =
        this.srcSelected ===
        e.selectedOptions.selected[0].value.content.annotations
          ? null
          : e.selectedOptions.selected[0].value.content.annotations;
    }
    if (!this.srcSelected) {
      e.deselectAll();
    }
  }

  onSubChangeTarget(sub: any): void {
    this.targetSubselected =
      this.targetSubselected === sub.selectedOptions.selected[0].value
        ? null
        : sub.selectedOptions.selected[0].value;
    if (!this.targetSubselected) {
      sub.deselectAll();
    }
  }

  onChangeTarget(e: any): void {
    this.targetSelectedCommentAnnotation = e.selectedOptions.selected[0].value;
    if (this.targetSelectedCommentAnnotation.type === 'Comment') {
      this.targetSelected =
        this.targetSelected ===
        e.selectedOptions.selected[0].value.content.comments
          ? null
          : e.selectedOptions.selected[0].value.content.comments;
    } else if (this.targetSelectedCommentAnnotation.type === 'Annotation') {
      this.targetSelected =
        this.targetSelected ===
        e.selectedOptions.selected[0].value.content.annotations
          ? null
          : e.selectedOptions.selected[0].value.content.annotations;
    }
    if (!this.targetSelected) {
      e.deselectAll();
    }
  }

  onChangeModel(sub: any): void {
    this.selectedModel =
      this.selectedModel === sub.selectedOptions.selected[0].value
        ? null
        : sub.selectedOptions.selected[0].value;
    if (!this.selectedModel) {
      sub.deselectAll();
    }
  }

  emitExecuteTool(): void {
    this.executeTool.emit();
  }

  nextStep(stepper: MatStepper): void {
    const steps: CdkStep[] = stepper.steps.toArray();

    // completes previous step
    const previousStep: CdkStep = stepper.selected;
    previousStep.completed = true;

    // updates step
    stepper.next();
    const currentStep: CdkStep = stepper.selected;
    const currentIndex: number = stepper.selectedIndex;

    // if last step: set state to all-done and steps are no longer editable
    if (currentIndex === steps.length - 1) {
      currentStep.state = 'all-done';
      steps.forEach( (step: CdkStep) => step.editable = false );
      this.emitExecuteTool();
    } else {
      // number state: touched but not completed yet.
      currentStep.state = 'number';
      // consecutive steps will be reset.
      for (let i: number = currentIndex; i < steps.length - 1; i++) {
        steps[i + 1].completed = false;
        steps[i + 1].state = 'null';
      }
    }
  }

  reset(stepper: MatStepper): void {
    const allSteps: MatStep[] = stepper._steps.toArray();
    allSteps.forEach((step: MatStep) => {
      step.completed = false;
      step.editable = true;
      step.state = 'null';
    });
    stepper.reset();
    this.srcSelected = [];
    this.srcSubselected = [];
    this.targetSelected = [];
    this.targetSubselected = [];
    this.selectedModel = null;
    this.resetEvent.emit();
  }

  calculateStyle(): any {
    return this.availableCommentsAndAnnotations.length === 0
      ? { 'artifact-selector-empty': true }
      : { 'artifact-selector': true };
  }
}
