import { Routes } from '@angular/router';
import { interfacePlugins } from './plugins.config';

export function createRoutes(): Routes {
  const result: Routes = [];
  interfacePlugins.forEach((plugin: any) => {
    result.push({
      path: plugin.path,
      loadChildren: () =>
          plugin.importFunction().then((m: any) => m[plugin.moduleName])
    });
  });
  return result;
}
