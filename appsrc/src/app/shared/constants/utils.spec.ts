import * as utils from './utils';
import { constants } from './constants';

describe('utils constans', () => {
    it('should obtain artifact types required', () => {
        expect(utils.artifactTypes()).toEqual([
            'Diagram',
            'Code',
            'Document',
            'ASFM Docs',
            'Annotation',
            'Comment',
            'Log',
            'UML Model',
            'TESTAR_State_Model',
            'NER',
            'SRL',
            'CVE',
            'Binary Executable',
            'NOT SUPPORTED'
        ]);
    });

    it('should obtain artifact types required with code', () => {
        expect(utils.artifactIconByType('Code')).toBe(constants.icons['CODE']);
    });

    it('should obtain artifact types required with Annotation', () => {
        expect(utils.artifactIconByType('Annotation')).toBe(constants.icons['ANNOTATION']);
    });
});
