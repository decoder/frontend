export interface AllowedArtifactExtensions {
  CODE_EXTENSIONS: {
    'C#': string[];
    'C++_HEADER': string[];
    'C++': string[];
    C_HEADER: string[];
    C: string[];
    CLOJURE: string[];
    COBOL: string[];
    COFFEESCRIPT: string[];
    COLDFUSION: string[];
    COMMON_LISP: string[];
    CSS: string[];
    CSV: string[];
    CURL: string[];
    D: string[];
    DCURL: string[];
    EIFFEL: string[];
    EMACS_LISP: string[];
    ERLANG: string[];
    EXPECT: string[];
    FORTH: string[];
    FORTRAN: string[];
    GO: string[];
    GROOVY: string[];
    HAML: string[];
    HASKELL: string[];
    HAXE: string[];
    HTML: string[];
    IDL: string[];
    JADE: string[];
    JAVA: string[];
    JAVASCRIPT: string[];
    JSON: string[];
    JSP: string[];
    JSX: string[];
    LESS: string[];
    LEX: string[];
    LUA: string[];
    MATLAB: string[];
    MCURL: string[];
    ML: string[];
    MODULA: string[];
    OBJSRC: string[];
    OCAML: string[];
    PASCAL: string[];
    PERL: string[];
    PHOTOSHOP_CUSTOM_SHAPES: string[];
    PHP: string[];
    PROLOG: string[];
    PYTHON: string[];
    R: string[];
    reStructuredText: string[];
    REXX: string[];
    RUBY: string[];
    SCALA: string[];
    SCHEME: string[];
    SCURL: string[];
    SED: string[];
    ShEx: string[];
    SQL: string[];
    STRUCTURED_TEXT: string[];
    UNIX_SHELL: string[];
    VBASIC_SCRIPT: string[];
    VBASIC: string[];
    VERILOG: string[];
    VHDL: string[];
  };

  DIAGRAM_EXTENSIONS: {
    UML: string[];
  };

  BINARY_DOCUMENT_EXTENSIONS: {
    // documents
    GDOC: string[];
    ODB: string[];
    ODC: string[];
    ODF: string[];
    ODFT: string[];
    ODG: string[];
    ODI: string[];
    ODM: string[];
    ODP: string[];
    ODS: string[];
    ODT: string[];
    OTC: string[];
    OTG: string[];
    OTH: string[];
    OTI: string[];
    OTP: string[];
    OTS: string[];
    OTT: string[];
    PDF: string[];
    POTX: string[];
    PPSX: string[];
    PPTX: string[];
    SLDX: string[];
    WORD: string[];
    XLSX: string[];
    XLTX: string[];
    XPS: string[];
  };

  BINARY_IMAGE_EXTENSIONS: {
    // images
    '3DS': string[];
    APNG: string[];
    BMP: string[];
    BPM: string[];
    BTIF: string[];
    CGM: string[];
    DDS: string[];
    DJVU: string[];
    DWG: string[];
    DXF: string[];
    EMF: string[];
    GIF: string[];
    HEIF: string[];
    ICO: string[];
    IEF: string[];
    JLS: string[];
    JNG: string[];
    JP2: string[];
    JPEG: string[];
    MDI: string[];
    PBM: string[];
    PCX: string[];
    PGM: string[];
    PICT: string[];
    PNG: string[];
    PNM: string[];
    PPM: string[];
    PSD: string[];
    RGB: string[];
    SVG: string[];
    TGA: string[];
    WBMP: string[];
    WDP: string[];
    WEBP: string[];
    WMF: string[];
    XIF: string[];
    XWD: string[];
  };

  TEXT_DOCUMENT_EXTENSIONS: {
    MARKDOWN: string[];
    MDX: string[];
    RICH_TEXT_FORMAT: string[];
    RICH_TEXT: string[];
    SGML: string[];
    TEXT: string[];
    TURTLE: string[];
    WML: string[];
  };

  LINUX_EXECUTABLE_BINARY_FILES_EXTENSIONS: {
    ELF: string[];
  };

  GET_ALL_BINARY_DOCUMENT_EXTENSIONS: () => string[];

  GET_ALL_BINARY_EXTENSIONS: () => string[];

  GET_ALL_BINARY_IMAGE_EXTENSIONS: () => string[];

  GET_ALL_CODE_EXTENTIONS: () => string[];

  GET_ALL_DIAGRAM_EXTENSIONS: () => string[];

  GET_ALL_DOCUMENT_EXTENSIONS: () => string[];

  GET_ALL_LINUX_EXECUTABLE_BINARY_FILES: () => string[];

  GET_ALL_TEXT_DOCUMENT_EXTENSIONS: () => string[];
}

export const allowedArtifactExtensions: AllowedArtifactExtensions = {
  CODE_EXTENSIONS: {
    'C#': ['cs'],
    'C++_HEADER': ['hpp'],
    'C++': ['cc', 'cp', 'cxx', 'cpp', 'c++'],
    C_HEADER: ['h'],
    C: ['c'],
    CLOJURE: ['clj'],
    COBOL: ['cbl', 'cob'],
    COFFEESCRIPT: ['coffee', 'litcoffee'],
    COLDFUSION: ['cfm', 'cfml', 'cfc'],
    COMMON_LISP: ['cl', 'jl', 'lisp', 'lsp'],
    CSS: ['css'],
    CSV: ['csv'],
    CURL: ['curl'],
    D: ['d'],
    DCURL: ['dcurl'],
    EIFFEL: ['e'],
    EMACS_LISP: ['el'],
    ERLANG: ['erl'],
    EXPECT: ['exp'],
    FORTH: ['4th'],
    FORTRAN: ['f', 'for', 'f77', 'f90'],
    GO: ['go'],
    GROOVY: ['groovy'],
    HAML: ['haml'],
    HASKELL: ['hs', 'lhs'],
    HAXE: ['hx'],
    HTML: ['html', 'htm', 'shtml'],
    IDL: ['idl'],
    JADE: ['jade'],
    JAVA: ['java'],
    JAVASCRIPT: ['js'],
    JSON: ['json'],
    JSP: ['jsp'],
    JSX: ['jsx'],
    LESS: ['less'],
    LEX: ['l'],
    LUA: ['lua'],
    MATLAB: ['mat'],
    MCURL: ['mcurl'],
    ML: ['ml'],
    MODULA: ['m3', 'i3', 'mg', 'ig'],
    OBJSRC: ['m'],
    OCAML: ['ocaml', 'mli'],
    PASCAL: ['p', 'pp', 'pas', 'dpr'],
    PERL: ['pl', 'pm', 'al', 'perl'],
    PHOTOSHOP_CUSTOM_SHAPES: ['csh'],
    PHP: ['php', 'php3', 'php4'],
    PROLOG: ['pro'],
    PYTHON: ['py'],
    R: ['r'],
    reStructuredText: ['rest', 'rst', 'restx'],
    REXX: ['rexx'],
    RUBY: ['rb'],
    SCALA: ['scala'],
    SCHEME: ['scm'],
    SCURL: ['scurl'],
    SED: ['sed'],
    ShEx: ['shex'],
    SQL: ['sql'],
    STRUCTURED_TEXT: ['st'],
    UNIX_SHELL: ['sh'],
    VBASIC_SCRIPT: ['vb'],
    VBASIC: ['cls', 'frm'],
    VERILOG: ['v'],
    VHDL: ['vhd', 'vhdl'],
  },

  DIAGRAM_EXTENSIONS: {
    UML: ['uml'],
  },

  BINARY_DOCUMENT_EXTENSIONS: {
    GDOC: ['gdoc'],
    ODB: ['odb'],
    ODC: ['odc'],
    ODF: ['odf'],
    ODFT: ['odf'],
    ODG: ['odg'],
    ODI: ['odi'],
    ODM: ['odm'],
    ODP: ['odp'],
    ODS: ['ods'],
    ODT: ['odt'],
    OTC: ['otc'],
    OTG: ['otg'],
    OTH: ['oth'],
    OTI: ['oti'],
    OTP: ['otp'],
    OTS: ['ots'],
    OTT: ['ott'],
    PDF: ['pdf'],
    POTX: ['potx'],
    PPSX: ['ppsx'],
    PPTX: ['pptx'],
    SLDX: ['sldx'],
    WORD: ['dotx', 'docm', 'dotm', 'dot', 'doc', 'docx'],
    XLSX: ['xlsx'],
    XLTX: ['xltx'],
    XPS: ['xps'],
  },

  BINARY_IMAGE_EXTENSIONS: {
    '3DS': ['3ds'],
    APNG: ['apng'],
    BMP: ['bmp'],
    BPM: ['bpm'],
    BTIF: ['btif'],
    CGM: ['cgm'],
    DDS: ['dds'],
    DJVU: ['djvu', 'djv'],
    DWG: ['dwg'],
    DXF: ['dxf'],
    EMF: ['emf'],
    GIF: ['gif'],
    HEIF: ['heif'],
    ICO: ['ico'],
    IEF: ['ief'],
    JLS: ['jls'],
    JNG: ['jng'],
    JP2: ['jp2', 'jpg2'],
    JPEG: ['jpeg', 'jpg', 'jpe' ],
    MDI: ['mdi'],
    PBM: ['pbm'],
    PCX: ['pcx'],
    PGM: ['pgm'],
    PICT: ['pic', 'pct'],
    PNG: ['png'],
    PNM: ['pnm'],
    PPM: ['ppm'],
    PSD: ['psd'],
    RGB: ['rgb'],
    SVG: ['svg', 'svgz'],
    TGA: ['tga'],
    WBMP: ['wbmp'],
    WDP: ['wdp'],
    WEBP: ['webp'],
    WMF: ['wmf'],
    XIF: ['xif'],
    XWD: ['wd'],
  },

  TEXT_DOCUMENT_EXTENSIONS: {
    MARKDOWN: ['markdown', 'md', 'mdtext', 'mkd'],
    MDX: ['mdx'],
    RICH_TEXT_FORMAT: ['rtf'],
    RICH_TEXT: ['rtx'],
    SGML: ['sgml', 'sgm'],
    TEXT: ['txt', 'text', 'conf', 'def', 'list', 'log', 'in', 'ini'],
    TURTLE: ['ttl'],
    WML: ['wml'],
  },

  LINUX_EXECUTABLE_BINARY_FILES_EXTENSIONS: {
    ELF: [
      'axf',
      'bin',
      'elf',
      'o',
      'out',
      'prx',
      'puff',
      'ko',
      'mod',
      'so',
    ], // ELF may not have an extension
  },

  GET_ALL_CODE_EXTENTIONS: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.CODE_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_DIAGRAM_EXTENSIONS: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.DIAGRAM_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_BINARY_DOCUMENT_EXTENSIONS: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.BINARY_DOCUMENT_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_BINARY_IMAGE_EXTENSIONS: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.BINARY_IMAGE_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_BINARY_EXTENSIONS: () => {
    let extensions: string[] = [];
    const {BINARY_DOCUMENT_EXTENSIONS, BINARY_IMAGE_EXTENSIONS} = allowedArtifactExtensions;
    const allBinaries: any = {...BINARY_DOCUMENT_EXTENSIONS, ...BINARY_IMAGE_EXTENSIONS};
    Object.values(allBinaries).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_TEXT_DOCUMENT_EXTENSIONS: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.TEXT_DOCUMENT_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_DOCUMENT_EXTENSIONS: () => {
    let extensions: string[] = [];
    const {BINARY_DOCUMENT_EXTENSIONS, TEXT_DOCUMENT_EXTENSIONS} = allowedArtifactExtensions;
    const allDocuments: any = {...BINARY_DOCUMENT_EXTENSIONS, ...TEXT_DOCUMENT_EXTENSIONS};
    Object.values(allDocuments).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },

  GET_ALL_LINUX_EXECUTABLE_BINARY_FILES: () => {
    let extensions: string[] = [];
    Object.values(allowedArtifactExtensions.LINUX_EXECUTABLE_BINARY_FILES_EXTENSIONS).forEach((group: string[]) => {
      extensions = extensions.concat(group);
    });
    return extensions;
  },
};
