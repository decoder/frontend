import { constants } from './constants';

export const artifactTypes: () => string[] = () => {
  const keys: string[] = Object.keys(constants.artifactTypes);
  return keys.map(
    (key: string) => constants.artifactTypes[key]
  );
};

export const artifactIconByType: (type: string) => string = (type: string) => {
  return constants.icons[type.toUpperCase()];
};
