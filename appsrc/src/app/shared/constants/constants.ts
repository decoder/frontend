export interface Constants {
  artifactTypes: {
    DIAGRAM: string;
    CODE: string;
    DOCUMENT: string;
    'ASFM Docs': string;
    ANNOTATION: string;
    COMMENT: string;
    LOG: string;
    'UML MODEL': string;
    TESTAR_STATE_MODEL: string;
    NER: string;
    SRL: string;
    CVE: string;
    'BINARY EXECUTABLE': string,
    'NOT SUPPORTED': string;
  };

  icons: {
    DIAGRAM: string;
    CODE: string;
    DOCUMENT: string;
    'ASFM Docs': string;
    ANNOTATION: string;
    COMMENT: string;
    LOG: string;
    'UML MODEL': string;
    TESTAR_STATE_MODEL: string;
    NER: string;
    SRL: string;
    CVE: string;
    'BINARY EXECUTABLE': string,
    'NOT SUPPORTED': string;
  };
}

export const constants: Constants = {
  artifactTypes: {
    DIAGRAM: 'Diagram',
    CODE: 'Code',
    DOCUMENT: 'Document',
    'ASFM Docs': 'ASFM Docs',
    ANNOTATION: 'Annotation',
    COMMENT: 'Comment',
    LOG: 'Log',
    'UML MODEL': 'UML Model',
    TESTAR_STATE_MODEL: 'TESTAR_State_Model',
    NER: 'NER',
    SRL: 'SRL',
    CVE: 'CVE',
    'BINARY EXECUTABLE': 'Binary Executable',
    'NOT SUPPORTED': 'NOT SUPPORTED'
  },
  icons: {
    DIAGRAM: 'integration_instructions',
    CODE: 'code',
    DOCUMENT: 'library_books',
    'ASFM Docs': 'description',
    ANNOTATION: 'alternate_email',
    COMMENT: 'comment',
    LOG: 'description',
    'UML MODEL': 'device_hub',
    TESTAR_STATE_MODEL: 'device_hub',
    NER: 'analytics',
    SRL: 'analytics',
    CVE: 'description',
    'BINARY EXECUTABLE': 'description',
    'NOT SUPPORTED': 'browser_not_supported'
  }
};
