import {
  Input,
  Output,
  OnDestroy,
  ElementRef,
  ViewChild,
  Component,
  AfterViewInit,
} from '@angular/core';
import { ScrollEmitter, HtmlRef } from './types/infinite-scroll';

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.scss'],
})
export class InfiniteScrollComponent implements AfterViewInit, OnDestroy {
  private observerBottom: IntersectionObserver;
  private observerRight: IntersectionObserver;

  @Input() options: {};
  @Output() horizontalScroll: ScrollEmitter;
  @Output() verticalScroll: ScrollEmitter;
  @ViewChild('bottomAnchor') bottomAnchor: HtmlRef;
  @ViewChild('rightAnchor') rightAnchor: HtmlRef;

  constructor(private host: ElementRef) {
    this.options = {};
    this.horizontalScroll = new ScrollEmitter();
    this.verticalScroll = new ScrollEmitter();
  }

  get element(): any {
    return this.host.nativeElement;
  }

  ngAfterViewInit(): void {
    this.observerBottom = this.observe(
      this.bottomAnchor.nativeElement,
      this.verticalScroll
    );

    this.observerRight = this.observe(
      this.rightAnchor.nativeElement,
      this.horizontalScroll
    );
  }

  ngOnDestroy(): void {
    this.observerBottom.disconnect();
    this.observerRight.disconnect();
  }

  private observe(target: any, emitter: ScrollEmitter): IntersectionObserver {
    const observerRef: IntersectionObserver = new IntersectionObserver(
      ([entry]: IntersectionObserverEntry[]) => {
        if (entry.isIntersecting) {
          emitter.emit();
        }
      },
      this.observerOptions()
    );

    observerRef.observe(target);
    return observerRef;
  }

  private observerOptions(): {} {
    return {
      root: this.isHostScrollable() ? this.host.nativeElement : null,
      ...this.options,
    };
  }

  private isHostScrollable(): boolean {
    const style: CSSStyleDeclaration = window.getComputedStyle(this.element);

    return (
      style.getPropertyValue('overflow') === 'auto' ||
      style.getPropertyValue('overflow-y') === 'scroll' ||
      style.getPropertyValue('overflow-x') === 'scroll'
    );
  }
}
