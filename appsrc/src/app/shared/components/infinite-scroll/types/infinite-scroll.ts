import { EventEmitter, ElementRef } from '@angular/core';

export type HtmlRef = ElementRef<HTMLElement>;

export class ScrollEmitter extends EventEmitter<never> {
  constructor() {
    super();
  }
}
