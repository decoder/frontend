import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableFieldTooltipComponent } from './editable-field-tooltip.component';

describe('EditableFieldTooltipComponent', () => {
  let component: EditableFieldTooltipComponent;
  let fixture: ComponentFixture<EditableFieldTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditableFieldTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableFieldTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
