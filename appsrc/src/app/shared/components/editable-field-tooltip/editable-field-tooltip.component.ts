import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-editable-field-tooltip',
  templateUrl: './editable-field-tooltip.component.html',
  styleUrls: ['./editable-field-tooltip.component.scss'],
})
export class EditableFieldTooltipComponent {
  @Input() icon: string;
  @Input() tooltip: string;
  mouseover: boolean;

  constructor() {
    this.mouseover = false;
    this.icon = 'edit';
    this.tooltip = 'Change value';
  }

  @HostListener('mouseover')
  onMouseOver(): void {
    this.mouseover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.mouseover = false;
  }
}
