import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EllipsisComponent } from './ellipsis.component';

describe('EllipsisComponent', () => {
  let component: EllipsisComponent;
  let fixture: ComponentFixture<EllipsisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EllipsisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EllipsisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should split the given text into filename and extension', () => {
    const filename: string = 'test';
    const extension: string = '.tst';
    component.text = `${filename}${extension}`;

    fixture.detectChanges();

    expect(component.filename).toEqual(filename);
    expect(component.extension).toEqual(extension);
  });
});
