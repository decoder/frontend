import { Component, Input } from '@angular/core';

const EXTENSION_SEPARATOR: string = '.';

interface File {
  filename: string;
  extension: string;
}

@Component({
  selector: 'app-ellipsis',
  templateUrl: './ellipsis.component.html',
  styleUrls: ['./ellipsis.component.scss']
})
export class EllipsisComponent {
  private _file: File;

  constructor() {
    this._file = { filename: '', extension: '' };
  }

  @Input() set text(value: string) {
    this._file = this.splitNameAndExtension(value);
  }

  get filename(): string {
    return this._file.filename;
  }
  get extension(): string {
    return this._file.extension;
  }

  decodeName(name: string): string {
    return decodeURIComponent(name);
  }

  private splitNameAndExtension(file: string): File {
    if (file !== undefined) {
      const fileChunks: string[] = file.split(EXTENSION_SEPARATOR);
      const splittedFile: File = { filename: file, extension: '' };

      if (fileChunks.length >= 2) {
        const extension: string = `${EXTENSION_SEPARATOR}${fileChunks.pop()}`;
        splittedFile.filename = fileChunks.join(EXTENSION_SEPARATOR);
        splittedFile.extension = extension;
      }

      return splittedFile;
    } else {
      return;
    }
  }
}
