import {
  PassedTime,
  ONE_SECOND,
  ONE_MINUTE,
  ONE_HOUR,
  ONE_DAY,
  ONE_WEEK
} from './passed-time';

describe('PassedTime', () => {
  let passedTime: PassedTime;
  let currentTime: number;

  beforeEach(() => {
    const current: Date = new Date('1995-12-17T03:24:00');
    passedTime = new PassedTime();
    currentTime = current.getTime();
    jasmine.clock().install();
    jasmine.clock().mockDate(current);
  });

  it('should have passed a second', () => {
    expectPassedTime(ONE_SECOND, 'second');
  });

  it('should have passed seconds', () => {
    expectPassedTime(2 * ONE_SECOND, 'seconds');
  });

  it('should have passed a minute', () => {
    expectPassedTime(ONE_MINUTE, 'minute');
  });

  it('should have passed minutes', () => {
    expectPassedTime(2 * ONE_MINUTE, 'minutes');
  });

  it('should have passed an hour', () => {
    expectPassedTime(ONE_HOUR, 'hour');
  });

  it('should have passed hours', () => {
    expectPassedTime(2 * ONE_HOUR, 'hours');
  });

  it('should have passed a day', () => {
    expectPassedTime(ONE_DAY, 'day');
  });

  it('should have passed days', () => {
    expectPassedTime(2 * ONE_DAY, 'days');
  });

  it('should have passed a week', () => {
    expectPassedTime(ONE_WEEK, 'week');
  });

  function expectPassedTime(reducedTime: number, reducedTimeName: string): void {
    const increasedDate: Date = new Date(currentTime - reducedTime);
    const passed: string = passedTime.whenDidItHappen(increasedDate);
    expect(passed.endsWith(`${reducedTimeName} ago`)).toBeTruthy();
  }

  afterEach(() => {
    jasmine.clock().uninstall();
  });
});
