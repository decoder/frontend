import { Injectable } from '@angular/core';

export const ONE_SECOND: number = 1000;
export const ONE_MINUTE: number = ONE_SECOND * 60;
export const ONE_HOUR: number = ONE_MINUTE * 60;
export const ONE_DAY: number = ONE_HOUR * 24;
export const ONE_WEEK: number = ONE_DAY * 7;

@Injectable()
export class PassedTime {
  private past: Date;
  private current: Date;
  private miliseconds: number;

  whenDidItHappen(past: Date | string): string {
    this.past = new Date(past);
    this.current = new Date();
    this.miliseconds = this.current.getTime() - this.past.getTime();

    return this.getPassedTime();
  }

  private getPassedTime(): string {
    let passedTime: string = this.past.toDateString();

    if (this.miliseconds < ONE_SECOND) {
      passedTime = this.timeAgo(ONE_SECOND, 'second');
    }

    if (this.miliseconds >= ONE_SECOND && this.miliseconds < ONE_MINUTE) {
      passedTime = this.timeAgo(ONE_SECOND, 'second');
    }

    if (this.miliseconds >= ONE_MINUTE && this.miliseconds < ONE_HOUR) {
      passedTime = this.timeAgo(ONE_MINUTE, 'minute');
    }

    if (this.miliseconds >= ONE_HOUR && this.miliseconds < ONE_DAY) {
      passedTime = this.timeAgo(ONE_HOUR, 'hour');
    }

    if (this.miliseconds >= ONE_DAY && this.miliseconds < ONE_WEEK) {
      passedTime = this.timeAgo(ONE_DAY, 'day');
    }

    if (this.miliseconds === ONE_WEEK) {
      passedTime = this.timeAgo(ONE_WEEK, 'week');
    }

    return passedTime;
  }

  private timeAgo(timeUnit: number, timeUnitName: string): string {
    const plural: string = this.getPlural(timeUnit);
    const time: number = Math.floor(this.miliseconds / timeUnit);
    return `${time} ${timeUnitName}${plural} ago`;
  }

  private getPlural(comparedTo: number): string {
    const lessThanTwo: boolean = this.miliseconds < 2 * comparedTo;
    if (lessThanTwo) {
      return '';
    }

    return 's';
  }
}
