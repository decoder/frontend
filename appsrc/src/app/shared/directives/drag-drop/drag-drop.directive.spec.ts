import { DragDropDirective } from './drag-drop.directive';
import { flush, fakeAsync } from '@angular/core/testing';

describe('DragDropDirective', () => {
  let directive: DragDropDirective;
  let dragEvent: DragEvent;

  beforeEach(() => {
    directive = new DragDropDirective();
    dragEvent = {
      preventDefault: () => null,
      stopPropagation: () => null,
      dataTransfer: {
        files: mockEmptyFileList()
      }
    } as DragEvent;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should prevent event propagation [onDragOver]', () => {
    spyOn(dragEvent, 'preventDefault');
    spyOn(dragEvent, 'stopPropagation');

    directive.onDragOver(dragEvent);
    expect(dragEvent.preventDefault).toHaveBeenCalled();
    expect(dragEvent.stopPropagation).toHaveBeenCalled();
  });

  it('should prevent event propagation [onDragLeave]', () => {
    spyOn(dragEvent, 'preventDefault');
    spyOn(dragEvent, 'stopPropagation');

    directive.onDragLeave(dragEvent);
    expect(dragEvent.preventDefault).toHaveBeenCalled();
    expect(dragEvent.stopPropagation).toHaveBeenCalled();
  });

  it('should emit files [ondrop] if there is any', fakeAsync(() => {
    dragEvent = {
      ...dragEvent,
      dataTransfer: {
        files: mockFileList()
      }
    } as DragEvent;

    directive.fileDrop.subscribe((files: FileList) => {
      expect(JSON.stringify(files)).toEqual(JSON.stringify(mockFileList()));
    });

    directive.ondrop(dragEvent);
    flush();
  }));

  it('should not emit files [ondrop] if there is none', fakeAsync(() => {
    let areFilesEmmited: boolean = true;
    directive.fileDrop.subscribe((_: FileList) => {
      areFilesEmmited = false;
    });

    directive.ondrop(dragEvent);
    flush();
    expect(areFilesEmmited).toBe(true);
  }));
});

function mockFileList(): FileList {
  const blob: Blob = new Blob([''], { type: 'text/html' });
  blob['lastModifiedDate'] = '';
  blob['name'] = 'filename';
  const file: File = <File>blob;
  return {
    0: file,
    1: file,
    length: 2,
    item: (_: number) => file
  };
}

function mockEmptyFileList(): FileList {
  return {
    length: 0,
    item: (_: number) => undefined
  };
}
