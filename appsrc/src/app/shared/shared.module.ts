import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { EllipsisComponent } from './components/ellipsis/ellipsis.component';
import { RouterModule } from '@angular/router';
import { TimeAgoPipe } from './pipes/time-ago/time-ago.pipe';
import { PassedTime } from './services/passed-time/passed-time';
import { MaterialModule } from '../material.module';
import { DragDropDirective } from './directives/drag-drop/drag-drop.directive';
import { InfiniteScrollComponent } from './components/infinite-scroll/infinite-scroll.component';
import { EditableFieldTooltipComponent } from './components/editable-field-tooltip/editable-field-tooltip.component';
import { LengthCheckPipe } from './pipes/length-check/length-check.pipe';
import { TimestampPipe } from './pipes/timestamp/timestamp.pipe';
import { TruncatePipe } from './pipes/truncate-pipe/truncate.pipe';

@NgModule({
  declarations: [
    SnackbarComponent,
    EllipsisComponent,
    TimeAgoPipe,
    EditableFieldTooltipComponent,
    DragDropDirective,
    InfiniteScrollComponent,
    LengthCheckPipe,
    TimestampPipe,
    TruncatePipe,
  ],
  imports: [CommonModule, MaterialModule, RouterModule],
  exports: [
    DragDropDirective,
    EditableFieldTooltipComponent,
    EllipsisComponent,
    InfiniteScrollComponent,
    TimeAgoPipe,
    TimestampPipe,
    LengthCheckPipe,
    TruncatePipe,
  ],
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2000 } },
    DatePipe,
    LengthCheckPipe,
    PassedTime,
  ],
  entryComponents: [SnackbarComponent],
})
export class SharedModule {}
