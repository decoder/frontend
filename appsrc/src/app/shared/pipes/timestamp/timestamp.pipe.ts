import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timestamp',
})
export class TimestampPipe implements PipeTransform {
  transform(dateTime: string): string {
    const aux: string[] = dateTime.split('_', 2);

    const year: string = aux[0].substr(0, 4);
    const month: string = aux[0].substr(4, 2);
    const day: string = aux[0].substr(6, 2);

    const hour: string = aux[1].substr(0, 2);
    const minutes: string = aux[1].substr(2, 2);
    const seconds: string = aux[1].substr(4, 2);

    const date: string = day + '/' + month + '/' + year;
    const time: string = hour + ':' + minutes + ':' + seconds;

    return date + ' at ' + time;
  }
}
