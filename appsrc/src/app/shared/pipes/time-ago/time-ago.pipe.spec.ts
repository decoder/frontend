import { async, TestBed } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { TimeAgoPipe } from './time-ago.pipe';
import { PassedTime } from '../../services/passed-time/passed-time';

interface SpyValues {
  passedTime: string;
  datePipe: string;
}

type Expectable = jasmine.ArrayLikeMatchers<string>;

describe('TimeAgoPipe', () => {
  let expectFormat: (spyValues: SpyValues) => Expectable;
  let pipe: TimeAgoPipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimeAgoPipe],
      providers: [PassedTime, DatePipe, TimeAgoPipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    pipe = TestBed.inject(TimeAgoPipe);
  });

  beforeEach(() => {
    expectFormat = (spyValues: SpyValues): Expectable => {
      const datePipe: DatePipe = TestBed.inject(DatePipe);
      const passedTime: PassedTime = TestBed.inject(PassedTime);

      spyOn(passedTime, 'whenDidItHappen').and.returnValue(
        spyValues.passedTime
      );
      spyOn(datePipe, 'transform').and.returnValue(spyValues.datePipe);

      const formattedDate: string = pipe.transform(new Date());
      return expect(formattedDate);
    };
  });
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should give the date formatted by DatePipe', () => {
    const spyValues: SpyValues = {
      passedTime: '1995-12-17',
      datePipe: '6 Dic 1995'
    };
    return expectFormat(spyValues).toEqual('6 Dic 1995');
  });

  it('should give the date formatted by PassedTime', () => {
    const spyValues: SpyValues = {
      passedTime: '2 days ago',
      datePipe: '6 Dic 1995'
    };
    return expectFormat(spyValues).toEqual('2 days ago');
  });
});
