import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PassedTime } from '../../services/passed-time/passed-time';

const DATE_FORMAT: string = 'd MMM yyyy';

@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

  constructor(private passedTime: PassedTime, private datePipe: DatePipe) {}

  transform(date: Date): string {
    return this.formatDate(date);
  }

  formatDate(date: Date): string {
    let stringifiedDate: string = this.passedTime.whenDidItHappen(date);
    const pastDate: Date = new Date(stringifiedDate);
    const isValidDate: boolean = !isNaN(pastDate.getTime());
    if (isValidDate) {
      stringifiedDate = this.datePipe.transform(pastDate, DATE_FORMAT);
    }

    return stringifiedDate;
  }

}
