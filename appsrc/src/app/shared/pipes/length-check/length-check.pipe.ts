import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lengthCheck'
})
export class LengthCheckPipe implements PipeTransform {

  transform(userName: string = ''): string {
    return this.formatUserName(userName);
  }

  formatUserName(userName: string): string {
    if (userName.length > 13) {
        return `${localStorage.getItem('currentUser').substring(0, 13)}...`;
      } else {
        return localStorage.getItem('currentUser');
      }
  }

}
