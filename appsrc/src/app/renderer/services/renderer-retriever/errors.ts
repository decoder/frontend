export class Errors {
  static throwRendererNotFound(): never {
    throw new Error('No renderers found in the config file');
  }

  static throwMissingConfig(): never {
    throw new Error(
      'Missing importFunction or componentName in configuration file'
    );
  }
}
