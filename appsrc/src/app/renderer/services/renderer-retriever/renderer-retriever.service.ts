import { Injectable } from '@angular/core';
import { Renderable } from '../../models/renderable.model';
import { rendererPlugins } from '../../../plugins/plugins.config';
import { Errors } from './errors';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { allowedArtifactExtensions } from 'src/app/shared/constants/allowed-extensions';

@Injectable({
  providedIn: 'root',
})
export class RendererRetrieverService {
  getRenderer(extension: string, artifact: PKMArtifact): Promise<Renderable> {
    const config: any = this.getRendererFromConfig(extension, artifact);

    return config
      .importFunction()
      .then((m: any) => {
        return m[config.componentName];
      })
      .catch(() => Errors.throwRendererNotFound());
  }

  private getRendererFromConfig(extension: string, artifact: PKMArtifact): any {
    const codeStrings: string[] =
      allowedArtifactExtensions.GET_ALL_CODE_EXTENTIONS();
    let config: any;
    if (codeStrings.includes(extension)) {
      if (
        artifact.type === 'Log' || artifact.type === 'CVE' || artifact.type === 'ASFM Docs'
        ) {
        config = rendererPlugins['json'];
      } else {
        config = rendererPlugins['code'];
      }
    } else if (
      artifact.type === 'Document' ||
      artifact.type === 'Binary Executable'
    ) {
      config = rendererPlugins['document'];
    } else {
      config = rendererPlugins[extension];
    }
    if (!config) {
      config = rendererPlugins.default;
    }

    if (this.isValidConfig(config)) {
      return config;
    } else {
      Errors.throwMissingConfig();
    }
  }

  private isValidConfig(config: any): boolean {
    if (!config) {
      return false;
    }
    const hasImport: boolean = !!config.importFunction;
    const hasName: boolean = !!config.componentName;

    return hasImport && hasName;
  }
}
