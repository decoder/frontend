import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { Renderable } from '../../models/renderable.model';

import { RendererRetrieverService } from './renderer-retriever.service';

describe('RenderRetrieverService', () => {
  let service: RendererRetrieverService;
  let dummyArtifact: PKMArtifact;
  dummyArtifact = new PKMArtifact();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RendererRetrieverService],
    });
    service = TestBed.inject(RendererRetrieverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getRenderer', () => {
    it('should call get with a valid extension', () => {
      service.getRenderer('java', dummyArtifact).then((renderable: Renderable) => {
        expect(renderable.name).toBe('CodeRendererComponent');
      });
    });
  });
});
