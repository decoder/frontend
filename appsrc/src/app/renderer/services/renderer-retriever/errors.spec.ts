import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RendererRetrieverService } from './renderer-retriever.service';

describe('Renderer retriever errors', () => {
  let service: RendererRetrieverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RendererRetrieverService],
    });

    service = TestBed.inject(RendererRetrieverService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
