import { SafeResourceUrl } from '@angular/platform-browser';
import { PKMArtifact } from 'src/app/core/models/artifact';

export interface RenderingConfiguration {
  extension: string;
  content: SafeResourceUrl;
  originalUrl: string;
  artifact?: PKMArtifact;
}
