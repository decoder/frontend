import { Type } from '@angular/core';

export type Renderable = Type<any>;
