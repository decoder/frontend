import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendererComponent } from './renderer.component';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getMockRetriever } from 'src/app/plugins/project/testing/services';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('RendererComponent', () => {
  let component: RendererComponent;
  let fixture: ComponentFixture<RendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendererComponent ],
      imports: [HttpClientTestingModule, MatSnackBarModule],
      providers: [
        {
          provide: ArtifactsRetrieverService,
          useValue: getMockRetriever(),
        },
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
