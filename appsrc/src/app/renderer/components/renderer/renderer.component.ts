import {
  Component,
  ComponentFactoryResolver,
  ViewChild,
  Input,
  ComponentFactory,
  ViewContainerRef,
  ComponentRef,
  Output,
  EventEmitter
} from '@angular/core';
import { Renderable } from '../../models/renderable.model';
import { RendererRetrieverService } from '../../services/renderer-retriever/renderer-retriever.service';
import { RendererPlaceholderDirective } from '../../directives/renderer-placeholder/renderer-placeholder.directive';
import { RenderingConfiguration } from '../../models/rendering-configuration.model';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
})
export class RendererComponent {
  private renderable: Renderable;
  private componentRef: ComponentRef<any>;
  @ViewChild(RendererPlaceholderDirective, { static: true })
  private rendererHost: RendererPlaceholderDirective;
  @Input() projectName: string;
  @Output() editRawSourceCode: EventEmitter<never>;
  errorMessage: string;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private rendererRetriever: RendererRetrieverService,
    private artifactRetrieverService: ArtifactsRetrieverService,
    public snackBarService: SnackbarService
  ) {
    this.editRawSourceCode = new EventEmitter();
   }

  @Input() set renderingConfig({ extension, content, originalUrl, artifact }: RenderingConfiguration) {
      this.setErrorMesage('');
      this.getRenderer(extension, artifact).then((renderable: Renderable) => {
        this.renderable = renderable;
        this.loadComponent();
        this.setComponentData(content, extension, artifact, this.projectName);
      }).catch((err: any) => this.setErrorMesage(err as string));
  }

  onSaveModifiedCode(): void {
    if (this.componentRef.instance.modifiedContent) {
      this.artifactRetrieverService
        .putPKMRawSourceCode(
          this.projectName,
          {
            ...this.componentRef.instance.artifact,
            content: this.componentRef.instance.modifiedContent
          }
        )
        .subscribe(() => {
          this.snackBarService.openSnackBar('File have been updated correctly!', 'check_circle');
          this.editRawSourceCode.emit();
        },
          (error: any) => {
            this.snackBarService.openSnackBar('ERROR: An error has occurred', 'error');
          });
    } else {
      this.snackBarService.openSnackBar('You have not made any changes to the file to be able to save them', 'warning');
    }
  }

  private getRenderer(extension: string, artifact: PKMArtifact): Promise<Renderable> {
    return this.rendererRetriever.getRenderer(extension, artifact);
  }

  private loadComponent(): void {
    const componentFactory: ComponentFactory<any> = this.componentFactoryResolver.resolveComponentFactory(
      this.renderable
    );
    const viewContainerRef: ViewContainerRef = this.rendererHost
      .viewContainerRef;
    viewContainerRef.clear();
    this.componentRef = viewContainerRef.createComponent(componentFactory);
  }

  private setComponentData(content: any, extension: any, artifact: PKMArtifact, projectName: string): void {
    this.componentRef.instance.content = content;
    this.componentRef.instance.extension = extension;
    this.componentRef.instance.artifact = artifact;
    this.componentRef.instance.projectName = projectName;
  }

  private setErrorMesage(error: string): void {
    this.errorMessage = error;
  }
}
