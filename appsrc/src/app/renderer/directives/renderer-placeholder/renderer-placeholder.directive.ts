import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appRendererPlaceholder]'
})
export class RendererPlaceholderDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
