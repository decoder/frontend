import { RendererPlaceholderDirective } from './renderer-placeholder.directive';

describe('RenderPlaceholderDirective', () => {
  it('should create an instance', () => {
    const directive: RendererPlaceholderDirective = new RendererPlaceholderDirective(null);
    expect(directive).toBeTruthy();
  });
});
