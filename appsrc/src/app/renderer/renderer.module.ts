import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RendererComponent } from './components/renderer/renderer.component';
import { RendererPlaceholderDirective } from './directives/renderer-placeholder/renderer-placeholder.directive';
import { RendererRetrieverService } from './services/renderer-retriever/renderer-retriever.service';
import { RenderersModule } from '../plugins/renderers/renderers.module';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [RendererComponent, RendererPlaceholderDirective],
  imports: [
    CommonModule,
    RenderersModule,
    FormsModule,
    MonacoEditorModule,
],
  providers: [RendererRetrieverService],
  exports: [RendererComponent]
})
export class RendererModule {}
