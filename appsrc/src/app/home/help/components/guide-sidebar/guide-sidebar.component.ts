import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-guide-sidebar',
  templateUrl: './guide-sidebar.component.html',
  styleUrls: ['./guide-sidebar.component.scss']
})
export class GuideSidebarComponent implements OnInit {
  userGuideLinks: string[];
  userGuideSize: number;
  selectedLink: string;
  @Output() selectedLinkToEmit: EventEmitter<number>;

  constructor() {
    this.selectedLinkToEmit = new EventEmitter<number>();
  }

  ngOnInit(): void {
    this.userGuideLinks = [
      'User guide',
      'Login',
      'Profile selection',
      'User area ',
      'Projects list',
      'Project version and import files',
      'Project members',
      'Git manager',
      'Reviews',
      'Project components view',
      'Decoder Tools',
      'Traceability matrix results',
      'Project browser renderers',
      'Jupyter search engine view',
      'Support page',
    ];

    this.userGuideSize = this.userGuideLinks.length;
  }

  onLinkSelection(link: string, index: number): void {
    this.selectedLink = link;
    this.selectedLinkToEmit.emit(index);
  }

  isLinkSelected(link: string, index: number): boolean {
    return link === this.selectedLink;
  }
}
