import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideSidebarComponent } from './guide-sidebar.component';

describe('GuideSidebarComponent', () => {
  let component: GuideSidebarComponent;
  let fixture: ComponentFixture<GuideSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuideSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select a link [onLinkSelection]', () => {
    const selectedLink: string = 'link1';
    const selectedIndex: number = 1;
    component.onLinkSelection(selectedLink, selectedIndex);
    expect(component.selectedLink).toEqual(selectedLink);
  });

  it('should return true if the Link is selected, false otherwise [isLinkSelected]', () => {
    const selectedLink: string = 'link1';
    const selectedIndex: number = 1;
    const nonSelectedLink: string = 'link2';
    const nonSelectedIndex: number = 2;

    component.onLinkSelection(selectedLink, selectedIndex);

    expect(component.isLinkSelected(selectedLink, selectedIndex)).toBeTruthy();
    expect(component.isLinkSelected(nonSelectedLink, nonSelectedIndex)).toBeFalsy();
  });
});
