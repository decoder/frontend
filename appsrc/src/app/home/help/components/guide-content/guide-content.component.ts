import { Component, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-guide-content',
  templateUrl: './guide-content.component.html',
  styleUrls: ['./guide-content.component.scss']
})

export class GuideContentComponent {
  @Input() selectedLinkId: number = 0;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedLinkId) {
      this.scroll(this.selectedLinkId);
    }
  }

  scroll(id: number): void {
    const el: HTMLElement = document.getElementById(id.toString());
    el.scrollIntoView();
  }
}
