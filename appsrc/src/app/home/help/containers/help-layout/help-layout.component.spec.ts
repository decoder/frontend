import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpLayoutComponent } from './help-layout.component';

describe('HelpLayoutComponent', () => {
  let component: HelpLayoutComponent;
  let fixture: ComponentFixture<HelpLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select a link', () => {
    const linkId: number = 1;
    component.onSelectLink(linkId);
    expect(component.selectedLinkId).toEqual(linkId);
  });
});
