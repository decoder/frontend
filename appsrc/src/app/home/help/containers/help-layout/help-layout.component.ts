import { Component } from '@angular/core';

@Component({
  selector: 'app-help-layout',
  templateUrl: './help-layout.component.html',
  styleUrls: ['./help-layout.component.scss']
})
export class HelpLayoutComponent {
  selectedLinkId: number = 0;

  onSelectLink(linkId: number): void {
    this.selectedLinkId = linkId;
  }
}
