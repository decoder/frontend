import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpLayoutComponent } from './containers/help-layout/help-layout.component';
import { Routes, RouterModule } from '@angular/router';
import { GuideSidebarComponent } from './components/guide-sidebar/guide-sidebar.component';
import { GuideContentComponent } from './components/guide-content/guide-content.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatListModule } from '@angular/material/list';

const routes: Routes = [
  {
    path: '',
    component: HelpLayoutComponent,
  },
];

@NgModule({
  declarations: [
    HelpLayoutComponent,
    GuideSidebarComponent,
    GuideContentComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedModule,
    MatListModule,
    RouterModule.forChild(routes),
  ],
})
export class HelpModule {}
