import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { createRoutes } from '../plugins/routes-creator';
import { ProjectsListLayoutComponent } from './projects-list/containers/projects-list-layout/projects-list-layout.component';
import { ProjectsListModule } from './projects-list/projects-list.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtHttpInterceptor } from '../interceptors/jwt-http-interceptor.interceptor';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'projects-list' },
  {
    path: 'projects-list',
    component: ProjectsListLayoutComponent,
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then((m: any) => m.ProfileModule),
  },
  {
    path: 'help',
    loadChildren: () =>
      import('./help/help.module').then((m: any) => m.HelpModule),
  },
];
routes.push(...createRoutes());

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProjectsListModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtHttpInterceptor, multi: true },
  ],
})
export class HomeModule {}
