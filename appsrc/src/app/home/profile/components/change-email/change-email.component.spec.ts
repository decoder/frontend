import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { ChangeEmailComponent } from './change-email.component';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

class LocationStub {
  back(): void { return; }
}

class SnackbarServiceStub {
  success(): void { return; }
}

describe('ChangeEmailComponent', () => {
  let component: ChangeEmailComponent;
  let fixture: ComponentFixture<ChangeEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeEmailComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: Location, useClass: LocationStub },
        { provide: SnackbarService, useClass: SnackbarServiceStub },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false [hasDifferentEmails]', fakeAsync(() => {
    component.emailForm.get('newEmail').setValue('alfred@capgemini.com');
    component.emailForm.get('confirmNewEmail').setValue('alfred@capgemini.com');
    spyOn(component, 'hasDifferentEmails').and.callThrough();

    fixture.detectChanges();

    const differentEmails: boolean = component.hasDifferentEmails();

    expect(differentEmails).toBeFalsy();
    expect(component.hasDifferentEmails).toHaveBeenCalled();
  }));

  it('should be true [hasDifferentEmails]', fakeAsync(() => {
    component.emailForm.get('newEmail').setValue('alfred@capgemini.com');
    component.emailForm.get('confirmNewEmail').setValue('alonso@capgemini.com');
    spyOn(component, 'hasDifferentEmails').and.callThrough();

    fixture.detectChanges();

    const differentEmails: boolean = component.hasDifferentEmails();

    expect(differentEmails).toBeTruthy();
    expect(component.hasDifferentEmails).toHaveBeenCalled();
  }));

  it('should be false [onSubmitEmail]', fakeAsync(() => {
    component.emailForm.get('newEmail').setValue('alfred@capgemini.com');
    component.emailForm.get('confirmNewEmail').setValue('alfred@capgemini.com');
    spyOn(component, 'onSubmitEmail').and.callThrough();

    component.onSubmitEmail();

    fixture.detectChanges();

    expect(component.onSubmitEmail).toHaveBeenCalled();
  }));
});
