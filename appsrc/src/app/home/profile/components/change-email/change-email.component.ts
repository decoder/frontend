import { Component } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl,
  ValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { Location } from '@angular/common';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { UserService } from '../../services/user/user.service';

const EMAIL_VALIDATORS: ValidatorFn[] = [Validators.required, Validators.email];
const SUCCESS_MESSAGE: string =
  'We have already sent you an autentication code.' +
  'Check your email and click on the link to verify.';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss'],
})
export class ChangeEmailComponent {
  emailForm: FormGroup;

  constructor(
    private location: Location,
    private snackbarService: SnackbarService,
    private userService: UserService
  ) {
    this.emailForm = new FormGroup(
      {
        newEmail: new FormControl('', EMAIL_VALIDATORS),
        confirmNewEmail: new FormControl('', EMAIL_VALIDATORS),
      },
      this.checkEqualEmails
    );

    this.emailForm.setValidators(this.checkEqualEmails.bind(this));
  }

  onSubmitEmail(): void {
    this.userService.updateEmail(this.emailForm.get('newEmail').value).subscribe(
      (response: any) => {
        this.snackbarService.success(SUCCESS_MESSAGE);
        this.location.back();
      }
    );
  }

  hasDifferentEmails(): boolean {
    return this.emailForm.get('confirmNewEmail').errors?.notEqualEmails;
  }

  private checkEqualEmails(form: FormGroup): ValidationErrors {
    const newEmail: string = form.get('confirmNewEmail').value;
    const confirmedNewEmail: string = form.get('newEmail').value;
    let errors: ValidationErrors = { notEqualEmails: true };

    if (newEmail === confirmedNewEmail) {
      errors = null;
    }

    form.controls.confirmNewEmail.setErrors(errors);
    return errors;
  }
}
