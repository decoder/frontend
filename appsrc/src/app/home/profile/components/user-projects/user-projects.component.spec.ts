import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProjectsComponent } from './user-projects.component';
import { ProfileModule } from '../../profile.module';

describe('UserProjectsComponent', () => {
  let component: UserProjectsComponent;
  let fixture: ComponentFixture<UserProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ProfileModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('table columns should be project and role', () => {
    expect(component.displayedColumns).toEqual(['project', 'role']);
  });

  it('table starts with only header row', () => {
    const tableRows: NodeListOf<HTMLElement> = fixture.nativeElement.querySelectorAll('tr');
    expect(tableRows.length).toBe(1);
  });

  it('table headers display: Project name and Role', () => {
    const table: HTMLElement = fixture.nativeElement.querySelector('table');
    const columns: NodeListOf<HTMLElement> = table.querySelectorAll('.mat-header-cell');
    expect(columns[0].textContent).toBe('Project name');
    expect(columns[1].textContent).toBe('Role');
  });
});
