import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeUserPasswordComponent } from './change-user-password.component';
import { ProfileModule } from '../../profile.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

describe('ChangeUserPasswordComponent', () => {
  let component: ChangeUserPasswordComponent;
  let fixture: ComponentFixture<ChangeUserPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ProfileModule,
        RouterTestingModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      providers: [AppConfiguration]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeUserPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a FormGroup on init with fields oldPassword, newPassword and confirmNewPassword', () => {
    component.ngOnInit();
    const controls: { [key: string]: AbstractControl } =
      component.changeUserPasswordForm.controls;
    const currentPassword: AbstractControl = controls.currentPassword;
    const newPassword: AbstractControl = controls.newPassword;
    const confirmNewPassword: AbstractControl = controls.confirmNewPassword;

    expect(currentPassword).toBeTruthy();
    expect(newPassword).toBeTruthy();
    expect(confirmNewPassword).toBeTruthy();
  });

  it('should return false for different passwords', () => {
    component.ngOnInit();
    const controls: { [key: string]: AbstractControl } =
      component.changeUserPasswordForm.controls;
    const newPassword: AbstractControl = controls.newPassword;
    const confirmNewPassword: AbstractControl = controls.confirmNewPassword;

    newPassword.setValue('pass1');
    confirmNewPassword.setValue('pass2');

    const validation: boolean = component.passwordEqual();
    expect(validation).toBe(false);
  });

  it('should get the new password', () => {
    expect(component.getNewPassword()).toBeTruthy();
  });

  it('should get the confirmed password', () => {
    expect(component.getConfirmNewPassword()).toBeTruthy();
  });

  it('should get the current password', () => {
    expect(component.getCurrentPassword()).toBeTruthy();
  });

  it('pristine', () => {
    component.ngOnInit();
    expect(component.checkPristine()).toBe(false);
  });

  it('should be false if the fields are not filled', () => {
    component.ngOnInit();
    expect(component.checkInvalidation()).toBe(true);
  });

  it('passwords should be equals after the creation', () => {
    component.ngOnInit();
    expect(component.passwordEqual()).toBe(true);
  });
});
