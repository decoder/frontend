import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from '../../../../core/services/snackbar/snackbar.service';
import { UserService } from '../../services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-change-user-password',
  templateUrl: './change-user-password.component.html',
  styleUrls: ['./change-user-password.component.scss']
})
export class ChangeUserPasswordComponent implements OnInit {
  changeUserPasswordForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly snackbarService: SnackbarService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.changeUserPasswordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required]
    });
  }

  checkCurrentPassword(): boolean {
    return (this.changeUserPasswordForm.controls.currentPassword.pristine
      || (!this.changeUserPasswordForm.controls.currentPassword.invalid && this.passwordNotEqual()));
  }

  checkNewPassword(): boolean {
    return (this.changeUserPasswordForm.controls.newPassword.pristine
      || (!this.changeUserPasswordForm.controls.newPassword.invalid && this.passwordEqual()
      && this.passwordNotEqual()));
  }

  checkConfirmNewPassword(): boolean {
    return (this.changeUserPasswordForm.controls.confirmNewPassword.pristine
      || (!this.changeUserPasswordForm.controls.confirmNewPassword.invalid
      && this.passwordEqual() && this.passwordNotEqual()));
  }

  checkPristine(): boolean {
    return !(this.changeUserPasswordForm.controls.currentPassword.pristine
      && this.changeUserPasswordForm.controls.newPassword.pristine
      && this.changeUserPasswordForm.controls.confirmNewPassword.pristine);
  }

  passwordEqual(): boolean {
    if (this.changeUserPasswordForm.controls.confirmNewPassword.value !== '') {
      return (this.changeUserPasswordForm.controls.newPassword.value === this.changeUserPasswordForm.controls.confirmNewPassword.value);
    }
    return true;
  }

  passwordNotEqual(): boolean {
    if (this.changeUserPasswordForm.controls.newPassword.value !== '') {
      return (this.changeUserPasswordForm.controls.currentPassword.value !== this.changeUserPasswordForm.controls.newPassword.value);
    }
    return true;
  }

  checkInvalidation(): boolean {
      return (this.changeUserPasswordForm.invalid || !this.passwordEqual() || !this.passwordNotEqual());
  }

  resetPassword(): void {
    // KIRBY - In error is recommended include some error message (maybe with snackBarService)
    this.userService.updatePassword(this.changeUserPasswordForm.get('newPassword').value).subscribe(
      (response: any) => {
        this.snackbarService.success('Password changed successfuly!');
        this.router.navigate(['/home/profile/user-area']);
      },
      (err: HttpErrorResponse) => this.changeUserPasswordForm.reset()
    );
  }

  getCurrentPassword(): AbstractControl {
    return this.changeUserPasswordForm.controls.currentPassword;
  }

  getNewPassword(): AbstractControl {
    return this.changeUserPasswordForm.controls.newPassword;
  }

  getConfirmNewPassword(): AbstractControl {
    return this.changeUserPasswordForm.controls.confirmNewPassword;
  }

}
