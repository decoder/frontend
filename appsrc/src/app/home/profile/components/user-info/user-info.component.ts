import { Component, Input } from '@angular/core';
import { Role } from 'src/app/core/models/role';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent {
  @Input() email: string;
  @Input() firstName: string;
  @Input() lastName: string;
  @Input() name: string;
  @Input() phone: string;
  @Input() projects: Role[];

  password: string = '********';
  hovering: boolean;
  imgSrc: string | ArrayBuffer = '';

  constructor() {
    this.hovering = false;
    this.imgSrc = '../../../../../assets/img/account_circle2.svg';
  }

  processFile(event: any): void {
    const files: File[] = event.target['files'];

    if (files && files[0]) {
      const reader: FileReader = new FileReader();

      reader.onloadend = () => {
        this.imgSrc = reader.result;
      };

      reader.readAsDataURL(files[0]);
    }
  }
}
