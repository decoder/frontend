import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAreaComponent } from './user-area.component';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';
import { HttpClientModule } from '@angular/common/http';
import { getProjectMockRetriever } from 'src/app/plugins/project/testing/services/project-retriever.mock';
import { pkmUserMock } from 'src/app/mocks/core/fixtures/pkmUser.mock';
import { rolesMapMock } from 'src/app/mocks/core/fixtures/roleMap.mock';
import { Role } from 'src/app/core/models/role';
import { AppConfiguration } from 'src/config/app-configuration';

describe('UserAreaComponent', () => {
  let component: UserAreaComponent;
  let fixture: ComponentFixture<UserAreaComponent>;
  let projectsServiceTest: ProjectsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserAreaComponent],
      imports: [HttpClientModule],
      providers: [
        { provide: ProjectsService, useValue: getProjectMockRetriever() },
        AppConfiguration
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    projectsServiceTest = TestBed.inject(ProjectsService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAreaComponent);
    component = fixture.componentInstance;
    component.currentUser = pkmUserMock[0];

    fixture.detectChanges();
  });

  it('should works service', () => {
    projectsServiceTest.getUserInfo();
    expect(component.currentUser).toBeTruthy();
  });

  it('should convert map to array', () => {
    component.allRoles = rolesMapMock;
    const retrieverRole: Role[] = component.convertMapToArray(rolesMapMock);
    expect(component.convertMapToArray(rolesMapMock)).toEqual(retrieverRole);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
