import { Component, OnInit } from '@angular/core';
import { ProjectsService } from 'src/app/home/projects-list/services/projects/projects.service';
import { PKMUser } from 'src/app/core/models/PKMUser';
import { Role } from 'src/app/core/models/role';

export interface Project {
  name: string;
  role: string;
  date: string;
}

@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.scss'],
})
export class UserAreaComponent implements OnInit {
  currentUser: PKMUser;
  allRoles: Map<string, string[]>;
  rolesArray: any;

  constructor(private projectsService: ProjectsService) {
    this.currentUser = {
      email: '',
      first_name: '',
      last_name: '',
      name: '',
      phone: '',
      roles: [],
    };
  }

  ngOnInit(): void {
    this.projectsService.getUserInfo().subscribe((user: PKMUser) => {
      this.currentUser = user;
      this.allRoles = this.getProjects(user.roles);
      this.rolesArray = this.convertMapToArray(this.allRoles);
    });
  }

  getProjects(roles: Role[]): Map<string, string[]> {
    const rolesDB: Map<string, string[]> = new Map();
    roles.forEach((p: Role) => {
      p.role = '  ' + p.role;
      let project: string[] = rolesDB.get(p.db);
      if (!project) {
        project = rolesDB.set(p.db, []).get(p.db);
      }
      project.push(p.role);
    });

    return rolesDB;
  }

  convertMapToArray(mapp: Map<string, string[]>): Role[] {
    return Array.from(mapp).map(([db, role]: any) => ({
      db,
      role,
    }));
  }
}
