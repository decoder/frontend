import { Injectable } from '@angular/core';
import { User } from 'src/app/core/models/user';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { STORAGE_ITEMS } from 'src/app/core/services/login/login.service';
import { AppConfiguration } from 'src/config/app-configuration';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  serverBaseEndpoint: string = `${this.appConfig.serverURL}/user`;

  constructor(private http: HttpClient, private appConfig: AppConfiguration) {}

  getProfile(): Observable<User> {
    const path: string = 'profile';
    const url: string = this.generateURL(path);
    return this.http.get<User>(url);
  }

  createNewUser(
    username: string,
    password: string,
    projectName: string,
    firstName: string = '',
    lastName: string = '',
    email: string = '',
    phone: string = '',
  ): Observable<any> {
    const body: any = {
      name: username,
      password,
      first_name: firstName,
      last_name: lastName,
      email,
      phone,
      roles: [
        {
          db: projectName,
          role: 'Developer',
        },
      ],
      git_user_credentials: [
        {
          git_remote_url: '',
          git_user_name: '',
          git_password: '',
          git_ssh_private_key: '',
        },
      ],
    };
    return this.http.post(this.serverBaseEndpoint, body);
  }

  updateUsername(username: string): Observable<any> {
    const path: string = 'username';
    const url: string = this.generateURL(path);
    const body: HttpParams = new HttpParams().set(path, username);

    return this.http.put<any>(url, body);
  }

  updateEmail(email: string): Observable<any> {
    const url: string = this.serverBaseEndpoint;
    const user: string = localStorage.getItem(STORAGE_ITEMS.currentUser);
    return this.http.put<any>(url, {
      name: user,
      email
    });
  }

  updatePassword(password: string): Observable<any> {
    const url: string = this.serverBaseEndpoint;
    const user: string = localStorage.getItem(STORAGE_ITEMS.currentUser);
    return this.http.put<any>(url, {
      name: user,
      password
    });
  }

  generateURL(path: string): string {
    const userId: string = localStorage.getItem(STORAGE_ITEMS.currentUserId);
    return `${this.serverBaseEndpoint}/${userId}/${path}`;
  }
}
