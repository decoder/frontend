import { TestBed, fakeAsync, flush } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { User } from 'src/app/core/models/user';
import { AppConfiguration } from 'src/config/app-configuration';

describe('UserService', () => {
  let service: UserService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule], providers: [AppConfiguration] });
    service = TestBed.inject(UserService);
    http = TestBed.inject(HttpTestingController);
    service.serverBaseEndpoint = `/user`;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return a user [getProfile]', fakeAsync(() => {
    const profile: string = 'profile';
    service.getProfile().subscribe((user: User) => {
      expect(user.username).toBeTruthy();
    });

    flush();

    const url: string = service.generateURL(profile);
    const req: TestRequest = http.expectOne(url);
    expect(req.request.method).toBe('GET');
  }));

  it('should update username [updateUsername]', fakeAsync(() => {
    const username: string = 'username';
    service.updateUsername(username).subscribe((success: boolean) => {
      expect(success).toBeTruthy();
    });

    flush();

    const url: string = service.generateURL(username);
    const req: TestRequest = http.expectOne(url);
    expect(req.request.method).toBe('PUT');
  }));

  it('should update username [updateEmail]', fakeAsync(() => {
    const email: string = 'email';
    spyOn(service, 'generateURL').and.returnValue('/user');
    service.updateEmail(email).subscribe((success: boolean) => {
      expect(success).toBeTruthy();
    });

    flush();

    const url: string = service.generateURL(email);
    const req: TestRequest = http.expectOne(url);
    expect(req.request.method).toBe('PUT');
  }));

  it('should update username [updatePassword]', fakeAsync(() => {
    const path: string = 'password';
    spyOn(service, 'generateURL').and.returnValue('/user');
    service.updatePassword(path).subscribe((success: boolean) => {
      expect(success).toBe(false);
    });

    flush();

    const url: string = service.generateURL(path);
    const req: TestRequest = http.expectOne(url);
    expect(req.request.method).toBe('PUT');
  }));
});
