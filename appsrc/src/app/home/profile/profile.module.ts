import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UserAreaComponent } from './containers/user-area/user-area.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';

import { ChangeEmailComponent } from './components/change-email/change-email.component';
import { ChangeUserPasswordComponent } from './components/change-user-password/change-user-password.component';
import { ProfileComponent } from './containers/profile/profile.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { UserProjectsComponent } from './components/user-projects/user-projects.component';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'user-area' },
  {
    path: 'user-area',
    component: ProfileComponent,
    children: [
      { path: '', component: UserAreaComponent },
      { path: 'change-user-password', component: ChangeUserPasswordComponent },
      { path: 'change-email', component: ChangeEmailComponent },
    ],
  },
  { path: '**', pathMatch: 'full', redirectTo: 'user-area' },
];

@NgModule({
  declarations: [
    UserAreaComponent,
    ChangeEmailComponent,
    ChangeUserPasswordComponent,
    ProfileComponent,
    UserInfoComponent,
    UserProjectsComponent,
    ChangeEmailComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
})
export class ProfileModule {}
