import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';

import { FileUploadComponent } from './file-upload.component';
import { ElementRef } from '@angular/core';

export class ElementRefStub extends ElementRef {
  constructor() {
    super({ value: 'value', click: () => null });
  }
}

describe('FileUploadComponent', () => {
  let component: FileUploadComponent;
  let fixture: ComponentFixture<FileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadComponent],
      providers: [{ provide: ElementRef, useClass: ElementRefStub }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.input = TestBed.inject(ElementRef);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset input on [uploadFromInput]', () => {
    spyOn(component.input.nativeElement, 'click');
    component.uploadFromInput();

    expect(component.input.nativeElement.value).toEqual(null);
    expect(component.input.nativeElement.click).toHaveBeenCalled();
  });
});
