import {
  Component,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  @ViewChild('fileInput') input: ElementRef;
  @Output() fileUpload: EventEmitter<FileList>;
  @Input() projectId: string;

  constructor() {
    this.fileUpload = new EventEmitter<FileList>();
  }

  onFileUpload(files: FileList): void {
    this.fileUpload.emit(files);
  }

  uploadFromInput(): void {
    this.input.nativeElement.value = null;
    this.input.nativeElement.click();
  }
}
