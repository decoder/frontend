export interface UploadProgress {
  finished: boolean;
  progress: number;
}
