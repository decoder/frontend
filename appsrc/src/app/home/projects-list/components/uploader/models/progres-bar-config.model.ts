import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';

export interface ProgressBarConfig {
  color: ThemePalette;
  mode: ProgressBarMode;
  bufferValue: number;
}
