import { ProgressBarConfig } from './models/progres-bar-config.model';
import { UploadProgress } from './models/upload-progress.model';

export const DEFAULT_PROGRESS_BAR: ProgressBarConfig = {
  color: 'primary',
  mode: 'determinate',
  bufferValue: 100
};

export const DEFAULT_PROGRESS: UploadProgress = {
  finished: false,
  progress: 0
};
