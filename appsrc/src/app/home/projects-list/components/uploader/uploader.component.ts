import { Component, Output, EventEmitter, Input } from '@angular/core';
import { readableBytes } from 'src/app/utils/utils';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { UploadProgress } from './models/upload-progress.model';
import { ProgressBarConfig } from './models/progres-bar-config.model';
import { DEFAULT_PROGRESS_BAR, DEFAULT_PROGRESS } from './defaults';
import { UploaderService } from '../../services/uploader/uploader.service';
import { AppConfiguration } from 'src/config/app-configuration';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { allowedArtifactExtensions } from 'src/app/shared/constants/allowed-extensions';
import { PKMCVEArtifact } from 'src/app/core/models/artifact';

const DIAGRAM_TYPE: string = 'Diagram';
const CODE_TYPE: string = 'Code';
const DOCUMENT_TYPE: string = 'Document';
const EXECUTABLE_TYPE: string = 'Binary Executable';
const CVE_TYPE: string = 'CVE';

const ALLOWED_TYPES: string = `These are the main allowed extentions for each artifact type:
  · Type CODE: .java, .c, .h, .cpp, .hpp, .js, .sh, .jsp, .sql
  · Type DOCUMENT: .docx, .pdf, .md, .txt, .jpg, .png
  · Type DIAGRAM: .uml
  · Type EXECUTABLE: ELF files
  (for more details consult the help section)`;

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})
export class UploaderComponent {
  serverBaseEndpoint: string = this.appConfig.serverURL;
  @Input() projectId: string;
  @Output() uploadComplete: EventEmitter<boolean>;
  uploadProgress: UploadProgress;
  progressBarConfig: ProgressBarConfig;
  paths: string[];
  uploadedFileList: File[];
  fileRelativePath: string[];
  progressInfos: any = [];
  allowedTypes: string = ALLOWED_TYPES;
  checkboxes: boolean[];

  // Counter to wait for all to signal finish of upload
  finishedCount: number;
  // file types for the select control of each file
  fileTypes: string[] = [
    CODE_TYPE,
    DIAGRAM_TYPE,
    DOCUMENT_TYPE,
    EXECUTABLE_TYPE,
    CVE_TYPE,
  ];

  constructor(
    private uploaderService: UploaderService,
    private snackbar: SnackbarService,
    private appConfig: AppConfiguration
  ) {
    this.uploadComplete = new EventEmitter<boolean>();
    this.progressBarConfig = DEFAULT_PROGRESS_BAR;
    this.setDefaultUploadState();
    this.paths = [];
    this.uploadedFileList = [];
    this.checkboxes = [];
    this.fileRelativePath = [];
  }

  onFileUpload(files: FileList): void {
    if (!files) {
      return;
    }
    Array.from(files).forEach((file: File) => {
      if (
        this.uploadedFileList.length === 0 ||
        this.uploadedFileList.some(
          (item: File) => item.name !== file.name || item.type !== file.type
        )
      ) {
        this.uploadedFileList.push(file);
        this.checkboxes.push(false);
        this.fileRelativePath.push('');
        const index: number = this.uploadedFileList.findIndex(
          (item: File) => item.name === file.name && item.type === file.type
        );
        this.progressInfos[index] = { value: 0, fileName: file.name };
        this.updateProgress(index, { finished: false, progress: 0 });
      }
    });
  }

  checkExtension(index: number): void {
    const fileNameControl: string = `fileName${index}`;
    const fileTypeControl: string = `fileType${index}`;

    const inputElem: HTMLInputElement = document.getElementById(
      fileNameControl
    ) as HTMLInputElement;
    const selectElem: HTMLSelectElement = document.getElementById(
      fileTypeControl
    ) as HTMLSelectElement;

    if (!inputElem || !selectElem) {
      return;
    }

    const value: string = inputElem.value.toLowerCase();
    const extension: string = value.split('.').pop();
    if (
      allowedArtifactExtensions
        .GET_ALL_DOCUMENT_EXTENSIONS()
        .includes(extension)
    ) {
      selectElem.value = DOCUMENT_TYPE;
    } else if (
      allowedArtifactExtensions
        .GET_ALL_DIAGRAM_EXTENSIONS()
        .includes(extension)
      ) {
      selectElem.value = DIAGRAM_TYPE;
    } else if (
      allowedArtifactExtensions
        .GET_ALL_CODE_EXTENTIONS()
        .includes(extension)
    ) {
      if (extension === 'json' && selectElem.value === CVE_TYPE) {
        selectElem.value = CVE_TYPE;
      } else {
        selectElem.value = CODE_TYPE;
      }
    }
  }

  uploadAllClicked(): void {
    this.finishedCount = 0;
    this.uploadedFileList.forEach((file: File, index: number) => {
      this.checkExtension(index);
      // fires the upload of the file
      this.upload(index, file);
    });
  }

  checkAllFinished(): void {
    if (this.uploadedFileList.length === this.finishedCount) {
      this.signalAllFinished();
    }
  }

  signalAllFinished(): void {
    let fileNameList: string = '';
    this.uploadedFileList.forEach((file: File) => {
      fileNameList += file.name + '\n';
    });
    this.snackbar.openSnackBar(
      fileNameList + '\n' + 'imported successfully!',
      'check_circle'
    );
    this.uploadComplete.emit();
    this.setDefaultUploadState();
  }

  onCancel(file?: File, index?: number): void {
    if (!this.uploadProgress.finished) {
      this.snackbar.openSnackBar('Upload canceled', 'warning');
    }
    if (file) {
      this.uploadComplete.emit();
      this.uploadedFileList.splice(index, 1);
      this.checkboxes.splice(index, 1);
      this.fileRelativePath.splice(index, 1);
    } else {
      this.uploadComplete.emit();
      this.setDefaultUploadState();
    }
  }

  sizeProgress(progress: number, file: File): string {
    const size: number = file.size;
    const currentBytes: number = Math.floor((size * progress) / 100);
    const alreadyUploaded: string = readableBytes(currentBytes);

    return `${alreadyUploaded} of ${readableBytes(size)}`;
  }

  validateRelativePath(path: string): string {
    if (!path.endsWith('/')) {
      path = path.concat('/');
    }
    return path;
  }

  onCheckboxChange(event: MatCheckboxChange): void {
    const index: string = event.source.name;
    const checkboxValue: boolean = event.checked;
    this.checkboxes[index] = checkboxValue;
    if (checkboxValue === false) {
      this.fileRelativePath[index] = '';
    }
  }

  private setDefaultUploadState(): void {
    this.uploadProgress = DEFAULT_PROGRESS;
    this.uploadedFileList = [];
    this.checkboxes = [];
    this.fileRelativePath = [];
  }

  private updateProgress(
    index: number,
    uploadProgress: Partial<UploadProgress>
  ): void {
    this.progressInfos[index] = {
      ...this.progressInfos[index],
      ...uploadProgress,
    };
  }

  private selectItemBaseEndpointAndTypeForFile(index: number): any {
    // take value of type from combo box for file
    const inputElem: HTMLSelectElement = document.getElementById(
      'fileType' + index
    ) as HTMLSelectElement;
    const selectedType: string = inputElem.value;
    // select endpoint and type based on the select combo box ("fileType{{index}}") value
    if (selectedType === CODE_TYPE) {
      return {
        endpoint: `${this.serverBaseEndpoint}/code/rawsourcecode`,
        type: CODE_TYPE,
      };
    } else if (selectedType === DIAGRAM_TYPE) {
      return {
        endpoint: `${this.serverBaseEndpoint}/uml/rawuml`,
        type: DIAGRAM_TYPE,
      };
    } else if (selectedType === DOCUMENT_TYPE) {
      return {
        endpoint: `${this.serverBaseEndpoint}/doc/rawdoc`,
        type: DOCUMENT_TYPE,
      };
    } else if (selectedType === EXECUTABLE_TYPE) {
      return {
        endpoint: `${this.serverBaseEndpoint}/bin/executable`,
        type: EXECUTABLE_TYPE,
      };
    } else if (selectedType === CVE_TYPE) {
      return {
        endpoint: `${this.serverBaseEndpoint}/cve`,
        type: CVE_TYPE,
      };
    }
    return '';
  }

  private createRawObjectForRequest(
    fileIndex: number,
    file: File,
    fileReader: FileReader,
    endpointAndType: any
  ): any {
    // take relative path from input on screen
    const inputElem: HTMLInputElement = document.getElementById(
      'fileName' + fileIndex
    ) as HTMLInputElement;
    const fileName: string = inputElem.value;
    let fileContent: string = '';

    if (
      (endpointAndType.type === DOCUMENT_TYPE && this.isBinary(file)) ||
      endpointAndType.type === EXECUTABLE_TYPE
    ) {
      // take the encoded base64 string after the first comma
      fileContent = (fileReader.result as string).split(',')[1];
    } else {
      // take the result as it is (string)
      fileContent = fileReader.result as string;
    }

    if (endpointAndType.type === CVE_TYPE) {
      const fileFormatedContent: PKMCVEArtifact = JSON.parse(fileContent);
      return [{...fileFormatedContent}];
    } else {
      return [
        {
          rel_path: this.checkboxes[fileIndex] && !!this.fileRelativePath[fileIndex].trim() ?
            this.validateRelativePath(this.fileRelativePath[fileIndex]) + fileName
            : fileName,
          content: fileContent,
          format:
            endpointAndType.type === EXECUTABLE_TYPE ||
            (endpointAndType.type === DOCUMENT_TYPE && this.isBinary(file))
              ? 'binary'
              : 'text',
          encoding: 'utf-8',
          type: endpointAndType.type,
          mime_type: file.type,
          git_repository_url: '',
          git_commit_id: '',
        },
      ];
    }
  }

  private upload(index: number, file: File): any {
    const fileReader: FileReader = new FileReader();
    this.progressInfos[index] = { value: 0, fileName: file.name };
    fileReader.onload = (_: any) => {
      const endpointAndType: any =
        this.selectItemBaseEndpointAndTypeForFile(index);
      const createdRawSourceCode: any = this.createRawObjectForRequest(
        index,
        file,
        fileReader,
        endpointAndType
      );
      this.uploaderService
        .upload(createdRawSourceCode, this.projectId, endpointAndType.endpoint)
        .subscribe(
          (res: any) => {
            if (res?.status === 'progress') {
              this.updateProgress(index, { progress: res.message });
            }
          },
          (err: any) => {
            this.updateProgress(index, { finished: false, progress: 0 });
            this.snackbar.openSnackBar(err.message, 'error');
          },
          () => {
            // at this point, the file with index "index" has finished upoloading
            this.updateProgress(index, { finished: true });
            this.finishedCount = this.finishedCount + 1;
            this.checkAllFinished();
          }
        );
    };
    if (this.isBinary(file) || this.isExecutable(file)) {
      fileReader.readAsDataURL(file);
    } else {
      fileReader.readAsText(file);
    }
  }

  private isBinary(file: File): boolean {
    const extension: string = file.name.split('.').pop();
    return allowedArtifactExtensions
      .GET_ALL_BINARY_EXTENSIONS()
      .includes(extension);
  }

  private isExecutable(file: File): boolean {
    const extension: string = file.name.split('.').pop();
    return allowedArtifactExtensions
      .GET_ALL_LINUX_EXECUTABLE_BINARY_FILES()
      .includes(extension)
    || file.name.toLowerCase().split('.').length === 1;
  }
}
