import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  flush,
} from '@angular/core/testing';

import { UploaderComponent } from './uploader.component';
import { MaterialModule } from 'src/app/material.module';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { DEFAULT_PROGRESS } from './defaults';
import { Observable, Subscriber } from 'rxjs';
import { UploaderService } from '../../services/uploader/uploader.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ArtifactsRetrieverService } from 'src/app/plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { getMockRetriever } from 'src/app/plugins/project/testing/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

class UploaderServiceStub {
  upload(): Observable<any> {
    return new Observable((subscriber: Subscriber<any>) => {
      subscriber.next({ status: 'progress', message: 100 });
      subscriber.complete();
    });
  }
}

describe('UploaderComponent', () => {
  let component: UploaderComponent;
  let fixture: ComponentFixture<UploaderComponent>;
  let snackbar: SnackbarService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploaderComponent],
      imports: [NoopAnimationsModule, MaterialModule, HttpClientTestingModule],
      providers: [
        SnackbarService,
        { provide: UploaderService, useClass: UploaderServiceStub },
        {
          provide: ArtifactsRetrieverService,
          useValue: getMockRetriever(),
        },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderComponent);
    component = fixture.componentInstance;
    snackbar = TestBed.inject(SnackbarService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should complete succesfully [onDone]', fakeAsync(() => {
    component.uploadAllClicked();
    expect(component.finishedCount).toBe(0);
  }));

  it('should [onFileUpload]', () => {
    expect(component).toBeTruthy();
  });

  it('should notify cancel [onCancel] if uploading', fakeAsync(() => {
    let completed: boolean;
    spyOn(snackbar, 'openSnackBar');
    component.uploadProgress.finished = false;
    component.uploadComplete.subscribe(() => (completed = true));
    fixture.detectChanges();

    component.onCancel();
    flush();
    expect(snackbar.openSnackBar).toHaveBeenCalled();
    expect(completed).toBeTruthy();
  }));

  it('should not notify cancel [onCancel] if not uploading', fakeAsync(() => {
    let completed: boolean;
    spyOn(snackbar, 'openSnackBar');
    component.uploadProgress.finished = true;
    component.uploadComplete.subscribe(() => (completed = true));
    fixture.detectChanges();

    component.onCancel();
    flush();
    expect(snackbar.openSnackBar).not.toHaveBeenCalled();
    expect(completed).toBeTruthy();
  }));

  it('should set default values for progress', () => {
    component['setDefaultUploadState']();
    expect(component.uploadProgress).toEqual(DEFAULT_PROGRESS);
    expect(component.uploadedFileList.length).toBe(0);
  });
});
