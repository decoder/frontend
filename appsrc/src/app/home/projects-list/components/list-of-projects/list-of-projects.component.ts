import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
  OnChanges,
  SimpleChanges,
  SimpleChange,
} from '@angular/core';
import { PKMProjectNameId, PKMProject } from 'src/app/core/models/project';
import { ProjectsService } from '../../services/projects/projects.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateProjectDialogComponent } from '../create-project-dialog/create-project-dialog.component';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-of-projects',
  templateUrl: './list-of-projects.component.html',
  styleUrls: ['./list-of-projects.component.scss'],
})
export class ListOfProjectsComponent implements OnInit, OnChanges {
  selectedItem: string;
  selectedProject: boolean;
  projects$: Observable<PKMProjectNameId[]> = of([]);

  @Input() superAdminRights: boolean;
  @Output() editMode: EventEmitter<boolean>;
  @Output() projectSelectedEvent: EventEmitter<string>;
  @Output() projectDeletedEvent: EventEmitter<string>;
  @Output() projectListRefreshedEvent: EventEmitter<any>;

  @Output() selectedItemEvent: EventEmitter<boolean>;

  constructor(
    private projectsService: ProjectsService,
    private dialog: MatDialog
  ) {
    this.editMode = new EventEmitter<boolean>();
    this.projectSelectedEvent = new EventEmitter<string>();
    this.projectDeletedEvent = new EventEmitter<string>();
    this.projectListRefreshedEvent = new EventEmitter<any>();
    this.selectedItemEvent = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
    this.refreshItems();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const superadminValue: SimpleChange = changes.superAdminRights;
    if (superadminValue.currentValue !== superadminValue.previousValue) {
      this.refreshItems();
    }
  }

  fetchProjectsForUser(): Observable<PKMProjectNameId[]> {
    const obs1$: Observable<PKMProject[]> =
      this.projectsService.getProjectsByUser(
        localStorage.getItem('currentUser'),
        this.superAdminRights
      );
    // Rename this to use it inside a lambda
    const self: ListOfProjectsComponent = this;
    return obs1$.pipe(
      map(this.convertProjectsToNameId(self))
    );
  }

  convertProjectToNameId(project: PKMProject): PKMProjectNameId {
    if (!project) {
      return null;
    }

    return {
      projectId: project.name,
      projectName: project.name,
      owner: false,
    };
  }

  convertProjectsToNameId =
    (self: ListOfProjectsComponent) => (projects: PKMProject[]) => {
      let newCol: PKMProjectNameId[] = [];
      if (projects) {
        newCol = projects
          .map((p: PKMProject) => self.convertProjectToNameId(p))
          .filter((p: PKMProjectNameId) => p !== null);
      }
      // else : if projects is null, return newCol === []

      return newCol;
      // tslint:disable-next-line:semicolon
    };

  onProjectSelection(item: string): void {
    this.selectedItem = item;
    this.projectSelectedEvent.emit(item);
    this.selectedProject = true;

    this.selectedItemEvent.emit(false);
  }

  onProjectDeselection(): void {
    this.selectedItem = null;
    this.projectSelectedEvent.emit(null);
    this.selectedProject = false;
    this.selectedItemEvent.emit(true);
  }

  isProjectSelected(item: string): boolean {
    return item === this.selectedItem;
  }

  createNewProject(projectName: string): void {
    this.projectsService.createNewProject(projectName)
    .subscribe((data: any) => {
      if (data === 'Created') {
        this.projectListRefreshedEvent.emit();
      }
    });
    this.onEnterEditMode();
  }

  @HostListener('click')
  onExitEditMde(): void {
    this.editMode.emit(false);
  }

  openCreateDialog(): void {
    this.dialog
      .open(CreateProjectDialogComponent, {
        width: '650px',
        height: '450px',
        data: {
          title: `create a new project`,
          accept: 'Create project',
          target: 'Introduce the name',
          cancel: 'Cancel',
        },
      })
      .afterClosed()
      .subscribe((result: string) => {
        if (result !== '' && typeof result !== 'undefined') {
          this.createNewProject(result);
        }
      });
  }

  refreshItems(): void {
    this.projects$ = this.fetchProjectsForUser();
  }

  private onEnterEditMode(): void {
    this.editMode.emit(true);
  }
}
