import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfProjectsComponent } from './list-of-projects.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProjectsService } from '../../services/projects/projects.service';
import { Observable, of } from 'rxjs';
import { Project } from 'src/app/core/models/project';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/app/material.module';

class ProjectsServiceMock {
  getProjects(): Observable<Project[]> {
    return of([]);
  }
  getProjectsByUser(): Observable<Project[]> {
    return of([]);
  }
}

describe('ListOfProjectsComponent', () => {
  let component: ListOfProjectsComponent;
  let fixture: ComponentFixture<ListOfProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListOfProjectsComponent],
      providers: [
        { provide: ProjectsService, useValue: new ProjectsServiceMock() }
      ],
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule,
        MaterialModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select an item', () => {
    const selectedItem: string = '1';
    component.onProjectSelection(selectedItem);
    expect(component.selectedItem).toEqual(selectedItem);
  });

  it('should return true if the item is selected, false otherwise', () => {
    const selectedItem: string = '1';
    const nonSelectedItem: string = '2';
    component.onProjectSelection(selectedItem);

    expect(component.isProjectSelected(selectedItem)).toBeTruthy();
    expect(component.isProjectSelected(nonSelectedItem)).toBeFalsy();
  });
});
