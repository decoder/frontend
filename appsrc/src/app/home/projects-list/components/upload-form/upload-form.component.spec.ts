import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  flush,
} from '@angular/core/testing';

import { UploadFormComponent } from './upload-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('UploadFormComponent', () => {
  let component: UploadFormComponent;
  let fixture: ComponentFixture<UploadFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploadFormComponent],
      imports: [NoopAnimationsModule, ReactiveFormsModule, MaterialModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change on selection', fakeAsync(() => {
    const selectedSource: string = 'local';
    component.sourceSelected.subscribe((source: string) =>
      expect(source).toEqual(selectedSource)
    );

    component.onSelectionChange(selectedSource);
    flush();
  }));

  it('should be invalid if there is no source selected', () => {
    expect(component.isInvalidSource()).toBeTruthy();
  });
});
