import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.scss']
})
export class UploadFormComponent {
  uploadForm: FormGroup;
  @Output() sourceSelected: EventEmitter<string>;

  constructor() {
    this.sourceSelected = new EventEmitter<string>();
    this.uploadForm = new FormGroup({
      source: new FormControl('', Validators.required)
    });
  }

  onSelectionChange(source: string): void {
    this.sourceSelected.emit(source);
  }

  isInvalidSource(): boolean {
    return this.uploadForm.get('source').invalid;
  }
}
