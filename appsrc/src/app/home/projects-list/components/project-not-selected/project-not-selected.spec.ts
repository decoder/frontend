import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectNotSelectedComponent } from './project-not-selected.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ProjectNotSelectedComponent', () => {
  let component: ProjectNotSelectedComponent;
  let fixture: ComponentFixture<ProjectNotSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectNotSelectedComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectNotSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
