import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from '.././delete-dialog/delete-dialog.component';
import { Project, PKMProject, PKMProjectMember } from 'src/app/core/models/project';
import { ProjectsService } from '../../services/projects/projects.service';
import { MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss'],
})
export class ProjectViewComponent {
  @Input() editMode: boolean;
  @Input() currentUserId: string;
  @Input() currentRole: string;
  @Input() superAdminRights: boolean;
  @Input() set projectId(id: string) {
    if (id) {
      this.setProject(id);
    }
  }
  @Output() nameChangedToEmit: EventEmitter<string>;
  @Output() projectDeletedEvent: EventEmitter<string>;
  @Output() memberAditionEvent: EventEmitter<boolean>;
  @Output() memberCreationEvent: EventEmitter<boolean>;
  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  displayedProject: Project;
  displayedProjectPKM: PKMProject;
  deleteDialog: any;
  diplaySaveIcon: boolean;
  selectedReview: boolean;

  constructor(
    public dialog: MatDialog,
    private projectsService: ProjectsService
  ) {
    this.projectsService.getUserId().subscribe((userId: string) => {
      this.currentUserId = userId;
    });
    this.nameChangedToEmit = new EventEmitter<string>();
    this.projectDeletedEvent = new EventEmitter<string>();
    this.memberAditionEvent = new EventEmitter<boolean>();
    this.memberCreationEvent = new EventEmitter<boolean>();
    this.diplaySaveIcon = false;
    this.selectedReview = false;
  }

  roleIsOwnerOrAdmin(): boolean {
    let projectOwnership: boolean = false;
    if (this.superAdminRights) { return true; }
    if (this.displayedProjectPKM && this.displayedProjectPKM.members) {
      this.displayedProjectPKM.members.forEach((element: PKMProjectMember) => {
        if (element.name === localStorage.getItem('currentUser')) {
          projectOwnership = element.roles.includes('Owner');
        }
      });
    }
    return projectOwnership;
  }

  onDelete(): void {
    this.setProjectAccordingToPKMProject();
    this.openDeleteDialog();
  }

  onMembersUpdate(members: PKMProjectMember[]): void {
    this.memberAditionEvent.emit(true);
  }

  onMemberAdition(event: PKMProjectMember): void {
    this.memberAditionEvent.emit(true);
  }

  onMemberCreation(event: boolean): void {
    this.memberCreationEvent.emit(event);
  }

  displaySaveIcon(): void {
    this.diplaySaveIcon = true;
  }

  redirectImportSourceTab(): void {
    this.tabGroup.selectedIndex = 1;
  }

  onReviewSelected(): void {
    this.selectedReview = true;
  }

  checkIfReviewSelected(): boolean {
    return this.selectedReview;
  }

  private setProject(id: string): void {
    this.projectsService
      .getProjectsByUser(localStorage.getItem('currentUser'), this.superAdminRights)
      .subscribe((pkmProjects: PKMProject[]) => {
        const filteredProjects: PKMProject[]  = pkmProjects.filter((project: PKMProject) => project.name === id);
        this.displayedProjectPKM = filteredProjects[0];
      });
  }

  private setProjectAccordingToPKMProject(): void {
    this.displayedProject = {} as Project;
    this.displayedProject.projectName = this.displayedProjectPKM?.name;
    this.displayedProject.projectId = this.displayedProjectPKM?.name;
  }

  private openDeleteDialog(): void {
    this.deleteDialog = this.dialog.open(DeleteDialogComponent, {
      width: '650px',
      height: '400px',
      data: {
        title: `Delete project "${this.displayedProject.projectName}"`,
        description:
          'If you delete the project no member can access. This action is irreversible.',
        accept: 'DELETE PROJECT',
      },
    });

    this.deleteDialog.afterClosed().subscribe((result: string) => {
      console.log(`Dialog was closed with action: ${result}`);
      if (result === 'delete') {
        this.projectsService
          .deleteProject(this.displayedProject)
          .subscribe((success: boolean) => {
            if (success) {
              console.log(
                `Project ${this.displayedProject.projectName} deleted successfuly`
              );
              this.projectDeletedEvent.emit(this.displayedProject.projectId);
            }
          });
      }
    });
  }
}
