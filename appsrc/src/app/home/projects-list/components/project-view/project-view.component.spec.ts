import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectViewComponent } from './project-view.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from 'src/app/material.module';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { ProjectsService } from '../../services/projects/projects.service';
import { HttpClientModule } from '@angular/common/http';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { of } from 'rxjs';
import { projectMock } from 'src/app/mocks/core/fixtures/project.mock';
import { pkmMemberDetailsMock } from 'src/app/mocks/core/fixtures/memberDetails.mock';
import { getProjectMockRetriever } from 'src/app/plugins/project/testing/services/project-retriever.mock';

describe('ProjectViewComponent', () => {
  let component: ProjectViewComponent;
  let fixture: ComponentFixture<ProjectViewComponent>;
  let dialogTest: MatDialog;
  let serviceStub: any;

  beforeEach(async(() => {
    serviceStub = {
      deleteProject: () => of(),
      getUserId: () => of('1'),
      getProjectById: () => of(projectMock[0]),
      updateProject: () => of(),
      sendInvitation: () => of(pkmMemberDetailsMock[0]),
    };

    TestBed.configureTestingModule({
      declarations: [ProjectViewComponent, DeleteDialogComponent],
      imports: [
        MaterialModule,
        MatDialogModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: ProjectsService,
          useValue: serviceStub,
        },
        { provide: ProjectsService, useValue: getProjectMockRetriever() },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    dialogTest = TestBed.inject(MatDialog);
    fixture = TestBed.createComponent(ProjectViewComponent);
    component = fixture.componentInstance;
    component.displayedProject = projectMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    component.currentUserId = '0';
    expect(component).toBeTruthy();
  });

  it('should create dialog component', () => {
    expect(component.dialog).toBeTruthy();
  });

  it('should open delete dialog when Delete button is clicked', () => {
    spyOn(dialogTest, 'open').and.returnValue({
      afterClosed: () => of(''),
    } as any);
    component.projectId = '0';
    component.onDelete();
    expect(dialogTest.open).toHaveBeenCalled();
  });
});
