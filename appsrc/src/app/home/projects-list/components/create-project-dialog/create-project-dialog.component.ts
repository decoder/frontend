import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create-project-dialog',
  templateUrl: './create-project-dialog.component.html',
  styleUrls: ['./create-project-dialog.component.scss']
})
export class CreateProjectDialogComponent {
  parts: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<CreateProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      description?: string;
      target?: string;
      accept: string;
      cancel: string;
    }
  , fb: FormBuilder
  ) {

    this.parts =  fb.group({
      name: '',
    });
  }

}
