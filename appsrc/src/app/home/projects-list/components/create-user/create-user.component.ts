import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl,
  ValidationErrors,
} from '@angular/forms';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { UserService } from 'src/app/home/profile/services/user/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent {
  userCreationForm: FormGroup;
  @Input() projecName: string;
  @Output() memberCreation: EventEmitter<boolean>;

  constructor(
    private snackbarService: SnackbarService,
    private userService: UserService
  ) {
    this.memberCreation = new EventEmitter<boolean>(false);
    this.userCreationForm = new FormGroup(
      {
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        passwordConfirmation: new FormControl('', Validators.required),
        firstName: new FormControl(''),
        lastName: new FormControl(''),
        email: new FormControl(''),
        phone: new FormControl(''),
      },
      this.checkEqualPassword
    );

    this.userCreationForm.setValidators(this.checkEqualPassword.bind(this));
  }

  public sendUserCreationRequest(): void {
    const username: string = this.userCreationForm.get('username').value;
    const password: string = this.userCreationForm.get('password').value;
    const firstName: string = this.userCreationForm.get('firstName').value;
    const lastName: string = this.userCreationForm.get('lastName').value;
    const email: string = this.userCreationForm.get('email').value;
    const phone: string = this.userCreationForm.get('phone').value;
    this.userService
      .createNewUser(
        username,
        password,
        this.projecName,
        firstName,
        lastName,
        email,
        phone,
      )
      .subscribe(
        (response: any) => {
          this.snackbarService.success('User has been created!');
          this.memberCreation.emit(true);
        },
        (err: HttpErrorResponse) => {
          this.snackbarService.error(err.message);
        }
      );
    this.userCreationForm.reset();
    Object.keys(this.userCreationForm.controls).forEach((key: any) => {
      this.userCreationForm.get(key).setErrors(null);
    });
  }

  hasDifferentPassWord(): boolean {
    return this.userCreationForm.get('passwordConfirmation').errors
      ?.notEqualPasswords;
  }

  private checkEqualPassword(form: FormGroup): ValidationErrors {
    const password: string = form.get('password').value;
    const passwordConfirmation: string = form.get('passwordConfirmation').value;
    let errors: ValidationErrors = { notEqualPasswords: true };

    if (password === passwordConfirmation) {
      errors = { notEqualPasswords: false };
    }

    form.controls.passwordConfirmation.setErrors(errors);
    return errors;
  }
}
