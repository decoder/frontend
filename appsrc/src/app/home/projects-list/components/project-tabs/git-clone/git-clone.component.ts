import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  GitCommandErrorResponse,
  GitCommandSuccessResponse,
  GitResponseUtils,
  GitService,
} from '../../../services/git/git.service';

@Component({
  selector: 'app-git-clone',
  templateUrl: './git-clone.component.html',
  styleUrls: ['./git-clone.component.scss'],
})
export class GitCloneComponent implements OnInit {
  @Input() projectName: string;

  imgSrc: string = '../../../../../assets/img/upload.png'; // fixed string pointing to the upload icon
  deleteSrc: string = '../../../../../assets/img/delete.png'; // fixed string pointing to the delete icon
  cloneForm: FormGroup;
  content: string = '';
  keyLoaded: boolean = false;
  authRequired: boolean;
  loading: boolean = false;

  constructor(
    private gitService: GitService,
  ) {}

  ngOnInit(): void {
    this.cloneForm = new FormGroup({
      projectURL: new FormControl('', Validators.required),
      username: new FormControl(''),
      password: new FormControl(''),
    });
    this.authRequired = false;
  }

  public privateKeyUpload(event: any): void {
    const files: File[] = event.target['files'];

    if (files && files[0]) {
      const reader: FileReader = new FileReader();

      reader.onloadend = () => {
        // Save the contents of the file
        this.content = reader.result as string;
        this.keyLoaded = true;
      };

      reader.readAsText(files[0]);
    }
  }

  public resetPrivateKey(): void {
    this.content = '';
    this.keyLoaded = false;
  }

  public resetForm(): void {
    this.resetPrivateKey();
    this.cloneForm.reset();
  }

  public sendGitCloneCommand(): void {
    const value: any = this.cloneForm.value;
    this.loading = true;
    this.gitService
      .cloneGitRepository(
        value.projectURL,
        this.projectName,
        value.username,
        value.password,
        this.content
      )
      .subscribe((resp: any) => {
        if (GitResponseUtils.isSuccessful(resp)) {
          const successR: GitCommandSuccessResponse =
            GitResponseUtils.returnSuccessObject(resp);
          let successMessage: string = 'GIT CLONE SUCCESSFULLY COMPLETED:\n\n';
          successMessage += `WARNINGS: ${successR.warnings.join('\n')}`;
          successMessage += `\nEND TIME: ${successR.end_date}`;
          successMessage += `\nSTATE: ${successR.state}`;
          this.loading = false;
          alert(successMessage);
        } else {
          const errorResp: GitCommandErrorResponse =
            GitResponseUtils.returnErrorObject(resp);
          alert(errorResp.errorMessage);
          this.loading = false;
        }
      });
  }

  public onCheckboxClicked(e: any): void {
    e.preventDefault();
    this.authRequired = !this.authRequired;
  }
}
