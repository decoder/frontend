import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportSourceComponent } from './import-source.component';

describe('ImportSourceComponent', () => {
  let component: ImportSourceComponent;
  let fixture: ComponentFixture<ImportSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImportSourceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enter uploadMode [onSourceSelected]', () => {
    const sourceSelected: string = 'local';
    component.onSourceSelected(sourceSelected);
    expect(component.uploadMode).toBe(true);
  });

  it('should exit uploadMode [onUploadComplete]', () => {
    component.onUploadComplete();
    expect(component.uploadMode).toBe(false);
  });
});
