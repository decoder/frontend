import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-import-source',
  templateUrl: './import-source.component.html',
  styleUrls: ['./import-source.component.scss']
})
export class ImportSourceComponent {
  uploadMode: boolean;
  @Input() projectId: string;

  constructor() {
    this.uploadMode = false;
  }

  onSourceSelected(source: string): void {
    this.uploadMode = true;
  }

  onUploadComplete(): void {
    this.uploadMode = false;
  }
}
