import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';

import { ProjectVersionsComponent } from './project-versions.component';
import { MaterialModule } from 'src/app/material.module';
import { TimeAgoPipe } from 'src/app/shared/pipes/time-ago/time-ago.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { versionMock } from 'src/app/mocks/core/fixtures/version.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ProjectVersionsComponent', () => {
  let component: ProjectVersionsComponent;
  let fixture: ComponentFixture<ProjectVersionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectVersionsComponent],
      imports: [
        NoopAnimationsModule,
        MaterialModule,
        SharedModule,
        RouterTestingModule,
      ],
      providers: [TimeAgoPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectVersionsComponent);
    component = fixture.componentInstance;
    component.dataSource = versionMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add date on init', () => {
    expect(component.dataSource).toBeTruthy();
  });
});
