import {
  Component,
  Input,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { Version } from 'src/app/core/models/version';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { PKMProject } from 'src/app/core/models/project';

const EMPTY_VERSIONS: Version[] = [
  {
    versionId: null,
    versionName: 'No sources',
    creationDate: null,
    versionStatus: 'Error',
  },
];

@Component({
  selector: 'app-project-versions',
  templateUrl: './project-versions.component.html',
  styleUrls: ['./project-versions.component.scss'],
})
export class ProjectVersionsComponent {
  @ViewChild(MatTable) private table: MatTable<any>;
  @Output() navigateImportSource: EventEmitter<boolean>;

  @Input() set project(project: PKMProject) {
    this.dataSource = EMPTY_VERSIONS;

    if (project) {
      this.dataSource = [{versionId: project.name, versionName: project.name, creationDate: null, versionStatus: null}];
      this.projectId = project.name;
    }

    if (this.table !== null && this.table !== undefined) {
      this.table.renderRows();
    }
  }
  displayedColumns: string[];
  dataSource: Version[];
  projectId: string;

  constructor(private dialog: MatDialog) {
    this.dataSource = EMPTY_VERSIONS;
    this.displayedColumns = ['version', 'creationDate'];
    this.dataSource = EMPTY_VERSIONS;

    this.navigateImportSource = new EventEmitter<boolean>();
  }

  redirectImportSource(): void {
    this.navigateImportSource.emit(true);
  }

}
