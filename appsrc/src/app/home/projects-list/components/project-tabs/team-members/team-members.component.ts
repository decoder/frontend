import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  OnChanges,
  ViewChild,
  EventEmitter,
  Output,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { MatTable } from '@angular/material/table';
import { DeleteDialogComponent } from '../../delete-dialog/delete-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { PKMProjectMember } from 'src/app/core/models/project';
import { ProjectsService } from '../../../services/projects/projects.service';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { HttpErrorResponse } from '@angular/common/http';

const EMPTY_MEMBER: PKMProjectMember[] = [
  {
    name: '',
    roles: [],
    owner: false,
  },
];

const NO_MEMBER: PKMProjectMember[] = [];
const AVAILABLE_ROLES: string[] = ['Developer', 'Reviewer', 'Maintainer'];
const READONLY_COLS: string[] = ['firstName', 'roles'];
const COLUMNS: string[] = [...READONLY_COLS, 'status', 'add', 'delete'];

const SUCCESS_MESSAGE_ADD_MEMBER: string = 'Member has been added succesfully!';
const ERROR_MESSAGE_ADD_MEMBER: string =
  'Something went wrong while adding a member!';
const SUCCESS_MESSAGE_DELETE_MEMBER: string =
  'Member has been deleted succesfully!';
const ERROR_MESSAGE_DELETE_MEMBER: string =
  'Something went wrong while deleting a member!';
const SUCCESS_MESSAGE_MODIFY_MEMBER: string =
  'Member has been modified succesfully!';
const ERROR_MESSAGE_MODIFY_MEMBER: string =
  'Something went wrong while tried to modify a member!';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss'],
})
export class TeamMembersComponent implements OnInit, OnChanges {
  @ViewChild(MatTable) private table: MatTable<any>;
  @Input() dataSource: PKMProjectMember[];
  @Input() currentUserId: string;
  @Input() projecName: string;
  @Input() superAdminRights: boolean;
  @Output() memberAdition: EventEmitter<PKMProjectMember>;
  @Output() memberCreation: EventEmitter<boolean>;
  @Output() membersUpdate: EventEmitter<PKMProjectMember[]>;
  newMemberMode: boolean;
  memberCreationDisplay: boolean;
  newMember: PKMProjectMember[];
  displayedColumns: string[];

  isOwner: boolean = false;
  isAdmin: boolean = false;
  memberForm: FormGroup;
  roles$: Observable<any>;
  availableRoles: string[];

  constructor(
    private dialog: MatDialog,
    private projectService: ProjectsService,
    private snackbarService: SnackbarService,
    private projectRetrieverService: ProjectsService
  ) {
    this.displayedColumns = READONLY_COLS;
    this.newMemberMode = false;
    this.newMember = NO_MEMBER;
    this.dataSource = [];
    this.memberAdition = new EventEmitter<PKMProjectMember>();
    this.membersUpdate = new EventEmitter<PKMProjectMember[]>();
    this.memberCreation = new EventEmitter<boolean>();
    this.availableRoles = AVAILABLE_ROLES;
  }

  ngOnInit(): void {
    this.setDefaultMemberForm();
    this.memberCreationDisplay = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentUserId) {
      this.isOwner = this.isCurrentUserOwner();
      if (this.isOwner) {
        this.displayedColumns = COLUMNS;
      } else {
        this.displayedColumns = READONLY_COLS;
      }
    }
    this.cleanAfterExitEdit();
  }

  isEditable(index: number): boolean {
    const isLastRow: boolean = index === this.dataSource.length;
    return this.newMemberMode && this.isCurrentUserOwner() && isLastRow ||
    this.newMemberMode && this.superAdminRights && isLastRow;
  }

  onNewMember(): void {
    if (this.newMemberMode === false) {
      this.newMemberMode = true;
      this.displayedColumns = COLUMNS;
      this.addEditableMember();
    } else {
      this.cleanAfterExitEdit();
    }
  }

  onDeleteMember(member: PKMProjectMember): void {
    this.openDeleteDialog(member);
  }

  onSubmitMember(): void {
    if (this.memberForm.valid) {
      const memberFormValues: any = this.memberForm.value;
      const member: PKMProjectMember = {
        name: memberFormValues.name,
        roles: [...memberFormValues.roles],
        owner: memberFormValues.owner === 'true',
      };
      this.projectService
        .updateProjectMembers(this.projecName, [...this.dataSource, member])
        .subscribe(
          (response: any) => {
            this.snackbarService.success(SUCCESS_MESSAGE_ADD_MEMBER);
          },
          (err: HttpErrorResponse) => {
            this.snackbarService.error(ERROR_MESSAGE_ADD_MEMBER);
          }
        );
      this.cleanAfterExitEdit();
      this.memberAdition.emit(member);
    }
  }

  checkCurrentUser(userId: string): boolean {
    return this.currentUserId === userId;
  }

  onModifyElement(): void {
    this.projectService
      .updateProjectMembers(this.projecName, this.dataSource)
      .subscribe(
        (response: any) => {
          this.snackbarService.success(SUCCESS_MESSAGE_MODIFY_MEMBER);
        },
        (err: HttpErrorResponse) => {
          this.snackbarService.error(ERROR_MESSAGE_MODIFY_MEMBER);
        }
      );
  }

  onRolesChange(index: number, value: any): void {
    this.dataSource[index].roles = value;
    this.membersUpdate.emit(this.dataSource);
  }

  onMemberCreation(event: boolean): void {
    this.memberCreation.emit(event);
  }

  onCreateMember(): void {
    this.memberCreationDisplay = !this.memberCreationDisplay;
  }

  private isCurrentUserOwner(): boolean {
    let projectOwnership: boolean = false;
    if (this.superAdminRights) { return true; }
    this.dataSource.forEach((element: PKMProjectMember) => {
      if (element.name === localStorage.getItem('currentUser')) {
        projectOwnership = element.roles.includes('Owner');
      }
    });
    return projectOwnership;
  }

  private cleanAfterExitEdit(): void {
    if (this.newMemberMode) {
      this.newMemberMode = false;
      this.memberForm.reset();
      this.removeEditableMember();
      this.setDefaultMemberForm();
    }
    if (this.memberCreationDisplay) {
      this.memberCreationDisplay = false;
      this.memberForm.reset();
      this.setDefaultMemberForm();
    }
  }

  private openDeleteDialog(member: PKMProjectMember): void {
    const name: string = member.name;
    this.dialog
      .open(DeleteDialogComponent, {
        width: '650px',
        height: '400px',
        data: {
          title: 'delete this member',
          accept: 'DELETE MEMBER',
          target: name,
        },
      })
      .afterClosed()
      .subscribe((result: string) => {
        if (result === 'delete') {
          const filteredMembers: PKMProjectMember[] = this.dataSource.filter(
            (element: PKMProjectMember) => element.name !== name
          );
          this.projectService
            .updateProjectMembers(this.projecName, [...filteredMembers])
            .subscribe(
              (response: any) => {
                this.snackbarService.success(SUCCESS_MESSAGE_DELETE_MEMBER);
              },
              (err: HttpErrorResponse) => {
                this.snackbarService.error(ERROR_MESSAGE_DELETE_MEMBER);
              }
            );
          this.cleanAfterExitEdit();
          this.memberAdition.emit();
        }
      });
  }

  private addEditableMember(): void {
    this.newMember = EMPTY_MEMBER;
    this.table.dataSource = this.dataSource.concat(this.newMember);
  }

  private removeEditableMember(): void {
    this.table.dataSource = this.dataSource;
  }

  private setDefaultMemberForm(): void {
    this.memberForm = new FormGroup({
      name: new FormControl('', Validators.required),
      roles: new FormControl(NO_MEMBER, Validators.required),
      owner: new FormControl(false, Validators.required),
    });
  }
}
