import {
  async,
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import { Observable, asyncScheduler, of, scheduled } from 'rxjs';

import { MaterialModule } from 'src/app/material.module';
import { TeamMembersComponent } from './team-members.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { pkmMemberDetailsMock } from 'src/app/mocks/core/fixtures/memberDetails.mock';
import { ProjectsService } from '../../../services/projects/projects.service';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { By } from '@angular/platform-browser';
import { PKMProjectMember } from 'src/app/core/models/project';
import { DebugElement } from '@angular/core';

class MockProjectService {
  updateProjectMembers(): Observable<any> {
    return scheduled(of(), asyncScheduler);
  }

  getUserData(userName: string): Observable<any> {
    return scheduled(of(), asyncScheduler);
  }
}

class SnackbarServiceStub {
  success: () => {};
}

const EMPTY_MEMBER: PKMProjectMember[] = [
  {
    name: '',
    roles: [],
    owner: false
  },
];

describe('TeamMembersComponent', () => {
  let component: TeamMembersComponent;
  let fixture: ComponentFixture<TeamMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamMembersComponent],
      providers: [
        { provide: ProjectsService, useClass: MockProjectService },
        { provide: SnackbarService, useClass: SnackbarServiceStub }
      ],
      imports: [MaterialModule, ReactiveFormsModule, BrowserAnimationsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.dataSource = pkmMemberDetailsMock;
    fixture.detectChanges();
  });

  it('should not be editable initially', () => {
    const penultimateRowIndex: number = component.dataSource.length - 2;
    expect(component.isEditable(penultimateRowIndex)).toBeFalsy();
  });

  it('should not be editable initially, event for the last row', () => {
    const lastRowIndex: number = component.dataSource.length - 1;
    expect(component.isEditable(lastRowIndex)).toBeFalsy();
  });

  it('should make call to onNewMember when the button is clicked', () => {
    component.isOwner = true;
    fixture.detectChanges();
    const newMemberButton: DebugElement = fixture.debugElement.queryAll(By.css('button'))[0];
    newMemberButton.triggerEventHandler('click', null);
    expect(component.newMemberMode).toEqual(true);
    expect(component.newMember).toEqual(EMPTY_MEMBER);
  });
});
