import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-tools-video-dialog',
  templateUrl: './tools-video-dialog.component.html',
  styleUrls: ['./tools-video-dialog.component.scss'],
})
export class ToolsVideoDialogComponent {
  private urlVideo: string;
  viewed: boolean;

  constructor(
    public dialogRef: MatDialogRef<ToolsVideoDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      description?: string;
      target?: string;
      target2?: string;
      accept: string;
      cancel: string;
    }
  ) {
    this.viewed = false;
  }

  // If tools video is hosted on youtube this function will have to be implemented
  checkIfVideoIsPlayed(): boolean {
    return true;
  }
}
