import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsListLayoutComponent } from './containers/projects-list-layout/projects-list-layout.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProjectViewComponent } from './components/project-view/project-view.component';
import { ProjectNotSelectedComponent } from './components/project-not-selected/project-not-selected.component';
import { ListOfProjectsComponent } from './components/list-of-projects/list-of-projects.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatListModule } from '@angular/material/list';
import { CoreModule } from 'src/app/core/core.module';
import { MaterialModule } from 'src/app/material.module';
import { ProjectVersionsComponent } from './components/project-tabs/project-versions/project-versions.component';
import { ImportSourceComponent } from './components/project-tabs/import-source/import-source.component';
import { TeamMembersComponent } from './components/project-tabs/team-members/team-members.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { TimeAgoPipe } from 'src/app/shared/pipes/time-ago/time-ago.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadFormComponent } from './components/upload-form/upload-form.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { UploaderService } from './services/uploader/uploader.service';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { RolesRetrieverService } from 'src/app/core/services/roles-retriever/roles-retriever.service';
import { CreateProjectDialogComponent } from './components/create-project-dialog/create-project-dialog.component';
import { ReviewerModule } from 'src/app/reviewer/reviewer.module';
import { ToolsVideoDialogComponent } from './components/tools-video-dialog/tools-video-dialog.component';
import { TimestampPipe } from 'src/app/shared/pipes/timestamp/timestamp.pipe';
import { GitCloneComponent } from './components/project-tabs/git-clone/git-clone.component';
import { CreateUserComponent } from './components/create-user/create-user.component';

@NgModule({
  declarations: [
    ProjectsListLayoutComponent,
    ProjectViewComponent,
    ListOfProjectsComponent,
    ProjectNotSelectedComponent,
    ProjectVersionsComponent,
    ImportSourceComponent,
    TeamMembersComponent,
    DeleteDialogComponent,
    UploadFormComponent,
    UploaderComponent,
    FileUploadComponent,
    CreateProjectDialogComponent,
    ToolsVideoDialogComponent,
    GitCloneComponent,
    CreateUserComponent
  ],
  entryComponents: [DeleteDialogComponent],
  imports: [
    ReviewerModule,
    CommonModule,
    SharedModule,
    FlexLayoutModule,
    RouterModule,
    MaterialModule,
    MatListModule,
    MatDialogModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
  ],
  providers: [
    RolesRetrieverService,
    TimeAgoPipe,
    TimestampPipe,
    SnackbarService,
    UploaderService,
  ],
  exports: [ListOfProjectsComponent],
})
export class ProjectsListModule {}
