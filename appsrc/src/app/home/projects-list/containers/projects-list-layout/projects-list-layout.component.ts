import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ProjectsService } from '../../services/projects/projects.service';
import { Subscription } from 'rxjs';
import { ListOfProjectsComponent } from '../../components/list-of-projects/list-of-projects.component';
import { STORAGE_ITEMS } from 'src/app/core/services/login/login.service';

@Component({
  selector: 'app-projects-list-layout',
  templateUrl: './projects-list-layout.component.html',
  styleUrls: ['./projects-list-layout.component.scss'],
})
export class ProjectsListLayoutComponent implements OnInit, OnDestroy {
  // projectsNameId: PKMProjectNameId[] = [];
  selectedItem: string;
  editMode: boolean;
  superAdminRights: boolean;
  currentRole: string;

  @ViewChild('listPane') listOfProjectsComponent: ListOfProjectsComponent;

  // array with all the suscriptions
  subscriptions$: Subscription[] = [];

  constructor(private projectRetrieverService: ProjectsService) {
    this.superAdminRights = false;
    this.currentRole = localStorage.getItem(STORAGE_ITEMS.currentRole);
  }

  ngOnInit(): void {
    // only subscribe to the event of looking for projects in init (once)
    const mySubscription1: Subscription = this.checkIfUserCanCreateProjects();
    this.subscriptions$.push(mySubscription1);
  }

  ngOnDestroy(): void {
    // unsubscribe from all suscriptions
    this.subscriptions$.forEach((s: Subscription) => {
      s.unsubscribe();
    });
  }

  /**
   * Check if the logged user is allowed to create projects base on his roles
   */
  checkIfUserCanCreateProjects(): Subscription {
    return this.projectRetrieverService
      .getUserData(localStorage.getItem('currentUser'))
      .subscribe(
        (userData: any) => {
          const superUserRole: any = userData.roles.filter((item: any) => item.role === 'root' && item.db === 'admin');
          this.superAdminRights = superUserRole.length > 0;
        },
        (error: any) => {
          console.error(error);
        }
      );
  }

  // Events on the project list pane
  onProjectListRefreshed(): void {
   this.listOfProjectsComponent.refreshItems();
  }

  onProjectDeleted(projectId: string): void {
   this.listOfProjectsComponent.refreshItems();
   this.listOfProjectsComponent.onProjectDeselection();
  }

  onProjectUpdated(projectId: string): void {
    this.listOfProjectsComponent.refreshItems();
    this.listOfProjectsComponent.onProjectDeselection();
  }

  onProjectSelected(projectId: string): void {
    this.selectedItem = projectId;
  }

  onEditMode(enabled: boolean): void {
    this.editMode = enabled;
  }

  onProjectNameChanged(name: string): void {
    // actions in layout component. None for now.
  }

}
