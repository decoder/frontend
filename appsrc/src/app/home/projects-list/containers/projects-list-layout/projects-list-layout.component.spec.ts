import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectsListLayoutComponent } from './projects-list-layout.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { asyncScheduler, scheduled, of } from 'rxjs';
import { ProjectsService } from '../../services/projects/projects.service';
import { AppConfiguration } from 'src/config/app-configuration';

export function getMockProjectService(): Partial<ProjectsService> {
  return {
    getUserData: jasmine
      .createSpy('getUserData')
      .and.returnValue(scheduled(of([{
        name: 'myproject',
        members: [{ name: 'test-user', owner: true, roles: ['Owner'] }]
      }]), asyncScheduler)),
    getProjectsByUser: jasmine
    .createSpy('getProjectByUser')
    .and.returnValue(scheduled(of([{
      name: 'myproject',
      members: [{ name: 'test-user', owner: true, roles: ['Owner', 'root'] }]
    }]), asyncScheduler)),
  };
}

describe('ProjectsListLayoutComponent', () => {
  let component: ProjectsListLayoutComponent;
  let fixture: ComponentFixture<ProjectsListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectsListLayoutComponent],
      imports: [ HttpClientTestingModule, MatSnackBarModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: ProjectsService,
          useValue: getMockProjectService(),
        },
        AppConfiguration
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select a new project [onSelectItem]', () => {
    const projectId: string = '0';
    component.onProjectSelected(projectId);
    expect(component.selectedItem).toEqual(projectId);
  });

  it('should change to edit mode [onEditMode]', () => {
    const enable: boolean = true;
    component.onEditMode(enable);
    expect(component).toBeTruthy();
  });

  it('should change to no edit mode [onEditMode]', () => {
    const enable: boolean = false;
    component.onEditMode(enable);
    expect(component).toBeTruthy();
  });

  it('should check if user can create projects', () => {
    spyOn(component, 'checkIfUserCanCreateProjects');
    component.checkIfUserCanCreateProjects();
    expect(component.checkIfUserCanCreateProjects).toHaveBeenCalled();
    expect(component.superAdminRights).toEqual(false);
  });
});
