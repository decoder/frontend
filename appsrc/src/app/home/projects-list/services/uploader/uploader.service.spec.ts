import { TestBed, flush, fakeAsync } from '@angular/core/testing';
import { UploaderService } from './uploader.service';
import {
  HttpClientTestingModule,
  TestRequest,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { AppConfiguration } from 'src/config/app-configuration';

interface HttpTestResponse {
  body: string[] | string;
  status?: {
    status: number;
    statusText: string;
  };
}

describe('UploaderService', () => {
  let service: UploaderService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppConfiguration]
    });
    service = TestBed.inject(UploaderService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should upload a file', fakeAsync(() => {
    let uploadSuccessfully: boolean = false;
    service.upload(null, '0', '').subscribe(() => {
      uploadSuccessfully = true;
    });

    const response: HttpTestResponse = { body: 'OK' };
    expectedHttpResponse(response);
    flush();
    expect(uploadSuccessfully).toBeTruthy();
  }));

  it('should not upload a file', fakeAsync(() => {
    let uploadError: boolean = false;
    service.upload(null, null, null).subscribe(
      () => null,
      () => (uploadError = true)
    );

    const response: HttpTestResponse = {
      body: 'Projects not found',
      status: { status: 400, statusText: 'Project not found' }
    };
    expectedHttpResponse(response);
    flush();
    expect(uploadError).toBeTruthy();
  }));

  function expectedHttpResponse(response: HttpTestResponse): void {
    const request: TestRequest = http.expectOne((req: HttpRequest<any>) => {
      return req.method === 'POST' && req.url.startsWith('');
    });
    request.flush(response.body, response.status);
    http.verify();
  }
});
