import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpEvent } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { OperatorFunction } from 'rxjs';
import { AppConfiguration } from 'src/config/app-configuration';
@Injectable({
  providedIn: 'root'
})
export class UploaderService {

  constructor(private http: HttpClient, private appConfig: AppConfiguration) {}

  public upload(data: any, projectId: string, uploadUrl: string): Observable<any> {
    return this.uploadRequest(data, projectId, uploadUrl).pipe(this.progressNotifier());
  }

  public convertDocxFileToASFM(data: any, projectId: string): Observable<any> {
    const url: string = `${this.appConfig.asfmServerURL}/asfm/doc_to_asfm/${projectId}`;
    return this.http.post<any>(url, data);
  }

  private uploadRequest(data: any, projectId: string, uploadUrl: string): Observable<any> {
    return this.http.post<any>(`${uploadUrl}/${projectId}`, data, {
      reportProgress: true,
      observe: 'events'
    });
  }

  private progressNotifier(): OperatorFunction<HttpEvent<any>, any> {
    return map((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress: number = Math.floor(
            (100 * event.loaded) / event.total
          );
          return { status: 'progress', message: progress };
        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    });
  }
}
