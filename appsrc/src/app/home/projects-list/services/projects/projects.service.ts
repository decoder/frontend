import { Injectable } from '@angular/core';
import {
  Project,
  PKMProject,
  PKMProjectMember,
} from 'src/app/core/models/project';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { messageType } from 'src/app/shared/components/snackbar/snackbar.component';
import { VersionChange } from 'src/app/core/models/version-change';
import { VersionComparison } from 'src/app/core/models/version-comparison';
import { PKMUser } from 'src/app/core/models/PKMUser';
import { AppConfiguration } from 'src/config/app-configuration';
import emptyTestarSettings from 'src/assets/empty_testar_settings.json';

const EMPTY_PROJECTS: PKMProject[] = [];

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {

  projectBaseURL: string = this.appConfig.serverURL;

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(
    private readonly http: HttpClient,
    private snackBarService: SnackbarService,
    private appConfig: AppConfiguration
  ) {}

  createNewProject(projectName: string): Observable<any> {
    const httpOptions: any = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }),
    };
    const url: string = `${this.projectBaseURL}/project`;
    const body: any = {
      name: projectName,
      members: [
        {
          name: localStorage.getItem('currentUser'),
          roles: ['Developer', 'Maintainer', 'Reviewer', 'Owner'],
        },
      ],
      testarSettings: this.writeTestarConfig(projectName)
    };
    return this.http.post(url, body, httpOptions).pipe(
      catchError((err: any) => {
        this.snackBarService.openSnackBar(err.message, 'error');
        return of([]);
      })
    );
  }

  writeTestarConfig(dbName: string): object {
    Object.keys(emptyTestarSettings).forEach((key: string) => {
      switch (key) {
        case 'ApplicationName':
        case 'DataStoreDB':
        case 'PKMdatabase':
        case 'PreviousApplicationName':
          emptyTestarSettings[key] = `${dbName}`;
          break;
        case 'sse':
          emptyTestarSettings[key] = `webdriver_${dbName}`;
          break;
        case 'PKMkey':
          localStorage.getItem('access_token');
          break;
        default: break;
      }
    });
    return emptyTestarSettings;
  }

  getProjectsByUser(username: string, superAdminRights: boolean = true): Observable<PKMProject[]> {
    const url: string = superAdminRights ?
      `${this.projectBaseURL}/project?abbrev=true`
    : `${this.projectBaseURL}/user/${username}/project?abbrev=true`;

    return this.http
      .get<PKMProject[]>(url)
      .pipe(catchError(() => of(EMPTY_PROJECTS)));
  }

  getSingleProjectByUserName(
    username: string,
    projectName: string
  ): Observable<PKMProject> {
    const url: string = `${this.projectBaseURL}/user/${username}/project/${projectName}`;
    return this.http.get<PKMProject>(url).pipe(catchError(() => []));
  }

  getUserId(): Observable<string> {
    return of(localStorage.getItem('currentUserId'));
  }

  updateProjectMembers(
    projectName: string,
    projectMembers: PKMProjectMember[]
  ): Observable<any> {
    const url: string = `${this.projectBaseURL}/project`;
    return this.http.put(url, {
      name: projectName,
      members: projectMembers,
    });
  }

  deleteProject(project: Project): Observable<boolean> {
    const id: string = project.projectId;
    const url: string = `${this.projectBaseURL}/project/${id}`;
    return this.http.delete<Project>(url, { headers: this.headers })
      .pipe(
        map((_: Project) => {
          // logueo
          this.logWithSnackbar(
            `deleted project ${project.projectName}`,
            'warning'
          );
          // devuelvo exito de la operacion
          return true;
        }),
        catchError(
          // manejo error y devuelvo false
          this.handleError<boolean>('deleteProject', false)
        )
      );
  }

  getUserData(userId: string): Observable<any> {
    const url: string = `${this.projectBaseURL}/user/${userId}`;
    return this.http.get(url).pipe(catchError(() => []));
  }

  getUserInfo(): Observable<PKMUser> {
    const userId: string = localStorage.getItem('currentUser');
    const url: string = `${this.projectBaseURL}/user/${userId}`;
    return this.http.get<PKMUser>(url).pipe(map((data: PKMUser) => data));
  }

  compareVersions(comparison: VersionComparison): Observable<VersionChange[]> {
    const url: string = `${this.projectBaseURL}/${comparison.projectId}/version-compare/`;
    const params: HttpParams = new HttpParams()
      .set('versionId1', comparison.baseVersionId)
      .set('versionId2', comparison.compareToVersionId);
    return this.http.get<VersionChange[]>(url, { params });
  }

  private logWithSnackbar(message: string, type: messageType): void {
    this.snackBarService.openSnackBar(`ProjectsService: ${message}`, type);
  }

  private handleError<T>(operation: string = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      this.logWithSnackbar(`${operation} failed: ${error.message}`, 'error');
      return of(result);
    };
  }
}
