import { TestBed, fakeAsync, flush } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest,
} from '@angular/common/http/testing';
import { PKMProjectMember, Project } from '../../../../core/models/project';
import { ProjectsService } from './projects.service';
import { projectMock } from '../../../../mocks/core/fixtures/project.mock';
import { HttpRequest, HttpParams } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { VersionComparison } from 'src/app/core/models/version-comparison';
import { VersionChange } from 'src/app/core/models/version-change';
import { versionChangesMock } from 'src/app/mocks/core/fixtures/version-change.mock';
import { pkmProjectMembersMock } from 'src/app/mocks/core/fixtures/pkmProjectMembers.mock';
import { userMock } from 'src/app/mocks/core/fixtures/user.mock';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { AppConfiguration } from 'src/config/app-configuration';

interface HttpTestResponse<T> {
  body: T;
  status?: {
    status: number;
    statusText: string;
  };
}

describe('ProjectService', () => {
  let service: ProjectsService;
  let httpMock: HttpTestingController;
  let mockProjects: Project[];
  let mockPKMProjectMembers: PKMProjectMember[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
      ],
      providers: [SnackbarService, AppConfiguration],
    });
  });

  beforeEach(() => {
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ProjectsService);
    mockProjects = projectMock;
    mockPKMProjectMembers = pkmProjectMembersMock;

    const store: any = {
      currentUser: '0'
    };

    const mockLocalStorage: any = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // Testing createNewProject
  it('should create new project when createNewProject is called', () => {
    const projectName: string = 'projectTest';
    const createProjectURL: string = `${service.projectBaseURL}/project`;

    service.createNewProject(projectName).subscribe((result: any) => {
      expect(result).toBe(mockProjects);
    });
    const createProjectRequest: TestRequest =
      httpMock.expectOne(createProjectURL);
    expect(createProjectRequest.request.method).toBe('POST');
    createProjectRequest.flush(mockProjects);

    httpMock.verify();
  });

  // Testing getProjectsByUser
  it('should get projects when getProjectsByUser is called with no superAdmindmin rights', () => {
    const username: string = 'jsmith';
    const superAdminRights: boolean = false;
    const url: string = `${service.projectBaseURL}/user/${username}/project?abbrev=true`;

    service.getProjectsByUser(username, superAdminRights).subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(mockProjects);
    httpMock.verify();
  });

  it('should get projects when getProjectsByUser is called with superAdmindmin rights', () => {
    const username: string = 'jsmith';
    const superAdminRights: boolean = true;
    const url: string = `${service.projectBaseURL}/project?abbrev=true`;

    service.getProjectsByUser(username, superAdminRights).subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(mockProjects);
    httpMock.verify();
  });

  // Testing getSingleProjectByUserName
  it('should get project when getSingleProject is called', () => {
    const username: string = 'jsmith';
    const projectName: string = 'mythaistar';
    const url: string = `${service.projectBaseURL}/user/${username}/project/${projectName}`;

    service.getSingleProjectByUserName(username, projectName).subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
  });

  // Testing getUserId
  it('should get user id when getUserId is called', () => {
    localStorage.setItem('currentUserId', '1');
    service.getUserId().subscribe((id: string) => {
      expect(id).toEqual('1');
    });
  });

  // Testing updateProjectMembers
  it('should update members when updateProjectMembers is called', () => {
    const projectName: string = 'project';
    const projectMembers: PKMProjectMember[] = mockPKMProjectMembers;
    const url: string = `${service.projectBaseURL}/project`;

    service.updateProjectMembers(projectName, projectMembers).subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('PUT');
    req.flush(mockPKMProjectMembers);
    httpMock.verify();
  });

  // Testing deleteProject
  it('should call delete http method when deleteProject is called', () => {
    const project: Project = mockProjects[0];
    const deleteProjectURL: string = `${service.projectBaseURL}/project/${project.projectId}`;

    service.deleteProject(project).subscribe();

    const deleteProjectRequest: TestRequest =
      httpMock.expectOne(deleteProjectURL);
    expect(deleteProjectRequest.request.method).toBe('DELETE');
    deleteProjectRequest.flush(mockProjects[0]);

    httpMock.verify();
  });

  // Testing getUserData
  it('should call get http method when getUserData is called', () => {
    const id: string = '0';
    const url: string = `${service.projectBaseURL}/user/${id}`;

    service.getUserData(id).subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(userMock[0]);
    httpMock.verify();
  });

  // Testing getUserInfo
  it('should call get http method when getUserInfo is called', () => {
    const id: string = '0';
    const url: string = `${service.projectBaseURL}/user/${id}`;

    service.getUserInfo().subscribe();
    const req: TestRequest = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
    req.flush(userMock[0]);
    httpMock.verify();
  });

  it('should get the changes between versions on [compareVersions]', fakeAsync(() => {
    const response: HttpTestResponse<VersionChange[]> = {
      body: versionChangesMock,
    };
    const versionComparison: VersionComparison = {
      projectId: '1',
      baseVersionId: '2',
      compareToVersionId: '3',
    };
    let receivedChanges: VersionChange[];
    service
      .compareVersions(versionComparison)
      .subscribe((changes: VersionChange[]) => {
        receivedChanges = changes;
      });
    const params: HttpParams = expectedHttpResponse(response);

    flush();
    expect(receivedChanges).toEqual(versionChangesMock);
    expect(params.get('versionId1')).not.toBeFalsy();
    expect(params.get('versionId2')).not.toBeFalsy();
  }));

  function expectedHttpResponse(response: HttpTestResponse<any>): HttpParams {
    const url: string = service.projectBaseURL;

    const request: TestRequest = httpMock.expectOne(
      (req: HttpRequest<any>) => req.method === 'GET' && req.url.includes(url)
    );
    request.flush(response.body, response.status);
    httpMock.verify();

    return request.request.params;
  }
});
