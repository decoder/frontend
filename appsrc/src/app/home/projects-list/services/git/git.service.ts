import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AppConfiguration } from 'src/config/app-configuration';

export class GitResponseUtils {
  static isSuccessful(resp: any): boolean {
    return !(resp && resp.errorMessage);
  }
  static returnSuccessObject(resp: any): GitCommandSuccessResponse {
    return resp as GitCommandSuccessResponse;
  }
  static returnErrorObject(resp: any): GitCommandErrorResponse {
    return resp as GitCommandErrorResponse;
  }
}

export class GitCommandSuccessResponse {
  id: string;
  // tslint:disable-next-line: variable-name
  service_name: string;
  messages: string[];
  warnings: string[];
  errors: string[];
  state: string;
  parameters: any;
  // tslint:disable-next-line: variable-name
  start_date: string;
  // tslint:disable-next-line: variable-name
  end_date: string;
}

export class GitCommandErrorResponse {
  errorMessage: string;
}

@Injectable({
  providedIn: 'root',
})
export class GitService {
  projectBaseURL: string = this.appConfig.serverURL;

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(
    private readonly http: HttpClient,
    private appConfig: AppConfiguration
  ) {
    // initialization here
  }

  public cloneGitRepository(
    gitRepoURL: string,
    dbName: string,
    username: string,
    password: string,
    key: string
  ): Observable<any> {
    const httpOptions: any = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      }),
    };
    const url: string = `${this.projectBaseURL}/git/run/${dbName}`;
    const body: any = {
      git_commands: [['clone', `${gitRepoURL}`]],
      options: {
        dont_delete_pkm_files: false,
        dont_delete_git_working_tree_files: false,
        git_user_credentials: [
          {
            git_remote_url: `${gitRepoURL}`,
            git_user_name: `${username}`,
            git_password: `${password}`,
            git_ssh_private_key: `${key}`,
          },
        ],
      },
    };
    return this.http.post<any>(url, body, httpOptions).pipe(
      map((resp: any) => {
        return GitResponseUtils.returnSuccessObject(resp);
      }),
      catchError((httpErr: any) => {
        const errResp: GitCommandErrorResponse = {
          errorMessage: `Git clone command failed:\n${httpErr.message}`,
        } as GitCommandErrorResponse;
        return of(errResp);
      })
    );
  }
}
