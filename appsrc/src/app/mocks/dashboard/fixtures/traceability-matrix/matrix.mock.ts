import { mockMatrixElements } from './matrix-elements.mock';
import { PKMMatrixJSON } from 'src/app/plugins/dashboard/services/traceability-matrix/types/PKMMatrixJSON';

export const mockMatrix: PKMMatrixJSON = mockMatrixElements;
