import { PKMMatrixJSON } from 'src/app/plugins/dashboard/services/traceability-matrix/types/PKMMatrixJSON';

export const mockMatrixElements: PKMMatrixJSON = [
  {
    _id: '6059cebf903afc0040c1dfef',
    // tslint:disable-next-line: max-line-length
    src_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    src_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%604015%60%5D.comments%5B%5D',
    src_text: 'test',
    tgt_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    tgt_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%602857%60%5D.comments%5B%5D',
    tgt_text: 'test',
    invocationID: 'generated ID',
    similarity: 0,
    role: ''
  },
  {
    _id: '6059cefa903afc0040c1dff1',
    // tslint:disable-next-line: max-line-length
    src_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    src_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%602857%60%5D.comments%5B%5D',
    src_text: 'test',
    // tslint:disable-next-line: max-line-length
    tgt_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    tgt_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%602857%60%5D.comments%5B%5D',
    tgt_text: 'test',
    invocationID: 'generated ID',
    similarity: 0,
    role: ''
  },
  {
    _id: '6059e960903afc0040c1e088',
    // tslint:disable-next-line: max-line-length
    src_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fapi%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fusermanagement%2Fservice%2Fapi%2Frest%2FUsermanagementRestService.java',
    src_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%602583%60%5D.comments%5B%5D',
    src_text: 'test',
    // tslint:disable-next-line: max-line-length
    tgt_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    tgt_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%6010259%60%5D.comments%5B%5D',
    tgt_text: 'test',
    invocationID: 'generated ID',
    similarity: 0,
    role: ''
  },
  {
    _id: '6059e993903afc0040c1e08a',
    // tslint:disable-next-line: max-line-length
    src_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    src_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%6025253%60%5D.comments%5B%5D',
    src_text: 'test',
    // tslint:disable-next-line: max-line-length
    tgt_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    tgt_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%6025253%60%5D.comments%5B%5D',
    tgt_text: 'test',
    invocationID: 'generated ID',
    similarity: 0,
    role: ''
  },
  {
    _id: '605a0faf903afc0040c1e495',
    // tslint:disable-next-line: max-line-length
    src_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fcore%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fbookingmanagement%2Flogic%2Fimpl%2FBookingmanagementImpl.java',
    src_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%604015%60%5D.comments%5B%5D',
    src_text: 'test',
    // tslint:disable-next-line: max-line-length
    tgt_path:
      // tslint:disable-next-line: max-line-length
      'code/comments/mythaistar/java%2Fmtsj%2Fapi%2Fsrc%2Fmain%2Fjava%2Fcom%2Fdevonfw%2Fapplication%2Fmtsj%2Fusermanagement%2Fservice%2Fapi%2Frest%2FUsermanagementRestService.java',
    tgt_access:
      '%5B0%5D.comments%5B%3Floc.pos_start.pos_cnum%20%3D%3D%20%601619%60%5D.comments%5B%5D',
    tgt_text: 'test',
    invocationID: 'generated ID',
    similarity: 0,
    role: ''
  },
];
