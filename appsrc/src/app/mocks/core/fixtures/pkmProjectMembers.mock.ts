import { PKMProjectMember } from 'src/app/core/models/project';

export const pkmProjectMembersMock: PKMProjectMember[] = [
  {
    name: 'José Fernandez',
    roles: ['Maintaner'],
  },
  {
    name: 'Rubber Man',
    roles: ['Devoloper'],
  },
  {
    name: 'Mario Rossi',
    roles: ['Maintaner', 'Reviewer'],
  },
];
