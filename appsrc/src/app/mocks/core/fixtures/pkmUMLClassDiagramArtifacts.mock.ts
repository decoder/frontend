import { PKMUMLClassDiagramArtifact } from 'src/app/core/models/artifact';

export const pkmUMLClassDiagramArtifactsMock: PKMUMLClassDiagramArtifact[] = [
  {
    _id: '3',
    name: 'Artifact uml diagram',
    type: 'UML Model',
    modules: '',
    sourcefile: 'sourcefile',
    diagram: '',
  },
  {
    _id: '4',
    name: 'Artifact uml diagram 2',
    type: 'UML Model',
    modules: '',
    sourcefile: 'sourcefile',
    diagram: '',
  },
  {
    _id: '5',
    name: 'Artifact uml diagram 3',
    type: 'UML Model',
    modules: '',
    sourcefile: 'sourcefile',
    diagram: '',
  },
];
