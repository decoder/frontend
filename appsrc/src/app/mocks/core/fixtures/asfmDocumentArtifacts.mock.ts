import { PKMASFMDocArtifact } from 'src/app/plugins/project/models/asfm-document-artifact.model';

export const asfmDocumentArtifactsMock: PKMASFMDocArtifact[] = [
  {
    _id: '12',
    name: 'name example',
    type: 'type',
    sourceFile: 'source file example',
    units: [
      {
        id: 1,
        name: 'example',
        classes: [
          {
            id: 2,
            name: 'name',
          },
          {
            id: 3,
            name: 'name',
          },
        ],
      },
      {
        id: 2,
        name: 'example',
        classes: [
          {
            id: 3,
            name: 'name',
          },
          {
            id: 5,
            name: 'name',
          },
        ],
      },
      {
        id: 3,
        name: 'example',
        classes: [
          {
            id: 4,
            name: 'name',
          },
          {
            id: 7,
            name: 'name',
          },
        ],
      },
    ],
  },
];
