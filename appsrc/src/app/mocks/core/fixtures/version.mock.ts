import { Version } from '../../../core/models/version';
import {
  ONE_SECOND,
  ONE_MINUTE,
  ONE_DAY,
  ONE_HOUR,
  ONE_WEEK
} from 'src/app/shared/services/passed-time/passed-time';

export const versionMock: Version[] = [
  {
    versionId: '0',
    versionName: '1.0',
    creationDate: createTimeAgo(ONE_SECOND),
    commits: 0,
    lastCommitId: '0',
    relatedArtifacts: undefined,
    versionStatus: 'Processed'
  },
  {
    versionId: '1',
    versionName: '2.0',
    creationDate: createTimeAgo(2 * ONE_SECOND),
    commits: 2,
    lastCommitId: '0',
    relatedArtifacts: [],
    versionStatus: 'Processed'
  },
  {
    versionId: '2',
    versionName: '2.1',
    creationDate: createTimeAgo(ONE_MINUTE),
    commits: 56,
    lastCommitId: '0',
    relatedArtifacts: [],
    versionStatus: 'Processing'
  },
  {
    versionId: '3',
    versionName: '1.0',
    creationDate: createTimeAgo(2 * ONE_MINUTE),
    lastCommitId: '0',
    commits: 0,
    relatedArtifacts: ['0', '1', '2', '3'],
    versionStatus: 'Error'
  },
  {
    versionId: '4',
    versionName: '1.1',
    creationDate: createTimeAgo(ONE_HOUR),
    lastCommitId: '0',
    commits: 2,
    relatedArtifacts: ['0'],
    versionStatus: 'Tested'
  },
  {
    versionId: '5',
    versionName: '1.0',
    creationDate: createTimeAgo(2 * ONE_HOUR),
    lastCommitId: '0',
    commits: 2,
    relatedArtifacts: ['0', '1'],
    versionStatus: 'Testing'
  },
  {
    versionId: '6',
    versionName: '1.1',
    creationDate: createTimeAgo(ONE_DAY),
    lastCommitId: '0',
    commits: 23,
    relatedArtifacts: ['1', '2'],
    versionStatus: 'Processed'
  },
  {
    versionId: '7',
    versionName: '1.6',
    creationDate: createTimeAgo(2 * ONE_DAY),
    lastCommitId: '0',
    commits: 56,
    relatedArtifacts: ['1'],
    versionStatus: 'Processing'
  },
  {
    versionId: '8',
    versionName: '1.10',
    creationDate: createTimeAgo(ONE_WEEK),
    lastCommitId: '0',
    commits: 77,
    relatedArtifacts: ['0', '1', '2', '3'],
    versionStatus: 'Tested'
  },
  {
    versionId: '9',
    versionName: '1.10.2',
    creationDate: createTimeAgo(2 * ONE_WEEK),
    lastCommitId: '0',
    commits: 79,
    relatedArtifacts: ['0', '1', '2', '3'],
    versionStatus: 'Tested'
  }
];

function createTimeAgo(milisecondsPassed: number): Date {
  return new Date(new Date().getTime() - milisecondsPassed);
}
