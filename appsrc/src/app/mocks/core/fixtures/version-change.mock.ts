import { VersionChange } from 'src/app/core/models/version-change';

export const versionChangesMock: VersionChange[] = [
  {
    newFiles: 15
  },
  {
    updatedFiles: 17
  },
  {
    removedFiles: 3
  }
];
