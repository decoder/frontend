import { MemberDetails } from '../../../core/models/memberDetails';

export const memberDetailsAdmin: MemberDetails[] = [
    {
        id: '0',
        username: 'JoferCatalunyo',
        email: 'fernandez@ibm.com',
        status: 'Accepted',
        requestDate: new Date(),
    },
    {
        id: '1',
        username: 'EraserHead',
        email: 'rubber@capgemini.com',
        status: 'Pending',
        requestDate: new Date(),
    },
    {
        id: '2',
        username: 'chyna.mckenzie',
        email: 'chyna.mckenzie@yahoo.com',
        status: 'Accepted',
        requestDate: new Date(),
    },
    {
        id: '3',
        username: 'Rossinante',
        email: 'rossi@microsoft.com',
        status: 'Rejected',
        requestDate: new Date(),
    },
    {
        id: '4',
        username: 'stracke.nova',
        email: 'stracke.nova@capgemini.com',
        status: 'Pending',
        requestDate: new Date(),
    },
    {
        id: '5',
        username: 'efren.rath',
        email: 'efren.rath@capgemini.com',
        status: 'Pending',
        requestDate: new Date(),
    },
    {
        id: '6',
        username: 'antonio_breitenberg',
        email: 'antonio_breitenberg@capgemini.com',
        status: 'Pending',
        requestDate: new Date(),
    },
    {
        id: '7',
        username: 'EraserHead',
        email: 'rubber@capgemini.com',
        status: 'Pending',
        requestDate: new Date(),
    },
];
