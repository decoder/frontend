import { Role } from '../../../core/models/role';

export const rolesMock: Role[] = [
  { db: 'test', role: 'Maintainer' },
  { db: 'test', role: 'Developer' },
  {db: 'test', role: 'Reviewer' }
];
export const rolesMockSorted: Role[] = [
  { db: 'test', role: 'Developer' },
  { db: 'test', role: 'Maintainer' },
  {db: 'test', role: 'Reviewer' }
];
export const connectionProfilesMock: Role[] = [
  { db: 'test', role: 'Developer' },
  { db: 'test', role: 'Maintainer' },
];
