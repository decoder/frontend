import { PKMArtifact } from 'src/app/core/models/artifact';

export const pkmArtifactsMock: PKMArtifact[] = [
    {
        id: 'artifactTest.java',
        content: '',
        encoding: 'utf-8',
        format: '',
        git_commit_id: '',
        mime_type: '',
        git_repository_url: '',
        name: 'artifactTest.java',
        rel_path: 'artifactTest.java',
        sourceFile: 'artifactTest.java',
        type: 'Code',
    },
    {
        id: 'artifactTest.c',
        content: '',
        encoding: 'utf-8',
        format: '',
        git_commit_id: '',
        mime_type: '',
        git_repository_url: '',
        name: 'artifactTest.c',
        rel_path: 'artifactTest.c',
        sourceFile: 'artifactTest.c',
        type: 'Code',
    },
    {
        id: 'artifactTest.docx',
        content: '',
        encoding: 'utf-8',
        format: '',
        git_commit_id: '',
        mime_type: '',
        git_repository_url: '',
        name: 'artifactTest.docx',
        rel_path: 'artifactTest.docx',
        sourceFile: 'artifactTest.docx',
        type: 'Document',
    }
];
