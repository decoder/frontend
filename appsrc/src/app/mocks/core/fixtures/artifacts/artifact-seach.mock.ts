import { ArtifactSearch } from 'src/app/core/models/artifacts/artifact-search';
import { artifactSearchItemsMock } from './artifact-search-item.mock';

export const artifactSearchMock: ArtifactSearch = {
  results: artifactSearchItemsMock,
  pagination: { page: 0, pageSize: 5, total: 1950 },
};
