import { User } from 'src/app/core/models/user';

export const userMock: User[] = [
  {
    id: '0',
    firstName: 'José',
    surname: 'Fernandez',
    username: 'JoferCatalunyo',
    registerDate: new Date(),
    roles: [{db: 'test', role: 'Developer' }],
    organization: 'IBM',
    location: 'Madrid',
    email: 'fernandez@ibm.com',
    picture: '../../../../assets/img/N3huegc.png',
    projectList: [0, 1],
    lastLogin: new Date()
  },
  {
    id: '1',
    firstName: 'Rubber',
    surname: 'Man',
    username: 'EraserHead',
    registerDate: new Date(),
    roles: [{db: 'test', role: 'Developer' },
    {db: 'test', role: 'Maintainer'}],
    organization: 'Capgemini',
    location: 'Valencia',
    email: 'rubber@capgemini.com',
    picture: '../../../../assets/img/N3huegc.png',
    projectList: [0],
    lastLogin: undefined
  },
  {
    id: '2',
    firstName: 'Mario',
    surname: 'Rossi',
    username: 'Rossinante',
    registerDate: new Date(),
    roles: [{db: 'test', role: 'Developer' }],
    organization: 'Microsoft',
    location: 'Rome',
    email: 'rossi@microsoft.com',
    picture: '../../../../assets/img/N3huegc.png',
    projectList: [0, 1, 2],
    lastLogin: new Date()
  }
];
