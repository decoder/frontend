import { Project, ProjectNameId } from '../../../core/models/project';
import { versionMock } from './version.mock';
import { memberDetailsMock } from './memberDetails.mock';

export const projectMock: Project[] = [
  {
    projectId: '0',
    projectName: 'Mock DECODER',
    company: 'Mock Capgemini AD Center',
    projectToken: 'f71f056ee1df46eaa202ab4ae534b2ee',
    versions: [versionMock[0], versionMock[1], versionMock[2]],
    repository: '',
    lastCommitDate: new Date(),
    creationDate: new Date(),
    userCreatorId: '0',
    members: [memberDetailsMock[0], memberDetailsMock[1], memberDetailsMock[2]],
  },
  {
    projectId: '1',
    projectName: 'Mock PPO',
    company: 'MOCK Capgemini España',
    projectToken: 'f71f056ee1df46eaa202ab4ae534b2ee',
    versions: [versionMock[3], versionMock[4]],
    repository: '',
    lastCommitDate: new Date(),
    creationDate: new Date(),
    userCreatorId: '1',
    members: [memberDetailsMock[3], memberDetailsMock[4]],
  },
  {
    projectId: '2',
    projectName: 'Mock FINEOS',
    projectToken: 'f71f056ee1df46eaa202ab4ae534b2ee',
    company: 'Mock Capgemini',
    versions: [versionMock[5], versionMock[6], versionMock[7], versionMock[8]],
    repository: '',
    lastCommitDate: new Date(),
    creationDate: new Date(),
    userCreatorId: '2',
    members: [memberDetailsMock[5]],
  },
];

export const projectNameIdMock: ProjectNameId[] = [
  {
    projectId: '0',
    projectName: 'Mock DECODER',
    owner: true,
  },
  {
    projectId: '1',
    projectName: 'Mock PPO',
    owner: true,
  },
  {
    projectId: '2',
    projectName: 'Mock FINEOS',
    owner: true,
  },
];
