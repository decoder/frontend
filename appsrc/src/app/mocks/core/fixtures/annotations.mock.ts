import { Annotation } from 'src/app/core/models/annotation';
export const posMock: Pos[] = [
  {
    pos_cnum: 3,
    pos_lnum: 45,
  },
  {
    pos_cnum: 6,
    pos_lnum: 56,
  },
  {
    pos_cnum: 7,
    pos_lnum: 67,
  },
  {
    pos_cnum: 8,
    pos_lnum: 72,
  },
];

export const locsMock: Loc[] = [
  {
    pos_end: posMock[1],
    pos_start: posMock[0],
  },
  {
    pos_end: posMock[2],
    pos_start: posMock[1],
  },
  {
    pos_end: posMock[3],
    pos_start: posMock[2],
  },
];

export const annotationsMock: Annotation[] = [
  {
    annotations: ['example', 'of', 'annotations'],
    commentInEnvironment: '',
    annotationInEnvironmentSplited: [''],
    loc: locsMock[0],
  },
  {
    annotations: ['example2', 'of', 'annotations'],
    commentInEnvironment: '',
    annotationInEnvironmentSplited: [''],
    loc: locsMock[1],
  },
  {
    annotations: ['example3', 'of', 'annotations'],
    commentInEnvironment: '',
    annotationInEnvironmentSplited: [''],
    loc: locsMock[2],
  },
  {
    annotations: ['example4', 'of', 'annotations'],
    commentInEnvironment: '',
    annotationInEnvironmentSplited: [''],
    loc: locsMock[3],
  },
];

interface Loc {
  pos_end: Pos;
  pos_start: Pos;
}

interface Pos {
  pos_cnum: number;
  pos_lnum: number;
}
