import { MemberDetails } from '../../../core/models/memberDetails';
import { PKMProjectMember } from 'src/app/core/models/project';

export const memberDetailsMock: MemberDetails[] = [
    {
        id: '0',
        memberId: '0',
        username: 'José Fernandez',
        email: 'fernandez@ibm.com',
        status: '-',
        roles: [ {db: 'test', role: 'Mantainer' } ],
        owner: true
    },
    {
        id: '1',
        memberId: '1',
        username: 'Rubber Man',
        email: 'rubber@capgemini.com',
        status: 'Pending',
        roles: [ {db: 'test', role: 'Developer' } ],
        owner: false
    },
    {
        id: '2',
        memberId: '2',
        username: 'Mario Rossi',
        email: 'rossi@microsoft.com',
        status: 'Accepted',
        roles: [ {db: 'test', role: 'Mantainer' }, {db: 'test', role: 'Reviewer' } ],
        owner: false
    },
    {
        id: '3',
        memberId: '0',
        username: 'José Fernandez',
        email: 'fernandez@ibm.com',
        status: 'Rejected',
        roles: [ {db: 'test', role: 'Mantainer' }, {db: 'test', role: 'Mantainer' }, {db: 'test', role: 'Developer' } ],
        owner: false
    },
    {
        id: '4',
        memberId: '2',
        username: 'Mario Rossi',
        email: 'rossi@microsoft.com',
        status: '-',
        roles: [ {db: 'test', role: 'Developer' } ],
        owner: true
    },
    {
        id: '5',
        memberId: '2',
        username: 'Mario Rossi',
        email: 'rossi@microsoft.com',
        status: '-',
        roles: [ {db: 'test', role: 'Reviewer' } ],
        owner: true
    },
];

export const pkmMemberDetailsMock: PKMProjectMember[] = [
    {
        name: 'José Fernandez',
        roles: ['Developer', 'Mantainer', 'Reviewer' ],
        owner: true
    },
    {
        name: 'Rubber Man',
        roles: ['Developer', 'Mantainer', 'Reviewer' ],
        owner: true
    },
    {
        name: 'Mario Rossi',
        roles: ['Developer', 'Mantainer', 'Reviewer' ],
        owner: true
    },
];
