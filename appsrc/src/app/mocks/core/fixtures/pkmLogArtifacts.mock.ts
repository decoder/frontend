import { PKMLogArtifact } from 'src/app/core/models/artifact';

export const pkmLogArtifacts: PKMLogArtifact[] = [
  {
    id: '18l',
    _id: '18',
    tool: 'tool example',
    'nature of report': 'execution log',
    type: 'Log',
    'start running time': '2021-02-26T11:43:34.000Z',
    'end running time': '2021-02-26T11:43:34.000Z',
    messages: ['msg1', 'msg2', 'msg3', 'msg4'],
    warnings: ['wrn1', 'wrn2', 'wrn3', 'wrn4'],
    errors: ['err1', 'err2', 'err3'],
    status: false,
  },
  {
    id: '19l',
    _id: '19',
    tool: 'tool example',
    'nature of report': 'execution log',
    type: 'Log',
    'start running time': '2021-02-26T11:43:34.000Z',
    'end running time': '2021-02-26T11:43:34.000Z',
    messages: ['msg1', 'msg2', 'msg3', 'msg4'],
    warnings: ['wrn1', 'wrn2', 'wrn3', 'wrn4'],
    errors: ['err1', 'err2', 'err3'],
    status: true,
  },
  {
    id: '20l',
    _id: '20',
    tool: 'tool example',
    'nature of report': 'execution log',
    type: 'Log',
    'start running time': '2021-02-26T11:43:34.000Z',
    'end running time': '2021-02-26T11:43:34.000Z',
    messages: ['msg1', 'msg2', 'msg3', 'msg4'],
    warnings: ['wrn1', 'wrn2', 'wrn3', 'wrn4'],
    errors: ['err1', 'err2', 'err3'],
    status: false,
  },
];
