import { PKMAnnotation } from 'src/app/core/models/annotation';
import { annotationsMock } from './annotations.mock';

export const pkmAnnotationsMock: PKMAnnotation[] = [
  {
    _id: '0',
    tool: 'zero',
    annotations: [annotationsMock[0], annotationsMock[1]],
    fileEncoding: 'fileEncoding',
    fileFormat: 'docx',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Document',
  },
  {
    _id: '1',
    tool: 'one',
    annotations: [annotationsMock[1], annotationsMock[2]],
    fileEncoding: 'fileEncoding',
    fileFormat: 'java',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Code',
  },
  {
    _id: '2',
    tool: 'two',
    annotations: [annotationsMock[0], annotationsMock[1]],
    fileEncoding: 'fileEncoding',
    fileFormat: 'docx',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Document',
  },
];
