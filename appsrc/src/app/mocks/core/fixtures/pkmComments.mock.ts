import { PKMComment } from 'src/app/core/models/comment';

export const pkmCommentsMock: PKMComment[] = [
  {
    _id: '0',
    comments: [],
    fileEncoding: 'fileEncoding',
    fileFormat: 'docx',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Document',
  },
  {
    _id: '1',
    comments: [],
    fileEncoding: 'fileEncoding',
    fileFormat: 'docx',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Document',
  },
  {
    _id: '2',
    comments: [],
    fileEncoding: 'fileEncoding',
    fileFormat: 'docx',
    fileMimeType: 'mime',
    sourceFile: 'onefile',
    type: 'Document',
  },
];
