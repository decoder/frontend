import { PKMDocumentArtifact } from 'src/app/core/models/artifact';

export const pkmDocumentArtifactsmock: PKMDocumentArtifact[] = [
  {
    rel_path: '',
    content: 'content testing example',
    format: '',
    encoding: 'UTF-8',
    type: 'Document',
    mime_type: '.docx',
    git_repository_url: '',
    git_commit_id: '23',
  },
  {
    rel_path: '',
    content: 'content testing example',
    format: '',
    encoding: 'UTF-8',
    type: 'Document',
    mime_type: '.docx',
    git_repository_url: '',
    git_commit_id: '26',
  },
  {
    rel_path: '',
    content: 'content testing example',
    format: '',
    encoding: 'UTF-8',
    type: 'Document',
    mime_type: '.docx',
    git_repository_url: '',
    git_commit_id: '31',
  },
];
