import { PKMProject } from 'src/app/core/models/project';
import { pkmProjectMembersMock } from './pkmProjectMembers.mock';

export const pkmProjectMock: PKMProject[] = [
  {
    name: 'PKM Mock DECODER',
    members: [
      pkmProjectMembersMock[0],
      pkmProjectMembersMock[1],
      pkmProjectMembersMock[2],
    ],
  },
];
