import { PKMUser } from 'src/app/core/models/PKMUser';

export const pkmUserMock: PKMUser[] = [
  {
    email: 'josefernandez@ibm.com',
    first_name: 'José',
    last_name: 'Fernandez',
    name: 'JoferCatalunyo',
    phone: '675345264',
    roles: [],
  },
  {
    email: 'rubberman@ibm.com',
    first_name: 'Rubber',
    last_name: 'Man',
    name: 'EraserHead',
    phone: '695798897',
    roles: [],
  },
  {
    email: 'mariorossi@ibm.com',
    first_name: 'Mario',
    last_name: 'Rossi',
    name: 'Rossinante',
    phone: '654783298',
    roles: [],
  },
];
