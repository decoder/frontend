import { Invocation } from 'src/app/core/models/invocation';

export enum InvocationStatus {
  Completed = 'COMPLETED',
  Running = 'RUNNING',
  Pending = 'PENDING',
}

export const invocationMock: Invocation[] = [
  {
    invocationID: '439893D',
    tool: 'FramaC',
    invocationConfiguration: { generate: 'all' },
    user: 'jsmith',
    timestampRequest: '20210614_082526',
    timestampStart: '20210614_082527',
    timestampCompleted: '20210614_082543',
    invocationArtifacts: [
      {
        dbName: 'RawSourcecode',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
      {
        dbName: 'sdf',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
    ],
    invocationStatus: InvocationStatus.Completed,
    invocationResults: [
      {
        path: 'sourcecodeC',
        type: 'c/mtsj/core/src/main/file.c',
      },
    ],
  },
  {
    invocationID: '531253P',
    tool: 'javaparser',
    invocationConfiguration: { generate: 'all' },
    user: 'jsmith',
    timestampRequest: '20210623_082226',
    timestampStart: '20210623_082227',
    timestampCompleted: '20210623_082843',
    invocationArtifacts: [
      {
        dbName: 'RawSourcecode',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
    ],
    invocationStatus: InvocationStatus.Running,
    invocationResults: [
      {
        path: 'sourcecodeC',
        type: 'c/mtsj/core/src/main/file.c',
      },
    ],
  },
  {
    invocationID: '739253A',
    tool: 'JMLGen',
    invocationConfiguration: { generate: 'all' },
    user: 'jsmith',
    timestampRequest: '20210624_082226',
    timestampStart: '20210624_082227',
    timestampCompleted: '20210624_082843',
    invocationArtifacts: [
      {
        dbName: 'RawSourcecode',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
    ],
    invocationStatus: InvocationStatus.Pending,
    invocationResults: [
      {
        path: 'sourcecodeC',
        type: 'c/mtsj/core/src/main/file.c',
      },
    ],
  },
  {
    invocationID: '839253F',
    tool: 'Testar',
    invocationConfiguration: { generate: 'all' },
    user: 'admin',
    timestampRequest: '20210622_082226',
    timestampStart: '20210622_082227',
    timestampCompleted: '20210622_082843',
    invocationArtifacts: [
      {
        dbName: 'RawSourcecode',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
    ],
    invocationStatus: InvocationStatus.Pending,
    invocationResults: [
      {
        path: 'sourcecodeC',
        type: 'c/mtsj/core/src/main/file.c',
      },
    ],
  },
  {
    invocationID: '839253F',
    tool: 'SRL tool',
    invocationConfiguration: { generate: 'all' },
    user: 'admin',
    timestampRequest: '20210626_082226',
    timestampStart: '20210626_082227',
    timestampCompleted: '20210626_082843',
    invocationArtifacts: [
      {
        dbName: 'RawSourcecode',
        project_id: 'c/mtsj/core/src/main/file.c',
      },
    ],
    invocationStatus: InvocationStatus.Pending,
    invocationResults: [
      {
        path: 'sourcecodeC',
        type: 'c/mtsj/core/src/main/file.c',
      },
    ],
  },
];
