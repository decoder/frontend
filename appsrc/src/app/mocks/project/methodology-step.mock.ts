import { MethodologyStep } from 'src/app/plugins/project/components/methodology-support/methodology-support.component';

export const methodologyStepsMock: MethodologyStep[] = [
  {
    phaseNumber: 1,
    name: 'HLD',
    description: 'High Level Design',
    completed: true,
    tasks: [
      {
        taskNumber: 1,
        name: 'Requirements',
        completed: true
      },
      {
        taskNumber: 2,
        name: 'System Design',
        completed: true
      },
      {
        taskNumber: 3,
        name: 'Architecture Design',
        completed: true
      }
    ]
  },
  {
    phaseNumber: 2,
    name: 'LLD',
    description: 'Low Level Design',
    completed: true,
    tasks: [
      {
        taskNumber: 1,
        name: 'Module Design',
        completed: true
      },
      {
        taskNumber: 2,
        name: 'System Implementation',
        completed: true
      }
    ]
  },
  {
    phaseNumber: 3,
    name: 'HLD',
    description: 'Low Level Verification',
    completed: true,
    tasks: [
      {
        taskNumber: 1,
        name: 'Unit Testing',
        completed: true
      },
      {
        taskNumber: 2,
        name: 'Integration Testing',
        completed: true
      }
    ]
  },
  {
    phaseNumber: 4,
    name: 'LLD',
    description: 'High Level Verification',
    completed: true,
    tasks: [
      {
        taskNumber: 1,
        name: 'System Testing',
        completed: true
      },
      {
        taskNumber: 2,
        name: 'Acceptance Testing',
        completed: true
      }
    ]
  }
];
