import {
  Review,
  ReviewHistory,
  ReviewArtifacts,
} from 'src/app/core/models/review';

export const reviewMock: Review[] = [
  {
    reviewID: '1',
    reviewTitle: 'Test1',
    reviewStatus: 'OPEN',
    reviewOpenDate: '10/11/2020',
    reviewCompletedDate: '11/11/2020',
    reviewCommets: [
      {
        author: 'admin',
        datetime: '17/09/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
      {
        author: 'jsmith',
        datetime: '13/12/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
    ],
    reviewAuthor: 'jsmith',
    reviewArtifacts: [
      {
        type: 'Code',
        path: 'code.java'
      }
    ],
  },
  {
    reviewID: '1',
    reviewTitle: 'Test1',
    reviewStatus: 'OPEN',
    reviewOpenDate: '10/01/2020',
    reviewCompletedDate: '25/02/2020',
    reviewCommets: [
      {
        author: 'admin',
        datetime: '17/09/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
      {
        author: 'jsmith',
        datetime: '13/12/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
    ],
    reviewAuthor: 'mgil',
    reviewArtifacts: [
      {
        type: 'Code',
        path: 'code.java'
      }
    ],
  },
  {
    reviewID: '1',
    reviewTitle: 'Test1',
    reviewStatus: 'OPEN',
    reviewOpenDate: '01/05/2021',
    reviewCompletedDate: '11/06/2020',
    reviewCommets: [
      {
        author: 'admin',
        datetime: '17/09/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
      {
        author: 'jsmith',
        datetime: '13/12/2019',
        comment:
          // tslint:disable-next-line: max-line-length
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      },
    ],
    reviewAuthor: 'admin',
    reviewArtifacts: [
      {
        type: 'Code',
        path: 'code.java'
      }
    ],
  },
];

export const reviewHistoryMock: ReviewHistory[] = [
  {
    number: 1,
    user: 'User 001',
    date: '20/10/2020',
    comments: 'Reviewed CD.uml(Comparison)',
  },
  {
    number: 2,
    user: 'User 002',
    date: '19/10/2020',
  },
];

export const reviewArtifactsMock: ReviewArtifacts[] = [
  {
    path: 'Person.java',
    type: 'Raw source code',
  },
  {
    path: 'Inst.java',
    type: 'Raw source code',
  },
];
