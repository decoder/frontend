import { ReviewComment } from 'src/app/core/models/reviewComment';

export const commentMock: ReviewComment[] = [
  {
    author: 'admin',
    datetime: '17/09/2019',
    comment:
      // tslint:disable-next-line: max-line-length
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
  {
    author: 'jsmith',
    datetime: '13/12/2019',
    comment:
      // tslint:disable-next-line: max-line-length
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  },
];
