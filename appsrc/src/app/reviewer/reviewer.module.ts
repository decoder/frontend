import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReviewerHomeComponent } from './containers/reviewer-home/reviewer-home.component';
import { ProjectInfoHeaderComponent } from './components/project-info-header/project-info-header.component';
import { MaterialModule } from '../material.module';
import { ReviewTableComponent } from './components/review-table/review-table.component';
import { MatIconModule } from '@angular/material/icon';
import { ReviewInfoComponent } from './components/review-info/review-info.component';
import { ReviewOverviewComponent } from './components/review-overview/review-overview.component';
import { ReviewSummaryComponent } from './components/review-summary/review-summary.component';
import { ReviewCommentsComponent } from './components/review-comments/review-comments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: 'summary' },
  // {
  //   path: 'summary',
  //   component: ReviewerHomeComponent,
  //   children: [
  //     { path: '', pathMatch: 'full', component: ReviewSummaryComponent },
  //     {
  //       path: 'review/:projectId',
  //       pathMatch: 'full',
  //       component: ReviewOverviewComponent,
  //     },
  //   ],
  // },
];

@NgModule({
  declarations: [
    ReviewerHomeComponent,
    ReviewTableComponent,
    ProjectInfoHeaderComponent,
    ReviewCommentsComponent,
    ReviewInfoComponent,
    ReviewSummaryComponent,
    ReviewOverviewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ReviewerHomeComponent],
})
export class ReviewerModule {}
