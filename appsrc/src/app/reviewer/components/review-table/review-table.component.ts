import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Review } from '../../../core/models/review';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ReviewInfoComponent } from '../review-info/review-info.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-review-table',
  templateUrl: './review-table.component.html',
  styleUrls: ['./review-table.component.scss'],
})
export class ReviewTableComponent {
  reviewSelected: Review;
  @Input() projectId: string;
  @Output() reviewRequestedSelected: EventEmitter<Review>;
  @Output() deleteReviewEventEmitter: EventEmitter<{projectId: string, reviewID: string}>;
  routeParams: Subscription;
  projectName: string;
  displayedColumns: string[] = ['reviewID', 'reviewTitle', 'reviewOpenDate', 'reviewAuthor', 'delete'];
  @Input() dataSource: MatTableDataSource<Review>;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.routeParams = this.route.params.subscribe((params: any) => {
      this.projectName = params.projectName;
    });
    this.reviewRequestedSelected = new EventEmitter<Review>();
    this.deleteReviewEventEmitter = new EventEmitter<{projectId: string, reviewID: string}>();
  }

  onReviewRequestedSelection(review: Review): void {
    this.reviewSelected = review;
    delete review._id;
    this.reviewRequestedSelected.emit(review);
  }

  openDialog(data: Review): void {
    this.dialog.open(ReviewInfoComponent, {
      data,
      height: '40vh',
      width: '90vh',
    });
  }

  goToEditReview(): void {
    this.router.navigate(['reviewer/home/review', this.projectId]);
  }

  deleteReview(review: Review): void {
    this.deleteReviewEventEmitter.next({projectId: this.projectId, reviewID: review.reviewID});
  }
}
