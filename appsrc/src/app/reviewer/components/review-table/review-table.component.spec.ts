import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';

import { ReviewTableComponent } from './review-table.component';
import { Review } from 'src/app/core/models/review';
import { ReviewInfoComponent } from '../review-info/review-info.component';
import { reviewMock } from 'src/app/mocks/reviewer/review.mock';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfiguration } from 'src/config/app-configuration';

class ActivatedRouteStub {
  snapshot: any;
  params: any;

  constructor() {
    this.snapshot = {
      paramMap: {
        get: () => '0',
      },
    };

    this.params = of({
      projectName: 'testName',
      projectId: 'testId',
    });
  }
}

describe('ReviewTableComponent', () => {
  let component: ReviewTableComponent;
  let fixture: ComponentFixture<ReviewTableComponent>;
  const mockReview: Review = reviewMock[0];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewTableComponent],
      imports: [MatDialogModule, BrowserAnimationsModule, NoopAnimationsModule, HttpClientTestingModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        {
          provide: Router,
          useValue: {},
        },
        AppConfiguration
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal', () => {
    const fixtureModal: ComponentFixture<ReviewInfoComponent> =
      TestBed.createComponent(ReviewInfoComponent);
    const modal: ReviewInfoComponent = fixtureModal.componentInstance;
    component.openDialog(mockReview);
    expect(modal).toBeTruthy();
  });
});
