import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import {
  Review,
} from '../../../core/models/review';
import { MatTableDataSource } from '@angular/material/table';
import { ReviewComment } from '../../../core/models/reviewComment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ArtifactsRetrieverService } from '../../../plugins/project/services/artifacts-retriever/artifacts-retriever.service';
import { ReviewService } from 'src/app/plugins/project/services/review/review.service';
import { forkJoin } from 'rxjs';
import { PKMArtifact } from 'src/app/core/models/artifact';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { HttpErrorResponse } from '@angular/common/http';

const SUCCESS_MESSAGE_MODIFY_REVIEW: string = 'Review has been modified succesfully!';
const ERROR_MESSAGE_MODIFY_REVIEW: string = 'Something went wrong while tried to modify a review!';

@Component({
  selector: 'app-review-overview',
  templateUrl: './review-overview.component.html',
  styleUrls: ['./review-overview.component.scss'],
})
export class ReviewOverviewComponent implements OnInit {
  @Input() selectedReviewRequest: Review;
  displayedColumns: string[] = ['type', 'path' ];
  artifactsTable: any;
  @Input() projectId: string;
  @Input() currentRole: string;
  @Input() addReview: boolean;
  newReview: FormGroup;
  @Output() editReviewEvent: EventEmitter<boolean>;
  editReview: boolean = false;
  artifacts: any[] = [];
  dataSource: MatTableDataSource<any>;
  artifactSelected: object;
  comments: ReviewComment[];
  currentUser: string = localStorage.getItem('currentUser');
  textArea: FormGroup;
  constructor(
    private snackbarService: SnackbarService,
    private artifactsRetriever: ArtifactsRetrieverService,
    private reviewService: ReviewService,
    ) {
    this.editReviewEvent = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<any>(this.selectedReviewRequest.reviewArtifacts);
    this.artifactsTable = this.selectedReviewRequest.reviewArtifacts;
    this.comments = this.selectedReviewRequest.reviewCommets;
    this.newReview = new FormGroup({
      title: new FormControl('', Validators.required),
    });
    this.textArea =  new FormGroup({
      comment: new FormControl('', Validators.required),
    });
    forkJoin([
      this.artifactsRetriever.getAllPKMFilesArtifactsFromProject(
        this.projectId
      ),
      this.artifactsRetriever.getPKMAnnotationsFromProject(
        this.projectId
      ),
      this.artifactsRetriever.getPKMCommentsFromAProject(this.projectId),
      this.artifactsRetriever.getPKMLogsFromAProject(this.projectId),
      this.artifactsRetriever.getPKMDiagramsFromAProject(this.projectId),
      this.artifactsRetriever.getPKMTESTARStateModelFromAProject(
        this.projectId
      ),
      this.artifactsRetriever.getPKMAnnotationsResults(this.projectId)
    ]).subscribe((result: any[]) => {
      result.forEach( (object: any[]) => {
        if (object.length > 0) {
          object.forEach( (artifact: PKMArtifact) => {
          const newArtifact: any  = {
            path: artifact.rel_path,
            type: artifact.type
          };
          this.artifacts.push(newArtifact);
          });
        }
      });
    });
  }

  artifactSelecionChange(value: object): void {
    this.artifactSelected = value;
  }
  addArtifact(): void {
    this.artifactsTable.push(this.artifactSelected);
    this.dataSource = new MatTableDataSource<any>(this.artifactsTable);
  }
  addComment(): void {
    const newComment: ReviewComment = {
      author: this.currentUser,
      datetime: new Date().toUTCString(),
      comment: this.textArea.get('comment').value
    };
    this.selectedReviewRequest.reviewCommets.push(newComment);
    this.textArea.get('comment').setValue('');
  }
  editReviewFunc(): void {
    this.newReview.get('title').setValue(this.selectedReviewRequest.reviewTitle);
    this.editReview = true;
  }
  update(): void {
    const arrayReviews: Review[] = [];
    this.selectedReviewRequest.reviewTitle = this.newReview.get('title').value;
    arrayReviews.push(this.selectedReviewRequest);
    this.reviewService.updateProjectReview(this.projectId, arrayReviews).subscribe(
      (response: any) => {
        this.snackbarService.success(SUCCESS_MESSAGE_MODIFY_REVIEW);
        this.editReview = false;
      },
      (err: HttpErrorResponse) => {
        this.snackbarService.error(ERROR_MESSAGE_MODIFY_REVIEW);
      }
    );
  }
}
