import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { reviewMock } from 'src/app/mocks/reviewer/review.mock';
import { AppConfiguration } from 'src/config/app-configuration';

import { ReviewOverviewComponent } from './review-overview.component';

describe('ReviewOverviewComponent', () => {
  let component: ReviewOverviewComponent;
  let fixture: ComponentFixture<ReviewOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewOverviewComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, MatSnackBarModule],
      providers: [AppConfiguration, MatSnackBar]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewOverviewComponent);
    component = fixture.componentInstance;
    component.selectedReviewRequest = reviewMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
