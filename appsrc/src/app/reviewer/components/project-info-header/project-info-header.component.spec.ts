import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectInfoHeaderComponent } from './project-info-header.component';

describe('ProjectInfoHeaderComponent', () => {
  let component: ProjectInfoHeaderComponent;
  let fixture: ComponentFixture<ProjectInfoHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectInfoHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectInfoHeaderComponent);
    component = fixture.componentInstance;
    component.projectName = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset selected on goBack() click', () => {
    component.selected = true;

    component.goBackSummary();

    expect(component.selected).toEqual(false);
  });

  it('should render goback button when conditions are met', () => {
    component.selected = true;
    component.selectReview = true;

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelectorAll('button').length).toEqual(1);

    component.selected = true;
    component.selectReview = false;

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelectorAll('button').length).toEqual(0);
  });
});
