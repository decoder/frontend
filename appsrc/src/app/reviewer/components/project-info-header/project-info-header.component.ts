import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Review } from 'src/app/core/models/review';

@Component({
  selector: 'app-project-info-header',
  templateUrl: './project-info-header.component.html',
  styleUrls: ['./project-info-header.component.scss'],
})
export class ProjectInfoHeaderComponent {
  @Input() projectName: string;
  @Input() selected: boolean;
  @Input() selectReview: boolean;
  @Input() selectedReview: Review;
  @Output() backEvent: EventEmitter<boolean>;

  constructor() {
    this.backEvent = new EventEmitter<boolean>();
  }

  goBackSummary(): any {
    this.selected = false;
    this.backEvent.emit(this.selected);
  }
}
