import { Component } from '@angular/core';
import { ReviewComment } from '../../../core/models/reviewComment';
import { commentMock } from '../../../mocks/reviewer/comment.mock';

@Component({
  selector: 'app-review-comments',
  templateUrl: './review-comments.component.html',
  styleUrls: ['./review-comments.component.scss'],
})
export class ReviewCommentsComponent {
  comments: ReviewComment[] = commentMock;
  currentUser: string = localStorage.getItem('currentUser');
}
