import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { commentMock } from 'src/app/mocks/reviewer/comment.mock';
import { ReviewCommentsComponent } from './review-comments.component';

describe('ReviewCommentsComponent', () => {
  let component: ReviewCommentsComponent;
  let fixture: ComponentFixture<ReviewCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewCommentsComponent],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize comments correctly', () => {
    component.comments = commentMock;
    fixture.detectChanges();
    expect(component.comments.length).toBe(2);
  });

  it('should render a div block for each title and each comment ', () => {
    component.comments = commentMock;
    fixture.detectChanges();

    const titleBlocks: any = fixture.nativeElement.querySelectorAll('.blue-tittle');
    const commentlocks: any = fixture.nativeElement.querySelectorAll('.comment-text');

    expect(titleBlocks.length).toBe(2);
    expect(commentlocks.length).toBe(2);
  });
});
