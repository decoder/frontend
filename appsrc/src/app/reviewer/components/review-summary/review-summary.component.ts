import {
  Component,
  EventEmitter,
  Output,
  Input,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Review } from 'src/app/core/models/review';

@Component({
  selector: 'app-review-summary',
  templateUrl: './review-summary.component.html',
  styleUrls: ['./review-summary.component.scss'],
})
export class ReviewSummaryComponent {
  @Output() reviewRequestSelectedEvent: EventEmitter<Review>;
  @Output() addReviewEvent: EventEmitter<boolean>;
  @Output() deleteReviewEventEmitter: EventEmitter<{projectId: string, reviewID: string}>;
  @Output() addReview: boolean = false;
  @Input() currentRole: string;
  @Input() projectId: string;
  @Input() dataSource: MatTableDataSource<Review>;
  selectedReview: Review;

  constructor() {
    this.reviewRequestSelectedEvent = new EventEmitter<Review>();
    this.addReviewEvent = new EventEmitter<boolean>();
    this.deleteReviewEventEmitter = new EventEmitter<{projectId: string, reviewID: string}>();
  }

  onReviewRequestSelected(review: Review): void {
    this.selectedReview = review;
    this.reviewRequestSelectedEvent.emit(review);
  }

  onDeleteReview(value: { projectId: string; reviewID: string; }): void {
    this.deleteReviewEventEmitter.next(value);
  }

  addReviewFunc(): void {
    this.addReviewEvent.emit(true);
    this.selectedReview = {
    reviewTitle: '',
    reviewOpenDate: new Date().toUTCString(),
    reviewAuthor: '',
    reviewStatus: '',
    reviewCommets: [],
    reviewArtifacts: [],
    };
    this.onReviewRequestSelected(this.selectedReview);
  }
}
