import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ReviewInfoComponent } from './review-info.component';
import { Review } from 'src/app/core/models/review';
import { reviewMock } from 'src/app/mocks/reviewer/review.mock';

describe('ReviewInfoComponent', () => {
  let component: ReviewInfoComponent;
  let fixture: ComponentFixture<ReviewInfoComponent>;
  const mockReview: Review = reviewMock[0];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewInfoComponent],
      imports: [ MatDialogModule ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewInfoComponent);
    component = fixture.componentInstance;
    component.data = mockReview;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should set data', () => {
    expect(component.data).toBeTruthy();
  });
});
