import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Review } from 'src/app/core/models/review';

@Component({
  selector: 'app-review-info',
  templateUrl: './review-info.component.html',
  styleUrls: ['./review-info.component.scss'],
})
export class ReviewInfoComponent {
  constructor(
    public dialogRef: MatDialogRef<ReviewInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Review
  ) {}

  close(): void {
    this.dialogRef.close();
  }
}
