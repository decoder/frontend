import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Review } from 'src/app/core/models/review';
import { reviewMock } from 'src/app/mocks/reviewer/review.mock';
import { AppConfiguration } from 'src/config/app-configuration';
import { ReviewerHomeComponent } from './reviewer-home.component';

class MatSnackBarStub {
  openFromComponent(): any {
    return null;
  }
}

describe('ReviewerHomeComponent', () => {
    let component: ReviewerHomeComponent;
    let fixture: ComponentFixture<ReviewerHomeComponent>;
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ReviewerHomeComponent],
        imports: [HttpClientTestingModule, MatSnackBarModule],
        providers: [AppConfiguration, { provide: MatSnackBar, useClass: MatSnackBarStub }],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    }));
    beforeEach(() => {
      fixture = TestBed.createComponent(ReviewerHomeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should emit event when review is selected', () => {
      const review: Review = reviewMock[0];
      spyOn(component.selectedReviewEvent, 'emit');
      component.onReviewRequestSelected(review);

      expect(component.selectedReviewEvent.emit).toHaveBeenCalled();
    });

    it('should unselected review when go back', () => {
      component.goBackSummary();
      expect(component.selectedReview).toBe(false);
    });

  });
