import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Review } from 'src/app/core/models/review';
import { ReviewSummaryComponent } from '../../components/review-summary/review-summary.component';
import { ReviewService } from 'src/app/plugins/project/services/review/review.service';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'src/app/core/services/snackbar/snackbar.service';
import { HttpErrorResponse } from '@angular/common/http';

const SUCCESS_MESSAGE_ADD_REVIEW: string = 'Review has been added succesfully!';
const ERROR_MESSAGE_ADD_REVIEW: string = 'Something went wrong while adding a review!';
const SUCCESS_MESSAGE_DELETE_REVIEW: string = 'Review has been deleted succesfully!';
const ERROR_MESSAGE_DELETE_REVIEW: string = 'Something went wrong while deleting a review!';

@Component({
  selector: 'app-reviewer-home',
  templateUrl: './reviewer-home.component.html',
  styleUrls: ['./reviewer-home.component.scss'],
})
export class ReviewerHomeComponent implements OnInit, OnChanges {
  selectedReviewRequest: Review;
  selectedReview: boolean;
  addReview: boolean;
  newReview: FormGroup;
  dataSource: MatTableDataSource<Review>;
  currentUser: string = localStorage.getItem('currentUser');
  @Input() projectName: string;
  @Input() currentRole: string;
  @Input() projectId: string;
  @Input() selectReview: boolean;
  @Output() selectedReviewEvent: EventEmitter<boolean>;
  @ViewChild('reviewSummary') reviewSummary: ReviewSummaryComponent;

  constructor(
    private snackbarService: SnackbarService,
    private reviewService: ReviewService
  ) {
    this.projectName = '';
    this.selectedReview = false;
    this.selectedReviewEvent = new EventEmitter<boolean>();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.projectName !== undefined && (changes.projectName.currentValue !== changes.projectName.previousValue)) {
      this.loadReviews();
      this.selectedReview = false;
    }
  }

  ngOnInit(): void {
    this.newReview = new FormGroup({
      title: new FormControl('', Validators.required),
    });
  }

  onReviewRequestSelected(review: Review): void {
    this.selectedReviewRequest = review;
    this.selectedReview = true;
    this.selectedReviewEvent.emit(true);
  }

  addReviewEvent(value: boolean): void {
    this.addReview = value;
  }

  loadReviews(): void {
    this.reviewService.getProjectReviews(this.projectId).subscribe( (reviews: Review[]) => {
      this.dataSource = new MatTableDataSource(reviews);
    });
  }

  goBackSummary(): void {
    this.selectedReview = false;
    this.newReview.get('title').setValue('');
    this.addReview = false;
  }

  deleteReviewEvent(value: { projectId: string; reviewID: string; }): void {
    this.reviewService.deleteProjectReview(value.projectId, value.reviewID)
      .subscribe(
        (response: any) => {
          this.snackbarService.success(SUCCESS_MESSAGE_DELETE_REVIEW);
          this.loadReviews();
        },
        (err: HttpErrorResponse) => {
          this.snackbarService.error(ERROR_MESSAGE_DELETE_REVIEW);
        }
      );
  }

  Save(): void {
    this.selectedReviewRequest.reviewAuthor = this.currentUser;
    this.selectedReviewRequest.reviewTitle = this.newReview.get('title').value;
    this.selectedReviewRequest.reviewStatus = 'OPEN';
    this.newReview.get('title').setValue('');
    this.addReview = false;
    const reviews: Review[] = [];
    reviews.push(this.selectedReviewRequest);
    this.reviewService.addProjectReview(this.projectId, reviews).subscribe(
      (response: any) => {
        this.snackbarService.success(SUCCESS_MESSAGE_ADD_REVIEW);
        this.goBackSummary();
      },
      (err: HttpErrorResponse) => {
        this.snackbarService.error(ERROR_MESSAGE_ADD_REVIEW);
      }
    );
  }
}
