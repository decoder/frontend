export const environment: any = {
  production: true,
  serverURL: 'https://decoder-tool.ow2.org/pkm',
  asfmServerURL: 'https://decoder-tool.ow2.org/asfm',
  jupyterURL: 'https://decoder-tool.ow2.org/jupyter-lab/lab',
  toolsServerUrl: 'https://decoder-tool.ow2.org',
};
